/*
 * Copyright (C) 2017  Florian GOLESTIN
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef SOLEIL__MODEL_H_
#define SOLEIL__MODEL_H_

#include <glm/matrix.hpp>
#include <string>

#include "Frame.h"
#include "Mesh.h"
#include "Node.h"

namespace Soleil {

  class Model
  {
  public:
    /**
     * Build a mesh from an asset resource. Currently only the Wavefront (.obj)
     * format is supported.
     *
     * If `loadPhysics` is true, a Physics Static triangle
     * mesh will be generated and pushed to the Physics World.
     * The physics bodies will have the same id as the hash of:
     * '${ObjectName}::${MaterialName}'.
     */
    Model(const std::string& resourceName, const bool loadPhysics = false);

    // void draw(Frame& frame, const MeshShader& config,
    //           const glm::mat4& transformation);

  public:
    // std::vector<Mesh>  meshes;
    Node               root;
    std::vector<Light> lights;
    std::string        directory;

  public:
    // TODO: Abstract 3d Object loading with a manager
    // The following is a quick hack while waiting for the refactor of the
    // Models.
    // It give the responsibility to the Model to load other objects but this
    // code should be in a ModelManager or in the SceneManager
    static std::vector<float> LoadModel(const std::string& resourceName);
  };

} // namespace Soleil

#endif /* SOLEIL__MODEL_H_ */
