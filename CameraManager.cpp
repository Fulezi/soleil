/*
 * Copyright (C) 2019  Florian GOLESTIN
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <CameraManager.h>

#include <Errors.h>
#include <Logger.h>
#include <memory>

namespace Soleil {
  Camera* CameraManager::getCurrentCamera() { return currentCamera; }

  void CameraManager::setCurrentCamera(Camera* cam)
  {
    if (cam == nullptr) {
      SOLEIL__RUNTIME_ERROR("Current camera should not be null");
      return;
    }
    cam->onActivate(currentCamera);
    currentCamera = cam;
  }

  //////////////////////////////////////////////////////////////
  // Statics

  namespace {
    std::unique_ptr<CameraManager> s_cameraManagerInstance = nullptr;
  }

  void CameraManager::Init()
  {
    Logger::error("Initializing the CameraManager");
    s_cameraManagerInstance = std::make_unique<CameraManager>();
  }

  void CameraManager::Destroy()
  {
    Logger::error("Destroying the CameraManager");
    s_cameraManagerInstance = nullptr;
  }

  Camera* CameraManager::GetCurrentCamera()
  {
    return s_cameraManagerInstance->getCurrentCamera();
  }

  void CameraManager::SetCurrentCamera(Camera* cam)
  {
    s_cameraManagerInstance->setCurrentCamera(cam);
  }

} // namespace Soleil
