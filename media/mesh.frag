
#version 330 core

struct Material
{
  vec3      emissive;
  vec3      ambiant;
  vec3      diffuse;
  vec3      specular;
  float     shininess;
  float     opacity;
  sampler2D diffuseTexture;
};

struct Light
{
  bool isLocal; // True for a point or spot light
  bool isSpot;
  vec3 ambiant;
  vec3 color;
  vec3 position;    // Location of the light if isLocal is true, otherwise the
                    // direction towards the light
  vec3  halfVector; // Direction of highlights for the directional light
  vec3  spotConeDirection; // Spotlight attributes
  float spotCosCutOff;
  float spotExponent;
  float constantAttenuation; // Local light attributes
  float linearAttenuation;
  float quadraticAttenuation;
};
const int MaxLights = 10;

uniform Material material;

uniform int numberOfLight;
uniform Light lights[MaxLights];
uniform sampler2DShadow shadowMap;
uniform int PCFIterations;

uniform vec3 EyeDirection;

out vec4 FragColor;

in vec2 fragTextureCoords;
in vec3 fragNormal;
in vec4 fragPosition;
in vec4 fragPositionLightSpace;

float
ShadowCalculation(vec4 _fragPositionLightSpace)
{
  // Convert space into range [-1,1]. Useless for ortho matrix
  //vec3 projCoords = _fragPositionLightSpace.xyz / _fragPositionLightSpace.w;
  // Convert space into range [0,1]
  //projCoords         = projCoords * 0.5 + 0.5;
  float shadow = textureProj(shadowMap, _fragPositionLightSpace);
  // float currentDepth = projCoords.z;
  // float bias         = 0.005;

  // float     shadow = 0.0;
  // const int PCF_4x = 1;
  // const int PCF_X = 2;
  // const int doPCF  = PCF_X;

  if (PCFIterations == 0) {
    // vec2  texelSize = 1.0 / textureSize(shadowMap, 0);
    // for (int x = -1; x <= 1; ++x) {
    //   for (int y = -1; y <= 1; ++y) {
    //     float pcfDepth =
    //       texture(shadowMap, projCoords.xy + vec2(x, y) * texelSize).r;
    //     shadow += (currentDepth - bias) > pcfDepth ? 1.0 : 0.0;
    //   }
    // }
    shadow += textureProjOffset(shadowMap, _fragPositionLightSpace, ivec2(-1,  1));
    shadow += textureProjOffset(shadowMap, _fragPositionLightSpace, ivec2( 1,  1));
    shadow += textureProjOffset(shadowMap, _fragPositionLightSpace, ivec2(-1, -1));
    shadow += textureProjOffset(shadowMap, _fragPositionLightSpace, ivec2( 1, -1));
    shadow = shadow / 4.0;
    // shadow /= 9.0;
  } else if (PCFIterations > 0) {
    int count = 0;
    vec2  texelSize = 1.0 / textureSize(shadowMap, 0);
    for (int x = -PCFIterations; x <= PCFIterations; ++x) {
      for (int y = -PCFIterations; y <= PCFIterations; ++y) {
        shadow +=
          textureProj(shadowMap, _fragPositionLightSpace + vec4(vec2(x, y) * texelSize, 0, 0));
	count++;
      }
    }
    shadow /= count;
  } else {
    //shadow = (currentDepth - bias) > closestDepth ? 1.0 : 0.0;
    shadow = textureProj(shadowMap, _fragPositionLightSpace);
  }

  return shadow;
}

void
main()
{
  // gl_FragColor = vec4(1.0);
  vec4 diffuseColor = vec4(0.0);

  // If there is an opaque texture, do not display the material color
  // Set the color From texture if any then from the material
  diffuseColor = texture(material.diffuseTexture, fragTextureCoords);
  if (diffuseColor.a < 1) {
    // diffuseColor = vec4(material.diffuse, material.opacity) + diffuseColor;

    // Better blending of the material and the texture (for instance to apply a
    // black texture on a material).
    diffuseColor =
      vec4(mix(material.diffuse, vec3(diffuseColor), diffuseColor.a),
           material.opacity);

    // TODO: Try with a clamp (avoid branching)
  }

  // Light Computation -----------------
  vec3 scatteredLight = vec3(0.0); // TODO: global Ambiant light
  vec3 reflectedLight = vec3(0.0);
  for (int light = 0; light < numberOfLight; ++light) {
    vec3  halfVector;
    vec3  lightDirection = lights[light].position;
    float attenuation    = 1.0;

    if (lights[light].isLocal) {
      lightDirection      = lightDirection - fragPosition.xyz;
      float lightDistance = length(lightDirection);
      lightDirection      = lightDirection / lightDistance;

      attenuation = 1.0 / (lights[light].constantAttenuation +
                           lights[light].linearAttenuation * lightDistance +
                           lights[light].quadraticAttenuation * lightDistance *
                             lightDistance);

      if (lights[light].isSpot) {
        float spotCos = dot(lightDirection, -lights[light].spotConeDirection);
        if (spotCos < lights[light].spotCosCutOff) {
          attenuation = 0.0;
        } else {
          attenuation = pow(spotCos, lights[light].spotExponent);
        }
      }

      // halfVector = normalize(lightDirection + EyeDirection);
      halfVector = normalize(lightDirection + (normalize(-fragPosition.xyz)));
    } else {
      halfVector = normalize(lightDirection + (normalize(-fragPosition.xyz)));
      // halfVector = lights[light].halfVector;
    }

    float diffuse  = max(0.0, dot(fragNormal, lightDirection));
    float specular = max(0.0, dot(fragNormal, halfVector));

    if (diffuse == 0.0) {
      specular = 0.0;
    } else {
      specular = pow(specular, material.shininess);
      // specular = pow(specular, 1.0);
    }

    // if (specular > 0.0){
    //   diffuseColor =vec4(0,1,0,1);
    // } else {
    //   diffuseColor =vec4(0,0,1,1);
    // }

    // scatteredLight +=
    //   lights[light].ambiant * material.ambiant * attenuation +
    //   lights[light].color * vec3(diffuseColor) * diffuse * attenuation; // *
    //   vec3(diffuseColor)
    // reflectedLight +=
    //   lights[light].color *  material.specular * specular * attenuation; //
    //   material.specular
    float shadow;
    if (light == 1) {
      // TODO: This is the only light with a shadow for now.
      shadow = ShadowCalculation(fragPositionLightSpace);
    } else {
      shadow = 1.0;
    }
    scatteredLight += lights[light].ambiant * material.ambiant * attenuation +
                      lights[light].color * diffuse * attenuation *
                        (shadow); // * vec3(diffuseColor)
    reflectedLight += lights[light].color * specular * attenuation *
                      (shadow); // material.specular
  }

  vec3 rgb = min(material.emissive + vec3(diffuseColor) * scatteredLight +
                   reflectedLight * material.specular,
                 vec3(1.0));
  // vec3 rgb =
  //   min(material.emissive + scatteredLight + reflectedLight, vec3(1.0));

  FragColor = vec4(rgb, diffuseColor.a);

  // FragColor += vec4(1.0f);
}
