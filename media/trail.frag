#version 330 core

// in vec4 fragColor;
uniform sampler2D diffuse;

in vec2 fragUv;

out vec4 color;

void
main()
{
  // color = texture(diffuse, gl_PointCoord) * fragColor;
  color = texture(diffuse, fragUv);
  //color = vec4(fragUv, 0.0, 1.0);
}
