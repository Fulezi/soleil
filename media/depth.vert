#version 330 core


uniform mat4 ModelViewProjection;

layout(location = 0) in vec4 position;

void
main()
{
  gl_Position = ModelViewProjection * position;
}
