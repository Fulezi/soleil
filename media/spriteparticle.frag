#version 330 core

in vec4 fragColor;
in vec2 fragUv;
uniform sampler2D diffuse;

out vec4 color;

void
main()
{
  //color = fragColor;
  color = texture(diffuse, gl_PointCoord * fragUv) * fragColor;
}
