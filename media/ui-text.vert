#version 330 core

uniform mat4 Transform;

layout(location = 0) in vec2 vertice;
layout(location = 1) in vec2 glyphUV;

// uniform mat4 Projection;

out vec2 backgroundImageUV;
out vec2 fontImageUV;

/*
 * The first six vertices are reserved for the background image.


+------------+	<-- backgroundImageUV
|xxxxxxxxxxxx|
|x+-++-++-+xx|
|x|B||o||!|xx|	<-- glyphUV
|x+-++-++-+xx|
|x+-++-++-+xx|
|x|H||e||y|xx|
|x+-++-++-+xx|
|xxxxxxxxxxxx|
+------------+


 */

void
main()
{
  gl_Position = Transform * vec4(vertice, 0.0, 1.0);

  if (gl_VertexID < 6) {
    // int vertexID      = int(mod(gl_VertexID, 6));
    //backgroundImageUV = vertices[gl_VertexID];
    backgroundImageUV = glyphUV;
  } else {
    backgroundImageUV = vec2(-1, -1);
    fontImageUV = glyphUV;
  }
}
