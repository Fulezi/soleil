#version 330 core

uniform mat4 ViewProjection;
uniform int NumberOfPoints;

layout(location = 0) in vec3 position;

//layout(location = 1) in vec4 color;
//
//out vec4 fragColor;

out vec2 fragUv;

void
main()
{
   gl_Position = ViewProjection * vec4(position, 1.0);
   float odd = (gl_VertexID % 2);
   fragUv.y = 1.0 - (float(gl_VertexID) - odd) /  NumberOfPoints;
   fragUv.x = odd;
     
   //fragColor = color;
}
