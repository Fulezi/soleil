#version 330 core

// Lights (in case of Bump Mapping)
// struct Light
// {
//   bool isLocal; // True for a point or spot light
//   bool isSpot;
//   vec3 ambiant;
//   vec3 color;
//   vec3 position;    // Location of the light if isLocal is true, otherwise
//   the
//                     // direction towards the light
//   vec3  halfVector; // Direction of highlights for the directional light
//   vec3  spotConeDirection; // Spotlight attributes
//   float spotCosCutOff;
//   float spotExponent;
//   float constantAttenuation; // Local light attributes
//   float linearAttenuation;
//   float quadraticAttenuation;
// };
// const int   MaxLights = 10;
// uniform int numberOfLight;
// uniform Light lights[MaxLights];

// Fog
uniform mat4 InverseProjectionMatrix;
uniform mat4 InverseViewMatrix;
uniform vec3 CameraPosition;

uniform float FogConstant;
uniform vec3  FogColor;
uniform float FogDistance;
uniform vec4 ViewPort;

// Fog Multi lane iteration
uniform float FogNoixOffsetX;
uniform float FogNoixOffsetY;
uniform float FogNoiseTiling;
uniform int   FogNumberIteration;

in vec4 pixel;

uniform sampler2D DepthMapTexture;
uniform sampler2D NoiseTexture;

// For shadow
uniform sampler2DShadow shadowMap;
uniform int             PCFIterations;
uniform mat4 LightSpace;

out vec4 color;

float
ShadowCalculation(vec4 _fragPositionLightSpace)
{
  float shadow = 0.0f;
  if (PCFIterations == 0) {
    shadow +=
      textureProjOffset(shadowMap, _fragPositionLightSpace, ivec2(-1, 1));
    shadow +=
      textureProjOffset(shadowMap, _fragPositionLightSpace, ivec2(1, 1));
    shadow +=
      textureProjOffset(shadowMap, _fragPositionLightSpace, ivec2(-1, -1));
    shadow +=
      textureProjOffset(shadowMap, _fragPositionLightSpace, ivec2(1, -1));
    shadow = shadow / 4.0;
  } else if (PCFIterations > 0) {
    int  count     = 0;
    vec2 texelSize = 1.0 / textureSize(shadowMap, 0);
    for (int x = -PCFIterations; x <= PCFIterations; ++x) {
      for (int y = -PCFIterations; y <= PCFIterations; ++y) {
        shadow += textureProj(shadowMap, _fragPositionLightSpace +
                                           vec4(vec2(x, y) * texelSize, 0, 0));
        count++;
      }
    }
    shadow /= count;
  } else {
    // shadow = (currentDepth - bias) > closestDepth ? 1.0 : 0.0;
    shadow = textureProj(shadowMap, _fragPositionLightSpace);
  }

  return shadow;
}

float
map(float value, float min1, float max1, float min2, float max2)
{
  // Convert the current value to a percentage
  // 0% - min1, 100% - max1
  float perc = (value - min1) / (max1 - min1);

  // Do the same operation backwards with min2 and max2
  float ret = perc * (max2 - min2) + min2;

  return ret;
}

void
main(void)
{
  // Fog Plane
  vec3 F   = vec3(0, 1, 0);
  vec3 C   = CameraPosition;
  vec3 P   = vec3(pixel);
  vec3 ray = normalize(P - C);
  // float dist = length(ray);
  float dist = 0;

  float nd = dot(ray, F);

  vec4 wspace = gl_FragCoord;
  wspace.x /= ViewPort.x;
  wspace.y /= ViewPort.y;
  wspace.z /= wspace.w;
  float depth = texture(DepthMapTexture, vec2(wspace)).r;
  float near  = ViewPort.z;
  float far   = ViewPort.w;
  float z     = (depth * 2.0) - 1.0;
  // float linearDepth = (2.0 * near * far) / (far + near - z * (far - near));
  float linearDepth = depth * far;

  color = vec4(0.0);
  if (abs(nd) > 0.0) {
    // float oneOverINumberIteration = 1.0 / FogNumberIteration;

    float pn = dot(C, F);
    // for (int i = 0; i < FogNumberIteration; ++i) {
    float i = 0.0;
    float t = ((FogDistance - i * 3.5) - pn) / nd;

    // Closest fragment position (the fog or the mesh frag)
    vec3 fragmentPosition = C + ray * (min(t, linearDepth));

    // TODO: Offset seems the same for the other plane?
    // Fragment in the noise texture (offseted if multi-planes)
    vec2 fragmentInNoise = vec2(fragmentPosition.xz / FogNoiseTiling) +
                           vec2(FogNoixOffsetX * i, FogNoixOffsetY * i);

    // Move the first layer in the same direction as the camera
    // if (i == 0) {
    //   // TODO: Blending of the plane
    fragmentInNoise += vec2(C.x, C.z) * 0.01;
    // }

    float noiseValue = texture(NoiseTexture, fragmentInNoise).r;

    // float tDepth = ((1.0 / t) - (1.0 / near)) / ((1.0 / far - 1.0 / near));
    // float tDepth = (t - near) / (500.0 - near);

    vec3 finalFogColor = FogColor;
    if ((t >= 0.0) && (t <= linearDepth)) {
      float alpha = noiseValue;

      // Distance from fragment to fog plane to see how much fog we need to
      // apply.
      dist = linearDepth - t;

      float FogDensity = .08;
      float fogFactor  = 1.0 / exp(dist * FogDensity);

      // Attenuation of the FogColor to noise value
      alpha /= 3.0;
      color = mix(vec4(FogColor - alpha, 1.0), vec4(vec3(1), 0.0), fogFactor);

      float shadow =
        ShadowCalculation(LightSpace * vec4(fragmentPosition, 1.0));
      shadow = max(0.35, shadow);
      color.r *= shadow;
      color.g *= shadow;
      color.b *= shadow;

      //
      // Shadow computation With lights
      // vec3 rgb = vec3(0.0);
      // for (int light = 0; light < numberOfLight; ++light) {
      //   vec3  halfVector;
      //   vec3  lightDirection = lights[light].position;
      //   float attenuation    = 1.0;

      //   if (lights[light].isLocal) { continue; }
      // 	// fragPosition??
      // 	vec3 fragPosition = P;
      // 	halfVector = normalize(lightDirection +
      // (normalize(-fragPosition.xyz))); 	vec3 fragNormal =
      // normalize(vec3(0, min(noiseValue, 0.3), 0.2)); 	float diffuse  =
      // max(0.0, dot(fragNormal, lightDirection)); 	float specular =
      // max(0.0, dot(fragNormal, halfVector)); 	if (diffuse == 0.0) {
      // specular = 0.0; 	} else { 	  specular = pow(specular,
      // 0.60);
      // 	  // specular = pow(specular, 1.0);
      // 	}
      // 	rgb += FogColor * diffuse * lights[light].color * attenuation
      // 	  ;//+ lights[light].color * specular * attenuation;
      // }
      // color = vec4(rgb, abs(1.0 - nd));
    } else if (linearDepth >= ViewPort.w) {
      /////////////////////////////////////
      // TODO: Sky can be rendered here: //
      /////////////////////////////////////

      // color.b = 1.0;
      // color.a = 1.0;
    }

    // color.xyz *= oneOverINumberIteration;
    // color *= oneOverINumberIteration;
  }

  // Experience to module the in-noise color
  // vec2 fragmentInNoise = vec2(P / 300.0);
  // fragmentInNoise += vec2(C.x, C.z) * 0.01;
  // float noiseValue = texture(NoiseTexture, fragmentInNoise).r;
  // vec3 finalFogColor = FogColor * (vec3(noiseValue) + 0.3);
  vec3 finalFogColor = FogColor;

  // Test to fog the horizon
  //  || (linearDepth > 800 && (ray * linearDepth).y < 1.0)
  //  || (abs(nd) >=  0.0 && abs(nd) < 0.01)

  // Scalar value is to start the blending a bit before being below the plane
  if (C.y < (0.35 * FogDistance)) {
    float v = 1.0;
    // // color = vec4(vec3(noiseValue) + 0.3, v);
    // float v = map(C.y, (0.6 * FogDistance), FogDistance, 0.01, 0.1);
    // color.rgb += v;
    // noiseValue = clamp(noiseValue, 0.1, 0.6);

    // dist = min (dist, linearDepth);
    dist             = linearDepth;
    float FogDensity = .08;
    float fogFactor  = 1.0 / exp(dist * FogDensity);
    fogFactor        = clamp(fogFactor, 0.0, 1.0);
    // mix function fogColor . (1−fogFactor) + lightColor . fogFactor
    vec4 finalColor = mix(vec4(FogColor, 1.0), color, fogFactor);
    color           = finalColor;
  }
}
