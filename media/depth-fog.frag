#version 330 core

uniform float Far;

in vec4 ViewSpace;

void
main()
{
  //gl_FragDepth = 0.5;
  gl_FragDepth = length(ViewSpace) / Far;
}
