#version 330 core

// uniform vec3 BillboardCenter;
// uniform vec3 CameraRight;
// uniform vec3 CameraUp;

layout(location = 0) in vec2 glyphAdvance;
layout(location = 1) in vec2 textureCoord;

uniform vec3 CameraPosition;
uniform vec3 BillboardCenter;
uniform vec3 LineHalfWidth;
uniform mat4 ViewProjection;
uniform float Scale;

out vec2 UV;

void
main()
{
  // vec3 front = normalize((BillboardCenter  + 0.1*LineHalfWidth) -
  // CameraPosition);
  vec3 front = normalize(BillboardCenter - CameraPosition);
  // vec3 front = normalize((BillboardCenter + vec3(0.1 * glyphAdvance, 0.0)) -
  // CameraPosition);
  vec3 right = normalize(cross(vec3(0, 1, 0), front));
  vec3 up    = normalize(cross(front, right));

  // vec3[6] vertices = vec3[6](
  //   vec3(-1.0, -1.0, 0), vec3(1.0, -1.0, 0), vec3(-1.0, 1.0, 0), // triangle 1
  //   vec3(1.0, -1.0, 0), vec3(1.0, 1.0, 0), vec3(-1.0, 1.0, 0)    // triangle 2
  // );
  vec3[6] vertices = vec3[6](
    vec3(-0.5, -0.5, 0), vec3(0.5, -0.5, 0), vec3(-0.5, 0.5, 0), // triangle 1
    vec3(0.5, -0.5, 0), vec3(0.5, 0.5, 0), vec3(-0.5, 0.5, 0)    // triangle 2
  );


  
  vec3 v = vertices[gl_VertexID];
  // gl_Position = vec4(BillboardCenter + v.x * up + v.y * right, 1.0);

  // vec3 pos =
  //   BillboardCenter + 0.1 * -glyphAdvance.x * right + 0.1 * glyphAdvance.y *
  //   up;
  // // + (1.0 * v.x * right) + (1.0 * v.y * up);

  vec3  middle = Scale * LineHalfWidth;
  vec2  glyph  = Scale * glyphAdvance;
  vec3  pos    = BillboardCenter + ((-glyph.x + middle.x) * right) + ((-glyph.y + middle.y) * up);

  // vec3 pos = BillboardCenter + 0.1*-glyphAdvance.x * right +
  // 0.1*glyphAdvance.y * up + (1.0 * v.x * right) + (1.0 * v.y * up);

  gl_Position = ViewProjection * vec4(pos, 1.0);

  UV = textureCoord;
}
