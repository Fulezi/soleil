#version 330 core

out vec4 color;

in vec2 UV;

uniform sampler2D font;

void main(void) {

  //color = vec4(vec3(1.0 - texture(font, UV).r), 1.0);// + vec4(.2, .1, .1, .5);
  color = vec4(vec3(0.0), texture(font, UV).r);// + vec4(.2, .51, .51, .5);
}
