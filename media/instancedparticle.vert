#version 330 core

uniform mat4 ModelViewProjection;

layout(location = 0) in vec3 vertex;
// layout(location = 1) in vec3 position;
layout(location = 1) in vec4 color;
// layout(location = 2) in float scale;
layout(location = 2) in mat4 transformation;

out vec4 fragColor;

void
main()
{
  fragColor = color;

  gl_Position = ModelViewProjection * transformation * vec4(vertex, 1.0);
  // gl_Position = ModelViewProjection * vec4(vertex, 1.0);
}
