#version 330 core

//out vec3 textureCoordinate;
uniform mat4 InverseViewMatrix;

// Fog
uniform vec3 CameraPosition;
uniform float FogConstant;

out vec4 pixel;


void main(void) {
  // vec3[6] vertices =
  //     vec3[6](vec3(-1.0, 1.0, 0.0), vec3(-1.0, -1.0, 0.0), vec3(1.0, -1.0, 0.0),
  //             vec3(1.0, -1.0, 0.0), vec3(1.0, 1.0, 0.0), vec3(-1.0, 1.0, 0.0));
  vec2[6] vertices =
    vec2[6](vec2(-1.0,  1.0), vec2(-1.0, -1.0), vec2( 1.0, -1.0),
	    vec2( 1.0, -1.0), vec2(1.0,   1.0), vec2(-1.0, 1.0));


  gl_Position = vec4(vertices[gl_VertexID], 1.0, 1.0);
  // gl_Position = (ViewMatrix * vec4(vertices[gl_VertexID], 1.0));

  vec4 P = InverseViewMatrix * vec4(vertices[gl_VertexID], 1.0, 1.0);
  P = P / P.w;

  pixel = P;
}
