#version 330 core

out vec4 color;

in vec2 backgroundImageUV;
in vec2 fontImageUV;

uniform sampler2D backgroundSampler;
uniform sampler2D font;

void main(void) {

  // Text:
  //color = vec4(vec3(0.0), texture(font, UV).r);

  if (backgroundImageUV.x > -1) {
    color = texture(backgroundSampler, backgroundImageUV);
    //color.a = 0.1;
  } else {
    color = vec4(vec3(0.0), texture(font, fontImageUV).r);
    //color = texture(font, fontImageUV);
  }
}
