#version 330 core

// uniform mat4 ModelViewProjection;
uniform mat4 View;
uniform mat4 Projection;
uniform vec2 ScreenSize;

layout(location = 0) in vec3 position;
layout(location = 1) in vec4 color;
layout(location = 2) in float scale;
layout(location = 3) in vec2 uv;

out vec4 fragColor;
out vec2 fragUv;

void
main()
{
  // gl_Position = ModelViewProjection * vec4(position, 1.0);

  vec4 eyePos         = View * vec4(position, 1.0);
  vec4 projectionCel  = Projection * vec4(scale, scale, eyePos.z, eyePos.w);
  vec2 projectionSize = ScreenSize * projectionCel.xy / projectionCel.w;
  gl_PointSize        = 0.25 * (projectionSize.x + projectionSize.y);
  gl_Position         = Projection * eyePos;

  fragColor = color;
  fragUv = uv;
  // fragColor = vec4(0, 1, 0, 0.5);
}
