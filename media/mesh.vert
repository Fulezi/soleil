#version 330 core

uniform mat4 ModelViewProjection;
uniform mat4 ModelView;
uniform mat4 Model;
// For shadow
uniform mat4 LightSpace;
uniform mat3 NormalMatrix;

in vec4 position;
in vec3 normal;
in vec2 textureCoord;

out vec2 fragTextureCoords;
out vec3 fragNormal;
out vec4 fragPosition;
out vec4 fragPositionLightSpace;

void
main()
{
  vec4 vertexPosition = position;

  fragTextureCoords = textureCoord;
  // fragNormal        = normalize(NormalMatrix * normal);
  fragNormal             = mat3(ModelView) * normal;
  fragPosition           = ModelView * vertexPosition;
  fragPositionLightSpace = LightSpace * Model * position;

  gl_Position = ModelViewProjection * vertexPosition;
  // Test with infinite plan:
  //   gl_Position = vertexPosition       * ModelViewProjection;
}
