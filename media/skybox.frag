#version 330 core

// Sky
uniform samplerCube textureCube;
in vec3 textureCoordinate;


out vec4 color;

void main(void) {

  color = texture(textureCube, textureCoordinate);
}
