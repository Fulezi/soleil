#version 330 core

uniform mat4 ViewProjection;

 in vec3 position;
 in vec4 color;

out vec4 fragColor;

void
main()
{
  fragColor = color;

  gl_Position = ViewProjection * vec4(position, 1.0);
}
