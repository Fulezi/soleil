#version 330 core

out vec3 textureCoordinate;

uniform mat4 ViewMatrix;


void
main(void)
{
  // vec3[4] vertices = vec3[4](vec3(-1.0, -1.0, 1.0), vec3(1.0, -1.0, 1.0),
  //                            vec3(-1.0, 1.0, 1.0), vec3(1.0, 1.0, 1.0));
  //  textureCoordinate = mat3(ViewMatrix) * vertices[gl_VertexID];

  vec3[36] vertices = vec3[36](
    vec3(-1.0, 1.0, -1.0), vec3(-1.0, -1.0, -1.0), vec3(1.0, -1.0, -1.0),
    vec3(1.0, -1.0, -1.0), vec3(1.0, 1.0, -1.0), vec3(-1.0, 1.0, -1.0),
    vec3(-1.0, -1.0, 1.0), vec3(-1.0, -1.0, -1.0), vec3(-1.0, 1.0, -1.0),
    vec3(-1.0, 1.0, -1.0), vec3(-1.0, 1.0, 1.0), vec3(-1.0, -1.0, 1.0),
    vec3(1.0, -1.0, -1.0), vec3(1.0, -1.0, 1.0), vec3(1.0, 1.0, 1.0),
    vec3(1.0, 1.0, 1.0), vec3(1.0, 1.0, -1.0), vec3(1.0, -1.0, -1.0),
    vec3(-1.0, -1.0, 1.0), vec3(-1.0, 1.0, 1.0), vec3(1.0, 1.0, 1.0),
    vec3(1.0, 1.0, 1.0), vec3(1.0, -1.0, 1.0), vec3(-1.0, -1.0, 1.0),
    vec3(-1.0, 1.0, -1.0), vec3(1.0, 1.0, -1.0), vec3(1.0, 1.0, 1.0),
    vec3(1.0, 1.0, 1.0), vec3(-1.0, 1.0, 1.0), vec3(-1.0, 1.0, -1.0),
    vec3(-1.0, -1.0, -1.0), vec3(-1.0, -1.0, 1.0), vec3(1.0, -1.0, -1.0),
    vec3(1.0, -1.0, -1.0), vec3(-1.0, -1.0, 1.0), vec3(1.0, -1.0, 1.0));

  textureCoordinate = vertices[gl_VertexID];
  gl_Position = (ViewMatrix * vec4(vertices[gl_VertexID], 1.0)).xyww;
  //gl_Position = (ViewMatrix * vec4(vertices[gl_VertexID], 1.0));
}
