#version 330 core

// uniform mat4 ModelViewProjection;

uniform mat4 ModelView;
uniform mat4 Projection;

layout(location = 0) in vec4 position;
layout(location = 1) in vec3 normal;
layout(location = 2) in vec2 textureCoord;

out vec4 ViewSpace;

void
main()
{
  //gl_Position = ModelViewProjection * position;

  ViewSpace = ModelView * position;
  gl_Position = Projection * ViewSpace;
}
