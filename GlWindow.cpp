/*
 * Copyright (C) 2018  Florian GOLESTIN
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include "GlWindow.h"

#include "OpenGLInclude.h"

#include "DebugUI.h"

#include "Assets.h"
#include "DebugManager.h"
#include "EventManager.h"
#include "Inputs.h"
#include "Logger.h"
#include "SceneManager.h"
#include <ApplicationEventManager.h>
#include <CameraManager.h>
#include <GLFW/glfw3.h>
#include <audio/AudioManager.h>
#include <stdexcept>

#include "DebugUI.h"

#if SOLEIL__USE_IMGUI
#include "examples/opengl3_example/imgui_impl_glfw_gl3.cpp"
#endif

// TODO: APIENTRY Deactivated as not recongnized on windows
static void /*APIENTRY*/
glDebugOutput(GLenum source, GLenum type, GLuint id, GLenum severity,
              GLsizei /*length*/, const GLchar* message,
              const void* /*userParam*/)
{
  // ignore non-significant error/warning codes
  // if (id == 131169 || id == 131185 || id == 131218 || id == 131204) return;

  std::ostringstream          log;
  const Soleil::Logger::Level level = [severity]() {
    switch (severity) {
      case GL_DEBUG_SEVERITY_LOW: return Soleil::Logger::Debug;
      case GL_DEBUG_SEVERITY_NOTIFICATION: return Soleil::Logger::Info;
      case GL_DEBUG_SEVERITY_MEDIUM: return Soleil::Logger::Info;
      case GL_DEBUG_SEVERITY_HIGH: return Soleil::Logger::Error;
    }
    return Soleil::Logger::Error; // Should not happened
  }();

  log << "OpenGL (" << id << "): " << message;

  switch (source) {
    case GL_DEBUG_SOURCE_API: log << "Source: API"; break;
    case GL_DEBUG_SOURCE_WINDOW_SYSTEM: log << "Source: Window System"; break;
    case GL_DEBUG_SOURCE_SHADER_COMPILER:
      log << "Source: Shader Compiler";
      break;
    case GL_DEBUG_SOURCE_THIRD_PARTY: log << "Source: Third Party"; break;
    case GL_DEBUG_SOURCE_APPLICATION: log << "Source: Application"; break;
    case GL_DEBUG_SOURCE_OTHER: log << "Source: Other"; break;
  }

  switch (type) {
    case GL_DEBUG_TYPE_ERROR: log << "Type: Error\n"; break;
    case GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR:
      log << "\nType: Deprecated Behaviour";
      break;
    case GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR:
      log << "\nType: Undefined Behaviour";
      break;
    case GL_DEBUG_TYPE_PORTABILITY: log << "\nType: Portability"; break;
    case GL_DEBUG_TYPE_PERFORMANCE: log << "\nType: Performance"; break;
    case GL_DEBUG_TYPE_MARKER: log << "\nType: Marker\n"; break;
    case GL_DEBUG_TYPE_PUSH_GROUP: log << "\nType: Push Group"; break;
    case GL_DEBUG_TYPE_POP_GROUP: log << "\nType: Pop Group"; break;
    case GL_DEBUG_TYPE_OTHER: log << "\nType: Other\n"; break;
  }

#if 0
  switch (severity) {
    case GL_DEBUG_SEVERITY_HIGH: log << "\nSeverity: high"; break;
    case GL_DEBUG_SEVERITY_MEDIUM: log << "\nSeverity: medium"; break;
    case GL_DEBUG_SEVERITY_LOW: log << "\nSeverity: low"; break;
    case GL_DEBUG_SEVERITY_NOTIFICATION: log << "Severity: notification"; break;
  }
#endif

  Soleil::Logger::Log(level, log.str());

#ifndef NDEBUG
  SOLEIL__ASSERT_BREAK(level != Soleil::Logger::Error);
#endif
}

static void
errorCallback(int error, const char* description)
{
  Soleil::Logger::error(
    Soleil::toString("GLFW failed with error N.", error, ": ", description));
}

static void
keyCallback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
  if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS) {
    glfwSetWindowShouldClose(window, 1);
    return;
  }

  Soleil::Inputs::Key translatedKey;
  if (Soleil::Inputs::TranslateKey(key, translatedKey)) {
    const bool state =
      (action == GLFW_PRESS || action == GLFW_REPEAT) ? true : false;
    const float pressure = (state) ? 1.0f : 0.0f;

    Soleil::Inputs::UpdateKeyState(translatedKey, state);

    Soleil::EventManager::Emit(
      std::make_shared<Soleil::KeyUpdateEvent>(translatedKey, state, pressure));
  }

  // ImGUI::
#if SOLEIL__USE_IMGUI
  ImGui_ImplGlfw_KeyCallback(window, key, scancode, action, mods);
#endif
}

static void
cursor_positionCallback(GLFWwindow* // window
                        ,
                        double xpos, double ypos)
{
  Soleil::Inputs::SetCursorPosition({xpos, ypos});
}

void
mouse_buttonCallback(GLFWwindow* window, int button, int action, int mods)
{

#if SOLEIL__USE_IMGUI
  ImGui_ImplGlfw_MouseButtonCallback(window, button, action, mods);
#endif
  Soleil::Inputs::SetMouseButtonPressed(button, action == GLFW_PRESS);
}

void
mouse_scrollCallback(GLFWwindow* window, double xoffset, double yoffset)
{
#if SOLEIL__USE_IMGUI
  ImGui_ImplGlfw_ScrollCallback(window, xoffset, yoffset);
#endif
  Soleil::Inputs::SetMouseScroll(glm::dvec2(xoffset, yoffset));
}

static void
framebuffer_resizeCallback(GLFWwindow* /*window*/, int width, int height)
{
  glViewport(0, 0, width, height);
}

namespace Soleil {

  GlWindow::GlWindow(int width, int height, std::string name)
    : window(nullptr)
  {
    glfwSetErrorCallback(errorCallback);
    if (!glfwInit()) { throw std::runtime_error("glfwInit failed"); }

    const int GL_Major = 3;
    const int GL_Minor = 3;

    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, GL_Major);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, GL_Minor);
    if ((GL_Major + GL_Minor) > 3) {
      glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    }
    glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, GL_TRUE);

    window = (void*)glfwCreateWindow(width, height, name.c_str(),
                                     glfwGetPrimaryMonitor(), nullptr);
    if (!window) {
      glfwTerminate();
      throw std::runtime_error("glfwCreateWindow failed");
    }

    glfwSetFramebufferSizeCallback((GLFWwindow*)window,
                                   framebuffer_resizeCallback);
    glfwSetKeyCallback((GLFWwindow*)window, keyCallback);
    glfwSetCursorPosCallback((GLFWwindow*)window, cursor_positionCallback);
    glfwSetMouseButtonCallback((GLFWwindow*)window, mouse_buttonCallback);
    glfwSetScrollCallback((GLFWwindow*)window, mouse_scrollCallback);
#if SOLEIL__USE_IMGUI
    glfwSetCharCallback((GLFWwindow*)window, ImGui_ImplGlfw_CharCallback);
#endif

    glfwMakeContextCurrent((GLFWwindow*)window);

    if (gl3wInit()) { throw std::runtime_error("Failed to initialize GL3W"); }
    if (!gl3wIsSupported(GL_Major, GL_Minor)) {
      throw std::runtime_error("OpenGL 3.3 not supported");
    }
    SOLEIL__LOGGER_DEBUG("OpenGL ", glGetString(GL_VERSION), ", GLSL ",
                         glGetString(GL_SHADING_LANGUAGE_VERSION), "\n");

    glfwSwapInterval(1);

    GLint flags;
    glGetIntegerv(GL_CONTEXT_FLAGS, &flags);
    if (flags & GL_CONTEXT_FLAG_DEBUG_BIT) {
      glEnable(GL_DEBUG_OUTPUT);
      glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);
      // TODO: Restore (deactivated to test windows build)
      glDebugMessageCallback(glDebugOutput, nullptr);
      glDebugMessageControl(GL_DONT_CARE, GL_DONT_CARE, GL_DONT_CARE, 0,
                            nullptr, GL_TRUE);
    }

    // Key-mapping Initialization --------------------------------------
    Inputs::KeyMap keymap;

    keymap[GLFW_KEY_W]     = SOLEIL__KEY_FORWARD;
    keymap[GLFW_KEY_S]     = SOLEIL__KEY_BACKWARD;
    keymap[GLFW_KEY_A]     = SOLEIL__KEY_LEFT;
    keymap[GLFW_KEY_D]     = SOLEIL__KEY_RIGHT;
    keymap[GLFW_KEY_F1]    = SOLEIL__KEY_F1;
    keymap[GLFW_KEY_F2]    = SOLEIL__KEY_F2;
    keymap[GLFW_KEY_F3]    = SOLEIL__KEY_F3;
    keymap[GLFW_KEY_F4]    = SOLEIL__KEY_F4;
    keymap[GLFW_KEY_F5]    = SOLEIL__KEY_F5;
    keymap[GLFW_KEY_F6]    = SOLEIL__KEY_F6;
    keymap[GLFW_KEY_F7]    = SOLEIL__KEY_F7;
    keymap[GLFW_KEY_F8]    = SOLEIL__KEY_F8;
    keymap[GLFW_KEY_F9]    = SOLEIL__KEY_F9;
    keymap[GLFW_KEY_F10]   = SOLEIL__KEY_F10;
    keymap[GLFW_KEY_F12]   = SOLEIL__KEY_F11;
    keymap[GLFW_KEY_F11]   = SOLEIL__KEY_F12;
    keymap[GLFW_KEY_PAUSE] = SOLEIL__KEY_PAUSE;
    keymap[GLFW_KEY_SPACE] = SOLEIL__KEY_SPACE;

    // Engine Initialization -------------------------------------------
    Soleil::Assets::Initialize("../media");
    Soleil::DebugManager::Init();
    Soleil::Inputs::Init(keymap);
    Soleil::SceneManager::Init();
    Soleil::EventManager::Init();
    Soleil::ApplicationEventManager::Init();
    Soleil::AudioManager::Init();
    Soleil::CameraManager::Init();

////////////////////////////////////////////////////////////////////////////
#if SOLEIL__USE_IMGUI
    IMGUI_CHECKVERSION();
    ImGui::CreateContext();
    ImGui_ImplGlfwGL3_Init((GLFWwindow*)window, false);
#endif
  }

  GlWindow::~GlWindow()
  {
#if SOLEIL__USE_IMGUI
    ImGui_ImplGlfwGL3_Shutdown();
    ImGui::DestroyContext();
#endif

    Soleil::Assets::Destroy();
    Soleil::DebugManager::Destroy();
    Soleil::Inputs::Destroy();
    Soleil::SceneManager::Destroy();
    Soleil::EventManager::Destroy();
    Soleil::ApplicationEventManager::Destroy();
    Soleil::AudioManager::Destroy();

    glfwTerminate();
  }

  bool GlWindow::isOpen(void) const noexcept
  {
    return !glfwWindowShouldClose((GLFWwindow*)window);
  }

  bool GlWindow::newFrame(void)
  {
    // Update must be before glfwPollEvents in order to have the justPressed
    // effect lasting one frame.
    Soleil::Inputs::Update();
    glfwPollEvents();

#if SOLEIL__USE_IMGUI
    ImGui_ImplGlfwGL3_NewFrame();
    static bool show_demo_window = true;
    ImGui::ShowDemoWindow(&show_demo_window);
#endif
    return isOpen();
  }

  glm::vec2 GlWindow::getFrameBufferSize(void) const noexcept
  {
    int width, height;
    glfwGetFramebufferSize((GLFWwindow*)window, &width, &height);

    return glm::vec2(width, height);
  }

  void GlWindow::displayMouseCursor() noexcept
  {
    // TODO: Why getting the instance?
    glfwSetInputMode((GLFWwindow*)GlWindowInstance::GetInstance()->window,
                     GLFW_CURSOR, GLFW_CURSOR_NORMAL);
  }

  void GlWindow::hideMouseCursor() noexcept
  {
    // TODO: Why getting the instance?
    glfwSetInputMode((GLFWwindow*)GlWindowInstance::GetInstance()->window,
                     GLFW_CURSOR, GLFW_CURSOR_DISABLED);
  }

  void GlWindow::endFrame(void)
  {
#if SOLEIL__USE_IMGUI
    ImGui::Render();
    ImGui_ImplGlfwGL3_RenderDrawData(ImGui::GetDrawData());
#endif

    glfwSwapBuffers((GLFWwindow*)window);
  }

  static GlWindow* instance = nullptr;

  void GlWindowInstance::SetInstance(GlWindow* window)
  {
    assert((instance == nullptr || window == nullptr) &&
           "You are setting a GlWindow instance while one already exists. If "
           "it's really what you want, explicit it by calling "
           "SetInstance(nullptr) before setting the new instance.");

    instance = window;
  }

  GlWindow* GlWindowInstance::GetInstance(void) noexcept
  {
    assert(instance != nullptr &&
           "Instance is null, please call SetInstance before.");

    return instance;
  }

} // namespace Soleil
