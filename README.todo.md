# TODO task with planning

## In Progress

### The current Target is /Zinc flight VS/

* Clean of the repository (Build system, code and comments)

## Backlog

 * A GUID System to be able to hash object and detect collisions (in hash)
 It will replace the `Hash()` everywhere.
 ID can be generated manually (from hash string or hash objects) or
 automatically (incremented). There is a domain (a short 0 -> 512) which
 represents: 0 The global domain, 1 The meshes, 2 ..., n The
 auto-incremented objects. (or 512 - the domain --> 512 -1 ==
 autoincremented mesh). The ID can also conatains a String in debug.
 * Allow to be able to change the Render System
 * Allow to be able to change the Input system
 * A better Scene Graph implementation
 * Merge Model and Mesh to an unique 'RenderMesh'
 RenderMesh will be constructed with a GL buffer not directly from the
 vertices.
 * Separate the Physics Mesh loading from the Render Mesh one.
 * An assert system that can log messages

## Current features

 * Phong Shader
 * OpenGL >= 3.3


## GUID prototype

        // Then it will be replaced with a GUID that contains a domain:
        // (GUID or ObjectID)
        // class GUID
        // {
        //	short unsigned	domain; // Contains also the NodeType
        //	std::size_t	id;
        // };
        //
        // And a delete object that will select the domain and delete it:
        // void removeObect(GUID guid)
        // {
        //	(try)removeNode(guid); // Try remove the node from the scene
        //	Objects[guid.domain].removeObect(guid);
        // }

