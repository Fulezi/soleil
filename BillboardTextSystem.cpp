/*
 * Copyright (C) 2019  Florian GOLESTIN
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <BillboardTextSystem.h>

#include <Assets.h>
#include <Errors.h>
#include <Font.h>
#include <Logger.h>
#include <OpenGLInclude.h>
#include <TypesToOStream.h>
#include <glm/gtc/type_ptr.hpp>
#include <memory>

// TODO: Temp
#include <CameraManager.h>
#include <DebugManager.h>
#include <DebugUI.h>

namespace Soleil {

  BillboardText::BillboardText(Font* font)
    : font(font)
  {
    glGenVertexArrays(1, &vertexArray);
    glGenBuffers(1, &vertexBuffer);
    throwOnGlError();

    setText("BBBBBBBBBBBBBBBBBBBBBB\nStock\nWarehouse\nAAAAAAAAAAAAAAAAAAAAAAAA"
            "AAAAAAAAAAAAAAAAAAA");
  }

  BillboardText::~BillboardText()
  {
    glDeleteVertexArrays(1, &vertexArray);
    glDeleteBuffers(1, &vertexBuffer);
  }

  void BillboardText::setText(const std::string& text)
  {
    glm::vec2             size;
    std::vector<TriGlyph> line = font->writeLine(text, size);
    lineTriSize                = line.size();
    lineHalfWidth.x            = size.x * 0.5f;
    lineHalfWidth.y            = size.y * 0.5f;
    // lineHalfWidth              = line.back().advance.x / 2.0f;

    glBindVertexArray(vertexArray);
    glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer);
    glBufferData(GL_ARRAY_BUFFER, line.size() * sizeof(TriGlyph), line.data(),
                 GL_DYNAMIC_DRAW);
    throwOnGlError();

    // Advance
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, sizeof(TriGlyph),
                          reinterpret_cast<GLvoid*>(0));
    // Textures
    glEnableVertexAttribArray(1);
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(TriGlyph),
                          reinterpret_cast<GLvoid*>(offsetof(TriGlyph, tex)));
    throwOnGlError();
  }

  void BillboardText::setCenter(glm::vec3 const& center)
  {
    this->center = center;
  }

  void BillboardText::setScale(const float scale) { this->scale = scale; }

  void BillboardText::draw(Frame&                       frame,
                           BillboardTextUniforms const& uniforms) const
  {
    glBindVertexArray(vertexArray);

#if 0
    ImGui::Begin("Billboard");
    {
      // ImGui::SliderFloat3("Billboard Center:", glm::value_ptr(center),
      // -200.0f,
      //                     200.0f);
      ImGui::Text("lineTriSize = %zu", lineTriSize);

      glm::vec3 lhw = lineHalfWidth;
      ImGui::SliderFloat3("Line half width:", glm::value_ptr(lhw), -200.0f,
                          200.0f);
      ImGui::SliderFloat3(
        "Camera Position",
        glm::value_ptr(CameraManager::GetCurrentCamera()->position), -200.0f,
        200.0f);

      // ImGui::SliderFloat("Scale", &scale, 0.01f, 1.0f);

      DebugManager::DrawPoint(center, glm::vec4(1, 0, 0, 1));
      DebugManager::DrawLine(center, center + lhw, glm::vec4(1.0));
    }
    ImGui::End();
#endif

    glUniform3fv(uniforms.BillboardCenter, 1, glm::value_ptr(center));
    throwOnGlError();
    glUniform3fv(uniforms.LineHalfWidth, 1, glm::value_ptr(lineHalfWidth));
    throwOnGlError();

    glBindTexture(GL_TEXTURE_2D, font->texture);
    throwOnGlError();

    glUniform1f(uniforms.Scale, scale);

    // glDisable(GL_DEPTH_TEST);
    glDrawArrays(GL_TRIANGLES, 0, lineTriSize);
  }

  //////////////////////////////////////////////////////////////////////////////
  // BillboardTextSystem

  
  BillboardTextSystem::BillboardTextSystem()
  {
    program.attachShader(GL_VERTEX_SHADER, "billboard-text.vert");
    program.attachShader(GL_FRAGMENT_SHADER, "billboard-text.frag");
    program.compile();

    uniforms.BillboardCenter = program.getUniform("BillboardCenter");
    uniforms.LineHalfWidth   = program.getUniform("LineHalfWidth");
    uniforms.Scale           = program.getUniform("Scale");
  }


  void BillboardTextSystem::draw(Frame& frame)
  {
    glUseProgram(program.program);

    glUniform3fv(program.getUniform("CameraPosition"), 1,
                 glm::value_ptr(CameraManager::GetCurrentCamera()->position));
    throwOnGlError();

    glUniform1i(program.getUniform("font"), 0);
    glActiveTexture(GL_TEXTURE0);

#if 0
    const glm::vec3 CameraPosition =
      CameraManager::GetCurrentCamera()->position;
    glm::vec3 front = glm::normalize(center - CameraPosition);
    glm::vec3 right = glm::normalize(cross(glm::vec3(0, 1, 0), front));
    glm::vec3 up    = glm::normalize(cross(front, right));
#endif

    const glm::mat4 skyboxMat = frame.Projection * frame.View;
    glUniformMatrix4fv(program.getUniform("ViewProjection"), 1, GL_FALSE,
                       glm::value_ptr(skyboxMat));
    throwOnGlError();

    for (auto const& bb : billboards) { bb->draw(frame, uniforms); }
    for (const BillboardText* bb : billboardsImmediate) {
      bb->draw(frame, uniforms);
    }

    billboardsImmediate.clear();
  }

  void BillboardTextSystem::pushBillboard(BillboardText* billboard)
  {
    billboardsImmediate.push_back(billboard);
  }

  BillboardText* BillboardTextSystem::createBillboard(Font*              font,
                                                      std::string const& text)
  {
    billboards.emplace_back(std::make_unique<BillboardText>(font));
    billboards.back()->setText(text);
    return billboards.back().get();
  }

  void BillboardTextSystem::destroyBillboard(BillboardText* billboard)
  {
    // TODO: The pedantic way
    // auto it = std::find(billboards.begin(), billboards.end(), billboard);
    // if (it == billboards.end()) {
    //   SOLEIL__RUNTIME_ERROR("Billboard not found");
    // } else {
    //   billboards.erase(it);
    // }

    for (auto it = billboards.begin(); it < billboards.end(); ++it) {
      if (it->get() == billboard) {
	billboards.erase(it);
	return;
      }
    }
    SOLEIL__RUNTIME_ERROR("Billboard not found");
  }

  namespace {
    std::unique_ptr<BillboardTextSystem> s_instance;
  }

  void BillboardTextSystem::Init()
  {
    s_instance = std::make_unique<BillboardTextSystem>();
  }

  void BillboardTextSystem::Destroy() { s_instance = nullptr; }

  void BillboardTextSystem::Draw(Frame& frame)
  {
    assert(s_instance);

    s_instance->draw(frame);
  }

  void BillboardTextSystem::PushBillboard(BillboardText* billboard)
  {
    s_instance->pushBillboard(billboard);
  }

  BillboardText* BillboardTextSystem::CreateBillboard(Font*              font,
                                                      std::string const& text)
  {
    return s_instance->createBillboard(font, text);
  }

  void BillboardTextSystem::DestroyBillboard(BillboardText* billboard)
  {
    s_instance->destroyBillboard(billboard);
  }

} // namespace Soleil
