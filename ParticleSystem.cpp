/*
 * Copyright (C) 2018  Florian GOLESTIN
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include "ParticleSystem.h"

#include "Assets.h"
#include "Frame.h"
#include "Random.h"
#include "SceneManager.h"
#include <glm/gtc/type_ptr.hpp>
#include <memory>
#include <physics/PhysicsWorld.h>

namespace Soleil {

  ParticleSystem::ParticleSystem(const std::string& textureName,
                                 std::size_t        maximumParticles)
    : maximumParticles(maximumParticles)
    , particles(maximumParticles)
    , sprites(maximumParticles)
    , spriteCount(0)
    , firstFreeParticle(0)
    , particleBuffer()
    , texture(Assets::GetTexture(textureName))
  {
    vao.bind();

    // Load the Program:
    program.attachShader(GL_VERTEX_SHADER, "spriteparticle.vert");
    program.attachShader(GL_FRAGMENT_SHADER, "spriteparticle.frag");
    program.compile();

    // Prepare the buffer
    particleBuffer.bind(GL_ARRAY_BUFFER);
    {
      glBufferData(GL_ARRAY_BUFFER, sizeof(SpriteParticle) * maximumParticles,
                   nullptr, GL_STREAM_DRAW);

      // Position
      glEnableVertexAttribArray(0);
      glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(SpriteParticle),
                            nullptr);
      throwOnGlError();

      // Color
      glEnableVertexAttribArray(1);
      glVertexAttribPointer(
        1, 4, GL_FLOAT, GL_FALSE, sizeof(SpriteParticle),
        reinterpret_cast<GLvoid*>(offsetof(SpriteParticle, color)));
      throwOnGlError();

      // scale
      glEnableVertexAttribArray(2);
      glVertexAttribPointer(
        2, 1, GL_FLOAT, GL_FALSE, sizeof(SpriteParticle),
        reinterpret_cast<GLvoid*>(offsetof(SpriteParticle, scale)));
      throwOnGlError();

      // uv
      glEnableVertexAttribArray(3);
      glVertexAttribPointer(
        3, 2, GL_FLOAT, GL_FALSE, sizeof(SpriteParticle),
        reinterpret_cast<GLvoid*>(offsetof(SpriteParticle, uv)));
      throwOnGlError();
    }
    throwOnGlError();
  }

  Particle& ParticleSystem::emitParticle()
  {
    auto savedSearch = firstFreeParticle;

    while (firstFreeParticle < maximumParticles) {
      if (particles[firstFreeParticle].life <= 0.0f) {
        // firstFreeParticle++;
        return particles[firstFreeParticle++];
      }
      firstFreeParticle++;
    }

    firstFreeParticle = 0;
    while (firstFreeParticle < savedSearch) {
      if (particles[firstFreeParticle].life <= 0.0f) {
        // firstFreeParticle++;
        return particles[firstFreeParticle++];
      }

      firstFreeParticle++;
    }

    SOLEIL__LOGGER_DEBUG("*** *** *** *** *** "
                         "Particle System is starving. No free particle found");
    firstFreeParticle = 0;
    // firstFreeParticle++;
    // if (firstFreeParticle > maximumParticles)
    //   {
    // 	firstFreeParticle = 0;
    //   }
    return particles[firstFreeParticle];
  }

  void ParticleSystem::update(const float deltaTime)
  {
    // TODO: Update the buffer only if a change has occured.

#if 0
    // Example of fountain particle
#warning Fix the first expulsion
    // constexpr int numberPerSecond = 1000;
    // constexpr int numberPerFrame  = numberPerSecond / 60;
    constexpr std::size_t numberPerFrame = 120;

    SOLEIL__LOGGER_DEBUG("Delta time: ", deltaTime);

    int numberParticleCreated = 0;
    for (std::size_t i = 0; i < maximumParticles; ++i) {
      // TODO: A GetUnusedParticle method
      // So far we do not recycle particles.

      if (numberParticleCreated >= numberPerFrame) {
        break;
      }

      Particle& particle = particles[i];
      if (particle.life <= 0.0f) {
        particle.position   = glm::vec3(0.0f);
        particle.life       = Random(3.0f, 5.5f);
        particle.velocity.x = Random(-2.0f, 2.0f);
        particle.velocity.z = Random(-2.0f, 2.0f);
        particle.velocity.y = Random(9.0f, 12.0f);
        particle.color.r    = Random(0.0f, 1.0f);
        particle.color.g    = Random(0.0f, 1.0f);
        particle.color.b    = Random(0.0f, 1.0f);
        particle.color.a    = Random(0.0f, 1.0f);
        particle.scale      = Random(0.01f, 0.1f);
        numberParticleCreated++;
      }
    }
#endif

    spriteCount = 0;
    for (auto& particle : particles) {
      particle.life -= deltaTime;
      if (particle.life > 0.0f) {
#if 0
        particle.position += particle.velocity * deltaTime;
        particle.velocity.y += -9.0f * deltaTime;
        particle.velocity *= std::pow(0.90f, deltaTime);
// TODO: Enable Physics via a bool on ParticleSystem
#else
        const glm::vec3 previousPosition = particle.position;
        particle.position += particle.velocity * deltaTime;

        // particle.velocity.y += -9.0f * deltaTime;
        // particle.velocity *= std::pow(0.90f, deltaTime);

#if 0
	RayCastResult r =
          SceneManager::RayCast(previousPosition, particle.position);
        if (r.size() > 0 && collisionSignal) {
          collisionSignal(r, particle);
        }
#elif 0
        if (collisionSignal) {
          const BoundingBox bounds(glm::vec3(-particle.scale),
                                   glm::vec3(particle.scale));
          RayCastResult r = SceneManager::RayCastGJK(bounds, previousPosition,
                                                     particle.position);
          //(previousPosition, particle.position);
          if (r.size() > 0) {
            particle.position = previousPosition;
            // TODO: We can add here a `r.closestCollisionDistance` and compute
            // the closest collision:
            // particle.position = previousPosition +
            // glm::normalize(particle.position - previousposition) *
            // r.closestCollisionDistance * factor;
            // Factor is a float to indicate 'just before the collision' (e.g.
            // .95f);

            collisionSignal(r, particle);
          }
        }
#else
        if (collisionSignal) {
          auto r = PhysicsWorld::RayCast(previousPosition, particle.position);
          if (r.count > 0) { collisionSignal(r, particle); }
        }
#endif

// TODO: At some point it will be good to have all physic related in a
// Manager
//  The particle will hold a pointer to physic (position, velocity, ...)
//  and all computation of every elements will be hold in the manager
// for instance:
// struct Particle { Physic* physic; }
// make sure to handle collision
// All the collisions can be done there
#endif

	// TODO: Allow infinite life with no color alteration
        SpriteParticle& sprite = sprites[spriteCount];
        sprite.position        = particle.position;
        sprite.color           = particle.color * particle.life;
        sprite.scale           = particle.scale;
        // sprite.uv              = glm::vec2(0.5f, 1.0f); Allow to modify the
        // UV
        spriteCount++;
      }
    }

    // TODO: Sort particles?
    // TODO: a dedicated update buffer method
    particleBuffer.bind(GL_ARRAY_BUFFER);
    {
      // Buffer orphaning
      glBufferData(GL_ARRAY_BUFFER, sizeof(SpriteParticle) * particles.size(),
                   nullptr, GL_STREAM_DRAW);
      glBufferSubData(GL_ARRAY_BUFFER, 0, spriteCount * sizeof(SpriteParticle),
                      sprites.data());
      throwOnGlError();
    }
  }

  void ParticleSystem::draw(Frame& frame)
  {
    glUseProgram(program.program);
    vao.bind();
    particleBuffer.bind(GL_ARRAY_BUFFER);
    {
      // TODO: store uniform
      // glUniformMatrix4fv(program.getUniform("ModelViewProjection"), 1,
      // GL_FALSE,
      //                    glm::value_ptr(frame.ViewProjection));
      glActiveTexture(GL_TEXTURE0);
      throwOnGlError();
      glBindTexture(GL_TEXTURE_2D, texture);
      glUniform1i(program.getUniform("diffuse"), 0);
 
      glUniformMatrix4fv(program.getUniform("View"), 1, GL_FALSE,
                         glm::value_ptr(frame.View));
      glUniformMatrix4fv(program.getUniform("Projection"), 1, GL_FALSE,
                         glm::value_ptr(frame.Projection));
      glUniform2f(program.getUniform("ScreenSize"), 1920.0f, 1080.0f);
//#warning Dynamic screen size - Store uniform

      glBindTexture(GL_TEXTURE_2D, texture);

      // TODO: Move in DrawParticleSystems method, update the example main
      glEnable(GL_DEPTH_TEST);
      glEnable(GL_BLEND);
      glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

      glEnable(GL_PROGRAM_POINT_SIZE);
      glPointSize(5.0f); // specify size of points in pixels
      glDrawArrays(GL_POINTS, 0, spriteCount); // draw the points
      throwOnGlError();
      // SOLEIL__LOGGER_DEBUG("Number Particle Drawed: ", spriteCount);
    }
  }

  ////////////////////////////////////////////////////////////////////////////
  // Statics

  static std::vector<std::unique_ptr<ParticleSystem>> s_automanagedSystems;

  void ParticleSystem::Init()
  {
    // Nothing to do
  }

  void ParticleSystem::Destroy() { s_automanagedSystems.clear(); }

  ParticleSystem* ParticleSystem::CreateParticleSystem(
    const std::string& textureName, std::size_t maximumParticles)
  {
    s_automanagedSystems.emplace_back(
      std::make_unique<ParticleSystem>(textureName, maximumParticles));
    return s_automanagedSystems.back().get();
  }

  void ParticleSystem::DrawParticleSystems(Frame& frame, const float deltaTime)
  {
    for (auto& ps : s_automanagedSystems) {
      ps->update(deltaTime); // TODO: Set this here?
      ps->draw(frame);
    }
  }

} // namespace Soleil
