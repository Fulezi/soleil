/*
 * Copyright (C) 2018  Florian GOLESTIN
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#pragma once

#include "OpenGLInclude.h"
#include <glm/vec2.hpp>
#include <string>

namespace Soleil {

  class GlWindow
  {
  public:
    GlWindow(int width, int height, std::string name);
    ~GlWindow();

  public:
    bool      isOpen(void) const noexcept;
    glm::vec2 getFrameBufferSize(void) const noexcept;

  public:
    void displayMouseCursor() noexcept;
    void hideMouseCursor() noexcept;

  public:
    // Start a new frame (poll events). Return false if application has to stop
    bool newFrame(void);
    // End Frame, Render and swap buffers
    void endFrame(void);

  public:
    using WindowImpl = void;

    //GLFWwindow* window;
    WindowImpl* window;
  };

  /// Holder of the main GlWindow instance, you have to manually call
  /// SetInstance when you create your main GLWindow.
  class GlWindowInstance
  {
  public:
    static void      SetInstance(GlWindow* window);
    static GlWindow* GetInstance(void) noexcept;
  };
} // namespace Soleil
