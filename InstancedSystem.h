/*
 * Copyright (C) 2018  Florian GOLESTIN
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#pragma once

#include "Frame.h"
#include "Particle.h"
#include "Program.hpp"
#include "ogl/Buffer.h"
#include "ogl/VertexArray.h"
#include <vector>

namespace Soleil {

  // TODO: Rename sprite...with mesh
  struct MeshParticle;

  struct MeshParticle
  {
    glm::vec4 color;
    glm::mat4 transformation;
  };

  class InstancedSystem
  {
  public:
    InstancedSystem(const std::string& modelName, std::size_t maximumParticles);

  public:
    Particle& emitParticle();

    void update(const float deltaTime);

    void draw(Frame& frame);

  private:
    std::size_t               maximumParticles;
    std::vector<Particle>     particles;
    std::vector<MeshParticle> sprites;
    std::size_t               spriteCount;

  private:
    std::size_t firstFreeParticle;

  private: // Draw Resource
    Buffer      meshBuffer;
    Buffer      particleBuffer;
    VertexArray vao;
    Program     program;
    int         meshNumberVertices;

    ////////////////////////////////////////////////////////////////////////////
    // Statics
  public:
    static void Init();
    static void Destroy();
    
    /**
     * Create a particle system that will be auto-managed: Creation - Update -
     * Draw - Destruction.
     */
    static InstancedSystem* CreateParticleSystem(const std::string& modelName,
                                                 std::size_t maximumParticles);

    /**
     * Destroy the related Instanced system.
     */
    static void DestroyParticleSystem(InstancedSystem* sys);

    /**
     * Update the buffers and draw all the managed Particle systemss
     */
    static void DrawParticleSystems(Frame& frame, const float deltaTime);
  };

} // Soleil
