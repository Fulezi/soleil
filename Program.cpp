/*
 * Copyright (C) 2017  Florian GOLESTIN
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include "Program.hpp"
#include "Assets.h"
#include "Logger.h"

namespace Soleil {

#ifndef SOLEIL__SHADER_VERSION
//# define SOLEIL__SHADER_VERSION "#version 330 core\n"
//# define SOLEIL__SHADER_VERSION "#version 130\n#define SOLEIL__GLSL_VERSION\n"

// So far, glslangValidator cannot be configured with shader version, so keep
// the "#version" in the shader file.
#define SOLEIL__SHADER_VERSION ""
#endif

  namespace {
    static const GLchar* Version = SOLEIL__SHADER_VERSION;
  }

  /* --- Util Shader RAII --- */
  class ShaderRAII
  {
  public:
    ShaderRAII(GLenum shaderType)
      : shader(glCreateShader(shaderType))
    {}

    ~ShaderRAII() { glDeleteShader(shader); }

    void compile(const std::string& name)
    {
      glCompileShader(shader);

      GLint isCompiled = 0;
      glGetShaderiv(shader, GL_COMPILE_STATUS, &isCompiled);
      std::string state = (isCompiled == GL_FALSE) ? "FAILED" : "SUCCESS";

      GLint maxLength = 0;
      glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &maxLength);

      // `maxLength` may be equal zero if we have no information
      std::string logs;
      if (maxLength > 0) {
        std::vector<GLchar> errorLog(maxLength);
        glGetShaderInfoLog(shader, maxLength, &maxLength, &errorLog[0]);

        // The `maxLength` includes the NULL character, so a size of 1 means
        // empty string.
        if (maxLength > 1) {
          logs = ":" + std::string(errorLog.begin(), errorLog.end());
        }
      }

      std::string fin =
        toString("[COMPILATION] Shader (", name, ") compilation ", state, logs);

      if (isCompiled == GL_FALSE) {
        throw std::runtime_error(fin);
      } else {
        Logger::info(fin);
      }
    }

  public:
    GLuint shader;
  };

  /* --- Util Read shader --- */
  static void readShader(ShaderRAII& shader, const std::string& resourceName)
  {
    const std::string source    = Assets::AsString(resourceName);
    const GLchar*     sources[] = {Version,
                               static_cast<const GLchar*>(source.c_str())};

    glShaderSource(shader.shader, 2, sources, nullptr);
  }

  /* --- Program Source code --- */

  Program::Program()
    : program(glCreateProgram())
  {
    SOLEIL__LOGGER_DEBUG("Constructring Program: ", program);
  }

  Program::~Program()
  {
    SOLEIL__LOGGER_DEBUG("Deleting Program: ", program);
    glDeleteProgram(program);
  }

  void Program::attachShader(GLenum shaderType, const std::string& resourceName)
  {
    ShaderRAII shader(shaderType);

    readShader(shader, resourceName);

    shader.compile(resourceName);
    glAttachShader(program, shader.shader);
  }

  void Program::compile(void)
  {
    glLinkProgram(program);

    GLint infoLen    = 0;
    GLint isCompiled = 0;

    glGetProgramiv(program, GL_LINK_STATUS, &isCompiled);
    std::string state = (isCompiled == GL_FALSE) ? "FAILED" : "SUCCESS";

    glGetProgramiv(program, GL_INFO_LOG_LENGTH, &infoLen);

    std::string logs;
    if (infoLen > 1) {
      std::vector<GLchar> errorLog(infoLen);
      glGetProgramInfoLog(program, infoLen, nullptr, &errorLog[0]);
      logs = ":" + std::string(errorLog.begin(), errorLog.end());
    }

    std::string fin = toString("[COMPILATION] Program compilation N.", program,
                               " ", state, logs);

    if (isCompiled == GL_FALSE) {
      // glDeleteProgram(program); // Destructor whill never be reached
      throw std::runtime_error(fin);
    } else {
      Logger::info(fin);
    }

    throwOnGlError();
  }

  GLint Program::getUniform(const GLchar* name) const
  {
    GLint location = glGetUniformLocation(program, name);
    if (location < 0) {
      // throw std::runtime_error(toString("Cannot find location: ", name));
      SOLEIL__LOGGER_DEBUG("Cannot find location: ", name);
      return std::numeric_limits<GLint>::max();
    }
    return location;
  }

} // namespace Soleil
