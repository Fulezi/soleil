/*
 * Copyright (C) 2019  Florian GOLESTIN
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#pragma once

#include <Font.h>
#include <Frame.h>
#include <Program.hpp>
#include <glm/matrix.hpp>
#include <string>

namespace Soleil {

  struct Margin
  {
    float right;
    float left;
    float up;
    float down;

    Margin(float uniform)
      : right(uniform)
      , left(uniform)
      , up(uniform)
      , down(uniform)
    {}

    Margin(const float left, const float right, const float up,
           const float down)
      : right(right)
      , left(left)
      , up(up)
      , down(down)
    {}

    glm::vec2 upperLeft(void) const
    {
      return glm::vec2(left, up);
    }
  };

  class UIText
  {
  public:
    UIText(Font* font, const std::string& text);

    void setText(const std::string& text);

    void draw(Frame& frame, const float deltaTime);

  public:
    glm::vec2 position = glm::vec2(0.0f);
    float     width    = 1.8f;
    float     height   = 0.5f;
    Margin    margin    = Margin(0.1f);
    float     textScale = 0.002f;
    glm::mat4 transform = glm::mat4(1.0f);
    GLuint    backgroundTexture = 0;

  private:
    GLuint      vertexArray;
    GLuint      vertexBuffer;
    std::size_t numVertices;

  private:
    Font* font;
  };

  class UITextSystem
  {
  public:
    UITextSystem();

    UIText* create(Font* font, const std::string& text);
    void    remove(UIText* text);

    void draw(Frame& frame, const float deltaTime);

  private:
    Program                              program;
    std::vector<std::unique_ptr<UIText>> texts;
  };

} // namespace Soleil
