# Soleil

## Game Toolkit

Soleil does not aim to be a Game Engine but more a Game Toolkit to build
a game engine or directly a game.
It currently embed Zinc, while both projects will be sperated.

Dependencies
------------

 * ImGUI	(Integrated into the project)
 * GLFW		(Built with the project or Linked)
 * Assimp	(Built with the project or Linked)
 * yaml-cpp	(Built with the project or Linked)
 * glm		(Integrated into the project)
 * OpenGL	(Linked)

## Compilation

Make sure you also got the submodules

    git submodule
    git submodule update

Gl3W is used and can be updated by running this command in the root
folder. It's not mandatory though.

    gl3w/gl3w_gen.py

Then run CMake:

    mkdir build
    cd build
    cmake -DBUILD_DEPEDENCIES=1 ../

The `-DBUILD_DEPEDENCIES=1` parameter tells CMake to build all
depenencies such as GLFW and link it to the project statically. If you
don't specify it, you should install them with the dev version on your
system.

## Rendering Meshes

A `Mesh` is a vector of Vertex with a `Material`. There is currently no
way to have multiple materials per mesh (it my change in the future
though).  The `Mesh`es are owned by a `Model` making the `Model` is the
root object of the scene. This way allows to have multiple material per
'rendered object'. For instance, if in the modeler tool there is one
sphere with a unique material and one cube with two materials, the
resulting scene in Soleil will be:

Model + Scene.obj
      | Sphere::MaterialSphere
      | Cube::MaterialCube_1
      | Cube::MaterialCube_2

The vertices (`Vertex`) in `Mesh` are packed such as

    [{position, normal, vertex}, {...}, ...]


## Error Management

Article 1. Do not use `assert` for error magement! See below.

### Assert

Use assert to tell something that is true, like you will do with a comment:

    if (myArray.size() < 1) {
       return ;
    }

    assert(myArray.size() > 0);
    const float avg = value / myArray.size();

If an assert fails, it means that a huge issue has been caught in development.
It's tolerated to use assert to check return value or parameters of a private
function.

### Runtime error

If a runtime error happens, such as a texture not found, use the RuntimeError
function and return from the function:

    if (resource == INVALID) {
        SOLEIL__RUNTIME_ERROR(...);
	return ;
    }
    // else
    // [...]

The implementation will be overridable by the user but will likely an error log
in debug and maybe an exception thrown in release.

### Irrecoverable error

If something went wrong and there is no chance to continue, nor this error can
be ignored by the user use the following function:

    SOLEIL_TERMINATE_ERROR(...);

As the `SOLEIL__RUNTIME_ERROR` the user will be able to choose the behavior but
it should stop the process, throw an exception or call exit.


## The Scene and the SceneGraph

(current goal)

This engine is targetted to be Data oriented and Immediate.
The main way to add object to your scene is to push them:

    MeshManager::PushMesh(myMeshID, myMeshTransform);

This way the mesh will be drawed in the current frame. Therefore you
will have to maintain the state yourself:

     while (runGameLoop)
     {
        // [ ... ]
        MeshManager::PushMesh(myMeshID, myMeshTransform);
        // [ ... ]
     }

This can be done also for otherType of objects:

     PhysicWorld::PushEntity(physicBodyID, physicBodyTransform);
     LightManager::PushLight(myLight, transform);
     // ...


