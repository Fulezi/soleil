/*
 * Copyright (C) 2017  Florian GOLESTIN
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <stdexcept>
#include <string>

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtx/string_cast.hpp>

//#define SOLEIL__DO_TIMER
#include <Logger.h>

#include "OpenGLInclude.h"

#include "Assets.h"
#include "Camera.h"
#include "DebugManager.h"
#include "DebugUI.h"
#include "Draw.h"
#include "EventManager.h"
#include "GlWindow.h"
#include "Inputs.h"
#include "InstancedSystem.h"
#include "Mesh.h"
#include "Model.h"
#include "ParticleSystem.h"
#include "Program.hpp"
#include "Random.h"
#include "SceneManager.h"
#include "Timer.h"
#include "stringutils.h"
#include <CameraManager.h>
#include <physics/Intersections.h>

using namespace Soleil;

#include "stringutils.h"

/// Allow from the editor to switch between multiples camera such as the
/// FreeLook and the editor's one.
class CameraSwitcher
{
public:
  CameraSwitcher();
  void registerCamera(const Inputs::Key key, Camera* camera);
  void switchCamera(const Inputs::Key key);
  bool isCurrentCamera(const Inputs::Key key);

  void displayCameraList();

  std::map<Inputs::Key, Camera*> registeredCameras;
};

/**
 * Usage: ./executable [Assets/Source/Path [Model1.obj, [...]]]
 */
int
main(int argc, const char* argv[])
{
  GlWindow window(1920, 1080, "Soleil");
  // GlWindow window(1024, 768, "Soleil");
  GlWindowInstance::SetInstance(&window);

  // const glm::vec2 bufferSize = window.getFrameBufferSize();

  Soleil::Program meshShader;
  meshShader.attachShader(GL_VERTEX_SHADER, "mesh.vert");
  meshShader.attachShader(GL_FRAGMENT_SHADER, "mesh.frag");
  meshShader.compile();
  Soleil::MeshShader meshConfig;
  meshConfig.initialize(meshShader);

  // Engine Initialization -------------------------------------------
  if (argc > 1) {
    // Reload the assets manager with a different root path
    Soleil::Assets::Initialize(argv[1]);
  }

  Soleil::Frame frame;
  frame.nullTexture = Soleil::generateNullTexture();

  /* Load all the model from arguments */
  std::vector<std::unique_ptr<Model>> models;
  Node&                               root = SceneManager::GetRootScene();
  for (int i = 2; i < argc; ++i) {
    models.push_back(std::make_unique<Model>(argv[i]));
    root.children.push_back(models.back()->root);
  }

  std::vector<Light> lights;
  lights.push_back({true, false, glm::vec3(0.1f), glm::vec3(1.0f),
                    glm::vec3(4.0f, 3.0f, 0.0f), glm::vec3(), 0.0f, 0.0f, 1.0f,
                    0.09f, 0.032f});

  lights.push_back({false, false, glm::vec3(0.4f), glm::vec3(0.60f),
                    glm::normalize(glm::vec3(1.0f, 1.0f, 0.0f)), glm::vec3(),
                    0.0f, 0.0f, 1.0f, 0.7f, 1.8f});

  throwOnGlError();

  // TODO: resize callback
  glEnable(GL_DEPTH_TEST);
  glClearColor(0.1f, 0.1f, 0.3f, 1.0f);

  std::unique_ptr<EditorCamera> editorCamera = std::make_unique<EditorCamera>(
    static_cast<float>(window.getFrameBufferSize().x),
    static_cast<float>(window.getFrameBufferSize().y));

  std::unique_ptr<Freecam> freecam = std::make_unique<Freecam>(
    static_cast<float>(window.getFrameBufferSize().x),
    static_cast<float>(window.getFrameBufferSize().y));
  freecam->onActivate(nullptr);

  CameraManager::SetCurrentCamera(freecam.get());

  CameraSwitcher cameraSwitcher;
  cameraSwitcher.registerCamera(SOLEIL__KEY_F1, freecam.get());
  cameraSwitcher.registerCamera(SOLEIL__KEY_F2, editorCamera.get());

  /* --- */
  while (window.newFrame()) {
    SOLEIL__TIMER_START(FrameDuration);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    /* --- Scene Update --- */
    Time::StartFrame();
    SOLEIL__TIMER_START(SceneGraphComputation);
    DebugManager::NewFrame(Time::GetDeltaTime());
    SceneManager::ComputeScene();
    SOLEIL__TIMER_COMPUTE(SceneGraphComputation);
    /* --- Scene Update --- */

    /* --- Camera Update --- */
    {
      Camera* camera = CameraManager::GetCurrentCamera();
      camera->update(0.16f); // TODO: delta time
      frame.updateViewProjectionMatrices(camera->view, camera->projection);
    }
    /* --- */

    glUseProgram(meshShader.program);

    // Lights:
    glUniform1i(meshConfig.numberOfLight, lights.size());
    unsigned int i = 0;
    for (const auto& l : lights) {
      setupLightUniform(l, meshConfig, glm::vec3(),
                        i); // TODO: Remove direction vector
      ++i;
    }

    SceneManager::RenderDrawList(meshConfig, lights, frame);

    // Debug
    // DebugManager::DisplayMetrics();
    DebugManager::DisplayTools(Time::GetDeltaTime() / 100.0);
    DebugManager::DisplayPrimitives(frame);
    cameraSwitcher.displayCameraList();

    // Poll Events------------------------------------------------------
    SOLEIL__TIMER_START(EndFrame);
    window.endFrame();
    SOLEIL__TIMER_COMPUTE(EndFrame);

    SOLEIL__TIMER_START(EventManagerProcessEvents);
    Soleil::EventManager::ProcessEvents();
    SOLEIL__TIMER_COMPUTE(EventManagerProcessEvents);

    SOLEIL__TIMER_COMPUTE(FrameDuration);
    throwOnGlError(); //
  }

  return 0;
}

CameraSwitcher::CameraSwitcher()
{
  EventManager::Enroll(KeyUpdateEvent::Type(), [this](Event& event) {
    if (event.as<KeyUpdateEvent>()->state) {
      auto it = registeredCameras.find(event.as<KeyUpdateEvent>()->key);
      if (it != registeredCameras.end()) {
        CameraManager::SetCurrentCamera(it->second);
      }
    }
  });
}

void
CameraSwitcher::registerCamera(const Inputs::Key key, Camera* camera)
{
  this->registeredCameras[key] = camera;
}

bool
CameraSwitcher::isCurrentCamera(const Inputs::Key key)
{
  auto it = registeredCameras.find(key);
  if (it != registeredCameras.end()) {
    return it->second == CameraManager::GetCurrentCamera();
  }
  return false;
}

void
CameraSwitcher::displayCameraList()
{
#ifdef SOLEIL__USE_IMGUI

  ImGui::Begin("Camera List");
  {
    bool isFreeLook = this->isCurrentCamera(SOLEIL__KEY_F1);
    ImGui::Selectable("F1\t-\tFree look Camera", isFreeLook);

    bool isEditor = this->isCurrentCamera(SOLEIL__KEY_F2);
    ImGui::Selectable("F2\t-\tEditor Camera", isEditor);
  }
  ImGui::End();
#endif
}
