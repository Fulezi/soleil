/*
 * Copyright (C) 2017  Florian GOLESTIN
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef SOLEIL__MESH_H_
#define SOLEIL__MESH_H_

#include <vector>

#include <glm/matrix.hpp>
#include <glm/vec2.hpp>
#include <glm/vec3.hpp>

#include "Frame.h"
#include "OpenGLInclude.h"
#include "Program.hpp"

namespace Soleil {

  struct Material
  {
    glm::vec3 emissive;
    glm::vec3 ambiant; // TODO: ambiEnt
    glm::vec3 diffuse;
    glm::vec3 specular;
    float     shininess;
    float     opacity;
    GLuint    diffuseTexture;

    Material()
      : emissive(0.0f)
      , ambiant(0.2f)
      , diffuse(0.8f)
      , specular(1.0f)
      , shininess(1.0f)
      , opacity(1.0f)
      , diffuseTexture(0)
    {
    }
  };

  struct Vertex
  {
    glm::vec4 position;
    glm::vec3 normal;
    glm::vec2 textureCoords;
  };

  struct LightShader
  {
    GLint isLocal;
    GLint isSpot;
    GLint ambiant;
    GLint color;
    GLint position;
    GLint halfVector;
    GLint spotConeDirection;
    GLint spotCosCutOff;
    GLint spotExponent;
    GLint constantAttenuation;
    GLint linearAttenuation;
    GLint quadraticAttenuation;
  };

  struct Light
  {
    bool      isLocal; // True for a point or spot light
    bool      isSpot;
    glm::vec3 ambiant;
    glm::vec3 color;
    glm::vec3
      position; // Location of the light if isLocal is true, otherwise the
                // direction towards the light
    glm::vec3 spotConeDirection; // Spotlight attributes
    float     spotCosCutOff;
    float     spotExponent;
    float     constantAttenuation; // Local light attributes
    float     linearAttenuation;
    float     quadraticAttenuation;
  };

  static constexpr int MaxLight = 10;

  struct MeshShader
  {
    GLint ModelViewProjection;
    GLint ModelView;
    GLint NormalMatrix;
    GLint EyeDirection;

    GLint materialEmissive;
    GLint materialAmbiant;
    GLint materialDiffuse;
    GLint materialSpecular;
    GLint materialShininess;
    GLint materialOpacity;
    GLint materialDiffuseTexture;

    GLint       numberOfLight;
    LightShader lights[MaxLight];

    GLint shadowMap;
    GLint LightSpace;
    GLint Model;

    GLuint program;

    void initialize(const Program& p);
  };

  void setupLightUniform(const Light& light, const MeshShader& shaderConfig,
                         const glm::vec3& eyeDirection, unsigned int pos);

  enum class TextureTpe
  {
    Diffuse,
    Specular
  };

  struct Texture
  {
    GLuint      id;
    GLuint      uniform;
    std::string type;
  };

  class Mesh
  {
  public:
    Mesh(std::vector<Vertex> vertices, std::vector<GLuint> indices,
         Material material);

    void draw(Frame& frame, const MeshShader& config,
              const glm::mat4& transformation);

  public:
    GLsizei  numIndices;
    Material material;

  public:
    GLuint vertexArray;
    GLuint vertexBuffer;
    GLuint elementBuffer;
  };

} // Soleil

#endif /* SOLEIL__MESH_H_ */
