/*
 * Copyright (C) 2017  Florian GOLESTIN
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#pragma once

#include <cassert>
#include <string>

namespace Soleil {

  class Logger
  {
  public:
    Logger();
    virtual ~Logger();

  public:
    enum Level
    {
      Debug,
      Info,
      Warning,
      Error
    };
    void log(const Level level, const std::string& message) noexcept;

  public:
    static void Log(const Level level, const std::string& message) noexcept;

    static void debug(const std::string& message) noexcept;
    static void info(const std::string& message) noexcept;
    static void warning(const std::string& message) noexcept;
    static void error(const std::string& message) noexcept;
  };
} // namespace Soleil

#ifdef NDEBUG
#define SOLEIL__LOGGER_DEBUG(...) ((void)0)
#else
#include "stringutils.h"
#include <csignal>
#define SOLEIL__LOGGER_DEBUG(...)                                              \
  ::Soleil::Logger::debug(::Soleil::toString(__VA_ARGS__))
#define SOLEIL__ASSERT_BREAK(cond)                                             \
  if (!(cond)) {                                                               \
    std::cerr << #cond << "\n";                                                \
    std::raise(SIGINT);                                                        \
  }
#endif // NDEBUG

#if !defined(NDEBUG) && defined(SOLEIL__DO_TIMER)
#define SOLEIL__TIMER_START(name) auto name = std::chrono::steady_clock::now()
#define SOLEIL__TIMER_COMPUTE(name)                                            \
  {                                                                            \
    auto end = std::chrono::steady_clock::now();                               \
    SOLEIL__LOGGER_DEBUG(                                                      \
      #name ": ",                                                              \
      std::chrono::duration_cast<std::chrono::milliseconds>(end - name)        \
        .count(),                                                              \
      "ms");                                                                   \
  }
#else
#define SOLEIL__TIMER_START(name) ((void)0)
#define SOLEIL__TIMER_COMPUTE(name) ((void)0)
#endif
