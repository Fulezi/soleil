/*
 * Copyright (C) 2018  Florian GOLESTIN
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#pragma once

#include <Font.h>
#include <map>
#include <string>

#include "OpenGLInclude.h"

namespace Soleil {

  // TODO: License
  // TODO: Rename as AssetManager
  // TODO: Destructor to free resources
  // TODO: Auto (hot) reload resources? Using signal

  /*
   * Management of asset can be done in different manner. From loading
   * everything and refreshing every changes to load only the resource when
   * required. We can also reference them to not free them until all owners
   * released it.
   */
  class Assets
  {
  public:
    std::string                   pathPrefix;
    std::map<std::string, GLuint> textures;

    //using FontKey = std::pair<std::string, float>;
    using FontKey = std::string;
    using FontPtr = std::unique_ptr<Font>;
    std::map<FontKey, FontPtr> fonts;

  public:
    static void        Initialize(const std::string& prefixPath);
    static void        Destroy();
    static std::string AsString(const std::string& resourceName);
    static std::string GetPath(const std::string& resourceName);

    /**
     * Return the GL texture, creating it if necessary.
     */
    static GLuint GetTexture(const std::string& resourceName);

    /**
     * Load a TrueType Font (pointer is held by the AssetManager).
     */
    static Font* LoadFont(const std::string& resourceName);
  };

} // namespace Soleil
