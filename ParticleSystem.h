/*
 * Copyright (C) 2018  Florian GOLESTIN
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include "Particle.h"
#include "Program.hpp"
#include "ogl/Buffer.h"
#include "ogl/VertexArray.h"
#include <functional>
#include <physics/PhysicsWorld.h>
#include <vector>

namespace Soleil {

  struct Frame;

  class ParticleSystem
  {
  public:
    ParticleSystem(const std::string& textureName,
                   std::size_t        maximumParticles);

    // public:
    //   void setEmitter();

  public:
    /**
     * Returns the first free particle. Or the first one if none free are found
     */
    Particle& emitParticle();

    /**
     * Decrease the life of the particles, Update the GL Buffers
     */
    void update(const float deltaTime);

    void draw(Frame& frame);

  private:
    std::size_t                 maximumParticles;
    std::vector<Particle>       particles;
    std::vector<SpriteParticle> sprites;
    std::size_t                 spriteCount;

  public:
    std::function<void(PhysicsWorld::RayCastResult&, Particle&)>
      collisionSignal;

  private:
    std::size_t firstFreeParticle;

  private: // Draw Resource
    Buffer      particleBuffer;
    VertexArray vao;
    Program     program;
    GLuint      texture;

    ////////////////////////////////////////////////////////////////////////////
    // Statics
  public:
    static void Init();
    static void Destroy();

    /**
     * Create a particle system that will be auto-managed: Creation - Update -
     * Draw - Destruction.
     */
    static ParticleSystem* CreateParticleSystem(const std::string& textureName,
                                                std::size_t maximumParticles);

    /**
     * Update the buffers and draw all the managed Particle systemss
     */
    static void DrawParticleSystems(Frame& frame, const float deltaTime);
  };

} // namespace Soleil
