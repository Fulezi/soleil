/*
 * Copyright (C) 2017  Florian GOLESTIN
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#pragma once

#include "BoundingBox.h"
#include "Draw.h"
#include "Mesh.h"
#include "Model.h"
#include "Node.h"
#include "stringutils.h"
#include <map>
#include <physics/Intersections.h>
#include <vector>

// TODO: Replace hash with an UID with collision checker
namespace Soleil {

  struct RayCastCollision
  {
    float       distance;
    Node*       target;
    glm::mat4   transformation;
    BoundingBox bounds;

    RayCastCollision(float distance, Node* target, glm::mat4 transformation,
                     BoundingBox bounds)
      : distance(distance)
      , target(target)
      , transformation(transformation)
      , bounds(bounds)
    {}
  };

  struct RayCastResult
  {
    std::size_t size() { return list.size(); }

    std::vector<RayCastCollision> list;
  };

  // TODO: Copy all static into non static functions
  class SceneManager
  {
  public:
    template <typename Key, typename Object>
    using Map = std::map<Key, Object>; // TODO: Use my custom map
    // using ModelHandle = std::weak_ptr<Model>;

  public:
    SceneManager()
      : root(0, 0)
    {
      SOLEIL__LOGGER_DEBUG("Creating a SceneManager");
    }
    ~SceneManager() {}

  public:
    /* Holders for Meshes and Models that are loaded for the scene */
    std::vector<Mesh>                   meshes;
    std::vector<std::unique_ptr<Model>> models;
    std::map<ObjectID, Model*>          modelsMap;

    /* Contains all the commands to be draw */
    DrawList drawList;

  public:
    /** Scene Root node of the scene. Every nodes that exists in the Scene Graph
     * will be children of this root node. */
    Node root;

    /** Computed transformation expressed in (absolute) world space. Only valid
     * after a call to `computeScene()`. The index of this array is stored in
     * the `NodeCursor::vectorIndex` and may be given by the `GetNode(ObjectID)`
     * function.*/
    std::vector<glm::mat4> transformations;

    /** Computed BoundingBox expressed in (absolute) world space. Only valid
     * after a call to `computeScene()`. The index of this array is stored in
     * the `NodeCursor::vectorIndex` and may be given by the `GetNode(ObjectID)`
     * function.*/
    std::vector<BoundingBox> bounds;

    /**
     * Hold and map an ObjectID to the index to the vectors lists
     * (transformations, bounds) and the Node data structure. An instance is
     * valid only after the call of the computeScene method and until the next
     * computeScene method call.
     */
    struct NodeCursor
    {
      std::size_t vectorIndex;
      Node*       nodePtr;

      NodeCursor(std::size_t vectorIndex, Node* nodePtr)
        : vectorIndex(vectorIndex)
        , nodePtr(nodePtr)
      {}
    };
    std::map<ObjectID, NodeCursor> nodes;

    // TODO:
    // Browse with callback?

    /**
     * Node type that can be defined per scene (or per game). @see `void
     * SetCustomNodeType()`
     */
    typedef ObjectID            CustomNodeType;
    std::vector<CustomNodeType> customNodeTypes;

  public:
    /**
     * Traverse the Root Node (SceneGraph::root) an all its children recursively
     * to compute the scene transformations, bounds, ...
     */
    void computeScene();

    /**
     * Import a Scene as a YAML file, load the models and push them to the scene
     * to be displayed if required.
     */
    void importScene(const std::string& assetName);

    /**
     * Push the `Mesh` to be draw in the next frame
     */
    void pushDrawItem(const glm::mat4 transformation, const ObjectID meshID);

    /**
     * Load a model from the resourceName and store it into the SceneManager
     * database. So far, only wavefront (.obj) are supported; GlTF is planned to
     * be included.
     * @return The index to be used in SceneManager::getModel(ObjectID)
     */
    uint32_t loadModel(const std::string& resourceName);

    /// Push the current node to scene to be displayed on the next frame.
    /// This is an immediate function (will be cleared the next frame)
    void pushToScene(Node& root, const glm::mat4& rootMatrix = glm::mat4(1.0f));

    /// Remove the node and all attached children from the scene. It cannot be
    /// the root node.
    void removeNode(const ObjectID id);

    /**
     * Register a new custom NodeType. It can be used to differentiate node that
     * come from the Game-Play (e.g. PlayerNodeType, EnemyNodeType). It must be
     * an index superior to NodeTypeSize and unused so far.
     *
     * @returns true if operation succeeded, false if the new ID was illegal
     * (also print an error message).
     */
    bool setCustomNodeType(CustomNodeType newNodeType);

  public:
    Model* getModel(ObjectID id);

  public:
    /**
     * Ray Cast using nodes bounding boxes. Useful for debug an scene object
     * selection
     */
    RayCastResult rayCast(const glm::vec3& startPoint,
                          const glm::vec3& endPoint);

  public:
    static void     Init();
    static void     Destroy();
    static ObjectID SetMesh(Mesh mesh);
    static Mesh*    GetMesh(ObjectID id);

  public:
    // Load Save Models, Scene, ...

    /**
     * @copydoc SceneManager::computeScene()
     */
    static void ImportScene(const std::string& assetName);

    /**
     * @copydoc SceneManager::loadModel(const std::string&)
     */
    static uint32_t LoadModel(const std::string& resourceName);
    // Compute world transformation, bounding boxes, ...
    static Node& GetRootScene();

    /**
     * @return the node associated to this id or nullptr if it was not found.
     */
    static Node* GetNode(const ObjectID id);

    static void RemoveNode(const ObjectID id);

    /**
     * @return The index of the Object (node) in the vectors lists
     * (transformation, bounds, ...)
     */
    static std::size_t GetObject(const ObjectID id);

    /**
     * Returns the World Transformation of a Node. Only valid after a
     * `computeScene()`.
     *
     * // TODO: To remove because Same as GetNodeWorldTransformation
     */
    static const glm::mat4& GetTransformation(std::size_t index) noexcept;

    /**
     * Set the World Transformation of a Node in `transformation`. Only valid
     * after a `computeScene()`.
     * @return true if the node was found, false otherwise.
     *
     * // TODO: Rename GetNodeWorldTransformation
     * // TODO: A non static version
     */
    static bool GetObjectTransformation(const ObjectID id,
                                        glm::mat4&     transformation);

    /**
     * Set the World BoundingBox of a Node in `bbox`. Only valid after a
     * `computeScene()`.
     * @return true of the node was found, false otherwise.
     *
     * // TODO: Rename GetNodeWorldBoundingBox
     * // TODO: A non static version
     */
    static bool GetObjectBoundingBox(const ObjectID id, BoundingBox& bbox);

    /**
     * @copydoc SceneManager::setCustomNodeType(CustomNodeType)
     */
    static bool SetCustomNodeType(CustomNodeType newNodeType);

    /**
     * @copydoc SceneManager::computeScene()
     */
    static void ComputeScene();

    /**
     * @copydoc SceneManager::pushDrawItem(const glm::mat4,const ObjectID);
     */
    static void PushDrawItem(const glm::mat4 tranformation,
                             const ObjectID  meshID);

    /**
     * Render all the pushed Meshes.
     *
     * // TODO: A non static version
     */
    static void RenderDrawList(const MeshShader&         config,
                               const std::vector<Light>& lights, Frame& frame);

    /**
     * Return the currently computed draw list
     *
     * // TODO: A non static version
     */
    static DrawList& GetDrawList();
    

    /**
     * @copydoc rayCast(const glm::vec3&,const glm::vec3&)
     */
    static RayCastResult RayCast(const glm::vec3& startPoint,
                                 const glm::vec3& endPoint);

  public: // ------ Collisions: ------
  };

} // namespace Soleil
