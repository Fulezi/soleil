/*
 * Copyright (C) 2017  Florian GOLESTIN
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#pragma once

#include "BoundingBox.h"
#include "stringutils.h"
#include <glm/matrix.hpp>
#include <memory>
#include <vector>

namespace Soleil {

  typedef std::size_t ObjectID;

  enum NodeType
  {
    NodeTypeModel,
    NodeTypeMesh,
    NodeTypeLight,
    NodeTypeGroup,
    NodeTypeRigidBody,
    NodeTypeBoundingBox,
    NodeTypeBoundingSphere,
    NodeTypeKinematicBody,

    NodeTypeSize
  };

  struct BoundingVolume
  {
    std::vector<BoundingVolume> childen;

    /*virtual*/ bool RayIntersect(const glm::vec3& startPoint,
                                  const glm::vec3& endPoint,
                                  float*           distance = nullptr,
                                  glm::vec3*       normal   = nullptr);
    // TODO: Tkae into consideration that we want GJK
    BoundingBox volume; // TODO: May be virtualized
  };

  typedef unsigned int NodeMask;

  // Node (or root) of the SceneGraph, represent all local informations
  class Node
  {
  public:
    Node(ObjectID object, ObjectID type, const std::string& name = "",
         glm::mat4 transformation = glm::mat4(1.0f), NodeMask mask = 0xffffffff)
      : object(object)
      , type(type)
      , name(name)
      , id((name.size() > 0) ? Hash(name) : 0)
      , transformation(transformation)
      , mask(mask)
    {}

    void updateName(const std::string& name)
    {
      this->name = name;
      this->id   = (name.size() > 0) ? Hash(name) : 0;
    }

  public:
    ObjectID          object;         // Link to underlying object
    ObjectID          type;           // Type of the node (Mesh, Sound, ...)
    std::string       name;           // Name (might be empty or not unique)
    ObjectID          id;             // ID generated from the name or 0
    std::vector<Node> children;       // Each nodes is also a group
    glm::mat4         transformation; // Local transformation
    BoundingBox       bounds;         // Local bounds
    NodeMask          mask;

    // TODO: At some point it may be wise to replace transformation with
    // Position (vec3) Attitude (quaternion)
  };

} // namespace Soleil
