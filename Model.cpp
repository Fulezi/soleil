/*
 * Copyright (C) 2017  Florian GOLESTIN
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#define TINYOBJLOADER_IMPLEMENTATION
#include <assets/tiny_obj_loader.h>

#include <glm/gtc/type_ptr.hpp>

#include "Assets.h"
#include "Logger.h"
#include "Mesh.h"
#include "Model.h"
#include "Node.h"
#include "SceneManager.h"
#include <Errors.h>
#include <map>
#include <physics/Intersections.h>
#include <physics/PhysicsWorld.h>

template <class T>
inline void
hash_combine(std::size_t& seed, const T& v)
{
  std::hash<T> hasher;
  seed ^= hasher(v) + 0x9e3779b9 + (seed << 6) + (seed >> 2);
}

namespace tinyobj {
  bool operator<(const tinyobj::index_t a, const tinyobj::index_t b)
  {
    /* So far we need to use the hash because some std implementation requires a
     * strict comparison
     */
#if 0

    return a.vertex_index < b.vertex_index || a.normal_index < b.normal_index ||
           a.texcoord_index < b.texcoord_index;

#else
    /* Comparison made with hash */
    std::size_t ha = std::hash<int>{}(a.vertex_index);
    hash_combine(ha, a.texcoord_index);
    hash_combine(ha, a.normal_index);

    std::size_t hb = std::hash<int>{}(b.vertex_index);
    hash_combine(hb, b.texcoord_index);
    hash_combine(hb, b.normal_index);

    return ha < hb;
#endif
  }

} // namespace tinyobj

namespace Soleil {

  /**
   * Compute a Soleil::Vertex from a loaded .obj and its attribute
   */
  static Vertex VertexFromAttrib(tinyobj::attrib_t const& attrib,
                                 tinyobj::index_t const&  idx,
                                 const std::string&       modelName)
  {
    Vertex vertex;
    vertex.position.x = attrib.vertices[3 * idx.vertex_index + 0];
    vertex.position.y = attrib.vertices[3 * idx.vertex_index + 1];
    vertex.position.z = attrib.vertices[3 * idx.vertex_index + 2];

    if (idx.normal_index >= 0) {
      vertex.normal.x = attrib.normals[3 * idx.normal_index + 0];
      vertex.normal.y = attrib.normals[3 * idx.normal_index + 1];
      vertex.normal.z = attrib.normals[3 * idx.normal_index + 2];
    } else {
      SOLEIL__RUNTIME_ERROR(toString("Normal missing from model: ", modelName));

      vertex.normal = glm::vec3(0, 1, 0);
    }

    if (idx.texcoord_index >= 0) {
      vertex.textureCoords.x = attrib.texcoords[2 * idx.texcoord_index + 0];
      // Y coordinate is inversed compare to ours
      vertex.textureCoords.y =
        1.0f - attrib.texcoords[2 * idx.texcoord_index + 1];
    } else {
      vertex.textureCoords = glm::vec2(0.0f, 0.0f);
    }
    // Optional: vertex colors
    /* So far we may have only one color per mesh (through the material).
     * Not one per vertex
     */
    // vertex.color.r = attrib.colors[3 * idx.vertex_index + 0];
    // vertex.color.g = attrib.colors[3 * idx.vertex_index + 1];
    // vertex.color.b = attrib.colors[3 * idx.vertex_index + 2];

    return vertex;
  }

  /**
   * Precise way to compare two float as defined in the book Real-Time Collision
   * Detection by Christer Ericson.
   * @returns true if the float are equal (or
   * really close). false otherwise.
   */
  static bool FloatCompare(const float x, const float y)
  {
    return fabsf((x) - (y)) <=
           FLT_EPSILON * fmaxf(1.0f, fmaxf(fabsf(x), fabsf(y)));
  }

  /**
   * Compare two vertex in a really flat way (hopefully optimized).
   *
   * @returns true if there are equal.
   */
  static bool VertexCompare(Vertex const& vertex, Vertex const& other)
  {
    if (FloatCompare(vertex.position.x, other.position.x) == false) {
      return false;
    }
    if (FloatCompare(vertex.position.y, other.position.y) == false) {
      return false;
    }
    if (FloatCompare(vertex.position.z, other.position.z) == false) {
      return false;
    }
    if (FloatCompare(vertex.position.w, other.position.w) == false) {
      return false;
    }
    if (FloatCompare(vertex.normal.x, other.normal.x) == false) {
      return false;
    }
    if (FloatCompare(vertex.normal.y, other.normal.y) == false) {
      return false;
    }
    if (FloatCompare(vertex.normal.z, other.normal.z) == false) {
      return false;
    }
    if (FloatCompare(vertex.textureCoords.x, other.textureCoords.x) == false) {
      return false;
    }
    if (FloatCompare(vertex.textureCoords.y, other.textureCoords.y) == false) {
      return false;
    }
    //
    //
    //
    return true;
  }

  /**
   * Convert a Mesh (vector of vertices and vector of indices) into a Physics
   * Polygon Body.
   */
  Polygon LoadPhysicsFromMesh(std::vector<Vertex> const&       vertices,
                              std::vector<unsigned int> const& indices)
  {
    constexpr unsigned int VertPerFace = 3;
    Polygon                physicsPoly;

    const std::size_t numberOfFaces = indices.size() / VertPerFace;
    assert((indices.size() % VertPerFace == 0) &&
           "Mesh does not conform to the number of vertices per face");

    physicsPoly.triangles.resize(numberOfFaces);
    for (std::size_t it = 0; it < numberOfFaces; ++it) {
      Triangle& tri = physicsPoly.triangles[it];
      for (std::size_t vert = 0; vert < VertPerFace; ++vert) {
        /* Index in the indices vector */
        const std::size_t vertexIndex = it * VertPerFace + vert;
        assert(indices.size() > vertexIndex);

        /* Index in the vertices vector */
        const std::size_t vertexCursor = indices[vertexIndex];
        assert(vertices.size() > vertexCursor);

        tri.vert[vert] = glm::vec3(vertices[vertexCursor].position);
      }
    }
    return physicsPoly;
  }

  Model::Model(const std::string& resourceName, const bool loadPhysics)
    : root(0, NodeTypeModel)
  {

    tinyobj::attrib_t                attrib;
    std::vector<tinyobj::shape_t>    shapes;
    std::vector<tinyobj::material_t> materials;

    std::string       warn;
    std::string       err;
    const std::string fullpath = Assets::GetPath(resourceName);
    const bool ret = tinyobj::LoadObj(&attrib, &shapes, &materials, &warn, &err,
                                      fullpath.c_str(),
                                      Assets::GetPath("").c_str(), true, true);

    if (err.empty() == false || ret == false) {
      throw std::runtime_error(
        toString("Failed to load Model: '", fullpath, "' -> ", err));
    }

    /* Err may contains warning message while the LoadObj was considered success
     */
    if (err.empty() == false) { Logger::error(err); }

    if (warn.empty() == false) { Logger::warning(warn); }

    /* So far we our meshes can handle ony one material per mesh. Hence, one
     * blender mesh with multiple materials will be converted into one `Model`
     * with multiple `Mesh`'es. That's why we have to do the conversion from
     * "Material per faces" to "Store the face per materials then create one
     * Mesh per material".
     */
    struct MeshBuilder
    {
      std::vector<Vertex>       vertices;
      std::vector<unsigned int> indices;
      std::vector<Texture>      textures;
      BoundingBox               bounds;
      std::string               name;
      Material                  material;
      Polygon                   physicsBody;

      /* A map to bind tinyobj::index_t to mesh index already stored.
       */
      std::map<tinyobj::index_t, std::size_t> storedVertices;

      bool materialSet = false;
    };

    /* Store the Mesh build in progress while we are looping over all the faces.
     * The index corresponds to the material one.
     */
    std::map<int, MeshBuilder> meshes;

    // Loop over shapes
    for (size_t s = 0; s < shapes.size(); s++) {
      // Loop over faces(polygon)
      size_t index_offset = 0;
      for (size_t f = 0; f < shapes[s].mesh.num_face_vertices.size(); f++) {
        const size_t fv = shapes[s].mesh.num_face_vertices[f];
        assert(fv == 3 && "Only triangulated faces are supported");

        // Loop over vertices in the face.
        for (size_t v = 0; v < fv; v++) {
          const tinyobj::index_t idx = shapes[s].mesh.indices[index_offset + v];
          const int              shapeID = shapes[s].mesh.material_ids[f];

          auto alreadyPresentVertex = meshes[shapeID].storedVertices.find(idx);
          if (alreadyPresentVertex == meshes[shapeID].storedVertices.end()) {

            Vertex vertex = VertexFromAttrib(attrib, idx, shapes[s].name);

            /* Before adding our vertex we want to check if we don't have a
             * similar one already present in our vertices list (and reuse its
             * index). Because tinyobj have one index per position, normal and
             * uv (which is wavefront spec) but we have one index per couple of
             * position&normal&uv, we need to search, each time, if a similar
             * `Vertex` isn't already present.
             */

            meshes[shapeID].vertices.push_back(vertex);
            const std::size_t vertexIndex = meshes[shapeID].vertices.size() - 1;
            meshes[shapeID].indices.push_back(vertexIndex);
            meshes[shapeID].storedVertices[idx] = vertexIndex;

          } else {
            meshes[shapeID].indices.push_back(alreadyPresentVertex->second);
          }
        }
        index_offset += fv;

        // per-face material
        auto Float3ToVec3 = [](tinyobj::real_t const v[3]) {
          return glm::vec3(v[0], v[1], v[2]);
        };

        /* We always have, at least (but not more), one material per shape */
        assert(int(materials.size()) > shapes[s].mesh.material_ids[f]);

        const int shapeID = shapes[s].mesh.material_ids[f];

        /* Set this to false to have a full greyscale scene */
        constexpr bool LoadMaterials = true;

        if (meshes[shapeID].materialSet == false && LoadMaterials) {
          Material mat;

          if (shapes[s].mesh.material_ids[f] >= 0) {
            const tinyobj::material_t& objMaterial =
              materials[shapes[s].mesh.material_ids[f]];

            /* We process the material only once the first time */
            meshes[shapeID].materialSet = true;

            mat.emissive  = Float3ToVec3(objMaterial.emission);
            mat.ambiant   = Float3ToVec3(objMaterial.ambient);
            mat.diffuse   = Float3ToVec3(objMaterial.diffuse);
            mat.specular  = Float3ToVec3(objMaterial.specular);
            mat.shininess = objMaterial.shininess;
            mat.opacity   = objMaterial.dissolve;

            if (objMaterial.diffuse_texname.empty() == false) {
              mat.diffuseTexture =
                Assets::GetTexture(objMaterial.diffuse_texname);

              Texture texture;
              texture.id   = mat.diffuseTexture;
              texture.type = "texture_diffuse";
              meshes[shapeID].textures.push_back(texture);
              assert(meshes[shapeID].textures.size() < 2 &&
                     "Only one texture is supported");
            }

            meshes[shapeID].material = mat;
            meshes[shapeID].name     = shapes[s].name + "::" + objMaterial.name;
          } else {
            SOLEIL__RUNTIME_ERROR(
              toString("Missing material on ", shapes[s].name));
          }
        }
      }

      // End faces -> Next shape
    }

    /* We grabbed all the vertices, finally build our mesh list */
    for (auto const& it : meshes) {
      const MeshBuilder& m = it.second;

      if (loadPhysics) {
        const ObjectID h = Hash(m.name);
        PhysicsWorld::AddRigidBody(h,
                                   PhysicsWorld::RigidBody::Type::StaticBody);
        PhysicsWorld::AddShape(LoadPhysicsFromMesh(m.vertices, m.indices), h);
      }

      const ObjectID meshID =
        SceneManager::SetMesh(Mesh(m.vertices, m.indices, m.material));
      Node meshNode(meshID, NodeTypeMesh, m.name, glm::mat4(1.0f));
      // TODO: meshNode.bounds from the vertex list.
      this->root.children.push_back(meshNode);
      // TODO: this->root.bounds = expand by meshNode.bounds

      SOLEIL__LOGGER_DEBUG("Loaded new Object: ", m.name);
    }

    SOLEIL__LOGGER_DEBUG(resourceName, " Model loaded");
  }

  std::vector<float> Model::LoadModel(const std::string& resourceName)
  {
    tinyobj::attrib_t                attrib;
    std::vector<tinyobj::shape_t>    shapes;
    std::vector<tinyobj::material_t> materials;

    std::string       warn;
    std::string       err;
    const std::string fullpath = Assets::GetPath(resourceName);
    const bool ret = tinyobj::LoadObj(&attrib, &shapes, &materials, &warn, &err,
                                      fullpath.c_str(),
                                      Assets::GetPath("").c_str(), true, true);

    if (err.empty() == false || ret == false) {
      throw std::runtime_error(
        toString("Failed to load Model: '", fullpath, "' -> ", err));
    }

    if (warn.empty() == false) { Logger::warning(warn); }

    std::vector<float> allVertices;
    allVertices.reserve(1024);
    // Loop over shapes
    for (size_t s = 0; s < shapes.size(); s++) {

      // Loop over faces(polygon)
      size_t index_offset = 0;
      for (size_t f = 0; f < shapes[s].mesh.num_face_vertices.size(); f++) {
        const size_t fv = shapes[s].mesh.num_face_vertices[f];
        assert(fv == 3 && "Only triangulated faces are supported");

        // Loop over vertices in the face.
        for (size_t v = 0; v < fv; v++) {
          // access to vertex
          tinyobj::index_t idx = shapes[s].mesh.indices[index_offset + v];
          allVertices.push_back(attrib.vertices[3 * idx.vertex_index + 0]);
          allVertices.push_back(attrib.vertices[3 * idx.vertex_index + 1]);
          allVertices.push_back(attrib.vertices[3 * idx.vertex_index + 2]);
        }

        index_offset += fv;
      }
    }

    allVertices.shrink_to_fit();
    return allVertices;
  }

} // namespace Soleil
