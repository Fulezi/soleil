/*
 * Copyright (C) 2017  Florian GOLESTIN
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include "DebugManager.h"

#include "Frame.h"
#include "Logger.h"
#include "DebugUI.h"
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/constants.hpp>

// TODO: For RayCastTool
#include "SceneManager.h"

namespace Soleil {

  constexpr int MaximumPrimitives = 128;

  // -----------------------------------------------------------------------------
  // TODO: In a separate file
  static void RayCastTool(const double /*deltaTime*/)
  {
#if SOLEIL__USE_IMGUI
    if (ImGui::Begin("RayCast Tool")) {
      static glm::vec3     orig(0);
      static glm::vec3     end(0);
      static RayCastResult rayCastResult;
      bool                 updateRequired = false;

      updateRequired |= ImGui::InputFloat3("Origine:", glm::value_ptr(orig));
      updateRequired |= ImGui::InputFloat3("End:", glm::value_ptr(end));
      if (updateRequired) {
        rayCastResult = SceneManager::RayCast(orig, end);
        // DebugManager::DrawLine(orig, end, glm::vec4(1, 0, 0, 1), 5.0f);
      }
      DebugManager::DrawLine(orig, end, glm::vec4(1, 0, 0, 1), 5.0f);
      ImGui::Separator();
      for (const auto& result : rayCastResult.list) {
        const char* name =
          (result.target) ? result.target->name.c_str() : "<empty>";
        ImGui::Bullet();
        ImGui::SameLine();
        ImGui::Text("%s", name);
        DebugManager::DrawBoundingBox(result.bounds, result.transformation,
                                      glm::vec4(1.0f));
      }
    }
    ImGui::End();
#endif // SOLEIL__USE_IMGUI
  }
  // -----------------------------------------------------------------------------
  // static void GJK(const double /*deltaTime*/)
  // {
  //   if (ImGui::Begin("GJK")) {
  //     static const SceneManager::RigidBody* selection = nullptr;
  //     static BoundingBox                    SelectedBbox;
  //     for (const auto& rigidBound : SceneManager::GetRigidBodiesBounds()) {

  //       BoundingBox      bbox = rigidBound.first;
  //       const glm::mat4& transformation =
  //         SceneManager::GetTransformation(rigidBound.second);
  //       bbox.transform(transformation);
  //       // ImGui::Text("BoundingBox [%f, %f, %f] -> [%f, %f, %f]",
  //       // bbox.getMin().x,
  //       //             bbox.getMin().y, bbox.getMin().z, bbox.getMax().x,
  //       //             bbox.getMax().y, bbox.getMax().z);
  //       const std::string label =
  //         toString("BoundingBox ", bbox.getMin(), "->", bbox.getMax());

  //       if (ImGui::Selectable(label.c_str(), selection == &rigidBound)) {
  //         selection = &rigidBound;

  //         // Compute selected bounding box
  //         SelectedBbox = bbox;
  //       }

  //       DebugManager::DrawBoundingBox(bbox, glm::mat4(1.0f),
  //       glm::vec4(1.0f));
  //     }

  //     if (selection) {
  //       static glm::vec3 end(0.0f);
  //       static glm::vec4 color(0.0f, 0.0f, 1.0f, 1.0f);
  //       bool             updateRequired = false;
  //       // updateRequired |= ImGui::InputFloat3("Origine:",
  //       // glm::value_ptr(orig));
  //       updateRequired |= ImGui::InputFloat3("End:", glm::value_ptr(end));
  //       if (updateRequired) {
  //         // Process
  //         auto gjkResult = SceneManager::RayCastGJK(*selection, end);
  //         for (const auto& r : gjkResult.list) {
  //           // ...
  //         }
  //         if (gjkResult.size() > 0) {
  //           color = glm::vec4(1.0f, 0.0f, 0.0f, 1.0f);
  //         } else {
  //           color = glm::vec4(0.0f, 1.0f, 0.0f, 1.0f);
  //         }
  //       }

  //       {
  //         DebugManager::DrawLine(SelectedBbox.center(), end, color);
  //       }
  //     }
  //   }
  //   ImGui::End();
  // };
  // -----------------------------------------------------------------------------

  DebugManager::DebugManager()
    : frameCount(0)
    , accumulatedTime(0.0)
    , averageFPS(0.0)
    , computeTimePerFrame(0.0)
    , averageTimePerFrame(0.0)
  {
    // --- Instantiate Pimitive part ---
    primitivesVao.bind();

    // Load the Program:
    primitiveProgram.attachShader(GL_VERTEX_SHADER, "primitive.vert");
    primitiveProgram.attachShader(GL_FRAGMENT_SHADER, "primitive.frag");
    primitiveProgram.compile();

    // Load the buffer
    primitiveBuffer.bind(GL_ARRAY_BUFFER);
    {
      glBufferData(GL_ARRAY_BUFFER, sizeof(Primitive) * MaximumPrimitives,
                   nullptr, GL_STREAM_DRAW);

      // Position
      glEnableVertexAttribArray(0);
      glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Primitive),
                            nullptr);

      // Color
      glEnableVertexAttribArray(1);
      glVertexAttribPointer(
        1, 4, GL_FLOAT, GL_FALSE, sizeof(Primitive),
        reinterpret_cast<GLvoid*>(offsetof(Primitive, color)));
      throwOnGlError();
    }

    // Register default tools:
    registerTool("RayCast Tool", RayCastTool);
    // registerTool("GJK", GJK);

    SOLEIL__LOGGER_DEBUG("DebugManager Init done.");
  }

  void DebugManager::newFrame(const double deltaTime)
  {
    constexpr double OneSecond = 1.0;

    frameCount++;
    accumulatedTime += deltaTime;

    if (accumulatedTime > OneSecond) {
      averageFPS          = frameCount;
      averageTimePerFrame = accumulatedTime / frameCount;

      accumulatedTime = 0.0;
      frameCount      = 0.0;
    }

    // --- Clean Primitives ---
    primitives.clear();
    primitiveSet.clear();

    primitives.reserve(1000);
    primitiveSet.reserve(1000); // TODO: Do clear shrink the size?
  }

  void DebugManager::displayMetrics()
  {
#if SOLEIL__USE_IMGUI
    if (ImGui::Begin("Metrics")) {
      ImGui::Text("Average time per frame: %f (%f FPS)", averageTimePerFrame,
                  averageFPS);
    }
    ImGui::End();
#endif //SOLEIL__USE_IMGUI
  }

  void DebugManager::registerTool(const std::string& toolName, UITool tool,
                                  bool enabled)
  {
    tools.emplace(toolName, std::make_pair(enabled, tool));
  }

  void DebugManager::displayTools(const double deltaTime)
  {
    for (auto& tool : tools) {
      if (tool.second.first) {
        tool.second.second(deltaTime);
      }
    }
  }

  void DebugManager::setToolEnabled(const std::string& toolName, bool enabled)
  {
    // TODO: clean log

    SOLEIL__LOGGER_DEBUG("Enabing... ", toolName);
    auto it = tools.find(toolName);
    if (it != tools.end()) {
      it->second.first = enabled;
    } else {
      // SOLEIL__LOGGER_DEBUG("Bouhouhou not found:", toolName);
      // assert(false);
    }
  }

  void DebugManager::drawPoint(const glm::vec3& point, const glm::vec4& color)
  {
    PrimitiveSet pset;

    pset.start = primitives.size();
    pset.type  = GL_POINTS;
    pset.count = 1;
    pset.scale = 5.0f;

    primitiveSet.push_back(pset);

    primitives.emplace_back(point, color);
  }

  
  void DebugManager::drawLine(const glm::vec3& start, const glm::vec3& end,
                              const glm::vec4& color, const float lineWidth)
  {
    PrimitiveSet pset;

    // TODO: A way to delete primitives
    // TODO: SCale might be in the buffer
    pset.start = primitives.size();
    pset.type  = GL_LINES;
    pset.count = 2;
    pset.scale = lineWidth;

    primitiveSet.push_back(pset);

    primitives.emplace_back(start, color);
    primitives.emplace_back(end, color);
  }

  void DebugManager::drawTriangle(const glm::vec3& p0, const glm::vec3& p1,
                                  const glm::vec3& p2, const glm::vec4& color)
  {

    PrimitiveSet pset;

    pset.start = primitives.size();
    pset.type  = GL_TRIANGLES;
    pset.count = 3;
    pset.scale = 1.0f;

    primitiveSet.push_back(pset);

    primitives.emplace_back(p0, color);
    primitives.emplace_back(p1, color);
    primitives.emplace_back(p2, color);
  }

  glm::vec3 SphereParametricEquation(const float u, const float v,
                                     const float r)
  {
    using glm::cos;
    using glm::sin;
    return glm::vec3(cos(u) * sin(v) * r, cos(v) * r, sin(u) * sin(v) * r);
  }

  void DebugManager::drawSphere(const Sphere& s, const glm::vec4& color)
  {
    const glm::vec2 start(0.0f); //= s.center;
    const glm::vec2 end(glm::pi<float>() * 2.0f);
    const float     resolution = 16.0f;

    const glm::vec2 step = (end - start) / resolution;
    for (float i = 0; i < resolution; ++i) {
      for (float j = 0; j < resolution; ++j) {
        const float u = i * step.x + start.x;
        const float v = j * step.y + start.y;
        const float un =
          (i + 1 == resolution) ? end.x : (i + 1) * step.x + start.x;
        const float vn =
          (j + 1 == resolution) ? end.y : (j + 1) * step.y + start.y;

        const glm::vec3 p0 =
          s.center + SphereParametricEquation(u, v, s.radius);
        const glm::vec3 p1 =
          s.center + SphereParametricEquation(u, vn, s.radius);
        const glm::vec3 p2 =
          s.center + SphereParametricEquation(un, v, s.radius);
        const glm::vec3 p3 =
          s.center + SphereParametricEquation(un, vn, s.radius);

        drawTriangle(p0, p1, p2, color);
        drawTriangle(p3, p1, p2, color);
      }
    }
  }

  glm::vec3 GetAnyPerpendicularUnitVector(const glm::vec3& vec)
  {
    if (vec.y != 0.0f || vec.z != 0.0f)
      return glm::vec3(1, 0, 0);
    else
      return glm::vec3(0, 1, 0);
  }

  void DebugManager::drawCapsule(const Capsule& c, const glm::vec4& color)
  {
    const glm::vec3 axis   = c.end - c.start;
    const float     length = glm::length(axis);
    const glm::vec3 localZ = axis / length;
    const glm::vec3 localX = GetAnyPerpendicularUnitVector(localZ);
    const glm::vec3 localY = glm::cross(localZ, localX);

    using glm::cos;
    using glm::sin;
    //constexpr float pi = glm::pi<float>();
    constexpr float pi = 3.14159265358979323846264338327950288;

    const glm::vec3 start(0.0f);
    const glm::vec3 end(1.0f);
    const float     resolution = 16.0f;

    const glm::vec3 step = (end - start) / resolution;

    auto cylinder = [localX, localY, localZ, c, length, pi](const float u,
                                                        const float v) {
      return c.start                                  //
             + localX * cos(2.0f * pi * u) * c.radius //
             + localY * sin(2.0f * pi * u) * c.radius //
             + localZ * v * length;                   //

    };

    auto sphereStart = [localX, localY, localZ, c, pi](const float u,
                                                   const float v) -> glm::vec3 {
      const float latitude = (pi / 2.0f) * (v - 1);

      return c.start                                                  //
             + localX * cos(2.0f * pi * u) * cos(latitude) * c.radius //
             + localY * sin(2.0f * pi * u) * cos(latitude) * c.radius //
             + localZ * sin(latitude) * c.radius;
    };

    auto sphereEnd = [localX, localY, localZ, c, pi](const float u, const float v) {
      const float latitude = (pi / 2.0f) * v;
      return c.end                                                    //
             + localX * cos(2.0f * pi * u) * cos(latitude) * c.radius //
             + localY * sin(2.0f * pi * u) * cos(latitude) * c.radius //
             + localZ * sin(latitude) * c.radius;
    };

    for (float i = 0; i < resolution; ++i) {
      for (float j = 0; j < resolution; ++j) {
        const float u = i * step.x + start.x;
        const float v = j * step.y + start.y;

        const float un =
          (i + 1 == resolution) ? end.x : (i + 1) * step.x + start.x;
        const float vn =
          (j + 1 == resolution) ? end.y : (j + 1) * step.y + start.y;

	// Draw Cylinder
        {
          const glm::vec3 p0 = cylinder(u, v);
          const glm::vec3 p1 = cylinder(u, vn);
          const glm::vec3 p2 = cylinder(un, v);
          const glm::vec3 p3 = cylinder(un, vn);

          drawTriangle(p0, p1, p2, color);
          drawTriangle(p3, p1, p2, color);
        }

	// Draw Sphere start
        {
          const glm::vec3 p0       = sphereStart(u, v);
          const glm::vec3 p1       = sphereStart(u, vn);
          const glm::vec3 p2       = sphereStart(un, v);
          const glm::vec3 p3       = sphereStart(un, vn);
          drawTriangle(p0, p1, p2, color);
          drawTriangle(p3, p1, p2, color);
        }

	// Draw Sphere end
        {
          const glm::vec3 p0       = sphereEnd(u, v);
          const glm::vec3 p1       = sphereEnd(u, vn);
          const glm::vec3 p2       = sphereEnd(un, v);
          const glm::vec3 p3       = sphereEnd(un, vn);
          drawTriangle(p0, p1, p2, color);
          drawTriangle(p3, p1, p2, color);
        }
      }
    }
  }

  void DebugManager::drawBoundingBox(const BoundingBox& bbox,
                                     const glm::mat4&   transformation,
                                     const glm::vec4&   color)
  {

    // clang-format: off
    std::vector<glm::vec3> unitBox = {
      // Front:
      {-1.0f, -1.0f, 1.0f}, //
      {1.0f, -1.0f, 1.0f},  //
      {1.0f, 1.0f, 1.0f},   //
      {-1.0f, 1.0f, 1.0f},  //
      // Back
      {-1.0f, -1.0f, -1.0f}, //
      {1.0f, -1.0f, -1.0f},  //
      {1.0f, 1.0f, -1.0f},   //
      {-1.0f, 1.0f, -1.0f},  //
      // Left:
      {-1.0f, -1.0f, 1.0f},  //
      {-1.0f, 1.0f, 1.0f},   //
      {-1.0f, 1.0f, -1.0f},  //
      {-1.0f, -1.0f, -1.0f}, //
      // Right:
      {1.0f, -1.0f, 1.0f},  //
      {1.0f, 1.0f, 1.0f},   //
      {1.0f, 1.0f, -1.0f},  //
      {1.0f, -1.0f, -1.0f}, //

    };
    // clang-format: on

    PrimitiveSet pset;
    pset.start = primitives.size();
    pset.type  = GL_LINE_LOOP;
    pset.count = 4;
    pset.scale = 1.0; // TODO: box line scale

    primitiveSet.push_back(pset);

    PrimitiveSet pset2 = pset;
    pset2.start        = primitives.size() + 4;
    primitiveSet.push_back(pset2);

    pset.start = primitives.size() + 8;
    primitiveSet.push_back(pset);

    pset.start = primitives.size() + 12;
    primitiveSet.push_back(pset);

    int       i = 0;
    glm::vec4 c = color;
    for (const auto& f : unitBox) {
      glm::vec3 point(0.0f);

      point.x = (f.x < 0.0f) ? bbox.getMin().x : bbox.getMax().x;
      point.y = (f.y < 0.0f) ? bbox.getMin().y : bbox.getMax().y;
      point.z = (f.z < 0.0f) ? bbox.getMin().z : bbox.getMax().z;

      point = glm::vec3(transformation * glm::vec4(point, 1.0f));
      // primitives.emplace_back(point, color);

      if (i >= 4) c  = glm::vec4(1, 0, 0, 1);
      if (i >= 8) c  = glm::vec4(0, 1, 0, 1);
      if (i >= 12) c = glm::vec4(0, 0, 1, 1);
      primitives.emplace_back(point, c);
      i++;
    }
  }

  void DebugManager::displayPrimitives(Frame& frame)
  {
    glUseProgram(primitiveProgram.program);
    primitivesVao.bind();
    primitiveBuffer.bind(GL_ARRAY_BUFFER);
    {
      // TODO: Do not update the buffer if there were no changes
      glBufferData(GL_ARRAY_BUFFER, sizeof(primitives[0]) * primitives.size(),
                   primitives.data(), GL_STREAM_DRAW);

      glUniformMatrix4fv(primitiveProgram.getUniform("ViewProjection"), 1,
                         GL_FALSE, glm::value_ptr(frame.ViewProjection));

      for (const auto& pset : primitiveSet) {
        switch (pset.type) {
	case GL_LINE: glLineWidth(pset.scale + 10.0f); break;
	case GL_POINTS: glPointSize(pset.scale + 10.0f); break;
          default: break;
        }

	glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
        glDrawArrays(pset.type, pset.start, pset.count);
        // SOLEIL__LOGGER_DEBUG("Displayed: [", pset.start, ", ",
        //                      pset.start + pset.count, "]");
        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
      }
    }
  }

  // Statics --------------------------------------------------------------

  static std::unique_ptr<DebugManager> s_debugManager = nullptr;

  void DebugManager::Init()
  {
    s_debugManager = std::make_unique<DebugManager>();
  }

  void DebugManager::Destroy()
  {
    s_debugManager = nullptr;
  }

  void DebugManager::NewFrame(const double dt) { s_debugManager->newFrame(dt); }

  void DebugManager::DrawPoint(const glm::vec3& point, const glm::vec4& color)
  {
    s_debugManager->drawPoint(point, color);
  }
  
  void DebugManager::DrawLine(const glm::vec3& start, const glm::vec3& end,
                              const glm::vec4& color, const float lineWidth)
  {
    s_debugManager->drawLine(start, end, color, lineWidth);
  }

  void DebugManager::DrawTriangle(const glm::vec3& p0, const glm::vec3& p1,
                      const glm::vec3& p2, const glm::vec4& color)
  {
    s_debugManager->drawTriangle(p0, p1, p2, color);
  }

  
  void DebugManager::DrawBoundingBox(const BoundingBox& bbox,
                                     const glm::mat4&   transformation,
                                     const glm::vec4&   color)
  {
    s_debugManager->drawBoundingBox(bbox, transformation, color);
  }

  void DebugManager::DrawSphere(const Sphere& s, const glm::vec4& color)
  {
    s_debugManager->drawSphere(s, color);
  }

  void DebugManager::DrawCapsule(const Capsule& c, const glm::vec4& color)
  {
    s_debugManager->drawCapsule(c, color);
  }

  void DebugManager::DisplayPrimitives(Frame& frame)
  {
    s_debugManager->displayPrimitives(frame);
  }

  void DebugManager::SetToolEnabled(const std::string& toolName, bool enabled)
  {
    s_debugManager->setToolEnabled(toolName, enabled);
  }

  void DebugManager::RegisterTool(const std::string& toolName, UITool tool,
                                  bool enabled)
  {
    s_debugManager->registerTool(toolName, tool, enabled);
  }

  void DebugManager::DisplayTools(const double deltaTime)
  {
    s_debugManager->displayTools(deltaTime);
  }

  // TODO: TEMP
void Hahaha(const glm::vec3& p0, const glm::vec3& p2,
                             const glm::vec3& p3, const glm::vec4& color)
{
  s_debugManager->drawTriangle(p0, p2, p3, color);
}

  
} // Soleil
