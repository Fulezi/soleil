/*
 * Copyright (C) 2017  Florian GOLESTIN
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include "SceneManager.h"
#include "Assets.h"
#include <cassert>
#include <deque>
#include <glm/gtc/matrix_transform.hpp>
#include <memory>
#include <yaml-cpp/yaml.h>

#include "DebugManager.h"
#include "DebugUI.h"
#include "Draw.h"
#include "Utils.h"
#include <glm/gtc/type_ptr.hpp>
#include <physics/PhysicsWorld.h>

// TODO: There is a race condition where `GetNode(ObectID)` could return a
// `Node*` but if the scene graph is modified in another thread the data may
// become invalid.

// YAML Specifics
namespace YAML {
  template <> struct convert<glm::vec3>
  {
    static Node encode(const glm::vec3& rhs)
    {
      Node node;
      node.push_back(rhs.x);
      node.push_back(rhs.y);
      node.push_back(rhs.z);
      return node;
    }

    static bool decode(const Node& node, glm::vec3& rhs)
    {
      if (!node.IsSequence() || node.size() != 3) { return false; }

      rhs.x = node[0].as<double>();
      rhs.y = node[1].as<double>();
      rhs.z = node[2].as<double>();
      return true;
    }
  };
} // namespace YAML

namespace Soleil {

  //////////////
  // Statics  //
  //////////////

  static std::unique_ptr<SceneManager> instance;

  void SceneManager::Init() { instance = std::make_unique<SceneManager>(); }

  void SceneManager::Destroy() { instance = nullptr; }

  struct Slot
  {
    glm::mat4 transformation; // accumulated from parent
    // const Node* node;
    Node* node;
  };

  namespace yaml {
    static void debugNode(const YAML::Node& node)
    {
      switch (node.Type()) {
        case YAML::NodeType::Null: SOLEIL__LOGGER_DEBUG("Null"); break;
        case YAML::NodeType::Scalar: SOLEIL__LOGGER_DEBUG("Scalar"); break;
        case YAML::NodeType::Sequence: SOLEIL__LOGGER_DEBUG("Sequence"); break;
        case YAML::NodeType::Map: SOLEIL__LOGGER_DEBUG("Map"); break;
        case YAML::NodeType::Undefined:
          SOLEIL__LOGGER_DEBUG("Undefined");
          break;
        default: SOLEIL__LOGGER_DEBUG("???"); break;
      }
    }
  } // namespace yaml

  void SceneManager::importScene(const std::string& assetName)
  {
    YAML::Node root = YAML::Load(Assets::AsString(assetName));

    yaml::debugNode(root);
    assert(root.IsMap() && "I think we have to use a map...");

    std::map<std::string, ObjectID>    loadedModels;
    std::map<std::string, BoundingBox> loadedBounds;
    std::map<std::string, Node>        templates;
    // TODO: ^ At this end we should have only this template

    for (const auto& component : root) {
      yaml::debugNode(component.first);
      yaml::debugNode(component.second);
      const std::string& name = component.first.as<std::string>();

      if (name == "scene") {
        const YAML::Node& elements = component.second;

        assert(elements.IsSequence());
        for (const auto& element : elements) {
          // assert(element.IsMap()); // TODO: May not always be the case?

          const std::string& key = element.begin()->first.as<std::string>();
          Model*             modelRoot = getModel(loadedModels.at(key));
          // pushToScene(modelRoot->root);

          // TODO: To be replaced with `templates`
          this->root.children.push_back(modelRoot->root);
          Node& n = this->root.children.back();
          {
            auto it = templates.find(key);
            if (it != templates.end()) { n.children.push_back(it->second); }
          }
          // TODO: is map
          auto& meta = element.begin()->second;
          if (meta.IsDefined()) {
            assert(meta.IsMap());

            if (meta["position"]) {
              auto& fieldPosition = meta["position"];
              assert(fieldPosition.IsSequence());
              assert(fieldPosition.size() == 3);

              glm::vec3 position(0.0f);
              // position.x       = fieldPosition[0].as<float>();
              // position.y       = fieldPosition[1].as<float>();
              // position.z       = fieldPosition[2].as<float>();
              position         = fieldPosition.as<glm::vec3>();
              n.transformation = glm::translate(glm::mat4(1.0f), position);
            }

            if (meta["name"]) { n.updateName(meta["name"].as<std::string>()); }
          }

          // TODO: Does now work as we can define only one bound per model
          // If a bound was specified:
          auto it = loadedBounds.find(key);
          if (it != loadedBounds.end()) {
            // No more boundigbox node

            // const BoundingBox& bbox = it->second;
            // this->rigidBodiesBounds.push_back(
            //   {bbox, std::numeric_limits<std::size_t>::max()});
            // const std::size_t bboxIndex = this->rigidBodiesBounds.size() - 1;

            // const Node sceneNode(bboxIndex, NodeTypeBoundingBox);
            // this->root.children.back().children.push_back(sceneNode);
          } else {
            assert(false);
          }
        }
      } else if (name == "debug") {
        const YAML::Node& elements = component.second;

        assert(elements.IsSequence());
        for (auto& element : elements) {
          DebugManager::SetToolEnabled(element.as<std::string>(), true);
        }
      } else {
        const YAML::Node& object = component.second;

        // TODO: Here w
        assert(object.IsMap());
        assert(object["model"]);

        const std::string& assetName = object["model"].as<std::string>();
        loadedModels[name]           = loadModel(assetName);
        if (object["bounds"]) {
          const YAML::Node& bounds = object["bounds"];

          assert(bounds.IsSequence());
          for (const auto& element : bounds) {
            yaml::debugNode(element);

            const std::string elementName =
              element.begin()->first.as<std::string>();
            const auto& second = element.begin()->second;

            if (elementName == "BoundingBox") {
              const glm::vec3   min = second["min"].as<glm::vec3>();
              const glm::vec3   max = second["max"].as<glm::vec3>();
              const BoundingBox bbox(min, max);
              loadedBounds[name] = bbox;
            } else if (elementName == "BoundingSphere") {
              const glm::vec3 position = second["position"].as<glm::vec3>();
              const float     radius   = second["radius"].as<float>();

              Sphere         s(position, radius);
              const ObjectID rigidBodyId = Hash(name);
              const ObjectID shapeId = Hash(toString(position, ":", radius));
              // PhysicsWorld::AddShape(s, shapeId);
              Node boundingSphereNode(rigidBodyId, NodeTypeBoundingSphere);
              boundingSphereNode.id = shapeId;
              auto it               = templates.find(name);
              if (it != templates.end()) {
                it->second.children.push_back(boundingSphereNode);
              } else {
                templates.emplace(name, boundingSphereNode);
              }
            } else if (elementName == "KinematicBody") {
              const glm::vec3 position = second["position"].as<glm::vec3>();
              const float     radius   = second["radius"].as<float>();
              Sphere          s(position, radius);
              const ObjectID  rigidBodyId = Hash(name);

              PhysicsWorld::AddRigidBody(
                rigidBodyId, PhysicsWorld::RigidBody::Type::KinematicBody);
              PhysicsWorld::AddShape(s, rigidBodyId);

              Node boundingSphereNode(rigidBodyId, NodeTypeBoundingSphere,
                                      toString(position, ":", radius));

              auto it = templates.find(name);
              if (it != templates.end()) {
                it->second.children.push_back(boundingSphereNode);
              } else {
                templates.emplace(name, boundingSphereNode);
              }
            }
          }
        }
      }
    }
  }

  void SceneManager::pushToScene(Node& root, const glm::mat4& rootMatrix)
  {
    Slot             iterator;
    glm::mat4        currentTransformation;
    std::deque<Slot> toProcess;
    // Node*            precomputed = &precomputedRoot;

    toProcess.push_front({rootMatrix, &root});
    // TODO: Avoid multiplication of identitty matrix
    while (toProcess.empty() == false) {
      iterator   = toProcess.front();
      Node* node = iterator.node;
      //      toProcess.pop_front();

      currentTransformation = iterator.transformation * node->transformation;

      transformations.push_back(currentTransformation);

      BoundingBox bbox = node->bounds;
      bbox.transform(currentTransformation);
      bounds.push_back(bbox);

      // DebugManager::DrawBoundingBox(bbox, glm::mat4(1.0f), glm::vec4(1.0f));

      assert(bounds.size() == transformations.size());

      if (node->type == NodeTypeMesh) {
        drawList.push_back(
          {currentTransformation, SceneManager::GetMesh(node->object)});
      } else if (node->type == NodeTypeBoundingSphere) {

        // TODO: Remove bounding Sphere node for only NodeTypeRigidBody

        // assert(false && "Deprecated");
        PhysicsWorld::UpdateRigidBodyTransform(node->object,
                                               currentTransformation);

      } else if (node->type == NodeTypeRigidBody) {
        //////////////////////////////////
        // Test if this node has physic:
        PhysicsWorld::MotionState* ms =
          PhysicsWorld::FindMotionState(node->object);
        assert(ms && "No motion stte found");

        // Do rotation
        currentTransformation = glm::translate(glm::mat4(1.0f), ms->position) *
                                glm::mat4_cast(ms->orientation);
        // drawList.push_back({m, SceneManager::GetMesh(node->object)});
        //////////////////////////////////
      } else if (node->type == NodeTypeKinematicBody) {
        PhysicsWorld::UpdateRigidBodyTransform(node->object,
                                               currentTransformation);
      }

      if (node->id > 0) {
        nodes.emplace(node->id, NodeCursor(transformations.size() - 1, node));
      }

      for (auto& n : node->children) {
        toProcess.push_back({currentTransformation, &n});
      }
      toProcess.pop_front();
    }
  }

  void SceneManager::removeNode(const ObjectID id)
  {
    std::deque<Node*> toProcess;

    //
    toProcess.push_back(&root);

    while (toProcess.empty() == false) {
      Node* n = toProcess.front();

      for (auto it = n->children.begin(); it < n->children.end();) {
        if (it->id == id) {
          SOLEIL__LOGGER_DEBUG("Erased Node: ", it->id, " (", it->name, ")");
          n->children.erase(it);
          return;
        } else {
          toProcess.push_back(&(*it));
          ++it;
        }
      }
      toProcess.pop_front();
    }
    assert(false && "Node not found");
  }

  bool SceneManager::setCustomNodeType(CustomNodeType newNodeType)
  {
    if (newNodeType <= NodeTypeSize) {
      Logger::error(
        "New custom node type is in the private range [0, NodeTypeSize]");
      return false;
    }
    if (std::find(customNodeTypes.begin(), customNodeTypes.end(),
                  newNodeType) != customNodeTypes.end()) {
      Logger::error("A node type with the same ID is already registered.");
      return false;
    }

    customNodeTypes.push_back(newNodeType);
    return true;
  }

  Model* SceneManager::getModel(ObjectID id)
  {
    assert(modelsMap.count(id) > 0 &&
           "Model was not loaded using SceneManager::LoadModel");
    return modelsMap[id];
  }

  void SceneManager::computeScene()
  {
    nodes.clear();
    transformations.clear();
    bounds.clear();
    drawList.clear();

    pushToScene(root);
  }

  RayCastResult SceneManager::rayCast(const glm::vec3& startPoint,
                                      const glm::vec3& endPoint)
  {
    // TODO: It may be more performant to first RayIntersect with a bounding
    // sphere then the boundingbox

    // TODO: This method has to be tested again.

    RayCastResult result;
    glm::mat4     currentTransformation;

    Slot             iterator;
    std::deque<Slot> toProcess;

    toProcess.push_front({glm::mat4(1.0f), &root});
    while (toProcess.empty() == false) {
      iterator   = toProcess.front();
      Node* node = iterator.node;

      const glm::vec3 segment = glm::normalize(endPoint - startPoint);
      const glm::vec3 dir     = segment;
      float           tMin    = 0.0f;

      currentTransformation = iterator.transformation * node->transformation;
      BoundingBox bbox      = node->bounds;
      if (bbox.isInitialized()) {
        bbox.transform(currentTransformation);

        if (bbox.rayIntersect(startPoint, dir, tMin)) {
          // The Ray has collided but check if it's in range [startPoint,
          // endPoint]
          const float distance = glm::length(segment);
          if (tMin <= distance) {
            result.list.push_back(
              {tMin, node, currentTransformation, node->bounds});
            for (Node& c : node->children) {
              toProcess.push_back({currentTransformation, &c});
            }
          }
        }
      } else {
        for (Node& c : node->children) {
          toProcess.push_back({currentTransformation, &c});
        }
      }
      toProcess.pop_front();
    }

    return result;
  }

  // ----------------- Statics -----------------------
  ObjectID SceneManager::SetMesh(Mesh mesh)
  {
    assert(instance && "SceneManager not initialized");
    instance->meshes.push_back(mesh);
    return instance->meshes.size() - 1;
  }

  Mesh* SceneManager::GetMesh(ObjectID id)
  {
    assert(instance && "SceneManager not initialized");
    assert(id < instance->meshes.size() && "ID out of bounds");

    return &(instance->meshes[id]);
  }

  Node& SceneManager::GetRootScene() { return instance->root; }

  void SceneManager::ComputeScene() { instance->computeScene(); }

  std::size_t SceneManager::GetObject(ObjectID id)
  {
    auto node = instance->nodes.find(id);
    if (node == instance->nodes.end()) {
      return std::numeric_limits<std::size_t>::max();
    }

    return node->second.vectorIndex;
  }

  bool SceneManager::GetObjectTransformation(const ObjectID id,
                                             glm::mat4&     transformation)
  {
    auto node = instance->nodes.find(id);
    if (node == instance->nodes.end()) { return false; }
    assert(node->second.vectorIndex < instance->transformations.size());
    transformation = instance->transformations[node->second.vectorIndex];
    return true;
  }

  bool SceneManager::GetObjectBoundingBox(const ObjectID id, BoundingBox& bbox)
  {
    auto node = instance->nodes.find(id);
    if (node == instance->nodes.end()) { return false; }
    assert(node->second.vectorIndex < instance->bounds.size());
    bbox = instance->bounds[node->second.vectorIndex];
    return true;
  }

  Node* SceneManager::GetNode(const ObjectID id)
  {
    auto node = instance->nodes.find(id);
    if (node == instance->nodes.end()) { return nullptr; }

    return node->second.nodePtr;
  }

  void SceneManager::RemoveNode(const ObjectID id) { instance->removeNode(id); }

  bool SceneManager::SetCustomNodeType(CustomNodeType newNodeType)
  {
    return instance->setCustomNodeType(newNodeType);
  }

  void SceneManager::pushDrawItem(const glm::mat4 tranformation,
                                  const ObjectID  modelID)
  {
    assert(modelsMap.count(modelID) > 0 &&
           "Model was not loaded using SceneManager::LoadModel");
    auto model = modelsMap[modelID];

    assert(model != nullptr);
    pushToScene(model->root, tranformation);
  }

  void SceneManager::PushDrawItem(const glm::mat4 tranformation,
                                  const ObjectID  modelID)
  {
    instance->pushDrawItem(tranformation, modelID);
  }

  void SceneManager::RenderDrawList(const MeshShader&         config,
                                    const std::vector<Light>& lights,
                                    Frame&                    frame)
  {
    // SOLEIL__LOGGER_DEBUG("List size = ", instance->drawList.size());
    Draw(instance->drawList, config, lights, frame);
  }

  DrawList& SceneManager::GetDrawList()
  {
    return instance->drawList;
  }


  uint32_t SceneManager::loadModel(const std::string& resourceName)
  {
    const auto hash = Hash(resourceName);
    models.push_back(std::make_unique<Model>(resourceName));
    modelsMap.emplace(hash, models.back().get());
    return hash;
  }

  uint32_t SceneManager::LoadModel(const std::string& resourceName)
  {
    return instance->loadModel(resourceName);
  }

  RayCastResult SceneManager::RayCast(const glm::vec3& startPoint,
                                      const glm::vec3& endPoint)
  {
    return instance->rayCast(startPoint, endPoint);
  }

  void SceneManager::ImportScene(const std::string& assetName)
  {
    instance->importScene(assetName);
  }

  const glm::mat4& SceneManager::GetTransformation(std::size_t index) noexcept
  {
    assert(instance->transformations.size() > index && "Index out of bounds");

    return instance->transformations[index];
  }
} // namespace Soleil
