/*
 * Copyright (C) 2019  Florian GOLESTIN
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include "GlWindow.h"

#include "Assets.h"
#include "DebugManager.h"
#include "DebugUI.h"
#include "EventManager.h"
#include "Inputs.h"
#include "Logger.h"
#include "OpenGLInclude.h"
#include "SceneManager.h"
#include <ApplicationEventManager.h>
#include <Errors.h>
#include <SDL.h>
#include <audio/AudioManager.h>

namespace Soleil {

  struct SDLStorage
  {
    SDLStorage() = default;

    SDL_Window*   window      = nullptr;
    SDL_GLContext mainContext = nullptr;
    bool          open        = true;
  };

  // TODO: APIENTRY Deactivated as not recongnized on windows
  static void /*APIENTRY*/
  glDebugOutput(GLenum source, GLenum type, GLuint id, GLenum severity,
                GLsizei /*length*/, const GLchar* message,
                const void* /*userParam*/)
  {
    // ignore non-significant error/warning codes
    // if (id == 131169 || id == 131185 || id == 131218 || id == 131204) return;

    std::ostringstream          log;
    const Soleil::Logger::Level level = [severity]() {
      switch (severity) {
        case GL_DEBUG_SEVERITY_LOW: return Soleil::Logger::Debug;
        case GL_DEBUG_SEVERITY_NOTIFICATION: return Soleil::Logger::Info;
        case GL_DEBUG_SEVERITY_MEDIUM: return Soleil::Logger::Info;
        case GL_DEBUG_SEVERITY_HIGH: return Soleil::Logger::Error;
      }
      return Soleil::Logger::Error; // Should not happened
    }();

    log << "OpenGL (" << id << "): " << message;

    switch (source) {
      case GL_DEBUG_SOURCE_API: log << "Source: API"; break;
      case GL_DEBUG_SOURCE_WINDOW_SYSTEM: log << "Source: Window System"; break;
      case GL_DEBUG_SOURCE_SHADER_COMPILER:
        log << "Source: Shader Compiler";
        break;
      case GL_DEBUG_SOURCE_THIRD_PARTY: log << "Source: Third Party"; break;
      case GL_DEBUG_SOURCE_APPLICATION: log << "Source: Application"; break;
      case GL_DEBUG_SOURCE_OTHER: log << "Source: Other"; break;
    }

    switch (type) {
      case GL_DEBUG_TYPE_ERROR: log << "Type: Error\n"; break;
      case GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR:
        log << "\nType: Deprecated Behaviour";
        break;
      case GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR:
        log << "\nType: Undefined Behaviour";
        break;
      case GL_DEBUG_TYPE_PORTABILITY: log << "\nType: Portability"; break;
      case GL_DEBUG_TYPE_PERFORMANCE: log << "\nType: Performance"; break;
      case GL_DEBUG_TYPE_MARKER: log << "\nType: Marker\n"; break;
      case GL_DEBUG_TYPE_PUSH_GROUP: log << "\nType: Push Group"; break;
      case GL_DEBUG_TYPE_POP_GROUP: log << "\nType: Pop Group"; break;
      case GL_DEBUG_TYPE_OTHER: log << "\nType: Other\n"; break;
    }

#if 0
  switch (severity) {
    case GL_DEBUG_SEVERITY_HIGH: log << "\nSeverity: high"; break;
    case GL_DEBUG_SEVERITY_MEDIUM: log << "\nSeverity: medium"; break;
    case GL_DEBUG_SEVERITY_LOW: log << "\nSeverity: low"; break;
    case GL_DEBUG_SEVERITY_NOTIFICATION: log << "Severity: notification"; break;
  }
#endif

    Soleil::Logger::Log(level, log.str());

#ifndef NDEBUG
    SOLEIL__ASSERT_BREAK(level != Soleil::Logger::Error);
#endif
  }

  GlWindow::GlWindow(int width, int height, std::string name)
  {
    const int GL_Major = 3;
    const int GL_Minor = 0;

    if (SDL_Init(SDL_INIT_VIDEO) < 0) {
      const std::string err =
        toString("Failed to initialize SDL: ", SDL_GetError());
      SOLEIL_TERMINATE_ERROR(err);
    }

    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 0);
    SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
    SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 24);
    SDL_GL_SetAttribute(SDL_GL_MULTISAMPLEBUFFERS, 1);
    SDL_GL_SetAttribute(SDL_GL_MULTISAMPLESAMPLES, 4);
    //
    SDL_GL_SetAttribute(SDL_GL_RED_SIZE, 5);
    SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE, 5);
    SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE, 5);
    SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 16);
    SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);

    // TODO: Deinit SDL if something went wrong

    SDL_Window*   window;
    SDL_GLContext mainContext;

    window = SDL_CreateWindow(name.c_str(), 100, 100, width, height,
                              SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN);
    if (window == nullptr) {
      const std::string err =
        toString("Failed to create SDL Window: ", SDL_GetError());
      SOLEIL_TERMINATE_ERROR(err);
    }

    mainContext = SDL_GL_CreateContext(window);

    if (mainContext == nullptr) {
      const std::string err =
        toString("Failed to initialize SDL GL context: ", SDL_GetError());
      SOLEIL_TERMINATE_ERROR(err);
    }

    if (gl3wInit()) { throw std::runtime_error("Failed to initialize GL3W"); }
    if (!gl3wIsSupported(GL_Major, GL_Minor)) {
      throw std::runtime_error("OpenGL 3.3 not supported");
    }
    SOLEIL__LOGGER_DEBUG("OpenGL ", glGetString(GL_VERSION), ", GLSL ",
                         glGetString(GL_SHADING_LANGUAGE_VERSION), "\n");

    SDL_GL_SetSwapInterval(1);

    GLint flags;
    glGetIntegerv(GL_CONTEXT_FLAGS, &flags);
    if (flags & GL_CONTEXT_FLAG_DEBUG_BIT) {
      glEnable(GL_DEBUG_OUTPUT);
      glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);
      // TODO: Restore (deactivated to test windows build)
      glDebugMessageCallback(glDebugOutput, nullptr);
      glDebugMessageControl(GL_DONT_CARE, GL_DONT_CARE, GL_DONT_CARE, 0,
                            nullptr, GL_TRUE);
    }

    // Key-mapping Initialization --------------------------------------
    Inputs::KeyMap keymap;
    // keymap[GLFW_KEY_W]     = SOLEIL__KEY_FORWARD;
    // keymap[GLFW_KEY_S]     = SOLEIL__KEY_BACKWARD;
    // keymap[GLFW_KEY_A]     = SOLEIL__KEY_LEFT;
    // keymap[GLFW_KEY_D]     = SOLEIL__KEY_RIGHT;
    // keymap[GLFW_KEY_F1]    = SOLEIL__KEY_F1;
    // keymap[GLFW_KEY_F2]    = SOLEIL__KEY_F2;
    // keymap[GLFW_KEY_F3]    = SOLEIL__KEY_F3;
    // keymap[GLFW_KEY_F4]    = SOLEIL__KEY_F4;
    // keymap[GLFW_KEY_F5]    = SOLEIL__KEY_F5;
    // keymap[GLFW_KEY_F6]    = SOLEIL__KEY_F6;
    // keymap[GLFW_KEY_F7]    = SOLEIL__KEY_F7;
    // keymap[GLFW_KEY_F8]    = SOLEIL__KEY_F8;
    // keymap[GLFW_KEY_F9]    = SOLEIL__KEY_F9;
    // keymap[GLFW_KEY_F10]   = SOLEIL__KEY_F10;
    // keymap[GLFW_KEY_F12]   = SOLEIL__KEY_F11;
    // keymap[GLFW_KEY_F11]   = SOLEIL__KEY_F12;
    // keymap[GLFW_KEY_PAUSE] = SOLEIL__KEY_PAUSE;
    // keymap[GLFW_KEY_SPACE] = SOLEIL__KEY_SPACE;

    // Engine Initialization -------------------------------------------
    Soleil::Assets::Initialize("../media");
    Soleil::DebugManager::Init();
    Soleil::Inputs::Init(keymap);
    Soleil::SceneManager::Init();
    Soleil::EventManager::Init();
    Soleil::ApplicationEventManager::Init();
    Soleil::AudioManager::Init();

    SDLStorage* storage  = new SDLStorage;
    storage->window      = window;
    storage->mainContext = mainContext;
    this->window         = (void*)storage;

    SOLEIL__LOGGER_DEBUG("Application INITED");
  }

  GlWindow::~GlWindow()
  {
    SDLStorage* storage = (SDLStorage*)window;

    SDL_GL_DeleteContext(storage->mainContext);
    SDL_DestroyWindow(storage->window);
    SDL_Quit();
  }

  bool GlWindow::isOpen(void) const noexcept
  {
    return ((SDLStorage*)window)->open;
  }

  glm::vec2 GlWindow::getFrameBufferSize(void) const noexcept
  {
    int w, h;
    SDL_GL_GetDrawableSize(((SDLStorage*)window)->window, &w, &h);
    return glm::vec2(w, h);
  }

  void GlWindow::displayMouseCursor() noexcept
  {
    // TODO:
  }

  void GlWindow::hideMouseCursor() noexcept
  {
    // TODO:
  }

  bool GlWindow::newFrame(void)
  {
    // Update must be before glfwPollEvents in order to have the justPressed
    // effect lasting one frame.
    Soleil::Inputs::Update();

    SDL_Event event;
    while (SDL_PollEvent(&event)) {
      switch (event.type) {

        case SDL_QUIT:
          SOLEIL__LOGGER_DEBUG("Event Quit");
          ((SDLStorage*)window)->open = false;
          break;

        case SDL_KEYDOWN: {
          switch (event.key.keysym.sym) {
            case SDLK_ESCAPE:
              SOLEIL__LOGGER_DEBUG("Event Quit");
              ((SDLStorage*)window)->open = false;
              break;
            default:
              Inputs::Key translatedKey;
              if (Inputs::TranslateKey(event.key.keysym.sym, translatedKey)) {
                Inputs::UpdateKeyState(translatedKey, true);
              }
              break;
          }
        } break;
        case SDL_MOUSEMOTION: {
          Inputs::SetCursorPosition(glm::vec2(event.motion.x, event.motion.y));
        } break;
        default: break;
      }
    }
    return isOpen();
  }

  void GlWindow::endFrame(void)
  {
    SDL_GL_SwapWindow(((SDLStorage*)window)->window);
  }

  static GlWindow* instance = nullptr;

  void GlWindowInstance::SetInstance(GlWindow* window)
  {
    assert((instance == nullptr || window == nullptr) &&
           "You are setting a GlWindow instance while one already exists. If "
           "it's really what you want, explicit it by calling "
           "SetInstance(nullptr) before setting the new instance.");

    instance = window;
  }

  GlWindow* GlWindowInstance::GetInstance(void) noexcept
  {
    assert(instance != nullptr &&
           "Instance is null, please call SetInstance before.");

    return instance;
  }

} // namespace Soleil
