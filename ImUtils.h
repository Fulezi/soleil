/*
 * Copyright (C) 2018  Florian GOLESTIN
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#pragma once

#include "DebugUI.h"

#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtx/euler_angles.hpp>
#include <glm/matrix.hpp>

namespace Soleil {

  inline void DisplayVector(const glm::vec3& v, const char* label = nullptr)
  {
#if SOLEIL__USE_IMGUI
    if (label) {
      ImGui::Text("%s", label);
      ImGui::SameLine();
    }
    ImGui::Text("[%f, %f, %f]", v.x, v.y, v.z);
#else
    (void)v;
    (void)label;
#endif
  }

  inline void EulerQuat(const glm::quat& q, const char* label = nullptr)
  {
#if SOLEIL__USE_IMGUI
    if (label) {
      ImGui::Text("%s", label);
      ImGui::SameLine();
    }
    const glm::vec3 euler = glm::eulerAngles(q);
    ImGui::Text("Pitch: %f", euler.x);
    ImGui::SameLine();
    ImGui::Text("Yaw: %f", euler.y);
    ImGui::SameLine();
    ImGui::Text("Roll: %f", euler.z);
#else
    (void)q;
    (void)label;
 #endif // SOLEIL__USE_IMGUI
 }
};
