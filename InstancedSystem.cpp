/*
 * Copyright (C) 2018  Florian GOLESTIN
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include "InstancedSystem.h"

#include "Model.h"
#include "Utils.h"
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

namespace Soleil {

  InstancedSystem::InstancedSystem(const std::string& modelName,
                                   std::size_t        maximumParticles)
    : maximumParticles(maximumParticles)
    , particles(maximumParticles)
    , sprites(maximumParticles)
    , spriteCount(0)
    , firstFreeParticle(0)
    , meshBuffer()
    , particleBuffer()
  {
    vao.bind();

    // TODO: On program for all InstancedSystem
    // Load the Program:
    program.attachShader(GL_VERTEX_SHADER, "instancedparticle.vert");
    program.attachShader(GL_FRAGMENT_SHADER, "instancedparticle.frag");
    program.compile();

// Mesh Buffer:
#if 0
    constexpr GLfloat meshBufferData[] = {
 // positions          
    -1.0f,  1.0f, -1.0f,
    -1.0f, -1.0f, -1.0f,
     1.0f, -1.0f, -1.0f,
     1.0f, -1.0f, -1.0f,
     1.0f,  1.0f, -1.0f,
    -1.0f,  1.0f, -1.0f,

    -1.0f, -1.0f,  1.0f,
    -1.0f, -1.0f, -1.0f,
    -1.0f,  1.0f, -1.0f,
    -1.0f,  1.0f, -1.0f,
    -1.0f,  1.0f,  1.0f,
    -1.0f, -1.0f,  1.0f,

     1.0f, -1.0f, -1.0f,
     1.0f, -1.0f,  1.0f,
     1.0f,  1.0f,  1.0f,
     1.0f,  1.0f,  1.0f,
     1.0f,  1.0f, -1.0f,
     1.0f, -1.0f, -1.0f,

    -1.0f, -1.0f,  1.0f,
    -1.0f,  1.0f,  1.0f,
     1.0f,  1.0f,  1.0f,
     1.0f,  1.0f,  1.0f,
     1.0f, -1.0f,  1.0f,
    -1.0f, -1.0f,  1.0f,

    -1.0f,  1.0f, -1.0f,
     1.0f,  1.0f, -1.0f,
     1.0f,  1.0f,  1.0f,
     1.0f,  1.0f,  1.0f,
    -1.0f,  1.0f,  1.0f,
    -1.0f,  1.0f, -1.0f,

    -1.0f, -1.0f, -1.0f,
    -1.0f, -1.0f,  1.0f,
     1.0f, -1.0f, -1.0f,
     1.0f, -1.0f, -1.0f,
    -1.0f, -1.0f,  1.0f,
     1.0f, -1.0f,  1.0f
    };
    meshNumberVertices = 36;

    meshBuffer.bind(GL_ARRAY_BUFFER);
    {
      glBufferData(GL_ARRAY_BUFFER, sizeof(meshBufferData), meshBufferData,
                   GL_STATIC_DRAW);

      // Vertex
      glEnableVertexAttribArray(0);
      glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, nullptr);
      throwOnGlError();
    }
#else
    const std::vector<float> meshBufferData = Model::LoadModel(modelName);
    meshBuffer.bind(GL_ARRAY_BUFFER);
    {
      glBufferData(GL_ARRAY_BUFFER,
                   sizeof(meshBufferData[0]) * meshBufferData.size(),
                   meshBufferData.data(), GL_STATIC_DRAW);
      meshNumberVertices = meshBufferData.size();

      // Vertex
      glEnableVertexAttribArray(0);
      glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, nullptr);
      throwOnGlError();
    }

#endif

    // Prepare the buffer
    particleBuffer.bind(GL_ARRAY_BUFFER);
    {
      glBufferData(GL_ARRAY_BUFFER, sizeof(MeshParticle) * maximumParticles,
                   nullptr, GL_STREAM_DRAW);
      // Position
      // glEnableVertexAttribArray(1);
      // glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(MeshParticle),
      //                       nullptr);
      // throwOnGlError();

      // Color -- Is now first
      glEnableVertexAttribArray(1);
      glVertexAttribPointer(
        1, 4, GL_FLOAT, GL_FALSE, sizeof(MeshParticle),
        reinterpret_cast<GLvoid*>(offsetof(MeshParticle, color)));
      throwOnGlError();

      // transformation -- Have to be last to don't mess with Attrib ID
      constexpr int TransformationID = 2;
      glEnableVertexAttribArray(TransformationID);
      glEnableVertexAttribArray(TransformationID + 1);
      glEnableVertexAttribArray(TransformationID + 2);
      glEnableVertexAttribArray(TransformationID + 3);
      GLvoid* stride1 =
        reinterpret_cast<GLvoid*>(offsetof(MeshParticle, transformation) + 0);
      GLvoid* stride2 = reinterpret_cast<GLvoid*>(
        offsetof(MeshParticle, transformation) + sizeof(float) * 4);
      GLvoid* stride3 = reinterpret_cast<GLvoid*>(
        offsetof(MeshParticle, transformation) + sizeof(float) * 8);
      GLvoid* stride4 = reinterpret_cast<GLvoid*>(
        offsetof(MeshParticle, transformation) + sizeof(float) * 12);
      glVertexAttribPointer(TransformationID, 4, GL_FLOAT, GL_FALSE,
                            sizeof(MeshParticle), stride1);
      glVertexAttribPointer(TransformationID + 1, 4, GL_FLOAT, GL_FALSE,
                            sizeof(MeshParticle), stride2);
      glVertexAttribPointer(TransformationID + 2, 4, GL_FLOAT, GL_FALSE,
                            sizeof(MeshParticle), stride3);
      glVertexAttribPointer(TransformationID + 3, 4, GL_FLOAT, GL_FALSE,
                            sizeof(MeshParticle), stride4);
      glVertexAttribDivisor(TransformationID, 1);
      glVertexAttribDivisor(TransformationID + 1, 1);
      glVertexAttribDivisor(TransformationID + 2, 1);
      glVertexAttribDivisor(TransformationID + 3, 1);

      // TODO:
      // https://stackoverflow.com/questions/17355051/using-a-matrix-as-vertex-attribute-in-opengl3-core-profile
      throwOnGlError();

      // scale
      // glEnableVertexAttribArray(3);
      // glVertexAttribPointer(
      //   3, 1, GL_FLOAT, GL_FALSE, sizeof(MeshParticle),
      //   reinterpret_cast<GLvoid*>(offsetof(MeshParticle, scale)));
      // throwOnGlError();
    }
    throwOnGlError();
  }

  Particle& InstancedSystem::emitParticle()
  {
    auto savedSearch = firstFreeParticle;

    while (firstFreeParticle < maximumParticles) {
      if (particles[firstFreeParticle].life <= 0.0f) {
        // firstFreeParticle++;
        return particles[firstFreeParticle++];
      }
      firstFreeParticle++;
    }

    firstFreeParticle = 0;
    while (firstFreeParticle < savedSearch) {
      if (particles[firstFreeParticle].life <= 0.0f) {
        // firstFreeParticle++;
        return particles[firstFreeParticle++];
      }

      firstFreeParticle++;
    }

    SOLEIL__LOGGER_DEBUG("*** *** *** *** *** "
                         "Particle System is starving. No free particle found");
    firstFreeParticle = 0;
    // firstFreeParticle++;
    // if (firstFreeParticle > maximumParticles)
    //   {
    // 	firstFreeParticle = 0;
    //   }
    return particles[firstFreeParticle];
  }

  void InstancedSystem::update(const float deltaTime)
  {
    spriteCount = 0;
    for (auto& particle : particles) {
      particle.life -= deltaTime;
      if (particle.life > 0.0f) {
#if 0
        particle.position += particle.velocity * deltaTime; Attention modify position!
        particle.velocity.y += -9.0f * deltaTime;
        particle.velocity *= std::pow(0.90f, deltaTime);
// TODO: Enable Physics via a bool on ParticleSystem
#else
        // particle.position += particle.velocity * deltaTime;
        particle.transformation *=
          glm::translate(glm::mat4(1.0f), particle.velocity * deltaTime);

        particle.velocity.y += -9.0f * deltaTime;
        // TODO: Specific to Shoot?
        // TODO: Not a linear curve?
        float alpha      = map(0.0f, 0.5f, 0.0f, 1.0f, particle.life);
        particle.color.a = alpha;
#endif

        auto& sprite          = sprites[spriteCount];
        sprite.color          = particle.color;
        sprite.transformation = particle.transformation;
        spriteCount++;
      }
    }

    // TODO: Sort particles?
    // TODO: a dedicated update buffer method
    particleBuffer.bind(GL_ARRAY_BUFFER);
    {
      // Buffer orphaning
      glBufferData(GL_ARRAY_BUFFER, sizeof(MeshParticle) * particles.size(),
                   nullptr, GL_STREAM_DRAW);
      glBufferSubData(GL_ARRAY_BUFFER, 0, spriteCount * sizeof(MeshParticle),
                      sprites.data());
      throwOnGlError();
    }
  }

  void InstancedSystem::draw(Frame& frame)
  {
    glUseProgram(program.program);
    vao.bind();
    particleBuffer.bind(GL_ARRAY_BUFFER);
    {
      // TODO: store uniform
      glUniformMatrix4fv(program.getUniform("ModelViewProjection"), 1, GL_FALSE,
                         glm::value_ptr(frame.ViewProjection));

      // glBindTexture(GL_TEXTURE_2D, texture);

      // TODO: Move in DrawParticleSystems method, update the example main
      glEnable(GL_DEPTH_TEST);
      glEnable(GL_BLEND);
      glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

      glVertexAttribDivisor(
        0, 0); // particles vertices : always reuse the same 4 vertices -> 0
      glVertexAttribDivisor(1, 1);
      // glVertexAttribDivisor(2, 1);
      // glVertexAttribDivisor(3, 1);
      constexpr int TransformationID = 2;
      glVertexAttribDivisor(TransformationID, 1);
      glVertexAttribDivisor(TransformationID + 1, 1);
      glVertexAttribDivisor(TransformationID + 2, 1);
      glVertexAttribDivisor(TransformationID + 3, 1);
      glDrawArraysInstanced(GL_TRIANGLES, 0, meshNumberVertices, spriteCount);

      throwOnGlError();
      // SOLEIL__LOGGER_DEBUG("Number Instanced Particle Drawed: ",
      // spriteCount);
    }
  }

  ////////////////////////////////////////////////////////////////////////////
  // Statics

  static std::vector<std::unique_ptr<InstancedSystem>> s_automanagedSystems;

  void InstancedSystem::Init()
  {
    // Nothing to do
  }

  void InstancedSystem::Destroy()
  {
    s_automanagedSystems.clear();
  }
  
  InstancedSystem* InstancedSystem::CreateParticleSystem(
    const std::string& modelName, std::size_t maximumParticles)
  {
    s_automanagedSystems.emplace_back(
      std::make_unique<InstancedSystem>(modelName, maximumParticles));
    return s_automanagedSystems.back().get();
  }

  void InstancedSystem::DrawParticleSystems(Frame& frame, const float deltaTime)
  {
    for (auto& ps : s_automanagedSystems) {
      ps->update(deltaTime); // TODO: Set this here?
      ps->draw(frame);
    }
  }

  void InstancedSystem::DestroyParticleSystem(InstancedSystem* sys)
  {
    for (auto it = s_automanagedSystems.begin();
         it != s_automanagedSystems.end(); ++it) {
      if (sys == it->get()) {
	SOLEIL__LOGGER_DEBUG("Deleted InstancedSystem: ", sys);
        s_automanagedSystems.erase(it);
        return;
      }
    }
    Soleil::Logger::error(
      toString("Cannot delete InstancedSystem: '", sys, "'. Already deleted?"));
  }

} // namespace Soleil
