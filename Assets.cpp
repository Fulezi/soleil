/*
 * Copyright (C) 2017  Florian GOLESTIN
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include "OpenGLInclude.h"

#include "Assets.h"
#include "Logger.h"
#include <stringutils.h>

#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"

#include <fstream>

namespace Soleil {
  static Assets assetManager;

  namespace {
    inline char PathSeparator() { return '/'; }
  } // namespace

  void Assets::Initialize(const std::string& pathPrefix)
  {
    assetManager.pathPrefix = pathPrefix;
    // if (assetManager.pathPrefix.back() != '/') assetManager.pathPrefix +=
    // '/';

    /* Windows accept both / and \\ but it should be terminated by something */
    if (assetManager.pathPrefix.back() != '/') {
      assetManager.pathPrefix += PathSeparator();
    }
  }

  void Assets::Destroy()
  {
    // Nothing to do so far
  }

  std::string Assets::AsString(const std::string& resourceName)
  {
    const std::string fileName = GetPath(resourceName);
    std::ifstream     in(fileName);

    if (in.is_open() == false)
      throw std::runtime_error(
        Soleil::toString("Failed to read file '", fileName, "'"));

    return std::string((std::istreambuf_iterator<char>(in)),
                       std::istreambuf_iterator<char>());
  }

  std::string Assets::GetPath(const std::string& resourceName)
  {
    SOLEIL__LOGGER_DEBUG("AssetManager: Requested file '",
                         assetManager.pathPrefix + resourceName, "'");
    return assetManager.pathPrefix + resourceName;
  }

  GLuint Assets::GetTexture(const std::string& resourceName)
  {
    // TODO: Seperate the load from the get method
    if (assetManager.textures.count(resourceName) > 0) {
      SOLEIL__LOGGER_DEBUG("Using cached texture: ", resourceName);
      return assetManager.textures.at(resourceName);
    }

    GLuint textureId;
    glGenTextures(1, &textureId);
    assetManager.textures.emplace(resourceName, textureId);
    // TODO: Use hash as keys

    int            width, height, numberComponents;
    unsigned char* data = stbi_load(GetPath(resourceName).c_str(), &width,
                                    &height, &numberComponents, 0);
    if (data == nullptr) {
      throw std::runtime_error(
        toString("Failed to read resource: '", resourceName, "'"));
    }

    GLenum format;
    switch (numberComponents) {
      case 1: format = GL_RED; break;
      case 3: format = GL_RGB; break;
      case 4: format = GL_RGBA; break;
      default:
        throw std::runtime_error(
          Soleil::toString("Non-existent pixel format: ", numberComponents));
    }

    SOLEIL__LOGGER_DEBUG("Loading new texture: ", resourceName,
                         " (format: ", numberComponents, ", witdth: ", width,
                         ", height: ", height, ")");

    glBindTexture(GL_TEXTURE_2D, textureId);
    glTexImage2D(GL_TEXTURE_2D, 0, format, width, height, 0, format,
                 GL_UNSIGNED_BYTE, data);
    glGenerateMipmap(GL_TEXTURE_2D);

    // TODO: Parametrable:
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,
                    GL_LINEAR_MIPMAP_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    return textureId;
  }

  Font* Assets::LoadFont(const std::string& resourceName)
  {
    auto it = assetManager.fonts.find(resourceName);

    if (it == assetManager.fonts.end()) {
      auto inserted = assetManager.fonts.emplace(
        resourceName, Font::CreateWithDefault(resourceName));
      return inserted.first->second.get();
    }
    return it->second.get();
  }

} // namespace Soleil
