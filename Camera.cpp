/*
 * Copyright (C) 2018  Florian GOLESTIN
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include "Camera.h"

#include "GlWindow.h"
#include "Inputs.h"
#include "OpenGLInclude.h"
#include "Utils.h"
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/quaternion.hpp>

namespace Soleil {

  Freecam::Freecam(const float width, const float height)
    : aspect(width / height)
    , fov(glm::radians(55.0f))
    , yaw(0.0f)
    , pitch(0.0f)
    , velocity(0.0f)
    //, direction(0.0f)
    , previousCusorPosition(Soleil::Inputs::GetCursorPosition())
  {
    // TODO: Resize callback
    position = glm::vec3(0.0f, 8.0f, 5.0f);
  }

  void Freecam::update(float deltaTime)
  {
    (void)deltaTime;

    velocity.z = Inputs::GetKeyPress(SOLEIL__KEY_FORWARD) -
                 Inputs::GetKeyPress(SOLEIL__KEY_BACKWARD);
    velocity.x = Inputs::GetKeyPress(SOLEIL__KEY_RIGHT) -
                 Inputs::GetKeyPress(SOLEIL__KEY_LEFT);

    const auto cursorPosition = Soleil::Inputs::GetCursorPosition();
    glm::vec2  offset;
    offset.x              = cursorPosition.x - previousCusorPosition.x;
    offset.y              = previousCusorPosition.y - cursorPosition.y;
    previousCusorPosition = cursorPosition;
    yaw += offset.x * 0.05f;
    pitch += offset.y * 0.05f;
    if (pitch > 89.0f) pitch = 89.0f;
    if (pitch < -89.0f) pitch = -89.0f;

    glm::vec3 direction;
    direction.x = cos(glm::radians(pitch)) * cos(glm::radians(yaw));
    direction.y = sin(glm::radians(pitch));
    direction.z = cos(glm::radians(pitch)) * sin(glm::radians(yaw));
    direction   = glm::normalize(direction);

    position +=
      .10f * ((direction * velocity.z) +
              (glm::normalize(glm::cross(direction, glm::vec3(0, 1, 0))) *
               velocity.x));
    this->view =
      glm::lookAt(position, position + direction, glm::vec3(0, 1, 0));

    // TODO: Do not update each frame
    this->projection = glm::perspective(fov, aspect, 0.1f, 1000.0f);
  }

  void Freecam::onActivate(Camera* /*previousCamera*/)
  {
    GlWindowInstance::GetInstance()->hideMouseCursor();
    const auto cursorPosition = Soleil::Inputs::GetCursorPosition();
    previousCusorPosition     = cursorPosition;
  }

  void Freecam::lookAt(const glm::vec3& point)
  {
    const glm::vec3 direction = glm::normalize(point - position);

    // glm::quat(direction) * glm::vec3(0, 0, 1);
    yaw   = glm::degrees(glm::atan(direction.y, direction.x));
    pitch = glm::degrees(glm::asin(direction.z));
  }

  // -- Editor Camera ------------------------------------------------

  EditorCamera::EditorCamera(const float width, const float height)
    : Freecam(width, height)
  {}

  void EditorCamera::update(float deltaTime)
  {
    // TODO: at the first time or onActivate (actually just a lookAt)
    // Freecam::update(deltaTime);

    (void)deltaTime;
    glm::vec3 direction;
    direction.x = cos(glm::radians(pitch)) * cos(glm::radians(yaw));
    direction.y = sin(glm::radians(pitch));
    direction.z = cos(glm::radians(pitch)) * sin(glm::radians(yaw));
    direction   = glm::normalize(direction);
    this->view =
      glm::lookAt(position, position + direction, glm::vec3(0, 1, 0));
    this->projection = glm::perspective(fov, aspect, 0.1f, 1000.0f);
  }

  void EditorCamera::onActivate(Camera* previousCamera)
  {
    if (previousCamera) {
      /* previousCamera->view as a glm::mat4 */
      const glm::mat4 inverted  = glm::inverse(previousCamera->view);
      position                  = glm::vec3(inverted[3]);
      const glm::vec3 direction = -glm::vec3(inverted[2]);
      yaw   = glm::degrees(glm::atan(direction.z, direction.x));
      pitch = glm::degrees(glm::asin(direction.y));
    }
    GlWindowInstance::GetInstance()->displayMouseCursor();
  }
} // namespace Soleil
