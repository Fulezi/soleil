/*
 * Copyright (C) 2018  Florian GOLESTIN
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include "Timer.h"

#include "OpenGLInclude.h"

#include <time.h>

#ifdef SOLEIL__USE_GLFW
// Will include glfw in the correct order (required to be included after gl3w).
#include "OpenGLInclude.h"
#endif

namespace Soleil {

  constexpr double LimitFPS    = 1.0 / 60.0;
  constexpr double LimitFPSMin = 0.8 * LimitFPS;
  constexpr double LimitFPSMax = 1.2 * LimitFPS;

  using Milliseconds = double;

  namespace {

    /// Implementation specific method to return a nonotonic elapsed time since
    /// a random point. It is however not affected by discontinuous jumps in the
    /// system time.
    /// TODO: Can be implemented in another file (i.e. platform/glfw/time.cpp,
    /// platform/posix/time.cpp) with `extern  double GetElapsedTime()`
    Milliseconds _GetElapsedTime()
    {
#ifdef SOLEIL__USE_GLFW
      return glfwGetTime();
#else // POSIX implementation
      struct timespec tp;

      int err = clock_gettime(CLOCK_MONOTONIC, &tp);
      if (err != 0) {
        // No  fallback
        assert(false && "Clock returned an error.");
        return 0.0;
      }
      constexpr double NanoToMilli = 0.000001;
      return tp.tv_sec * 1000.0 + tp.tv_nsec * NanoToMilli;
#endif
    }
  } // namespace

  Time::Time()
    : initialized(false)
    , previousTime(0.0)
    , deltaTime(0.0)
  {}

  void Time::startFrame()
  {
    const Milliseconds now = Soleil::_GetElapsedTime();

    if (initialized == false) {
      initialized  = true;
      previousTime = now;
      deltaTime    = 0.016;
    }

    deltaTime = (now - previousTime);

    if (deltaTime < LimitFPSMin)
      deltaTime = LimitFPS;
    else if (deltaTime > LimitFPSMax)
      deltaTime = LimitFPS;

    previousTime = now;
  }

  void Time::endFrame() {}

  double Time::getDeltaTime() const noexcept { return deltaTime; }

  double Time::getElapsedTime() const noexcept
  {
    return Soleil::_GetElapsedTime();
  }

  //////////////////////////////////////////////////////////////////////////////
  // Statics

  static Time timeInstance;

  void Time::StartFrame() { timeInstance.startFrame(); }

  void Time::EndFrame() { timeInstance.endFrame(); }

  double Time::GetDeltaTime() noexcept { return timeInstance.getDeltaTime(); }

  double Time::GetElapsedTime() noexcept
  {
    return timeInstance.getElapsedTime();
  }

} // namespace Soleil
