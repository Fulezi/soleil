
#ifndef SOLEIL__FRAME_H_
#define SOLEIL__FRAME_H_

#include "OpenGLInclude.h"
#include <glm/matrix.hpp>

namespace Soleil {

  struct Frame
  {
    GLuint nullTexture;

    /** 
     * Size, Near and Far values of the Viewport.
     * ViewPortSizes.xy	= size on axis X, Y
     * ViewPortSizes.z	= Near
     * ViewPortSizes.z	= Far
     */
    glm::vec4 ViewPort;
    
    /**
     * An up to date View * Projection Matrix
     * @see updateViewProjectionMatrices(const glm::mat4& view, const glm::mat4
     * projection)
     */
    glm::mat4 ViewProjection;

    /**
     * the View without the projection
     * @see updateViewProjectionMatrices(const glm::mat4& view, const glm::mat4
     * projection)
     */
    glm::mat4 View;

    /**
     * the Projection without the view
     * @see updateViewProjectionMatrices(const glm::mat4& view, const glm::mat4
     * projection)
     */
    glm::mat4 Projection;

    /**
     * Update related matrices and set the MVP one.
     */
    inline void updateViewProjectionMatrices(const glm::mat4& view,
                                             const glm::mat4&  projection)
    {
      this->View           = view;
      this->Projection     = projection;
      this->ViewProjection = projection * view;
    }
  };

  /**
   * Helper to create a null (black) texture
   */
  inline GLuint generateNullTexture()
  {
    const GLuint chessBoard[4] = {0x00000000, 0xFFFFFFFF, 0x00000000,
                                  0xFFFFFFFF};

    GLuint nullTexture;
    glGenTextures(1, &nullTexture);
    glBindTexture(GL_TEXTURE_2D, nullTexture);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, 1, 1, 0, GL_RGBA, GL_UNSIGNED_BYTE,
                 chessBoard);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glGenerateMipmap(GL_TEXTURE_2D);
    throwOnGlError();

    return nullTexture;
  }

} // Soleil

#endif /* SOLEIL__FRAME_H_ */
