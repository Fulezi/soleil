/*
 * Copyright (C) 2018  Florian GOLESTIN
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#pragma once

#include "BoundingBox.h"
#include "Program.hpp"
#include "ogl/Buffer.h"
#include "ogl/VertexArray.h"
#include <functional>
#include <glm/vec3.hpp>
#include <glm/vec4.hpp>
#include <map>
#include <physics/Intersections.h>

namespace Soleil {

  struct Frame;

  class DebugManager
  {
  public:
    DebugManager();
    ~DebugManager() = default;

  public:
    void newFrame(const double deltaTime);
    void displayMetrics();

    //----------------------------
    // Draw multiples tool windows
  public:
    typedef std::function<void(const double deltaTie)> UITool;
    void registerTool(const std::string& toolName, UITool tool,
                      bool enabled = false);
    void setToolEnabled(const std::string& toolName, bool enabled);
    void displayTools(const double deltaTime);

  private:
    std::map<std::string, std::pair<bool, UITool>> tools;

    //----------------------------
    // Draw Primitives (Line, points, ...)
  public:
    struct Primitive
    {
      glm::vec3 position;
      glm::vec4 color;

      Primitive(const glm::vec3& position, const glm::vec4& color)
        : position(position)
        , color(color)
      {
        // TODO: std move?
      }
    };

    struct PrimitiveSet
    {
      int    start;
      int    count;
      GLenum type;
      int    scale;
    };

    void drawPoint(const glm::vec3& point, const glm::vec4& color);
    void drawLine(const glm::vec3& start, const glm::vec3& end,
                  const glm::vec4& color, const float lineWidth = 1.0f);
    void drawTriangle(const glm::vec3& p0, const glm::vec3& p2,
                      const glm::vec3& p3, const glm::vec4& color);
    void drawBoundingBox(const BoundingBox& bbox,
                         const glm::mat4&   transformation,
                         const glm::vec4&   color);
    void drawSphere(const Sphere& s, const glm::vec4& color);
    void drawCapsule(const Capsule& c, const glm::vec4& color);
    void displayPrimitives(Frame& frame);

    static void Init();
    static void Destroy();
    static void NewFrame(const double dt);
    static void DrawPoint(const glm::vec3& point, const glm::vec4& color);
    static void DrawLine(const glm::vec3& start, const glm::vec3& end,
                         const glm::vec4& color, const float lineWidth = 1.0f);
    static void DrawSphere(const Sphere& s, const glm::vec4& color);
    static void DrawTriangle(const glm::vec3& p0, const glm::vec3& p2,
                             const glm::vec3& p3, const glm::vec4& color);
    static void DrawBoundingBox(const BoundingBox& bbox,
                                const glm::mat4&   transformation,
                                const glm::vec4&   color);
    static void DrawCapsule(const Capsule& c, const glm::vec4& color);
    static void DisplayPrimitives(Frame& frame);
    static void DisplayTools(const double deltaTime);
    static void RegisterTool(const std::string& toolName, UITool tool,
                             bool enabled = false);
    static void SetToolEnabled(const std::string& toolName, bool enabled);

  private: // TODO: Primitive or Primitives
    Buffer                    primitiveBuffer;
    VertexArray               primitivesVao;
    Program                   primitiveProgram;
    std::vector<PrimitiveSet> primitiveSet;
    std::vector<Primitive>    primitives;

  private:
    unsigned int frameCount;
    double       accumulatedTime;
    double       averageFPS;
    double       computeTimePerFrame; // TODO: Remove ?
    double       averageTimePerFrame;
  };

} // namespace Soleil
