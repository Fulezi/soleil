/*
 * Copyright (C) 2017  Florian GOLESTIN
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef SOLEIL__DRAW_H_
#define SOLEIL__DRAW_H_

#include "Mesh.h"
#include "Node.h"
#include <glm/matrix.hpp>
#include <vector>

namespace Soleil {

  struct DrawCommand
  {
    glm::mat4 transformation;
    Mesh*     mesh;
  };

  typedef std::vector<DrawCommand> DrawList;

  /**
   * Create a DrawList from a scene graph
   */
  void ComputeDrawList(DrawList& list, const Node& root);


  /// Draw Dynamics meshes
  void Draw(const DrawList& list, const MeshShader& config,
            const std::vector<Light>& lights, Frame& frame);

  /// Render Depth to current frame buffer
  void DrawDepth(const DrawList& list, const MeshShader& config,
            Frame& frame);

  // TODO: Temp hack waiting for a true rendering system.
  void SetShadowMap(const unsigned int texture, const glm::mat4& mat);

  
} // Soleil

#endif /* SOLEIL__DRAW_H_ */
