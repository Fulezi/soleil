/*
 * Copyright (C) 2017  Florian GOLESTIN
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <cstddef>

#include "Mesh.h"

#include "Logger.h"
#include "TypesToOStream.h"
#include <glm/gtc/type_ptr.hpp>

namespace Soleil {
  Mesh::Mesh(std::vector<Vertex> vertices, std::vector<unsigned int> indices,
             Material material)
    : numIndices(indices.size())
    , material(material)
  {
    glGenVertexArrays(1, &vertexArray);
    glGenBuffers(1, &vertexBuffer);
    glGenBuffers(1, &elementBuffer);

    glBindVertexArray(vertexArray);
    glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer);
    glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(Vertex),
                 &vertices[0], GL_STATIC_DRAW);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, elementBuffer);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(unsigned int),
                 &indices[0], GL_STATIC_DRAW);

    // Vertex
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex),
                          reinterpret_cast<GLvoid*>(0));
    // Normal
    glEnableVertexAttribArray(1);
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex),
                          reinterpret_cast<GLvoid*>(offsetof(Vertex, normal)));
    // Texture
    glEnableVertexAttribArray(2);
    glVertexAttribPointer(
      2, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex),
      reinterpret_cast<GLvoid*>(offsetof(Vertex, textureCoords)));
  }

  void Mesh::draw(Frame& frame, const MeshShader& config,
                  const glm::mat4& transformation)
  {

  }

  void setupLightUniform(const Light& light, const MeshShader& shaderConfig,
                         const glm::vec3& eyeDirection, unsigned int pos)
  {
    //const glm::vec3 lightDirection(1, 1, 0);
    glUniform1i(shaderConfig.lights[pos].isLocal, light.isLocal);
    glUniform1i(shaderConfig.lights[pos].isSpot, light.isSpot);

    glUniform3fv(shaderConfig.lights[pos].ambiant, 1,
                 glm::value_ptr(light.ambiant));
    glUniform3fv(shaderConfig.lights[pos].color, 1,
                 glm::value_ptr(light.color));
    glUniform3fv(shaderConfig.lights[pos].position, 1,
                 glm::value_ptr(light.position));

    if (light.isLocal) {
      // glUniform3fv(shaderConfig.lights[pos].halfVector, 1,
      //              glm::value_ptr(normalize(light.position + eyeDirection)));

      glUniform1f(shaderConfig.lights[pos].constantAttenuation,
                  light.constantAttenuation);
      glUniform1f(shaderConfig.lights[pos].linearAttenuation,
                  light.linearAttenuation);
      glUniform1f(shaderConfig.lights[pos].quadraticAttenuation,
                  light.quadraticAttenuation);
      if (light.isSpot) {
        assert(false && "// TODO: Light Spot");
      }

    } else {
      // Directional Light
      // glUniform3fv(shaderConfig.lights[pos].halfVector, 1,
      //              glm::value_ptr(normalize(lightDirection + eyeDirection)));
      glUniform3fv(shaderConfig.lights[pos].halfVector, 1,
                   glm::value_ptr(normalize(light.position + eyeDirection)));
    }
  }

  void MeshShader::initialize(const Program& meshShader)
  {
    program             = meshShader.program;
    ModelViewProjection = meshShader.getUniform("ModelViewProjection");
    ModelView           = meshShader.getUniform("ModelView");
    // NormalMatrix        = meshShader.getUniform("NormalMatrix");
    // EyeDirection        = meshShader.getUniform("EyeDirection");
    materialEmissive       = meshShader.getUniform("material.emissive");
    materialAmbiant        = meshShader.getUniform("material.ambiant");
    materialDiffuse        = meshShader.getUniform("material.diffuse");
    materialSpecular       = meshShader.getUniform("material.specular");
    materialShininess      = meshShader.getUniform("material.shininess");
    materialOpacity        = meshShader.getUniform("material.opacity");
    materialDiffuseTexture = meshShader.getUniform("material.diffuseTexture");

    numberOfLight = meshShader.getUniform("numberOfLight");
    for (int i = 0; i < Soleil::MaxLight; ++i) {
      lights[i].isLocal =
        meshShader.getUniform(toString("lights[", i, "].isLocal").data());
      lights[i].isSpot =
        meshShader.getUniform(toString("lights[", i, "].isSpot").data());
      lights[i].ambiant =
        meshShader.getUniform(toString("lights[", i, "].ambiant").data());
      lights[i].color =
        meshShader.getUniform(toString("lights[", i, "].color").data());
      lights[i].position =
        meshShader.getUniform(toString("lights[", i, "].position").data());
      lights[i].halfVector =
        meshShader.getUniform(toString("lights[", i, "].halfVector").data());
      lights[i].spotConeDirection = meshShader.getUniform(
        toString("lights[", i, "].spotConeDirection").data());
      lights[i].spotCosCutOff =
        meshShader.getUniform(toString("lights[", i, "].spotCosCutOff").data());
      lights[i].spotExponent =
        meshShader.getUniform(toString("lights[", i, "].spotExponent").data());
      lights[i].constantAttenuation = meshShader.getUniform(
        toString("lights[", i, "].constantAttenuation").data());
      lights[i].linearAttenuation = meshShader.getUniform(
        toString("lights[", i, "].linearAttenuation").data());
      lights[i].quadraticAttenuation = meshShader.getUniform(
        toString("lights[", i, "].quadraticAttenuation").data());
    }

    shadowMap = meshShader.getUniform("shadowMap");
    LightSpace = meshShader.getUniform("LightSpace");
    Model = meshShader.getUniform("Model");

    
    throwOnGlError();
  }

} // Soleil
