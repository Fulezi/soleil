/*
 * Copyright (C) 2018  Florian GOLESTIN
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#pragma once

#include <Logger.h>
#include <stringutils.h>


#ifndef SOLEIL__RUNTIME_ERROR
/** Macro that will raise an error to the user (game designer) but not stop the
 * program.
 *
 * The behavior is overidable by the user by defining the macro.
 */
#define SOLEIL__RUNTIME_ERROR(errorstr)                                        \
  ::Soleil::Logger::error(                                                     \
    ::Soleil::toString(__FILE__, "(", __LINE__, "): ", errorstr))
#endif

#ifndef SOLEIL_TERMINATE_ERROR
#include <stdexcept>
/** Macro that will raise an error to the user (the game designed in development
 * or the user while the product is released) and stop the program, or at least
 * fall back in a secure mode.
 *
 * The behavior is overidable by the user by defining the macro.
 */
#define SOLEIL_TERMINATE_ERROR(errorstr)                                       \
  throw std::runtime_error(                                                    \
    ::Soleil::toString(__FILE__, "(", __LINE__, "): ", errorstr))
#endif
