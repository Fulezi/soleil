/*
 * Copyright (C) 2019  Florian GOLESTIN
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <UITextSystem.h>

#include <Assets.h>
#include <Errors.h>
#include <Font.h>
#include <Logger.h>
#include <OpenGLInclude.h>
#include <TypesToOStream.h>
#include <glm/gtc/type_ptr.hpp>
#include <memory>
#include <zinc/Zinc.h>

namespace Soleil {

  struct UIVertex
  {
    glm::vec2 pos;
    glm::vec2 tex;
  };

  UIText::UIText(Font* font, const std::string& text)
    : font(font)
  {
    glGenVertexArrays(1, &vertexArray);
    glGenBuffers(1, &vertexBuffer);
    throwOnGlError();

    setText(text);
  }

  void UIText::setText(const std::string& text)
  {
    glm::vec2 size;

    std::vector<TriGlyph> line =
      font->writeLine(text, size, textScale, width - (margin.right + margin.left));
      
    std::vector<UIVertex> vertices;
    numVertices = 6 + line.size();
    vertices.reserve(numVertices);

    vertices.push_back(
      {glm::vec2(position.x, position.y + height), glm::vec2(+0, +0)});
    vertices.push_back(
      {glm::vec2(position.x + width, position.y + height), glm::vec2(+1, +0)});
    vertices.push_back({glm::vec2(position.x, position.y), glm::vec2(+0, +1)});
    vertices.push_back(
      {glm::vec2(position.x + width, position.y + height), glm::vec2(+1, +0)});
    vertices.push_back(
      {glm::vec2(position.x + width, position.y), glm::vec2(+1, +1)});
    vertices.push_back({glm::vec2(position.x, position.y), glm::vec2(+0, +1)});

    // Spread a text in a text-box
    for (const auto& glyph : line) {
      const glm::vec2 adv = margin.upperLeft() + glyph.advance;

      // We subtract glm::vec2(1.0f, 0.0f) because line glyphs go form 0 to x,
      // while x screen go from -1 to 1.
      //vertices.push_back({adv - (glm::vec2(1.0f, 0.0f), glyph.tex});
      const glm::vec2 pos(position.x, -1.0 - position.y);
      vertices.push_back({(pos + adv), glyph.tex});

      vertices.back().pos.y = -vertices.back().pos.y;
    }

    assert(vertices.size() == numVertices);

    glBindVertexArray(vertexArray);
    glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer);
    // glBufferData(GL_ARRAY_BUFFER, (vertices + glyphs) vertices.size() *
    // sizeof(glm::vec2), vertices.data(),
    glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(UIVertex),
                 vertices.data(), GL_DYNAMIC_DRAW);
    throwOnGlError();

    // Vertices
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, sizeof(UIVertex),
                          reinterpret_cast<GLvoid*>(0));
    // Textures
    glEnableVertexAttribArray(1);
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(UIVertex),
                          reinterpret_cast<GLvoid*>(offsetof(UIVertex, tex)));
    throwOnGlError();
  }

  void UIText::draw(Frame& frame, const float deltaTime)
  {
    glBindVertexArray(vertexArray);

    // glBindTexture(GL_TEXTURE_2D, font->texture);
    // glBindTexture(GL_TEXTURE_2D,
    //               Assets::GetTexture("zinc/textures/printpaper.png"));
    // throwOnGlError();
    // glBindTexture(GL_TEXTURE_2D, font->texture);
    // throwOnGlError();

    glDisable(GL_DEPTH_TEST);
    glDrawArrays(GL_TRIANGLES, 0, numVertices);
    glEnable(GL_DEPTH_TEST);

    throwOnGlError();
  }

  //////////////////////////////////////////////////////////////////////////////
  // UITextSystem

  UITextSystem::UITextSystem()
  {
    program.attachShader(GL_VERTEX_SHADER, "ui-text.vert");
    program.attachShader(GL_FRAGMENT_SHADER, "ui-text.frag");
    program.compile();
  }

  UIText* UITextSystem::create(Font* font, const std::string& text)
  {
    texts.emplace_back(std::make_unique<UIText>(font, text));
    return texts.back().get();
  }

  void UITextSystem::remove(UIText* text)
  {
    for (auto it = texts.begin(); it < texts.end(); ++it) {
      if (it->get() == text) {
        texts.erase(it);
        return;
      }
    }
    SOLEIL__RUNTIME_ERROR("UIText not found");
  }

  void UITextSystem::draw(Frame& frame, const float deltaTime)
  {
    glUseProgram(program.program);

    // glBindTexture(GL_TEXTURE_2D,
    //               Assets::GetTexture("zinc/textures/printpaper.png"));
    // throwOnGlError();

    for (auto const& text : texts) {
      glUniformMatrix4fv(program.getUniform("Transform"), 1, GL_FALSE,
                         glm::value_ptr(text->transform));

      assert(text->backgroundTexture > 0);
      
      glUniform1i(program.getUniform("backgroundSampler"), 0);
      glActiveTexture(GL_TEXTURE0);
      glBindTexture(GL_TEXTURE_2D, text->backgroundTexture);
      throwOnGlError();

      glUniform1i(program.getUniform("font"), 1);
      glActiveTexture(GL_TEXTURE1);
      // TODO: Not one font can exists
      glBindTexture(GL_TEXTURE_2D, Zinc::GetUIFont()->texture);
      throwOnGlError();

      text->draw(frame, deltaTime);
    }
  }

} // namespace Soleil
