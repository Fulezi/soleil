

#include "Inputs.h"

#include <memory>

#include "Logger.h"
#include <Errors.h>

#define GLM_ENABLE_EXPERIMENTAL 1
#include <glm/gtx/string_cast.hpp>

namespace Soleil {

  Inputs::Inputs()
    : offsetCursorPosition(0, 0)
    , cursorPosition(0, 0)
    , keys{false}
  {}

  glm::vec2 Inputs::getMouseOffsetMove(void) const noexcept
  {
    return offsetCursorPosition;
  }

  glm::vec2 Inputs::getCursorPosition(void) const noexcept
  {
    return cursorPosition;
  }
  void Inputs::setCursorPosition(glm::vec2 position)
  {
    offsetCursorPosition = cursorPosition - position;
    // SOLEIL__LOGGER_DEBUG("offsetCursorPosition = cursorPosition -
    // position\t",
    //                      glm::to_string(offsetCursorPosition), "=",
    //                      glm::to_string(cursorPosition), "-",
    //                      glm::to_string(position));
    cursorPosition = position;
  }

  void Inputs::setMouseButtonPressed(int button, bool pressed)
  {
    // assert(button < 3);
    if (button >= MouseMaximumInputButton) {
      return; // So far we do limit to only the three first buttons
    }
    const bool wasPressed = this->mouseButtons[button].pressed;
    this->mouseButtons[button].justReleased =
      pressed == false && wasPressed == true;

    this->mouseButtons[button].pressed     = pressed;
    this->mouseButtons[button].justPressed = pressed;
  }

  const MouseButton& Inputs::getMouseButton(int button) const noexcept
  {
    if (button >= MouseMaximumInputButton) {
      assert(false);
      return mouseButtons[0]; // So far we do limit to only the three first
                              // buttons
    }
    return this->mouseButtons[button];
  }

  void Inputs::setMouseScroll(glm::dvec2 const& offset)
  {
    this->scrollOffset = offset;
  }

  glm::dvec2 const& Inputs::getMouseScroll(void) const noexcept
  {
    return scrollOffset;
  }

  float Inputs::getKeyPress(Key key)
  {
    if (key >= KeyboardMaximumInputKeys) {
      SOLEIL__RUNTIME_ERROR("Requested key out of bounds");
      return 0.0f;
    }

    if (keys[key]) { return 1.0f; }

    return 0.0f;
  }

  bool Inputs::translateKey(int key, Key& out)
  {
    auto it = keymap.find(key);
    if (it != keymap.end()) {
      out = it->second;
      return true;
    }

    return false;
  }

  void Inputs::updateKeyState(Key key, KeyState state) { keys[key] = state; }

  void Inputs::update()
  {
    // Remove the just pressed button flag
    for (int button = 0; button < MouseMaximumInputButton; ++button) {
      // if (this->mouseButtons[button].justPressed) {
      this->mouseButtons[button].justPressed  = false;
      this->mouseButtons[button].justReleased = false;
      //}
    }

    // Remove the scroll offset
    scrollOffset = glm::dvec2(0.0);
  }

  /* --------------------- */
  /* --- Main instance --- */

  static std::unique_ptr<Inputs> mainInputs;

  void Inputs::Init(KeyMap keymap)
  {
    mainInputs         = std::make_unique<Inputs>();
    mainInputs->keymap = keymap;
  }

  void Inputs::Destroy() { mainInputs = nullptr; }

  void Inputs::Update() { mainInputs->update(); }

  glm::vec2 Inputs::GetCursorPosition(void) noexcept
  {
    assert(mainInputs && "Inputs not initialized yet");

    return mainInputs->getCursorPosition();
  }

  glm::vec2 Inputs::GetMouseOffsetMove(void) noexcept
  {
    assert(mainInputs && "Inputs not initialized yet");

    return mainInputs->getMouseOffsetMove();
  }

  void Inputs::SetCursorPosition(glm::vec2 position)
  {
    assert(mainInputs && "Inputs not initialized yet");

    mainInputs->setCursorPosition(position);
  }

  void Inputs::SetMouseButtonPressed(int button, bool pressed)
  {
    mainInputs->setMouseButtonPressed(button, pressed);
  }

  const MouseButton& Inputs::GetMouseButton(int button)
  {
    return mainInputs->getMouseButton(button);
  }

  void Inputs::SetMouseScroll(glm::dvec2 const& offset)
  {
    mainInputs->setMouseScroll(offset);
  }

  glm::dvec2 const& Inputs::GetMouseScroll(void) noexcept
  {
    return mainInputs->getMouseScroll();
  }

  float Inputs::GetKeyPress(Key key) { return mainInputs->getKeyPress(key); }

  bool Inputs::TranslateKey(int key, Key& out)
  {
    return mainInputs->translateKey(key, out);
  }

  void Inputs::UpdateKeyState(Key key, KeyState state)
  {
    mainInputs->updateKeyState(key, state);
  }

} // namespace Soleil
