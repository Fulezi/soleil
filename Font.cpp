/*
 * Copyright (C) 2018  Florian GOLESTIN
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <Font.h>

#define STB_RECT_PACK_IMPLEMENTATION
#define STBRP_STATIC
#include <assets/stb_rect_pack.h>

#define STB_TRUETYPE_IMPLEMENTATION
#define STBTT_STATIC
#include <assets/stb_truetype.h>

#include <Assets.h>
#include <Errors.h>

namespace Soleil {
  std::unique_ptr<Font> Font::CreateWithDefault(const std::string& path)
  {
    std::unique_ptr<Font> font = std::make_unique<Font>();

    const int numCharToPack = 96; // ASCII 32..126 is 95 glyphs

    font->cdata.resize(numCharToPack);

    std::string                fontdata = Assets::AsString(path);
    std::vector<unsigned char> bitmap;

    bitmap.resize(font->width * font->height);

    // TODO: Read from the AssetManager
    unsigned char ttf_buffer[1 << 20];
    fread(ttf_buffer, 1, 1 << 20, fopen(Assets::GetPath(path).c_str(), "rb"));

    stbtt_pack_context spc;
    {
      int err = stbtt_PackBegin(&spc, bitmap.data(), font->width, font->height,
                                0, 1, nullptr);
      if (err == 0) { SOLEIL__RUNTIME_ERROR("Failed to start packing"); }
    }

    {
      //stbtt_PackSetOversampling(&spc, 2, 2);
      
      int err = stbtt_PackFontRange(&spc, ttf_buffer, 0, font->fontSize,
                                    32 /*FirstUnicode*/, numCharToPack,
                                    font->cdata.data());
      if (err == 0) { SOLEIL__RUNTIME_ERROR("Failed to pack the text"); }
    }

    stbtt_InitFont(&font->info, ttf_buffer, 0);

    glGenTextures(1, &font->texture);
    glBindTexture(GL_TEXTURE_2D, font->texture);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RED, font->width, font->height, 0, GL_RED,
                 GL_UNSIGNED_BYTE, bitmap.data());
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    throwOnGlError();

    stbtt_PackEnd(&spc);

    return font;
  }

  std::vector<TriGlyph> Font::writeLine(std::string const& line,
                                        glm::vec2& size, const float textScale,
                                        float widthMax)
  {
    std::vector<TriGlyph> ret;
    ret.reserve(line.size() * 6);

    // Get the bounds and scale of our font.
    float scale = stbtt_ScaleForPixelHeight(&info, fontSize);
    int   x0, y0, x1, y1;
    stbtt_GetFontBoundingBox(&info, &x0, &y0, &x1, &y1);

    size = glm::vec2(0.0f);

    glm::vec2 accum(0.0f); // The moving cursor while writing.

    // Cursor to the last space on this line (0 means no space).
    std::size_t lastSpaceCursor = 0;
    std::size_t lastSpaceVertex;
    glm::vec2   lastSpaceAccum;
    glm::vec2   lastSpaceSize;

    // Produce a "new line + return carriage"
    auto breakLine = [&accum, &lastSpaceCursor, y0, y1, scale]() mutable {
      accum.x = 0.0f;
      accum.y += (y0 - y1) * scale;
      lastSpaceCursor = 0;
    };

    auto computeCharVertex = [&accum, textScale](stbtt_aligned_quad const& q,
                                                 const int i) mutable {
      constexpr glm::ivec2 UVs[6] = {
        glm::ivec2(0, 0), glm::ivec2(1, 0), glm::ivec2(0, 1), // triangle 1
        glm::ivec2(1, 0), glm::ivec2(1, 1), glm::ivec2(0, 1)  // triangle 2
      };

      glm::vec2 uv;
      glm::vec2 adv;

      if (UVs[i].x == 0) {
        uv.s  = q.s0;
        adv.x = accum.x + q.x0;
      } else {
        uv.s  = q.s1;
        adv.x = accum.x + q.x1;
      }

      if (UVs[i].y == 0) {
        uv.t  = q.t1;
        adv.y = q.y1 + accum.y;
      } else {
        uv.t  = q.t0;
        adv.y = q.y0 + accum.y;
      }
      return TriGlyph{adv * textScale, uv};
    };

    for (std::size_t currentChar = 0; currentChar < line.size();
         ++currentChar) {
      const char         c = line[currentChar];
      float              x = 0.0f;
      float              y = 0.0f;
      stbtt_aligned_quad q;

      if (c == '\n') {
        breakLine();
        continue;
      }

      stbtt_GetPackedQuad(
        cdata.data(), width, height, // same data as above
        c - 32,                      // character to display
        &x, &y, // pointers to current position in screen pixel space
        &q,     // output: quad to draw
        1);

      if (c == ' ') {
        accum.x += x;
        accum.y += y;
        lastSpaceCursor = currentChar;
        lastSpaceAccum  = accum;
        lastSpaceSize   = size;
        lastSpaceVertex = ret.size();
        continue;
      }

      if ((currentChar > 0) // We can't break on the first char
          && (widthMax > 0) // We don't break if user don't want to
          && ((accum.x + x) * textScale > widthMax)) {
        // The easiest way to break the line (surely not the most optimized one)
        // is to revert all changes since the last space (remove pushed vertices
        // and accumulated advance). Then do it again.
        if (lastSpaceCursor > 1) {
          currentChar = lastSpaceCursor;
          ret.erase(ret.begin() + lastSpaceVertex, ret.end());

          // BreakLine overwrite our lastSpaceCursor, so do it last then
          // re-start the loop.
          breakLine();
          continue;
        }

        // Else, if we have no space to fallback, just break the line here.
        breakLine();
      }

      // Compute our two triangles (six vertices)
      for (int i = 0; i < 6; ++i) {
        ret.push_back(computeCharVertex(q, i));
        size.x = std::max(size.x, ret.back().advance.x);
        size.y = std::max(size.y, ret.back().advance.y);
      }

      accum.x += x;
      accum.y += y;
    }

    return ret;
  }

  void Font::release() { glDeleteTextures(1, &texture); }

} // namespace Soleil
