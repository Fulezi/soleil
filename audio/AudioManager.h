/*
 * Copyright (C) 2018  Florian GOLESTIN
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#pragma once

// TODO: ObjectID in its own (GUID) header

#include <Node.h> // For ObjectID

namespace Soleil {

  /**
   * AudioManager to play sound and music
   *
   * A 'Sound' is fully decompressed and loaded in memory while a 'Music' is
   * kept compressed. Sounds and music can be located in the world space.
   */
  class AudioManager
  {
  public:
    // AudioManager();
    // ~AudioManager();

    struct SoundSlot
    {
      std::size_t position = -1;
      int         soundInstanceID = -1;

      SoundSlot() = default;
      SoundSlot(const std::size_t position, const int soundInstanceID)
	: position(position)
	, soundInstanceID(soundInstanceID)
      {}
    };

    struct SoundProperties
    {
      glm::vec3 position; ///< World Position, ignored if ambient == true
      bool      repeat;   ///< If true the sound will rewind and play again
      bool      ambient;  ///< If true, ti will ignore the position
      float     volume;   ///< 1.0f Means normal. 2.0f Is twice the volume

      SoundProperties(glm::vec3 const& pos, const float volume = 1.0f)
        : position(pos)
        , repeat(false)
        , ambient(false)
        , volume(volume)
      {}

      static SoundProperties Ambient(bool repeat = false)
      {
        SoundProperties s(glm::vec3(0.0f));
        s.repeat  = repeat;
        s.ambient = true;

        return s;
      }
    };

    struct MusicProperties
    {
      float volume;

      MusicProperties(float volume = 0.1f)
        : volume(volume)
      {}
    };

  public:
    static void Init();
    static void Destroy();
    static void Process(const float dt);

    static void      LoadAndRegisterSound(ObjectID           soundID,
                                          const std::string& sound);
    static SoundSlot PlaySound(ObjectID              soundID,
                               SoundProperties const properties);
    static void      StopSound(SoundSlot slot);

    static void SetListenerPosition(glm::vec3 const& position,
                                    glm::vec3 const& direction);

    static void LoadAndRegisterMusic(ObjectID           musicID,
                                     const std::string& sound);
    static void PlayMusic(ObjectID musicID, MusicProperties const properties);
    static void StopMusic();
  };
} // namespace Soleil
