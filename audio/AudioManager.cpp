/*
 * Copyright (C) 2018  Florian GOLESTIN
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <audio/AudioManager.h>

#include <Assets.h>
#include <Logger.h>
#include <portaudio.h>
#include <stdexcept>
#include <stringutils.h>

#include <Errors.h>
#include <array>
#include <cerrno>
#include <cstring>
#include <deque>
#include <map>
#include <memory>
#include <vector>

#include <audio/stb_vorbis.h>

namespace Soleil {

  struct PlayingSound
  {
    // constants
    float const*                  buffer;
    std::size_t                   length;
    AudioManager::SoundProperties properties;

    // dynamics
    /// Current position to play, std::size_t(-1) for a non playing sound.
    std::size_t cursor;

    /// An ID to make be compared with SoundSlot (to make sure we stop,edit the
    /// correct sound and not a finished/replaced one.
    int soundInstanceID;

    PlayingSound(std::vector<float> const&     data,
                 AudioManager::SoundProperties properties)
      : buffer(data.data())
      , length(data.size())
      , properties(properties)
      , cursor(0)
    {}

    PlayingSound()
      : buffer(nullptr)
      , length(0)
      , properties(glm::vec3(0.0f))
      , cursor(0)
    {}
  };

  class AudioManagerImpl
  {
  public:
    typedef std::vector<float>      SoundBuffer;    ///< Raw sound buffers
    std::map<ObjectID, SoundBuffer> soundBufferMap; ///< ID assocciation
    std::array<PlayingSound, 32>    playingSound;   ///< All playing sound
    int nextSoundInstanceID = 0; ///< The next id to assign to a SoundSlot

    bool destroying = false;

    // Music Data
    std::map<ObjectID, stb_vorbis*> loadedMusics; ///< All musics ready to play
    stb_vorbis* currentMusic = nullptr; ///< Current playing one or null
    AudioManager::MusicProperties musicProperties;
    SoundBuffer                   currentMusicBuffer;

  public: // Listener
    glm::vec3 listenerPosition;
    glm::vec3 listenerDirection;
    glm::vec3 listenerRight;
  };

  /**
   * Returns string related to some of the known error of stb_vorbis.
   */
  static std::string VORBIS_my_error(const int error)
  {
    switch (error) {
      case VORBIS__no_error: return "No error";
      case VORBIS_outofmem: return "Out Of Memory";
      case VORBIS_feature_not_supported: return "Feature not supported";
      case VORBIS_too_many_channels: return "Too many channels";
      case VORBIS_seek_without_length:
        return "Can't seek in unknown-length file";
      case VORBIS_unexpected_eof: return "File is truncated";
      case VORBIS_seek_invalid: return "Seek past EOF";
      case VORBIS_file_open_failure:
        return toString("Failed to open file (", std::strerror(errno), ")");
      default: return "N/a";
    }
  }

  std::unique_ptr<AudioManagerImpl> s_audioManagerImpl = nullptr;

  /* This routine will be called by the PortAudio engine when audio is needed.
     It may called at interrupt level on some machines so don't do anything
     that could mess up the system like calling malloc() or free().
  */
  static int patestCallback(const void* inputBuffer, void* outputBuffer,
                            unsigned long                   framesPerBuffer,
                            const PaStreamCallbackTimeInfo* timeInfo,
                            PaStreamCallbackFlags statusFlags, void* userData)
  {
    float* out = (float*)outputBuffer;
    /* Prevent unused variable warning. */
    (void)inputBuffer;
    (void)userData;
    (void)timeInfo;
    (void)statusFlags;

    if (s_audioManagerImpl.get() == nullptr || s_audioManagerImpl->destroying) {
      return paComplete;
    }

    /* So far do everything in the callback, however the decode of the music
     * should be done in the process method.
     */
    std::size_t musicSamples = std::size_t(-1);
    if (s_audioManagerImpl->currentMusic != nullptr) {
      s_audioManagerImpl->currentMusicBuffer.resize(framesPerBuffer * 2);
      musicSamples = stb_vorbis_get_samples_float_interleaved(
        s_audioManagerImpl->currentMusic, 2,
        s_audioManagerImpl->currentMusicBuffer.data(),
        s_audioManagerImpl->currentMusicBuffer.size());
    }

    /* Reduce the sound by the number of max channels that should be played
     * simultaneously. If the number of channel is higher than that, it will
     * start to clamp.
     */
    const float oneOverNumSounds = 1.0f / 8;

    /* Maximum distance from which the sound will not be audible
     */
    const float maxDistance = 200.0f;

    /* First setup the music or blank the buffer */
    out                = (float*)outputBuffer;
    const float* music = s_audioManagerImpl->currentMusicBuffer.data();
    const float  volume =
      oneOverNumSounds * s_audioManagerImpl->musicProperties.volume;
    for (unsigned long k = 0; k < framesPerBuffer; ++k) {
      if (k < musicSamples && musicSamples <= framesPerBuffer) {
        out[0] = music[0] * volume;
        out[1] = music[1] * volume;
        music += 2;
      } else {
        out[0] = 0.0f;
        out[1] = 0.0f;
      }

      out += 2;
    }

    /* Then play all the sounds FX */
    out = (float*)outputBuffer;
    for (unsigned long k = 0; k < framesPerBuffer; ++k) {

      for (PlayingSound& p : s_audioManagerImpl->playingSound) {
        if (p.cursor >= p.length) { continue; }

        float left  = p.buffer[p.cursor] * oneOverNumSounds;
        float right = left;

        if (p.properties.ambient == false) {
          /* Sound is not ambient, therefore can be positioned in space */

          const glm::vec3 ab =
            s_audioManagerImpl->listenerPosition - p.properties.position;

          /* The sound will be played with a volume between 0 if superior to
           * maxDistance and  max power if close to the listener. Then we will
           * multiply this result with the sound volume.
           */
          const float attenuation =
            (1.0f - (glm::length(ab) / maxDistance)) * p.properties.volume;
          const float angle = glm::dot(s_audioManagerImpl->listenerRight, ab);

          /*
           * Always play the sound in both channels, but play it more left or
           * right depending on the sign of the angle.
           */
          // TODO Works but could be better with a left/right factor
          left *= attenuation * ((angle <= 0.1f) ? 1.0f : 0.3f);
          right *= attenuation * ((angle >= -0.1f) ? 1.0f : 0.3f);
        } else {
          // So far don't play the sound at the max level, later we will have
          // a volume in the SoundProperties.
          left *= 0.8f;
          right *= 0.8f;
        }
        out[0] += right;
        out[1] += left;

        /* Advance the cursor and rewind it if a repeating sound reach the end
         */
        p.cursor++;
        if (p.cursor >= p.length && p.properties.repeat) { p.cursor = 0; }
      }
      out += 2;
    }

    /*
     * Traverse again the sound buffer to clamp it, Make sure no sound go
     * upper 1.0 or below -1.0f
     */
    out = (float*)outputBuffer;
    for (unsigned long k = 0; k < framesPerBuffer * 2; ++k) {
      *out = glm::clamp(*out, -0.95f, 0.95f);
      out++;
    }

    return paContinue;
  }

#define SAMPLE_RATE (44100)
  static PaStream* stream;

  //////////////////////////////////////////////////////////////////////

  void AudioManager::Init()
  {
    s_audioManagerImpl = std::make_unique<AudioManagerImpl>();

    // We initialize PortAudio here as it may only have one instance. We
    // should be able to create and delete ultiple times the AudioManager but
    // without touching at PortAudio.
    const PaError err = Pa_Initialize();
    if (err != paNoError) {
      throw std::runtime_error(
        toString("Failed to initialize PortAudio: ", Pa_GetErrorText(err)));
    }
    SOLEIL__LOGGER_DEBUG("PortAudio (", Pa_GetVersionText(),
                         ") and AudioManager Initialized");

    // First Query all the streams
    {
      int numDevices;
      numDevices = Pa_GetDeviceCount();
      if (numDevices < 0) {
        throw std::runtime_error(
          toString("Failed to get the devices count: ", Pa_GetErrorText(err)));
      } else if (numDevices == 0) {
        Logger::error("Portaudio no device found!");
      }

      for (int i = 0; i < numDevices; i++) {
        const PaDeviceInfo* deviceInfo = Pa_GetDeviceInfo(i);

        Logger::info(
          toString("Name: ", deviceInfo->name,
                   ", Max output channels: ", deviceInfo->maxOutputChannels));
      }
    }

    // Open Stream
    {
      /* Open an audio I/O stream. */
      const PaError err = Pa_OpenDefaultStream(
        &stream, 0, /* no input channels */
        2,          /* stereo output */
        paFloat32,  /* 32 bit floating point output */
        SAMPLE_RATE, paFramesPerBufferUnspecified, /* frames per
                                 buffer, i.e. the number of sample frames that
                                 PortAudio will request from the callback.
                                 Many apps may want to use
                                 paFramesPerBufferUnspecified, which
                                 tells PortAudio to pick the best,
                                 possibly changing, buffer size.*/
        patestCallback, /* this is your callback function */
        nullptr);       /*This is a pointer that will be passed to
                                your callback*/
      if (err != paNoError) {
        // throw std::runtime_error(toString(
        //   "Failed to Open the PortAudio stream: ", Pa_GetErrorText(err)));
        Soleil::Logger::error(
          toString("Failed to start the stream: ", Pa_GetErrorText(err)));
        return;
      }
    }

    // Play Stream
    {
      const PaError err = Pa_StartStream(stream);
      if (err != paNoError) {
        throw std::runtime_error(
          toString("Failed to start the stream: ", Pa_GetErrorText(err)));
        // Soleil::Logger::error(toString("Failed to start the stream: ",
        // Pa_GetErrorText(err)));
      }
    }
  }

  void AudioManager::Destroy()
  {
    SOLEIL__LOGGER_DEBUG(" START terminating PortAudio and AudioManager...");

    s_audioManagerImpl->destroying = true;

    // Stop the stream
    {
      Pa_AbortStream(stream);
      const PaError err = Pa_CloseStream(stream);
      if (err != paNoError) {
        Soleil::Logger::error(toString("Failed to close the PortAudio stream: ",
                                       Pa_GetErrorText(err)));
      } else {
        SOLEIL__LOGGER_DEBUG("\tStream closed");
      }
    }

    // Terminate PortAudio
    const PaError err = Pa_Terminate();
    if (err != paNoError) {
      Soleil::Logger::error(
        toString("Failed to Terminate PortAudio: ", Pa_GetErrorText(err)));
    } else {
      SOLEIL__LOGGER_DEBUG("\tPortAudio terminated");
    }

    // Remove all the opened Vorbis
    for (auto music : s_audioManagerImpl->loadedMusics) {
      stb_vorbis_close(music.second);
    }

    SOLEIL__LOGGER_DEBUG("PortAudio and AudioManager Terminated");
  }

  void AudioManager::Process(const float dt) { (void)dt; }

  void AudioManager::LoadAndRegisterSound(ObjectID           soundID,
                                          const std::string& sound)
  {
    int              error = 0;
    stb_vorbis_alloc alloc{nullptr, 0};
    stb_vorbis*      soundBuffer =
      stb_vorbis_open_filename(Assets::GetPath(sound).c_str(), &error, &alloc);
    if (soundBuffer == nullptr) {
      SOLEIL__RUNTIME_ERROR(
        toString("Cannot open ogg file: ", VORBIS_my_error(error)));
      return;
    }

    /* For sounds we are going to play only one channel, this will allow us to
     * fake stereo.
     */
    constexpr int AudioNumChannels = 1;

    const unsigned int nsamples =
      stb_vorbis_stream_length_in_samples(soundBuffer);
    const unsigned int numberOfFloats = nsamples * AudioNumChannels;

    AudioManagerImpl::SoundBuffer buffer;
    buffer.resize(numberOfFloats, 0.0f);

    // int c     = 0;
    // int num_c = 0;
    // while ((n = stb_vorbis_get_frame_float(soundBuffer, &num_c, &outputs))
    // >
    //        0) {
    //   memcpy(&b[c], outputs[0], sizeof(float) * n);
    //   c += n;
    // }
    // outputs[0] = b;
    // outputs[1] = b;
    // n          = nsamples;
    // const int remainingSamples =
    stb_vorbis_get_samples_float_interleaved(soundBuffer, AudioNumChannels,
                                             buffer.data(), numberOfFloats);
    // TODO: Check if remainingSamples returns indeed the number of red sample
    // and then 0 on the second read

    // If everything is done emplace the buffer.
    s_audioManagerImpl->soundBufferMap.emplace(soundID, buffer);
    stb_vorbis_close(soundBuffer);
  }

  AudioManager::SoundSlot AudioManager::PlaySound(
    ObjectID soundID, SoundProperties const properties)
  {
    PlayingSound* ptr = nullptr;
    std::size_t   i   = 0;
    for (PlayingSound& ps : s_audioManagerImpl->playingSound) {
      if (ps.cursor >= ps.length) {
        ptr                  = &ps;
        ptr->soundInstanceID = s_audioManagerImpl->nextSoundInstanceID;
        s_audioManagerImpl->nextSoundInstanceID++;
        break;
      }
      i++;
    }
    if (ptr == nullptr) {
      SOLEIL__RUNTIME_ERROR(toString("No more buffer to play sound. Maximum=",
                                     s_audioManagerImpl->playingSound.size()));
      return AudioManager::SoundSlot();
    }

    auto const& sound = s_audioManagerImpl->soundBufferMap.find(soundID);

    if (sound == s_audioManagerImpl->soundBufferMap.end()) {
      SOLEIL__RUNTIME_ERROR(
        toString("Sound with following ID was not loaded: ", soundID));
      return AudioManager::SoundSlot();
    }

    *ptr = PlayingSound(sound->second, properties);
    return AudioManager::SoundSlot(i, ptr->soundInstanceID);
  }

  void AudioManager::StopSound(SoundSlot slot)
  {
    if (slot.position == std::size_t(-1)) {
      SOLEIL__LOGGER_DEBUG("Trying to stop an invalid sound");
      return;
    }

    if (slot.position > s_audioManagerImpl->playingSound.size()) {
      SOLEIL__RUNTIME_ERROR("No sound to stop at this index");
      return;
    }

    auto& sound = s_audioManagerImpl->playingSound[slot.position];
    if (sound.soundInstanceID != slot.soundInstanceID) {
      SOLEIL__LOGGER_DEBUG("Sound already finished and replaced");
      return;
    }

    sound.cursor = -1;
  }

  void AudioManager::SetListenerPosition(glm::vec3 const& position,
                                         glm::vec3 const& direction)
  {
    s_audioManagerImpl->listenerPosition  = position;
    s_audioManagerImpl->listenerDirection = direction;
    s_audioManagerImpl->listenerRight =
      glm::cross(s_audioManagerImpl->listenerDirection, glm::vec3(0, 1, 0));
  }

  void AudioManager::LoadAndRegisterMusic(ObjectID           musicID,
                                          const std::string& musicFileName)
  {
    /* Register a stb stream and a buffer, decode it on the main thread (or
     * another one, not the callback.
     */
    int              error = 0;
    stb_vorbis_alloc alloc{nullptr, 0};
    stb_vorbis*      soundBuffer = stb_vorbis_open_filename(
      Assets::GetPath(musicFileName).c_str(), &error, &alloc);
    if (soundBuffer == nullptr) {
      SOLEIL__RUNTIME_ERROR(toString("Cannot open ogg file: ", error));
      return;
    }

    s_audioManagerImpl->loadedMusics.emplace(musicID, soundBuffer);
    SOLEIL__LOGGER_DEBUG("Loaded music: ", musicFileName);
  }

  void AudioManager::PlayMusic(ObjectID              musicID,
                               MusicProperties const properties)
  {
    /*assert((s_audioManagerImpl->loadedMusics.find(musicID) !=
                    s_audioManagerImpl->loadedMusics.end()) &&
                   "Music not loaded");*/
    if (s_audioManagerImpl->loadedMusics.find(musicID) ==
        s_audioManagerImpl->loadedMusics.end()) {
      SOLEIL__RUNTIME_ERROR(
        toString("Cannot play sound: ", musicID,
                 ". Media not loaded or Sound system not initailized."));
    }

    s_audioManagerImpl->currentMusic =
      s_audioManagerImpl->loadedMusics[musicID];
    s_audioManagerImpl->musicProperties = properties;
  }

  void AudioManager::StopMusic()
  {
    if (s_audioManagerImpl->currentMusic == nullptr) { return; }

    stb_vorbis_seek_start(s_audioManagerImpl->currentMusic);
    s_audioManagerImpl->currentMusic = nullptr;
  }

} // namespace Soleil
