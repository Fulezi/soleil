/*
 * Copyright (C) 2018  Florian GOLESTIN
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <zinc/ShootTracer.h>

#include <Assets.h>
#include <cassert>
#include <glm/geometric.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <vector>

#define GLM_ENABLE_EXPERIMENTAL
#include "glm/ext.hpp"
#include "glm/gtx/string_cast.hpp"
#include <memory>

namespace Soleil {

  ShootTracer::ShootTracer(const int numberOfPoints, const float width,
                           const float timeToLive)
    : numberOfPoints(numberOfPoints)
    , halfWidth(width / 2.0f)
    , timeToLive(timeToLive)
    , trailBuffer()
  {
    // --- Initialize the trail --------------------------------------
    const glm::vec3 origin(halfWidth, 0.0f, 0.0f);
    // const osg::Vec3 normal(0.0f, 0.0f, 1.0f);
    assert(numberOfPoints > 2 && "Cannot have less than two point");
    assert(((numberOfPoints % 2) == 0) && "Number of points must be even");

    vertices.resize(numberOfPoints);
    for (int i = 0; i < numberOfPoints - 1; i += 2) {
      vertices[i]     = origin;
      vertices[i + 1] = -origin;

      // (*normals)[i] = normal;
      // (*normals)[i + 1] = normal;
      // // TODO: Use easing function is necessary:
      // const float alpha =
      //     1.0f - (numberOfPoints - i) / static_cast<float>(numberOfPoints);
      // (*colors)[i] = osg::Vec4(color, alpha);
      // (*colors)[i + 1] = osg::Vec4(color, alpha);
    }
    // TODO: One buffer for all

    // --- Initialize graphic resources ------------------------------
    vao.bind();

    // Prepare the buffer
    trailBuffer.bind(GL_ARRAY_BUFFER);
    {
      glBufferData(GL_ARRAY_BUFFER, sizeof(vertices[0]) * numberOfPoints,
                   nullptr, GL_STREAM_DRAW);

      // Position
      glEnableVertexAttribArray(0);
      glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(vertices[0]),
                            nullptr);
      throwOnGlError();
    }

    // TODO: Idea: One buffer for all the trails
  }

  void ShootTracer::updateTrail(const float deltaTime)
  {
    timeToLive -= deltaTime;
    if (timeToLive <= 0.0f) { return; }

    for (int i = 0; i < numberOfPoints - 3; i += 2) {
      vertices[i]     = vertices[i + 2];
      vertices[i + 1] = vertices[i + 3];
    }

    // TODO: Conf speed
    constexpr float speed = 300.6f;
    vertices[numberOfPoints - 2] += direction * speed * deltaTime;
    vertices[numberOfPoints - 1] += direction * speed * deltaTime;

    trailBuffer.bind(GL_ARRAY_BUFFER);
    {
      // Buffer orphaning
      const std::size_t size = sizeof(vertices[0]) * numberOfPoints;
      glBufferData(GL_ARRAY_BUFFER, size, nullptr, GL_STREAM_DRAW);
      glBufferSubData(GL_ARRAY_BUFFER, 0, size, vertices.data());
      throwOnGlError();
    }
  }

  void ShootTracer::setUpTrail(const glm::vec3& position,
                               const glm::vec3& direction)
  {
    const glm::vec3 min = position + glm::vec3(halfWidth, 0.0f, 0.0f);
    const glm::vec3 max = position - glm::vec3(halfWidth, 0.0f, 0.0f);
    for (int i = 0; i < numberOfPoints - 1; i += 2) {
      vertices[i]     = min;
      vertices[i + 1] = max;
    }
    this->direction = glm::normalize(direction);
  }

  void ShootTracer::draw(Frame& frame)
  {
    (void)frame;
    vao.bind();
    throwOnGlError();
    trailBuffer.bind(GL_ARRAY_BUFFER);
    {
      glEnable(GL_DEPTH_TEST);
      // glDisable(GL_DEPTH_TEST);
      glEnable(GL_BLEND);
      glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
      glDrawArrays(GL_TRIANGLE_STRIP, 0, numberOfPoints);
      throwOnGlError();
    }
  }

  // --- Static --------------------------------------------------------

  //#include <memory>

  typedef std::unique_ptr<ShootTracer> TracerPtr;

  struct ShootTracerSystem
  {
    std::vector<TracerPtr> tracers; // TODO:
    Program                program;
    GLuint                 texture; // TODO: Auto-managed (ogl/) texture

    ShootTracerSystem()
      : tracers()
      , program()
      , texture(Assets::GetTexture("PlayerShoot.png"))
    {
      // Load the Program:
      program.attachShader(GL_VERTEX_SHADER, "trail.vert");
      program.attachShader(GL_FRAGMENT_SHADER, "trail.frag");
      program.compile();
      throwOnGlError();
    }
  };

  std::unique_ptr<ShootTracerSystem> s_shootTracerSystem;

  void ShootTracer::Initialize()
  {
    s_shootTracerSystem = std::make_unique<ShootTracerSystem>();
    s_shootTracerSystem->tracers.reserve(64);
  }

  void ShootTracer::Destroy() { s_shootTracerSystem = nullptr; }

  ShootTracer* ShootTracer::Create(const int numberOfPoints, const float width,
                                   const float timeToLive)
  {
    for (auto& t : s_shootTracerSystem->tracers) {
      if (t->timeToLive <= 0.0f) {
        t->timeToLive = timeToLive;
        return t.get();
      }
    }

    s_shootTracerSystem->tracers.emplace_back(
      new ShootTracer(numberOfPoints, width, timeToLive));
    return s_shootTracerSystem->tracers.back().get();
  }

  void ShootTracer::Update(const float deltaTime)
  {
    if (s_shootTracerSystem == nullptr) { return; }
    
    for (auto& t : s_shootTracerSystem->tracers) {
      if (t->timeToLive <= 0.0f) continue;
      t->updateTrail(deltaTime);
    }
  }

  void ShootTracer::Draw(Frame& frame)
  {
    if (s_shootTracerSystem == nullptr) { return; }
    
    glUseProgram(s_shootTracerSystem->program.program);
    throwOnGlError();
    // TODO: store uniform
    glUniformMatrix4fv(
      s_shootTracerSystem->program.getUniform("ViewProjection"), 1, GL_FALSE,
      glm::value_ptr(frame.ViewProjection));
    throwOnGlError();

    glBindTexture(GL_TEXTURE_2D, s_shootTracerSystem->texture);
    throwOnGlError();

    for (auto& t : s_shootTracerSystem->tracers) {
      if (t->timeToLive <= 0.0f) continue;

      glUniform1i(s_shootTracerSystem->program.getUniform("NumberOfPoints"),
                  t->numberOfPoints - 2);

      t->draw(frame);
    }
  }

} // namespace Soleil
