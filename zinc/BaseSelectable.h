/*
 * Copyright (C) 2019  Florian GOLESTIN
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#pragma once

#include <BillboardTextSystem.h>
#include <BoundingBox.h>
#include <Frame.h>
#include <UITextSystem.h>
#include <memory>
#include <vector>

namespace Soleil {

  class BaseSelectable
  {
  public:
    BaseSelectable(BoundingBox const& box, std::string const& text);
    ~BaseSelectable();

    void initBillboardPosition();

  public:
    struct Button
    {
      std::string label;
      bool        enabled;
    };

  public:
    BoundingBox         box;
    BillboardText*      billboard;
    std::string         label;
    std::string         dialog;
    std::vector<Button> buttons;
    int              characterPicture;
  };

  class BaseSelectableSystem
  {
  public:
    BaseSelectableSystem();
    ~BaseSelectableSystem();

    BaseSelectable* create(BoundingBox const& box, std::string const& text);
    void            destroy(BaseSelectable* selectable);

    void draw(Frame& frame) const;
    void process(Frame& frame, const double deltaTime);

  public:
    BaseSelectable* Create(BoundingBox const& box, std::string const& text);
    void            Destroy(BaseSelectable* selectable);
    void            Draw(Frame& frame) const;
    void            Process(Frame& frame, const double deltaTime);

  private:
    void updatePointed(const float deltaTime, BaseSelectable* pointed);

  private:
    std::vector<std::unique_ptr<BaseSelectable>> selectables;
    float                                        interpolation     = 0.0f;
    BaseSelectable*                              previouslyPointed = nullptr;
    BaseSelectable*                              selected          = nullptr;
  };

} // namespace Soleil
