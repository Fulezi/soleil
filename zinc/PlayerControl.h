/*
 * Copyright (C) 2018  Florian GOLESTIN
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#pragma once

#include "Camera.h"
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

namespace Soleil {

  class ParticleSystem;
  // class InstancedSystem;

  class FlightCamera : public Camera
  {
  public:
    FlightCamera(const float width, const float height);
    ~FlightCamera();

    void update(float deltaTime) override;

  private:
    void drawFlightInstruments(const float speedRatio, const float roll);
    float aspect; // TODO: Viewport
    float fov;

  private:
    ParticleSystem* shootimpostor;
    // InstancedSystem* shoots;

  public:
    bool lockControls;

  public:
    // float previousTime;
    glm::vec3 offset;           ///< Camera Offset to the target
    glm::quat planeAttitude;    ///< Plane orientation (no roll)
    float     planePitch;       ///< Plane pitch
    glm::vec3 planePosition;    ///< Plane Node position
    glm::quat cameraAttitude;   ///< Used to slerp the rotation to the target
    glm::quat cameraCenter;     ///< Used to slerp the rotation to the center
    float     rotationY;        ///< Current Rotation accumulation.
    glm::quat modelOrientation; ///< Attitude and roll
    float     timeRemaingBeforeFire;

    static constexpr float MaxPitch = 1.256637061f; // 80% of PI/2
    
  public:
    /// 450 rounds per minute (based on
    /// https://en.wikipedia.org/wiki/Vickers_machine_gun)
    constexpr static float FireRate = 60.0f / 450.0f;
  };

} // namespace Soleil
