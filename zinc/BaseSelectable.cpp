/*
 * Copyright (C) 2019  Florian GOLESTIN
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <zinc/BaseSelectable.h>

#include <Assets.h>
#include <Errors.h>
#include <Inputs.h>
#include <Utils.h>
#include <easing.h>
#include <ui/fox_ui.h>
#include <zinc/Zinc.h>

#include <DebugManager.h>
#include <DebugUI.h>

#include <easing.h>

namespace Soleil {

  BaseSelectable::BaseSelectable(BoundingBox const& box,
                                 std::string const& text)
    : box(box)
    , label(text)
  {
    billboard = BillboardTextSystem::CreateBillboard(Zinc::GetUIFont(), text);

    initBillboardPosition();
  }

  BaseSelectable::~BaseSelectable()
  {
    BillboardTextSystem::DestroyBillboard(billboard);
  }

  void BaseSelectable::initBillboardPosition()
  {
    glm::vec3 center = 0.5f * (box.getMax() + box.getMin());
    center.y         = box.getMax().y - 2.0f;
    billboard->setCenter(center);
  }

  //////////////////////////////////////////////////////////////////////////////
  // Base Selectable

  BaseSelectableSystem::BaseSelectableSystem() {}

  BaseSelectableSystem::~BaseSelectableSystem() {}

  BaseSelectable* BaseSelectableSystem::create(BoundingBox const& box,
                                               std::string const& text)
  {
    selectables.emplace_back(std::make_unique<BaseSelectable>(box, text));
    return selectables.back().get();
  }

  void BaseSelectableSystem::destroy(BaseSelectable* selectable)
  {
    for (auto it = selectables.begin(); it != selectables.end(); ++it) {
      if (it->get() == selectable) {
        selectables.erase(it);
        return;
      }
    }
    SOLEIL__RUNTIME_ERROR("BaseSelectable not found");
  }

  void BaseSelectableSystem::draw(Frame& frame) const
  {
    //////////////////////////////////////////////////////////////////////////////
    // TODO: Now the problem is that Billboards are always displayed. Therefore
    // we would need to delete them while the player is flying. Another solution
    // is to not use the Billboard system singleton but embed a system and call
    // its draw from the BaseSelectableSystem.
    //////////////////////////////////////////////////////////////////////////////

    // for (auto const& selectable : selectables)
    //   {

    //   }
    (void)frame;
  }

  // TODO: In Math
  static float SmoothStep(const float edge0, const float edge1, const float x)
  {
    const float t = glm::clamp((x - edge0) / (edge1 - edge0), 0.0f, 1.0f);
    return t * t * (3.0f - 2.0f * t);
  }

  void BaseSelectableSystem::process(Frame& frame, const double deltaTime)
  {
    // if (selected) { selected->drawUI(); return ; }

    glm::vec2 cur =
      (Inputs::GetCursorPosition() / glm::vec2(frame.ViewPort) * 2.0f) - 1.0f;
    cur.y = -cur.y;

    const glm::vec3 orig =
      CursorToWorld(cur, frame.View, frame.Projection, 0.0f);
    const glm::vec3 end =
      CursorToWorld(cur, frame.View, frame.Projection, 1.0f);
    const glm::vec3 dir = glm::normalize(end - orig);

    float           min     = std::numeric_limits<float>::max();
    BaseSelectable* pointed = nullptr;

    for (auto& selectable : selectables) {
#if 0
      DebugManager::DrawBoundingBox(selectable->box, glm::mat4(1.0f),
                                    glm::vec4(1.0f));
#endif

      float tmin;
      if (selectable->box.rayIntersect(orig, dir, tmin) && tmin <= min) {
        min     = tmin;
        pointed = selectable.get();
      }
    }

    if (previouslyPointed && (pointed != previouslyPointed)) {
      previouslyPointed->initBillboardPosition();

      previouslyPointed = nullptr;
      interpolation     = 0.0f;
    }

    const bool alienFocus = ImGui::IsAnyItemActive();

    if (pointed) {
      updatePointed(deltaTime, pointed);
      previouslyPointed = pointed;

      if (Inputs::GetMouseButton(MouseButtonLeft).justReleased) {
        selected = pointed;
      }
    } else if (selected && alienFocus == false) {
      if (Inputs::GetMouseButton(MouseButtonLeft).justReleased) {
        selected = nullptr;
      }
    }

    static float selectedTime = 0.0f;

    static float time         = 2.0f;
    static int   easingMethod = 0;
    static int   opening      = 0;
    double (*easing)(double)  = ElasticEaseOut;
    if (ImGui::Begin("Window opening A")) {
      ImGui::SliderFloat("Time", &time, 0.1f, 10.0f);

      ImGui::RadioButton("Sliding from bottom", &opening, 0);
      ImGui::RadioButton("Stretching from center", &opening, 1);

      const std::vector<const char*> items = {"Elastic ease out",
                                              "Quintic ease out"};
      ImGui::Combo("Easing method", &(easingMethod), items.data(),
                   items.size());
      if (easingMethod == 0) {
        easing = ElasticEaseOut;
      } else {
        easing = QuinticEaseOut;
      }
    }
    ImGui::End();

    if (selected) {
      foxui_rectangle   full_rect;
      foxui_rectangle   pos;
      foxui_renderdata* render = Zinc::GetFoxuiRender();

      // The original
      foxui_rectangle org =
        foxui_sub_construct_rectangle(0.02f, 0.5f, 0.96f, 0.5f);

      // const float v = ElasticEaseOut(SmoothStep(0.0f, 2.0f, selectedTime));
      // const float v =  QuinticEaseOut(SmoothStep(0.0f, time, selectedTime));
      const float v        = easing(SmoothStep(0.0f, time, selectedTime));
      const float oneOverV = 1.0f - v;
      float fontScale = 1.0f;
      
      // Bouncing from bottom opening
      if (opening == 0) {
        pos.x      = 0.02;
        pos.y      = 0.5f + oneOverV * 0.5f;
        pos.width  = .96;
        pos.height = 0.5;
      } else {
        // Stretching from center
        const float halfWidth  = org.width * 0.5f;
        const float halfHeight = org.height * 0.5f;

        pos.x      = org.x + halfWidth * oneOverV;
        pos.width  = org.width * v;
        pos.y      = org.y + halfHeight * oneOverV;
        pos.height = org.height * v;
	fontScale = v;
      }

      int ret = foxui_widget_textbox(Zinc::GetFoxuiRender(), &pos, "");
      assert(ret == 0);
      // foxui_layout_table(12, 6);
      foxui_set_cell(render, 0, 0, 4, 6);
      {
        foxui_set_cell(render, 0, 0, 12, 1);
        {
          float font_size = 100.0f * fontScale;
          // ImGui::DragInt("Font size", &font_size);
          int ret = foxui_write_text(render, full_rect, selected->label.c_str(),
                                     font_size);
          assert(ret == foxui_ret_success);

          foxui_layer_pop(render);
        }

        foxui_set_cell(render, 1, 2, 8, 6 - 2);
        {
          foxui_print_image(render, selected->characterPicture, foxui_image_extend);

          foxui_layer_pop(render);
        }

        foxui_layer_pop(render);
      }

      foxui_set_cell(render, 4, 0, 12 - 5, 6 - 0);
      {
        foxui_write_text(render, full_rect, selected->dialog.c_str(),
                         72.0f * fontScale);
        foxui_layer_pop(render);
      }

      foxui_layer_pop(render); // Text-box layer

      selectedTime += deltaTime;
    } else {
      selectedTime = 0;
    }
  }

  void BaseSelectableSystem::updatePointed(const float     deltaTime,
                                           BaseSelectable* pointed)
  {
    interpolation += deltaTime;
    interpolation = std::min(interpolation, 1.0f);

    float elevation = 5.0f;
    elevation *= CircularEaseOut(interpolation);

    glm::vec3 center = pointed->billboard->getCenter();
    center.y         = pointed->box.getMax().y + elevation;
    pointed->billboard->setCenter(center);
  }
} // namespace Soleil
