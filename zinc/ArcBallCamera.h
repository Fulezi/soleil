/*
 * Copyright (C) 2019  Florian GOLESTIN
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#pragma once

#include <Camera.h>

#include <glm/gtc/quaternion.hpp>

namespace Soleil {

  class ArcBallCamera : public Camera
  {
  public:
    ArcBallCamera(const float width, const float height);
    virtual void update(float deltaTime) override;
    virtual void onActivate(Camera* previousCamera) override;

    void lookAt(const glm::vec3& point);

  protected:
    void updateRotation();

  private:
    float screenWidth;
    float screenHeight;
    float aspect;
    float fov;
    float yaw;
    float pitch;
    glm::vec3 distance;

  private:
    glm::vec2 previousCusorPosition;
    glm::quat dockFlightQuat =
      glm::quat(0.279187, {0.030260, -0.954172, 0.103417});
    glm::quat interpolationStartQuat = glm::quat();
    float     toDockSlerpTime        = 0.0f;
    bool      toDock                 = false;

  public:
    bool toFlight = false;
  };

} // namespace Soleil
