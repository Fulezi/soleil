/*
 * Copyright (C) 2018  Florian GOLESTIN
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#pragma once

#include <Node.h>
#include <glm/matrix.hpp>
#include <memory>
#include <vector>

#include "DebugUI.h"

namespace Soleil {

  class ParticleSystem;

  struct Boid
  {
    glm::mat4 transformation = glm::mat4();
    ObjectID  boidId         = 0;
    glm::vec3 velocity       = glm::vec3(0.0f);
    glm::vec3 force          = glm::vec3(0.0f);
    float     friction       = 0.0f;
    float     mass           = 1.0f;
    float     maxSpeed       = 10.0f;        // 12.0f
    float     maxForce       = 0.05f;       // 0.2f
    float     maxRotation    = 0.5f;        // 0.6f
    float     fireRate       = 1.0f / 2.0f; // Two fire per second

    Boid(const ObjectID boidId, glm::mat4 transformation)
      : transformation(transformation)
      , boidId(boidId)
    {}

    // Boid() = default;
    Boid() = delete;

  public:
    float previousTime        = 0.0;
    float lastShootTime       = 0.0f;
    float remainingActionTime = 0.0f;
    int   behavior            = 0;
  };

  class AlienCraft
  {
  public:
    AlienCraft();

    // Add a boid, can also add default behavior. Returns the index of the boid?
    void instance(ObjectID boidId, glm::mat4 transformation);
    void removeBoid(ObjectID boidId);
    void updateBoids(const float deltaTime);

    enum
    {
      AlienCraftNoBehavior,
      AlienCraftChasePlayer,
      AlienCraftFlee,
      AlienCraftGoto
    };

  private:
    std::vector<Boid> boids;

    // Behavior
    glm::vec3 chasePlayer(const glm::vec3& targetToCraft,
                          const glm::vec3& position, const float deltaTime,
                          Boid& boid);

    // Forces
    void applyForceCohesion(Boid& meBoid);
    void applyForceAlignment(Boid& meBoid);
    void applyForceSeparate(Boid& meBoid);

    ParticleSystem* shoots;

  public:
    static void      Init();
    static void      Instance(ObjectID boidId, glm::mat4 transformation);
    static void      RemoveBoid(ObjectID boidId);
    static glm::mat4 GetBoidTransform(ObjectID const boidId);
    static void      Update(const float deltaTime);

////////////////////////////////////////////////////////////////////////////
// Debug
#if SOLEIL__USE_IMGUI
    static void DisplayDebug();
#endif
    ////////////////////////////////////////////////////////////////////////////

  public:
    static constexpr float ChaseRange      = 200.0f; // * 200.0f;
    static constexpr float FireRange       = 50.0f;  // * 50.0f;
    static constexpr float Avoidance       = 20.0f;  // * 20.0f;
    static constexpr float SeparationRange = 40.5f;
    static constexpr float Facing          = 0.98f;
  };

} // namespace Soleil
