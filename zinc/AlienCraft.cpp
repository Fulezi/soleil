/*
 * Copyright (C) 2018  Florian GOLESTIN
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include "AlienCraft.h"

#include "DebugUI.h"
#include "ImUtils.h"
#include "Random.h"
#include "SceneManager.h"
#include "Utils.h"
#include <EventManager.h>
#include <glm/common.hpp>
#include <glm/gtc/epsilon.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/matrix.hpp>
#include <glm/vec3.hpp>
#include <physics/PhysicsWorld.h>
#include <zinc/GameEvents.h>
#include <audio/AudioManager.h>
#include <Errors.h>

#include "ParticleSystem.h"

namespace Soleil {

  AlienCraft::AlienCraft()
    : boids()
    , shoots(nullptr)
  {}

  glm::vec3 AlienCraft::chasePlayer(const glm::vec3& targetToCraft,
                                    const glm::vec3& position,
                                    const float deltaTime, Boid& boid)
  {
    // Shoot the player if close enough --------------------------------------
    boid.lastShootTime += deltaTime;
    if (boid.lastShootTime >= boid.fireRate) {
      const glm::vec3 normalizedVelocity = glm::normalize(boid.velocity);

      const float facing =
        glm::dot(glm::normalize(targetToCraft), normalizedVelocity);

      if (glm::length(targetToCraft) < AlienCraft::FireRange &&
          facing > AlienCraft::Facing) {
	AudioManager::PlaySound(Hash("Fire.ogg"),  position + (normalizedVelocity * 5.0f));

        // SOLEIL__LOGGER_DEBUG("Pew pew!");
        auto& particle    = shoots->emitParticle();
        particle.life     = 5.0f;
        particle.velocity = normalizedVelocity * 100.0f;
        particle.position = position + (normalizedVelocity * 5.0f);
        particle.color    = glm::vec4(1.0f);
        particle.scale    = 1.0f;
#if 0
	osg::Node* node; // <<-- parameter Boid node
        const glm::vec3 gun =
          position + normalizedVelocity * node->getBound().radius() * 1.1f;
        // To avoid the alien craft destruct itself when shooting;

        ShootEmitter::EmitAlienShoot(gun, normalizedVelocity * 100.0f);
#endif
      }
      boid.lastShootTime = 0.0f;
    }

    return glm::normalize(targetToCraft) * boid.maxSpeed;
  }

  static glm::vec3 flee(Boid& boid, const glm::vec3& playerDirection)
  {
    return glm::normalize(playerDirection) *
           boid.maxSpeed; // TODO: Add Some randomeness
  }

  static glm::vec3 goTo(Boid& boid, const glm::vec3& targetToCraft)
  {
    return glm::normalize(targetToCraft) * boid.maxSpeed;
  }

  void AlienCraft::instance(ObjectID boidId, glm::mat4 transformation)
  {
    boids.emplace_back(boidId, transformation);
  }

  void AlienCraft::removeBoid(ObjectID boidId)
  {
    assert(boidId > 0 && "Null Boid ID");

    int erased = 0;
    for (auto it = boids.begin(); it != boids.end(); ++it) {
      if (it->boidId == boidId) {
        SOLEIL__LOGGER_DEBUG("Removed Boid:  ", boidId);
        boids.erase(it);
        erased++;
		return;
      }
    }
    SOLEIL__RUNTIME_ERROR(toString("No boid found with this ID", boidId));
  }

  void AlienCraft::updateBoids(const float deltaTime)
  {
    if (shoots == nullptr) {
      shoots =
        ParticleSystem::CreateParticleSystem("textures/alienshoot.png", 256);

      shoots->collisionSignal = [](PhysicsWorld::RayCastResult& r,
                                   Particle&                    p) {
        const glm::vec3 normal(0, 1, 0); // Faked

        // p.velocity = glm::reflect(p.velocity, normal);
        p.life = 0.0f;
        for (auto object : r.objects) {
          EventManager::Emit(std::make_shared<ObjectGetHitEvent>(object));
        }
      };
    }

#if 0
    // Clean the removed node from parent.
    // TODO: Not the optimum way (add a onRemoved Signal on node?)
    // TODO: onremovedSignal move the node to a queue of deleted nodes and is
    // processed by another thred
    Node* alienCraftSystem = SceneManager::GetNode(Hash("AlienCraftSystem"));
    for (auto& b : boids)
      {
	if (c.object == 0)
	  {
	    
	  }
      }
#endif

    Node* player           = SceneManager::GetNode(Hash("ZincAF100"));
    bool  playerDestructed = false;

    if (player == nullptr) {
      // assert(player && "Player (ID Zincaf100) not found");
      // Playe might be destructed or just not present
      // TODO: Set default behavior to wander on random point or just wait

      // For the moment set a Random player so the air craft don't do anything
      Node n(0, 0, "Fake node",
             glm::translate(glm::mat4(1.0f), glm::vec3(Random(-20.0f, 20.0f))));
      player           = &n;
      playerDestructed = true;
    }

    const glm::vec3 target            = GetPosition(player->transformation);
    const glm::quat targetOrientation = GetOrientation(player->transformation);
    const glm::vec3 targetDirection   = targetOrientation * VectorFront();

    for (auto& boid : boids) {
      const glm::vec3 current          = GetPosition(boid.transformation);
      const glm::vec3 targetToCraft    = target - current;
      const float     lengthToTarget   = glm::length(targetToCraft);
      const bool      isPlayerInRange  = lengthToTarget < ChaseRange;
      const bool      isPlayerTooClose = lengthToTarget < Avoidance;
      const bool      isPlayerFacingMe =
        facing(target, targetDirection, current) > Facing;

      glm::vec3 desired(0.0f);

      boid.remainingActionTime -= deltaTime;
      if (boid.remainingActionTime <= 0.0f) {
        boid.behavior = AlienCraftNoBehavior;
      }
      if (boid.behavior == AlienCraftNoBehavior) {
        if ((isPlayerInRange && isPlayerFacingMe) || isPlayerTooClose) {
          boid.behavior            = AlienCraftFlee;
          boid.remainingActionTime = Random(1.5f, 2.0f);
        } else if (isPlayerInRange) {
          boid.behavior            = AlienCraftChasePlayer;
          boid.remainingActionTime = Random(2.50f, 3.0f);
        } else {
          boid.behavior            = AlienCraftGoto;
          boid.remainingActionTime = 0;
        }
      }
      if (playerDestructed) {
        boid.behavior = AlienCraftGoto; // TODO: Wander
      }

      switch (boid.behavior) {
        case AlienCraftChasePlayer:
          desired = chasePlayer(targetToCraft, current, deltaTime, boid);
          break;
        // case AlienCraftFlee: desired = flee(playerDirection); break;
        case AlienCraftFlee:
          desired = glm::normalize(current - target) * boid.maxSpeed;
          break;

        case AlienCraftGoto: desired = goTo(boid, targetToCraft); break;
      };

      assert(glm::isnan(desired.x) == false);
      // const float desi
      if (glm::length(desired) > 0.0f) {
        // If the vehicle has no velocity, allows an abrupt turn
        if (glm::length(boid.velocity) > 0.0f) {
          const float dot =
            glm::dot(glm::normalize(boid.velocity), glm::normalize(desired));
          const float angle =
            glm::acos(dot / (glm::length(boid.velocity),
                             glm::length(desired))); // TODO: use map?
          // TODO FIXME what is this comma?

          // Constraint the rotation to Max
          if (std::abs(angle) > boid.maxRotation) {
            const float length = glm::length(desired);

            // const glm::vec3 NDesired  = glm::normalize(desired);
            // const glm::vec3 NVelocity = glm::normalize(boid.velocity);
            // glm::vec3       axis      = glm::cross(NDesired, NVelocity);
            glm::vec3 axis = glm::cross(desired, boid.velocity);
            // TODO: Normalize the normalization
            if (glm::length(axis) > 0.0f) { axis = glm::normalize(axis); }
            // TODO FIXME cross of inverse return 0?

            desired = glm::normalize(boid.velocity) * length;

            desired = glm::angleAxis(-boid.maxRotation, axis) * desired;
          }
        }
      } // (glm::length(desired) > 0.0f)

      glm::vec3 steering = limit(desired - boid.velocity, boid.maxForce);
      assert(glm::isnan(steering.x) == false);
      boid.force = steering;

      assert(glm::isnan(desired.x) == false);
      this->applyForceSeparate(boid);
      this->applyForceAlignment(boid);
      this->applyForceCohesion(boid);

      // Compute Physics -------------------------------------------
      const glm::vec3 acceleration = boid.force / boid.mass;

      boid.velocity += acceleration;

      assert(glm::isnan(boid.velocity.x) == false);
      if (boid.friction > 0.0f) {
        boid.velocity.x *= std::pow(boid.friction * boid.mass, deltaTime);
        boid.velocity.y *= std::pow(boid.friction * boid.mass, deltaTime);
        boid.velocity.z *= std::pow(boid.friction * boid.mass, deltaTime);
      }

      if (glm::length(boid.velocity) > 0.0f) {
        // TODO: Orient the craft event if velocity == 0
        glm::vec3 new_forward = glm::normalize(boid.velocity);
#if 0
      // TODO: For the roll operation if needed
      glm::vec3 approximate_up(0, 0, 1);
      glm::vec3 new_side = new_forward ^ approximate_up; // cross product
      glm::vec3 new_up   = new_forward ^ new_side;       // cross product
#endif
        const glm::quat q = quatLookAt(new_forward, VectorUp());

        const glm::vec3 nextPosition = current + (boid.velocity * deltaTime);
        // glm::vec3       collisionNormal;
        // if (Soleil::SceneManager::SegmentCollision(current, nextPosition,
        // node,
        //                                            &collisionNormal)) {

        //   osg::NodePath p = visitor->getNodePath();
        //   Soleil::EventManager::Emit(
        //     std::make_shared<Soleil::EventDestructObject>(p));
        // } else {
        //   // node->setMatrix(m);
        // node->setAttitude(q);
        // node->setPosition(nextPosition);
        boid.transformation =
          glm::translate(glm::mat4(1.0f), nextPosition) * glm::mat4_cast(q);
        SceneManager::PushDrawItem(boid.transformation, Hash("AlienCraft.obj"));
	
        PhysicsWorld::UpdateRigidBodyTransform(boid.boidId, boid.transformation);
        // }
      }
    } // while
  }

  void AlienCraft::applyForceSeparate(Boid& meBoid)
  {
    int       count = 0;
    glm::vec3 sum(0.0f);

    for (auto const& nextBoid : boids) {
      if (&meBoid == &nextBoid) continue;

      const glm::vec3 diff = GetPosition(meBoid.transformation) -
                             GetPosition(nextBoid.transformation);
      assert(glm::isnan(diff.x) == false);
      const float distance = glm::length(diff);
      if (distance > SeparationRange) { continue; }
      if (distance < glm::epsilon<float>()) { continue; }

      sum += (diff / (distance * distance)); // weight by distance
      assert(glm::isnan(sum.x) == false);

      // SOLEIL__LOGGER_DEBUG((diff / distance * distance),
      //                      "==", glm::normalize(diff) / distance);

      count++;
    }

    if (count > 0 && glm::length(sum)) {
      sum /= count;
      sum = glm::normalize(sum);
      sum *= meBoid.maxSpeed;
      glm::vec3 steer = sum - meBoid.velocity;
      steer           = limit(steer, meBoid.maxForce);

      steer *= 2.0f; // // TODO: configurable

      meBoid.force += steer;
      assert(glm::isnan(meBoid.force.x) == false);
    }
  }

  // TODO: // TODO: // TODO: MERGE THOSE THREE METHOS ApllyForce in one vector
  // browse
  void AlienCraft::applyForceAlignment(Boid& meBoid)
  {
    int       count = 0;
    glm::vec3 sum(0.0f);

    for (auto const& nextBoid : boids) {
      if (&meBoid == &nextBoid) continue;

      const glm::vec3 diff = GetPosition(meBoid.transformation) -
                             GetPosition(nextBoid.transformation);
      const float distance = glm::length(diff);
      if (distance > SeparationRange) { continue; }
      if (distance < glm::epsilon<float>()) { continue; }

      // sum += meBoid.velocity;
      sum += nextBoid.velocity;
      // SOLEIL__LOGGER_DEBUG((diff / distance * distance),
      //                      "==", glm::normalize(diff) / distance);

      count++;
    }

    if (count > 0 && glm::length(sum)) {
      sum /= count;
      if (glm::length(sum)) {
        sum = glm::normalize(sum);
        sum *= meBoid.maxSpeed;
        glm::vec3 steer = sum - meBoid.velocity;
        steer           = limit(steer, meBoid.maxForce);

        steer *= .40f; // // TODO: configurable

        meBoid.force += steer;
        assert(glm::isnan(meBoid.force.x) == false);
      }
    }
  }

  void AlienCraft::applyForceCohesion(Boid& meBoid)
  {
    int       count = 0;
    glm::vec3 sum(0.0f);

    const glm::vec3 mePosition = GetPosition(meBoid.transformation);
    for (auto const& nextBoid : boids) {
      if (&meBoid == &nextBoid) continue;

      const glm::vec3 otherPosition = GetPosition(nextBoid.transformation);
      const glm::vec3 diff          = mePosition - otherPosition;
      const float     distance      = glm::length(diff);
      if (distance > SeparationRange) { continue; }
      if (distance < glm::epsilon<float>()) { continue; }

      sum += otherPosition;
      // SOLEIL__LOGGER_DEBUG((diff / distance * distance),
      //                      "==", glm::normalize(diff) / distance);

      count++;
    }

    if (count > 0 && glm::length(sum)) {
      sum /= count;

      const glm::vec3 desired =
        glm::normalize(sum - mePosition) * meBoid.maxSpeed;
      const glm::vec3 steer =
        (desired - meBoid.velocity) * .010f; // // TODO: configurable

      meBoid.force += steer;
      assert(glm::isnan(meBoid.force.x) == false);
    }
  }

  // Statics -------------------------------------------------------

  AlienCraft s_alienCrafts;

  void AlienCraft::Init()
  {
    s_alienCrafts = AlienCraft();
  }
  
  void AlienCraft::Update(const float deltaTime)
  {
    s_alienCrafts.updateBoids(deltaTime);
  }

  void AlienCraft::Instance(ObjectID boidId, glm::mat4 transformation)
  {
    s_alienCrafts.instance(boidId, transformation);
  }

  void AlienCraft::RemoveBoid(ObjectID boidId)
  {
    s_alienCrafts.removeBoid(boidId);
  }

  glm::mat4 AlienCraft::GetBoidTransform(ObjectID const boidID)
  {
    // auto l = [boidID](const Boid& b) -> bool { return b.boidId == boidID; };
    // auto it =
    //   std::find(s_alienCrafts.boids.begin(), s_alienCrafts.boids.end(), l);
    // if (it != s_alienCrafts.boids.end()) { return it->transformation; }
    for (Boid const& b : s_alienCrafts.boids) {
      if (b.boidId == boidID) return b.transformation;
    }

    assert(false && "Boid not found");
    return glm::mat4(1.0f);
  }

////////////////////////////////////////////////////////////////////////////
// Debug
#if SOLEIL__USE_IMGUI
  void AlienCraft::DisplayDebug()
  {
    if (ImGui::Begin("AlienCraft")) {
      Node* player = SceneManager::GetNode(Hash("ZincAF100"));

      assert(player && "Player (ID Zincaf100) not found");
      // Playe might be destructed or just not present
      // TODO: Set default behavior to wander on random point or just wait

      const glm::vec3 target = GetPosition(player->transformation);

      int CraftNumber = 0;

      for (auto& boid : s_alienCrafts.boids) {
        const glm::vec3 current        = GetPosition(boid.transformation);
        const glm::vec3 targetToCraft  = target - current;
        const float     lengthToTarget = glm::length(targetToCraft);

        if (ImGui::CollapsingHeader(
              toString("Craft N.", CraftNumber).c_str())) {
          ImGui::Text("Velocity: [%f, %f, %f]", boid.velocity.x,
                      boid.velocity.y, boid.velocity.z);
          ImGui::Text("Force:    [%f, %f, %f]", boid.force.x, boid.force.y,
                      boid.force.z);
          ImGui::Text("Behavior: %i - For %f ms", boid.behavior,
                      boid.remainingActionTime);
          ImGui::Text("Distance to target: %f", lengthToTarget);
        }
        CraftNumber++;
      }
    }
    ImGui::End();
  }
#endif
  ////////////////////////////////////////////////////////////////////////////

} // namespace Soleil
