
#pragma once

#include <Frame.h>
#include <Program.hpp>
#include <glm/vec3.hpp>
#include <ogl/Buffer.h>
#include <ogl/VertexArray.h>

// TODO: Order by transparancy
// TODO:
namespace Soleil {

  struct ShootTracerSystem;

  class ShootTracer
  {
    friend ShootTracerSystem;

  public:
    ShootTracer(const int numberOfPoints, const float width,
                const float timeToLive);
    ~ShootTracer() = default;

  public:
    /// Move the head and update the trail
    void updateTrail(const float deltaTime);
    /// Set all point at position and initialize the direction
    void setUpTrail(const glm::vec3& position, const glm::vec3& direction);
    /// Draw the trail
    void draw(Frame& frame);

  public: // TODO: Create a (trail) system if needed
    static void         Initialize();
    static void         Update(const float deltaTime);
    static void         Draw(Frame& frame);
    static ShootTracer* Create(const int numberOfPoints, const float width,
                               const float timeToLive);
    static void         Destroy();

  private:
    int                    numberOfPoints;
    float                  halfWidth;
    std::vector<glm::vec3> vertices;
    glm::vec3              direction;
    float                  timeToLive;

  private: // Draw Resource
    Buffer      trailBuffer;
    VertexArray vao;
  };

} // namespace Soleil
