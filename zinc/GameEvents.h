/*
 * Copyright (C) 2018  Florian GOLESTIN
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#pragma once

#include <EventManager.h>
#include <glm/vec3.hpp>
#include <stringutils.h> // For hash

namespace Soleil {

  class PlayerDestructedEvent : public Event
  {
  public:
    constexpr static EventType Type(void)
    {
      return Hash("PlayerDestructedEvent");
    }

  public:
    PlayerDestructedEvent(const glm::vec3& playerPosition)
      : Event(Type())
      , playerPosition(playerPosition)
    {}
    ~PlayerDestructedEvent() = default;

    glm::vec3 playerPosition;
  };

  class ObjectGetHitEvent : public Event
  {
  public:
    constexpr static EventType Type(void)
    {
      return Hash("ObjectGetHitEvent");
    }

  public:
    // TODO: ObjectID in a class in a header
    using ObjectID = std::size_t;
    
    ObjectGetHitEvent(const ObjectID shootedObject, const ObjectID shooter = 0)
      : Event(Type())
      , shootedObject(shootedObject)
      , shooter(shooter)
    {}
    
    ~ObjectGetHitEvent() = default;

    ObjectID shootedObject;
    ObjectID shooter;
  };

} // namespace Soleil
