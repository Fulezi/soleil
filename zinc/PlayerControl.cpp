/*
 * Copyright (C) 2018  Florian GOLESTIN
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include "PlayerControl.h"

#include "Inputs.h"
#include "Utils.h"
#include "easing.h"
#include <DebugUI.h>
#include <ParticleSystem.h>
#include <audio/AudioManager.h>
#include <glm/gtc/matrix_transform.hpp>
#include <physics/PhysicsWorld.h>
#include <ui/fox_ui.h>
#include <zinc/GameEvents.h>
#include <zinc/ShootTracer.h>
#include <zinc/Zinc.h>
#include <glm/common.hpp>

namespace Soleil {

  FlightCamera::FlightCamera(const float width, const float height)
    : aspect(width / height)
    , fov(glm::radians(55.0f))
    , shootimpostor(nullptr)
    // , shoots(nullptr)
    , lockControls(false)
    , offset(0, 1.0f, 7.0f)
    , planeAttitude()
    , planePitch(0.0f)
    , planePosition(0.0f)
    , cameraAttitude()
    , cameraCenter()
    , rotationY(0.0f)
    , timeRemaingBeforeFire(0.0f)
  {
    SOLEIL__LOGGER_DEBUG("Created FlightCamera");
  }

  FlightCamera::~FlightCamera()
  {
    if (shootimpostor != nullptr) { ShootTracer::Destroy(); }
    SOLEIL__LOGGER_DEBUG("Destructed FlightCamera");
  }

  void FlightCamera::update(float deltaTime)
  {
#if SOLEIL__USE_IMGUI
    ImGui::Begin("Player's Control");
    {
      ImGui::Text("PlanePosition: %s", toString(planePosition).c_str());
      ImGui::Text("Cam Offset: %s", toString(offset).c_str());
      // ImGui::Text("Cam attitude: %s", toStrin\g(cameraAttitude).c_str());
    }
    ImGui::End();
#endif // SOLEIL__USE_IMGUI
    constexpr float Min = 0.1f;
    // constexpr float Speed = 10.5f;
    float Speed = 10.5f;
    // constexpr float Speed      = 0.0f;
    constexpr float RollSpeed  = 1.0f;
    constexpr float PitchSpeed = 0.005f;
    constexpr float SlerpSpeed = 1.8f;

    if (lockControls) {
      ///////////////////////////////////////
      ///////////////////////////////////////
      // CAMERA TEST
      {
        cameraAttitude =
          // glm::slerp(planeAttitude, cameraAttitude, SlerpSpeed * deltaTime);
          glm::slerp(cameraAttitude, planeAttitude, SlerpSpeed * deltaTime);
        this->position = planePosition + (cameraAttitude * offset);

        cameraCenter =
          glm::slerp(cameraCenter, planeAttitude, SlerpSpeed * deltaTime);
        this->center = this->position + cameraCenter * VectorFront();

        this->view = glm::lookAt(this->position, this->center, VectorUp());

        AudioManager::SetListenerPosition(planePosition,
                                          planeAttitude * VectorFront());

        // TODO: Do not update each frame
        this->projection = glm::perspective(fov, aspect, 0.1f, 1000.0f);
        // TODO: Autocompute far?
      }
      ///////////////////////////////////////
      ///////////////////////////////////////

      return;
    }

    const auto cursor = (Soleil::Inputs::GetCursorPosition() /
                         glm::vec2(1920.0f / 2.0f, 1080.0f / 2.0f)) -
                        glm::vec2(1.0f); // TODO:  FIXME

    if (shootimpostor == nullptr) {
      // TODO: 1024 May be too much for the shoot impostor
      shootimpostor =
        ParticleSystem::CreateParticleSystem("zinc/textures/0026.png", 1024);
      // shoots = InstancedSystem::CreateParticleSystem("PlayerShoot.obj",
      // 1024);
      ShootTracer::Initialize();
    }

    // SOLEIL__LOGGER_DEBUG("Cursor: ", cursor);

    // constexpr float MaxPitch = glm::half_pi<float>() * 0.8f;
    //constexpr float MaxPitch = 1.256637061f; // 80% of PI/2

    float planeRoll = 0.0f;
    planeRoll =
      RollSpeed * atan2(Sign(cursor.x) * QuadraticEaseIn(glm::abs(cursor.x)),
                        (glm::abs(cursor.y) > Min) ? glm::abs(cursor.y) : Min);
    planePitch +=
      (glm::abs(cursor.y) > Min)
        ? PitchSpeed * Sign(cursor.y) * QuadraticEaseIn(glm::abs(cursor.y))
        : 0.0f;
    planePitch = glm::clamp(planePitch, -MaxPitch, MaxPitch);

    // Use easing method? Use physics?
    Speed += (planePitch * 3.0f);

    rotationY -= planeRoll * 0.008f;

    // Plane Attitude: Plane direction where the planes goes
    planeAttitude = glm::angleAxis(rotationY, VectorUp()) *
                    glm::angleAxis(-planePitch, VectorRight());

    // Orient (Roll, Pitch) the model in the world but not its direction
    modelOrientation = planeAttitude * glm::angleAxis(planeRoll, VectorFront());

    // After rotation, move the plane:
    const glm::vec3 futurePlanePosition =
      planePosition + (planeAttitude * ((Speed * deltaTime) * VectorFront()));
    // If there is no Collision:
    if (PhysicsWorld::RayCast(planePosition, futurePlanePosition,
                              Hash("ZincAF100"))
          .count > 0) {
      EventManager::Emit(
        std::make_shared<PlayerDestructedEvent>(futurePlanePosition));
      lockControls = true; // Or register to the signal
    } else {
      planePosition = futurePlanePosition;
    }

    cameraAttitude =
      // glm::slerp(planeAttitude, cameraAttitude, SlerpSpeed * deltaTime);
      glm::slerp(cameraAttitude, planeAttitude, SlerpSpeed * deltaTime);
    this->position = planePosition + cameraAttitude * offset;

    cameraCenter =
      glm::slerp(cameraCenter, planeAttitude, SlerpSpeed * deltaTime);
    this->center = this->position + cameraCenter * VectorFront();

    this->view = glm::lookAt(this->position, this->center, VectorUp());

    AudioManager::SetListenerPosition(planePosition,
                                      planeAttitude * VectorFront());

    // TODO: Do not update each frame
    this->projection = glm::perspective(fov, aspect, 0.1f, 1000.0f);
    // TODO: Autocompute far?

    ImGui::InputFloat("Speed", &Speed);
    ImGui::Text("SpeedRatio = %f", Speed / 20.0f);
    this->drawFlightInstruments(Speed / 20.0f, planeRoll);

    ////////////////////////////////////////////////////////////////////////////
    // Shoots
    timeRemaingBeforeFire -= deltaTime;
    if (Inputs::GetMouseButton(MouseButtonLeft).pressed == true &&
        (timeRemaingBeforeFire <= 0.0f)) {

      timeRemaingBeforeFire = FireRate;

      // PLays FireSound just at Plane Position
      AudioManager::PlaySound(
        Hash("Fire.ogg"), AudioManager::SoundProperties(planePosition, 0.5f));

      const glm::vec3 shootStart =
        planePosition + glm::normalize(planeAttitude * VectorFront()) * Speed *
                          deltaTime * 50.0f;

      const glm::vec3 shootDirection = planeAttitude * VectorFront();
#if 0 // Particles
      auto& particle = shoots->emitParticle();
      // particle.position = planePosition;
      particle.velocity = VectorFront() * 100.0f;

      // Start 30 frame in advance
      particle.transformation = glm::translate(glm::mat4(1.0f), shootStart) *
                                glm::mat4_cast(planeAttitude);
      particle.life = 10.5f;
#else
      ShootTracer* t = ShootTracer::Create(64, .50f, 3.0f);
      t->setUpTrail(shootStart, shootDirection);
#endif
      // TODO: Configure distance
      // TODO: Mask to select destroyable
      // TODO: Api to add RayCast Request (that send to the EventManager on
      // collision)
      const glm::vec3 shootEnd =
        shootStart + glm::normalize(shootDirection) * 100.0f;

      auto& p    = shootimpostor->emitParticle();
      p.life     = 0.5f;
      p.position = shootEnd;

      auto result = PhysicsWorld::RayCast(shootStart, shootEnd);
      if (result.count > 0) {
        // TODO: Detroy what have to be destroyed.
        // TODO: Sort by first shooted
        if (result.objects.size() > 0) {
          for (auto id : result.objects) {
            SOLEIL__LOGGER_DEBUG("INTERSECTED: ", id);
          }
          EventManager::Emit(
            std::make_shared<ObjectGetHitEvent>(result.objects[0]));
        }
      }
    }
  }

  void FlightCamera::drawFlightInstruments(const float speedRatio, const float roll)
  {
    static float parentRotation = 0.0f;
    float        needleRotation = glm::pi<float>() * 2.0f * speedRatio;
    ImGui::SliderAngle("Parent", &parentRotation);
    ImGui::SliderAngle("Needle", &needleRotation);

    constexpr int GaugeImageIdx      = 3;
    constexpr int NeedleImageIdx     = 4;
    constexpr int GaugeGlassImageIdx = 5;
    constexpr int AltitudeGauge      = 6;
    constexpr int AltitudeBackground = 7;

    foxui_renderdata* render = Zinc::GetFoxuiRender();

    // Pivot relative to the image.
    // foxui_vec2 pivot = foxui_sub_vec2_set(26.5, 68);
    // foxui_vec2 pivot = foxui_sub_vec2_set(0, 0);
    foxui_vec2      gaugeSize = foxui_image_size(render, GaugeImageIdx);
    foxui_rectangle r; // = foxui_sub_construct_rectangle(0.5, 0.5, 0.5, 0.5);
    r.rot = parentRotation;
    // Last layer should be the viewport-size.
    foxui_rectangle lastLayer = foxui_last_layer(render);
    r.x                       = 0.3f * (lastLayer.width - gaugeSize.x);
    r.y                       = 1.0f * (lastLayer.height - gaugeSize.y);
    r.width                   = gaugeSize.x;
    r.height                  = gaugeSize.y;
    foxui_layer_push(render, r, nullptr); // TODO: foxui_layer_push
    {
      foxui_print_image(render, GaugeImageIdx, foxui_image_extend);

      foxui_vec2 needleSize = foxui_image_size(render, NeedleImageIdx);
      foxui_vec2 pivot =
        foxui_sub_vec2_set(0.5f * needleSize.x, 0.79f * needleSize.y);
      foxui_rectangle needleRec;
      needleRec.rot    = needleRotation;
      needleRec.x      = 0.51f * (r.width - needleSize.x);
      needleRec.y      = 0.31f * (r.height - needleSize.y);
      needleRec.width  = needleSize.x;
      needleRec.height = needleSize.y;
      foxui_layer_push(render, needleRec, &pivot);
      {
        foxui_print_image(render, NeedleImageIdx, foxui_image_extend);
      }
      foxui_layer_pop(render);

      foxui_print_image(render, GaugeGlassImageIdx, foxui_image_extend);
    }
    foxui_layer_pop(render);

    r.x += r.width;
    foxui_layer_push(render, r, nullptr);
    {
      foxui_rectangle backgroundRec;
      foxui_vec2 backgroundSize = foxui_image_size(render, AltitudeBackground);
      foxui_vec2 pivot;

      //const float mix = glm::mix(-MaxPitch, MaxPitch, planePitch);
      static float adjustmentKnob = 0.5f;
      ImGui::Begin("Adjustment Knob");
      {
	ImGui::SliderFloat("adj", &adjustmentKnob, 0.0f, 1.5f);
      }
      ImGui::End();
      const float y = map(-MaxPitch, MaxPitch, 0.0f, backgroundSize.y * adjustmentKnob, planePitch);
      //const float y = glm::smoothstep(0.0f, backgroundSize.y * 0.75f, mix);      
      backgroundRec.x      = -(backgroundSize.x * 0.20f);
      backgroundRec.y      = -y;
      backgroundRec.width  = backgroundSize.x;
      backgroundRec.height = backgroundSize.y;
      backgroundRec.rot    = -roll;
      pivot.x = backgroundSize.x * 0.5f;
      pivot.y = backgroundSize.y * 0.5f;

      foxui_vec2 stencilPos;
      stencilPos.x = backgroundRec.x + pivot.x + 0.01f;
      stencilPos.y = backgroundSize.y * 0.29f;
      foxui_vec2 stencilSize = foxui_image_size(render, AltitudeGauge);
      foxui_stencil_circle(render, stencilPos, stencilSize.x*0.44f); // TODO: Elipse?
      foxui_layer_push(render, backgroundRec, &pivot);
      {
        foxui_print_image(render, AltitudeBackground, foxui_image_extend);
	foxui_stencil_reset(render);
      }
      foxui_layer_pop(render);

      foxui_print_image(render, AltitudeGauge, foxui_image_extend);
      foxui_print_image(render, GaugeGlassImageIdx, foxui_image_extend);
    }
    foxui_layer_pop(render);
  }

} // namespace Soleil
