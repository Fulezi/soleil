/*
 * Copyright (C) 2018  Florian GOLESTIN
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include "OpenGLInclude.h"

////////////////////////////////////////
// Test of the new UI
#define SOLEIL__FOXUI_IMPLEMENTATION 1
#include <ui/fox_ui.h>

#include "AlienCraft.h"
#include "Assets.h"
#include "Camera.h"
#include "DebugManager.h"
#include "DebugUI.h"
#include "Draw.h"
#include "EventManager.h"
#include "GlWindow.h"
#include "Inputs.h"
#include "InstancedSystem.h"
#include "Mesh.h"
#include "Model.h"
#include "ParticleSystem.h"
#include "PlayerControl.h"
#include "Program.hpp"
#include "Random.h"
#include "SceneManager.h"
#include "Timer.h"
#include "Utils.h"
#include "easing.h"
#include "stringutils.h"
#include <BillboardTextSystem.h>
#include <CameraManager.h>
#include <Errors.h>
#include <Font.h>
#include <audio/AudioManager.h>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <math.h>
#include <physics/PhysicsWorld.h>
#include <set>
#include <zinc/ArcBallCamera.h>
#include <zinc/BaseSelectable.h>
#include <zinc/GameEvents.h>
#include <zinc/ShootTracer.h>
#include <zinc/Zinc.h>
// TODO: FIXME Two object with same material name override
// TODO: resize callback
// TODO: FIXME Viewport size in particle system

// Temp:
#include "stb_image.h"
#include <glm/gtc/matrix_access.hpp>
#include <glm/gtx/matrix_interpolation.hpp>
#include <glm/gtx/quaternion.hpp>
#include <glm/gtx/spline.hpp>

/** Foxui Atlas values. Defined in foxui_configuration.c */
void foxui_configure_lnf(foxui_param* param);


// TODO: Configurable and not statis
constexpr int SHADOW_MAP_WIDTH = 2048;
constexpr int SHADOW_MAP_HEIGHT = 2048;

// TODO: Will be moved in a Render unit.
void RenderDepth(Soleil::Frame& frame, const Soleil::MeshShader& depthConfig,
                 GLuint depthFBO, GLuint depthTexture);

// TODO: Will be moved in a Render unit.
// TODO: Provide width and height instead of the Soleil::Frame
void CreateDepthFBO(Soleil::Frame& frame, GLuint* depthFBO,
                    GLuint* depthTexture);

// -----------------------------
// Animation Manager
namespace Soleil {

  void TempRotatePropeller()
  {
    Node* n = SceneManager::GetNode(Hash("PlaneRC1_Plane.008::Material.002"));
    if (n) {
      n->transformation *=
        glm::rotate(glm::mat4(1.0f), 0.25f, glm::vec3(0, 0, 1));
    }
  }

  // TODO: Set in scene Manager
  void DebugOutputSceneGraph(const Node* root, const std::string indent = "")
  {
    {
      std::string name = indent + "Node ";
      name.reserve(512);

      switch (root->type) {
        case NodeTypeModel: name += "Model"; break;
        case NodeTypeMesh: name += "Mesh"; break;
        case NodeTypeLight: name += "Light"; break;
        case NodeTypeGroup: name += "Group"; break;
        case NodeTypeRigidBody: name += "RigidBody"; break;
        case NodeTypeBoundingSphere: name += "BoundSphere"; break;
        case NodeTypeKinematicBody: name += "KinematicBody"; break;
        default: name += "CustomeType";
      }

      if (root->name.empty() == false) {
        name += toString("(", root->name, ")");
      }

      SOLEIL__LOGGER_DEBUG(name);
    }

    for (const Node& child : root->children) {
      DebugOutputSceneGraph(&child, indent + "  ");
    }
  }
} // namespace Soleil
// -----------------------------

namespace Soleil {

  struct Transform
  {
    glm::vec3 position;
    glm::quat orientation;
    // TODO: Scale;

    Transform() = default;
    Transform(glm::vec3 const& position,
              glm::quat const& orientation = glm::quat())
      : position(position)
      , orientation(orientation)
    {}
  };

  struct KeyFrame
  {
    float     deltaTime; /// Arrival time in milliseconds.
    Transform transform; /// Arrival transform.

    KeyFrame(const float deltaTime, Transform const& transform)
      : deltaTime(deltaTime)
      , transform(transform)
    {}
  };

  struct Animation
  {
    std::vector<KeyFrame> keyFrames;

    // TODO: In an AnimationInstance (slot?)
    // AnimationInstance --------------------------
    using OnFinishCallback           = std::function<void(Animation*)>;
    std::size_t      currentKeyFrame = 0;
    float            currentTime     = 0.0f;
    glm::mat4*       node            = nullptr;
    OnFinishCallback onFinishCallback;

    Transform currentTransform;

    void AddKeyFrame(KeyFrame keyFrame);
    void update(const float dt);
    bool isFinished() const;
    // AnimationInstance --------------------------
  };

  void Animation::AddKeyFrame(KeyFrame keyFrame)
  {
    // keyFrames.push_back()
    keyFrames.emplace_back(keyFrame);

    if (keyFrames.size() > 1) {
      // TODO: Another way could be using a mod on the current time
      keyFrames.back().deltaTime += keyFrames[keyFrames.size() - 2].deltaTime;
    }
  }

  void Animation::update(const float dt)
  {
    // previous 3.0f
    // Current 5.0f (Set in AddKeyFrame: 3.0 + 2.0)
    // Time 3.16f

    static bool s_pause = false;
#if SOLEIL__USE_IMGUI
    ImGui::Begin("Animation N.");
    {
      const std::size_t previousKFId = currentKeyFrame - 1;
      KeyFrame*         previousKF   = &keyFrames[previousKFId];
      KeyFrame&         currentKF    = keyFrames[currentKeyFrame];
      const float       nextTime     = currentKF.deltaTime;
      const float       previousTime =
        (previousKF) ? (previousKF->deltaTime) : (0.0f);

      const float mixValue =
        map(previousTime, nextTime, 0.0f, 1.0f, currentTime);

      ImGui::Text("Current Key Frame: %zu", currentKeyFrame);
      ImGui::Text("Current Time: %f", currentTime);
      ImGui::Text("Next Time: %f", currentKF.deltaTime);
      ImGui::Text("percent: %f", mixValue);
      ImGui::Text("Current Position: %s",
                  toString(currentTransform.position).c_str());
      if (ImGui::Button("Replay")) {
        currentKeyFrame = 0;
        currentTime     = 0.0f;
      }

      if (ImGui::Button("Pause")) { s_pause = !s_pause; }
    }
    ImGui::End();
#endif // SOLEIL__USE_IMGUI

    if (s_pause) { return; };

    if (currentKeyFrame >= keyFrames.size()) { return; }

    if (currentKeyFrame > 0) {
      const std::size_t previousKFId = currentKeyFrame - 1;
      KeyFrame*         previousKF   = &keyFrames[previousKFId];
      KeyFrame&         currentKF    = keyFrames[currentKeyFrame];
      const float       previousTime = previousKF->deltaTime;
      const float       nextTime     = currentKF.deltaTime;
      // const float mixValue = currentTime / nextTime;
      const float mixValue =
        map(previousTime, nextTime, 0.0f, 1.0f, currentTime);

      // TODO: Slerp, lerp, ... in Transform
      currentTransform.position = glm::mix(
        previousKF->transform.position, currentKF.transform.position, mixValue);
      currentTransform.orientation =
        glm::slerp(previousKF->transform.orientation,
                   currentKF.transform.orientation, mixValue);
    } else {
      KeyFrame& currentKF          = keyFrames[currentKeyFrame];
      currentTransform.position    = currentKF.transform.position;
      currentTransform.orientation = currentKF.transform.orientation;
    }

    if (node) {
      *node = glm::translate(glm::mat4(1.0), currentTransform.position) *
              glm::mat4_cast(currentTransform.orientation);
    }

    currentTime += dt;

    if (keyFrames[currentKeyFrame].deltaTime < currentTime &&
        (currentKeyFrame < keyFrames.size())) {
      // TODO: Loop and rewind
      currentKeyFrame++;
    }

    if (currentKeyFrame >= keyFrames.size() && onFinishCallback) {
      onFinishCallback(this);
    }
  }

  bool Animation::isFinished() const
  {
    return currentKeyFrame >= keyFrames.size();
  }

  template <typename T>
  T QuadraticBezier(const T A, const T B, const T C, const T D, const float t)
  {
    float OneMinusT = 1.0f - t;
    float ThreeT    = 3 * t;

    return (OneMinusT * OneMinusT * OneMinusT) * A + (t * t * t) * D +
           3 * (ThreeT * ThreeT) * OneMinusT * C +
           3 * (OneMinusT * OneMinusT) * t * B;
  }

} // namespace Soleil

struct CameraMode
{
  enum class Mode
  {
    Landed,
    ToTakeOff,
    Flight
  };

  Mode  mode = Mode::Landed;
  float mix  = 0.0f;

  glm::vec3 startPosition = glm::vec3();
  glm::vec3 startCenter   = glm::vec3();
  glm::vec3 finalPosition = glm::vec3();
  glm::vec3 center        = glm::vec3();
};

// Game State
namespace Soleil {

  class GameStateSystem
  {
  public:
    using GameState = std::function<void(GameStateSystem*, float)>;

  public:
    GameStateSystem();

  public:
    void execute(const float dt);

  private:
    std::map<std::size_t, GameState> states;
    std::size_t                      state = 0;
  };

  void GameStateSystem::execute(const float dt)
  {
    if (states.find(state) == states.end()) {
      SOLEIL__RUNTIME_ERROR("No state associated.");
    }

    states[state](this, dt);
  }

} // namespace Soleil

using namespace Soleil;

static float                                 s_delayRestart = 0.0f;
static int                                   s_playerLife   = 2;
static std::unique_ptr<FlightCamera>         freecam;
static std::unique_ptr<ArcBallCamera>        landedCamera;
static bool                                  paused = false;
static DrawList                              drawList;
static std::vector<Light>                    lights;
static CameraMode                            cameraMode;
static std::unique_ptr<BaseSelectableSystem> selectables;
static AudioManager::SoundSlot               cessnaEngineAmbiantSound;
#ifdef ZINC__FOGPARTICLE
static ParticleSystem* fogParticles = nullptr;
#endif

static Animation LandOffAnimation;

static void
PopEnemies()
{
  /* We load a bunch of enemy that will be randomly placed.
   */
  /* Don't forget to create a new NodeType for our Aliens Nodes so we can
   * recognize them when they get events.
   */
  constexpr ObjectID NodeTypeAienCraft = Hash("NodeTypeAlienCraft");
  SceneManager::SetCustomNodeType(NodeTypeAienCraft);

  /* Parent group. Just because its looks like less messy.
   */
  Node alienCraftSystem(Hash("AlienCraftSystem"), NodeTypeGroup,
                        "AlienCraftSystem");
  for (int i = 0; i < 6; ++i) {
    const std::string name = "AlienCraft_" + std::to_string(i);
    const ObjectID    hash = Hash(name);

    const glm::mat4 acTransformation = glm::translate(
      glm::mat4(1.0f),
      glm::vec3(50.f + Random(30, 70), 10.0f + Random(-5, 5), Random(20, 40)));

    /* Create a new instance from the Alien IA system */
    AlienCraft::Instance(hash, acTransformation);

    /* Don't forget to add a Physics Body to make it destructible.
     *
     * TODO Note: that instead of loading a .obj and adding a physics body, we
     * should load a scene file. */
    PhysicsWorld::AddRigidBody(hash,
                               PhysicsWorld::RigidBody::Type::KinematicBody);
    PhysicsWorld::AddShape(Sphere(glm::vec3(0.0f, .9f, -.1f), 1.5f), hash);

    /* The `hash` is the target Object ID (i.e. the AlienCraft ID used in the
     * `Instance` method)
     */
    Node alienCraft(hash, NodeTypeAienCraft, name);
    alienCraft.id = hash;
    alienCraftSystem.children.push_back(alienCraft);
  }
  Node& root = SceneManager::GetRootScene();
  root.children.push_back(alienCraftSystem);
}

/**
 * While waiting for a true game game loop, this function will help to reset
 * and restart the level
 */
static bool s_gamePlayStarted = false;
static void
StartLevel(const glm::vec2&                     bufferSize,
           std::vector<std::unique_ptr<Model>>& models)
{
  SOLEIL__LOGGER_DEBUG("------------------------ Starting new level");

  /* Reset all Managers and systems
   */
  EventManager::Init();
  SceneManager::Init();
  PhysicsWorld::Init();
  AlienCraft::Init();
  InstancedSystem::Init();
  ParticleSystem::Init();
  CameraManager::Init();
  BillboardTextSystem::Init();
  drawList.clear();
  paused = false;
  lights.clear();
  s_gamePlayStarted = false;
  s_playerLife      = 2;

  cameraMode = CameraMode();

  /* Load static Models (the terrain)
   * // TODO: We need to keep a models vector and bypass the
   * `SceneManager::LoadModel` due to the physics loading. We will be able to
   * use only the SceneManager When we will be able to load physics meshes.
   */
  models.push_back(std::make_unique<Model>("islands.obj", true));
  models.back()->root.transformation =
    glm::translate(glm::mat4(1.0f), glm::vec3(20.0f, 10, 410));
  models.push_back(std::make_unique<Model>("Base1.obj", true));
  // models.push_back(std::make_unique<Model>("fogplane.obj", false));
  Node& root = SceneManager::GetRootScene();
  /* And push them to the scene to be drawn
   */
  for (const auto& m : models) { root.children.push_back(m->root); }

  /* Import a scene as a YAML config file
   */
  SceneManager::ImportScene("ZincPlayer.yaml");

  // ComputeDrawList(drawList, root);
  /* Scene graph is finished to be populated, let's compute it now. */
  SceneManager::ComputeScene();

  Node* playerNode = SceneManager::GetNode(Hash("ZincAF100"));
  assert(playerNode);
  playerNode->transformation =
    glm::translate(glm::mat4(1.0), glm::vec3(27, 2, 24));

  landedCamera = nullptr;
  landedCamera = std::make_unique<ArcBallCamera>(
    static_cast<float>(bufferSize.x), static_cast<float>(bufferSize.y));
  landedCamera->view =
    glm::lookAt(glm::vec3(5, 5, 10), glm::vec3(0.0f), glm::vec3(0, 1, 0));
  CameraManager::SetCurrentCamera(landedCamera.get());

  freecam = nullptr;
  freecam = std::make_unique<FlightCamera>(static_cast<float>(bufferSize.x),
                                           static_cast<float>(bufferSize.y));
  // freecam->position = glm::vec3(0.0f, 0.0f, 200.0f);
  freecam->planePosition =
    glm::vec3(27, 2, 14); // TODO: Must be set by the animation
  // GetPosition(SceneManager::GetNode(Hash("ZincAF100"))->transformation);

  LandOffAnimation = Animation();
  LandOffAnimation.AddKeyFrame(KeyFrame(0.0f, Transform(glm::vec3(27, 2, 26))));
  LandOffAnimation.AddKeyFrame(KeyFrame(
    1.0f, Transform(glm::vec3(27, 2, 20), glm::quat(glm::vec3(0.05, 0, 0)))));
  LandOffAnimation.AddKeyFrame(KeyFrame(
    0.5f, Transform(glm::vec3(27, 2, 15), glm::quat(glm::vec3(0.1, 0, 0)))));
  LandOffAnimation.AddKeyFrame(KeyFrame(
    0.5f, Transform(glm::vec3(27, 3, 9.5), glm::quat(glm::vec3(0.2, 0, 0)))));
  LandOffAnimation.AddKeyFrame(KeyFrame(
    1.0f, Transform(glm::vec3(27, 6, -1.0), glm::quat(glm::vec3(0.05, 0, 0)))));
  LandOffAnimation.AddKeyFrame(
    KeyFrame(1.0f, Transform(glm::vec3(27, 6, -11.5))));
  LandOffAnimation.node = &(playerNode->transformation);

  FlightCamera* freecamPtr          = freecam.get();
  LandOffAnimation.onFinishCallback = [freecamPtr](Animation*) {
    freecamPtr->lockControls = false;
  };

  glEnable(GL_DEPTH_TEST);
  glClearColor(0.1f, 0.1f, 0.3f, 1.0f);

  /* Push a point light */
  lights.push_back({true, false, glm::vec3(0.1f), glm::vec3(1.0f),
                    glm::vec3(4.0f, 3.0f, 0.0f), glm::vec3(), 0.0f, 0.0f, 1.0f,
                    0.09f, 0.032f});

  /* And push a directional one */
  lights.push_back({false, false, glm::vec3(0.4f), glm::vec3(0.60f),
                    glm::normalize(glm::vec3(0.5f, 1.0f, 0.0f)), glm::vec3(),
                    0.0f, 0.0f, 1.0f, 0.7f, 1.8f});

  /* Event triggered when the player is destructed. Remove the Node and the
   * Physics Body, lock the controls and spurt a soup of triangles. Start the
   * delay of three seconds before restart.
   */
  InstancedSystem* explosionParticles =
    InstancedSystem::CreateParticleSystem("CubePlayer.obj", 1024);
  Soleil::EventManager::Enroll(
    PlayerDestructedEvent::Type(), [explosionParticles](Soleil::Event& e) {
      PlayerDestructedEvent* pde = static_cast<PlayerDestructedEvent*>(&e);
      const ObjectID         playersNodeID = Hash("ZincAF100");

      {
        AudioManager::SoundProperties prop(pde->playerPosition);
        prop.volume = 1.0f;
        AudioManager::PlaySound(Hash("explosion.ogg"), prop);
        AudioManager::StopSound(cessnaEngineAmbiantSound);
        cessnaEngineAmbiantSound = AudioManager::SoundSlot();
      }

      SceneManager::RemoveNode(playersNodeID);
      // For the moment Node::Id correspond to the Physic ID
      PhysicsWorld::RemoveRigidBody(playersNodeID);

      freecam->lockControls = true;

      for (int i = 0; i < 200; ++i) {
        Particle& p = explosionParticles->emitParticle();
        p.velocity  = glm::vec3(Random(-1.0f, 1.0f), Random(-1.0f, 1.0f),
                               Random(-1.0f, 1.0f)) *
                     100.0f;
        p.life = 3.0f;
        p.transformation =
          glm::scale(glm::translate(glm::mat4(1.0f), pde->playerPosition),
                     glm::vec3(0.1f));
        p.color = glm::vec4(Random(0.9, 1.0), Random(0.9, 1.0), 0.0f, 1.0f);
      }

      s_delayRestart = 3.0f;
      AudioManager::StopMusic();
    });

  /* General Event Hook triggered when an object is destroyed (actually just
   * hit by a ray). If it's an AlienCraft remove its node and Physics Body and
   * spurt a soup of triangles. If it's the player emit the appropriate event,
   * otherwise do nothing.
   */
  Soleil::EventManager::Enroll(
    ObjectGetHitEvent::Type(), [explosionParticles](Soleil::Event& e) {
      ObjectGetHitEvent* E = static_cast<ObjectGetHitEvent*>(&e);

      Node* n = SceneManager::GetNode(E->shootedObject);
      if (n == nullptr) {
        assert(false && "Destroying a non-existent node");
        return;
      }

      if (n->type == Hash("NodeTypeAlienCraft")) {
        glm::mat4 tr = AlienCraft::GetBoidTransform(E->shootedObject);

        for (int i = 0; i < 20; ++i) {
          Particle& p = explosionParticles->emitParticle();
          p.velocity  = glm::vec3(Random(-1.0f, 1.0f), Random(-1.0f, 1.0f),
                                 Random(-1.0f, 1.0f)) *
                       100.0f;
          p.life           = 3.0f;
          p.transformation = glm::scale(
            glm::translate(glm::mat4(1.0f), GetPosition(tr)), glm::vec3(0.1f));
          p.color = glm::vec4(Random(0.9, 1.0), Random(0.9, 1.0), 0.0f, 1.0f);
        }
        // ----

        {
          /* Play an explosion sound if the AlienCraft get a hit.
           * For the player, it will be played in the PlayerDestructedEvent
           * call.
           */
          AudioManager::SoundProperties prop(GetPosition(tr));
          prop.volume = 1.0f;
          AudioManager::PlaySound(Hash("explosion.ogg"), prop);
        }

        AlienCraft::RemoveBoid(E->shootedObject);

        // Destroy only the AlienCraft
        SceneManager::RemoveNode(E->shootedObject);
        // For the moment Node::Id correspond to the Physic ID
        PhysicsWorld::RemoveRigidBody(E->shootedObject);
      } else if (E->shootedObject == Hash("ZincAF100")) {

        /* Allow the player to be hit multiple times
         */
        if (s_playerLife > 0) {
          s_playerLife--;
          return;
        }

        const glm::vec3 position =
          GetPosition(SceneManager::GetNode(E->shootedObject)->transformation);
        EventManager::Emit(std::make_shared<PlayerDestructedEvent>(position));
      }
      return; // Destroy only the AlienCraft or the plane
    });

  // ------------------------------
  // Billboards
  selectables = std::make_unique<BaseSelectableSystem>();

  auto* foreman = selectables->create(
    BoundingBox(glm::vec3(-3, 0, -3), glm::vec3(3, 10, 3)), "Foreman");
  foreman->characterPicture = 0;
  foreman->dialog =
    "Hey Bo!\n"
    "Welcome to our modest runway. I've never constructed one as fast. I "
    "hope they won't destroy it again!";

  auto* towerControl = selectables->create(
    BoundingBox(glm::vec3(12, 2, 3), glm::vec3(18, 32, -3)), "Tower Control");
  towerControl->characterPicture = 2;
  towerControl->dialog = "Hi Bo, are you happy with your new gun?\nI guess... "
                         "Wait....\nHo Dear!.... They are coming!";
#if 0
    "We were developing a secret military invention called radio. It helped "
    "pilots close to the tower control to communicate with us. However, no "
    "electricity, no communication.";
#endif

  auto* runway = selectables->create(
    BoundingBox(glm::vec3(24, 2, 30), glm::vec3(30, 12, 12)), "Runway");
  runway->characterPicture = 1;
  runway->dialog = "Model <AF-100> - Ready\n----------------------\n* Press "
                   "<Space> to Start the mission";
  runway->buttons.push_back({"* Press <Space> to Start the mission", true});
  // runway->buttons.push_back({"* Flight to First Outpost", false});
  // runway->buttons.push_back({"* Flight to island of Lorre", false});

#ifdef ZINC__FOGPARTICLE
  // TODO: Destroy particle system
  fogParticles = ParticleSystem::CreateParticleSystem("FogParticle.png", 1000);
  for (int x = -0; x < 20; ++x) {
    for (int z = -0; z < 20; ++z) {
      Particle& p = fogParticles->emitParticle();
      p.position  = glm::vec3(float(x) * 10.0f, -9.0f, float(z) * 10.0f);
      p.life      = 9999; /// TODO: Infinite life
      p.scale     = 3.0f;
    }
  }
#endif
}

int
main(int argc, const char* argv[])
{
  (void)argc;
  (void)argv;

  /* Open the Gl Windows (fullscreen). For the moment it will do all the
   * manager initialization.
   */
  GlWindow window(1920, 1080, "Zinc");
  Zinc::Init();

  Soleil::Program meshShader;
  meshShader.attachShader(GL_VERTEX_SHADER, "mesh.vert");
  meshShader.attachShader(GL_FRAGMENT_SHADER, "mesh.frag");
  meshShader.compile();
  Soleil::MeshShader meshConfig;
  meshConfig.initialize(meshShader);

  ///////////////////
  Soleil::Program depthShader;
  depthShader.attachShader(GL_VERTEX_SHADER, "depth.vert");
  depthShader.attachShader(GL_FRAGMENT_SHADER, "depth.frag");
  depthShader.compile();
  Soleil::MeshShader depthConfig;
  depthConfig.initialize(depthShader);
  ///////////////////
  Soleil::Program depthFogShader;
  depthFogShader.attachShader(GL_VERTEX_SHADER, "depth-fog.vert");
  depthFogShader.attachShader(GL_FRAGMENT_SHADER, "depth-fog.frag");
  depthFogShader.compile();
  Soleil::MeshShader depthFogConfig;
  depthFogConfig.initialize(depthFogShader);
  ///////////////////

  Soleil::Frame frame;
  frame.nullTexture = Soleil::generateNullTexture();

  const glm::vec2 bufferSize = window.getFrameBufferSize();
  // frame.ViewPort.x = 1920.0; //bufferSize.x;
  // frame.ViewPort.y = 1080.0; //bufferSize.y;
  frame.ViewPort.x = bufferSize.x;
  frame.ViewPort.y = bufferSize.y;
  frame.ViewPort.z = 0.1f;    // Actually computed in the PlayerControl class
  frame.ViewPort.w = 1000.0f; // Actually computed in the PlayerControl class

  /////////////////////
  // foxui_renderdata uirender;
  foxui_renderdata* render = Zinc::GetFoxuiRender();
  foxui_param       uiparam;
  bzero(&uiparam, sizeof(uiparam));
  uiparam.viewport_width  = bufferSize.x;
  uiparam.viewport_height = bufferSize.y;
  uiparam.atlas_texture   = Assets::GetTexture("zinc/textures/zinc_ui.png");

  uiparam.extra_widgets_count         = 1;
  uiparam.extra_widget_names[0]       = "Gauge";
  uiparam.extra_widget_parts_count[0] = 3;

  // Looks'n'Feels loading
  foxui_configure_lnf(&uiparam);

  // TODO: MUST be in foxui_configure_lnf
  {
    size_t total = 0;
    for (size_t i = 0; i < uiparam.extra_widgets_count; ++i) {
      total += uiparam.extra_widget_parts_count[i];
    }
    uiparam.extra_widget_parts = (foxui_widget_lnf2*)foxui_malloc(
      sizeof(*uiparam.extra_widget_parts) * total);
  }

  // Create the buffer and related stuff
  // Save foxui_param in foxui_renderdata?
  const int ret = foxui_create(&uiparam, render);
  if (ret != foxui_ret_success) {
    SOLEIL__RUNTIME_ERROR("Failed to init foxui");
    throwOnGlError();
    assert(false);
  }
  const int ret_font = foxui_font_from_ttf_file(
    &uiparam, render, "/home/florian/jeux/soleil/media/zinc/PARPG.ttf");
  if (ret_font != foxui_ret_success) {
    SOLEIL__RUNTIME_ERROR("Failed to init foxui FONT");
  }

  throwOnGlError();
  /////////////////////

  // --------------------------
  // Dynamic Fog
  // TODO: Should be in a class
  // --------------------------
  Program dynfog;
  {
    // dynfog.attachShader(GL_VERTEX_SHADER, "procedural-sky.vert");
    // dynfog.attachShader(GL_FRAGMENT_SHADER, "primitive.frag");
    dynfog.attachShader(GL_VERTEX_SHADER, "procedural-fog.vert");
    dynfog.attachShader(GL_FRAGMENT_SHADER, "procedural-fog.frag");
    dynfog.compile();
  }
  // --------------------------
  // --------------------------

  // --------------------------
  // Skybox
  // TODO: Should be in a class
  // --------------------------
  Program p;
  p.attachShader(GL_VERTEX_SHADER, "skybox.vert");
  p.attachShader(GL_FRAGMENT_SHADER, "skybox.frag");
  p.compile();
  const GLint viewMatrixUniform  = p.getUniform("ViewMatrix");
  const GLint textureCubeUniform = p.getUniform("textureCube");
  GLuint      emptyVAO;
  glGenVertexArrays(1, &emptyVAO);
  GLuint textureCubeMap;

  glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
  glPixelStorei(GL_UNPACK_ROW_LENGTH, 0);
  glPixelStorei(GL_UNPACK_SKIP_PIXELS, 0);
  glPixelStorei(GL_UNPACK_SKIP_ROWS, 0);

  glGenTextures(1, &textureCubeMap);
  glBindTexture(GL_TEXTURE_CUBE_MAP, textureCubeMap);
  {
    int width, height, numberComponents;

    const std::vector<std::string> faces = {"right.jpg", "left.jpg",
                                            "top.jpg",   "bottom.jpg",
                                            "front.jpg", "back.jpg"};
    for (unsigned int i = 0; i < 6u; ++i) {

      unsigned char* data =
        stbi_load(Assets::GetPath("skybox/" + faces[i]).c_str(), &width,
                  &height, &numberComponents, 0);
      assert(data != nullptr && "Texture cube not loaded");

      glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
      glPixelStorei(GL_UNPACK_ROW_LENGTH, 0);
      glPixelStorei(GL_UNPACK_SKIP_PIXELS, 0);
      glPixelStorei(GL_UNPACK_SKIP_ROWS, 0);

      glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, GL_RGB, width, height,
                   0, GL_RGB, GL_UNSIGNED_BYTE, data);
      stbi_image_free(data);
    }
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    // glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S,
    // GL_CLAMP_TO_EDGE);
    // glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T,
    // GL_CLAMP_TO_EDGE);
    // glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R,
    // GL_CLAMP_TO_EDGE);
    glEnable(GL_TEXTURE_CUBE_MAP_SEAMLESS);
  }
  throwOnGlError();
  // --------------------

  /* Prepare the sound and music to be played
   */
  AudioManager::LoadAndRegisterSound(Hash("cessna.ogg"), "cessna.ogg");
  AudioManager::LoadAndRegisterSound(Hash("Fire.ogg"), "Fire.ogg");
  AudioManager::LoadAndRegisterSound(Hash("explosion.ogg"), "explosion.ogg");
  AudioManager::LoadAndRegisterMusic(Hash("0017.Critical_Point.ogg"),
                                     "battleThemeA.ogg");

  /* StartLevel (re)load everything. Static meshes will be stored in the
   * `models` vector (as it was back in the past). TODO now the SceneManager
   * should hold every models.
   */
  std::vector<std::unique_ptr<Model>> models;
  StartLevel(bufferSize, models);
  DebugOutputSceneGraph(&Soleil::SceneManager::GetRootScene());

  /* Noise texture used by the procedural fog */
  auto texBruit = Assets::GetTexture("bruit3.png");

  /* General Application (not only game) Loop */
  while (window.newFrame()) {
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    /* --- Camera Update --- */
    Time::StartFrame();
    DebugManager::NewFrame(Time::GetDeltaTime());
    // SOLEIL__LOGGER_DEBUG("--- DELTA TIME: ", Time::GetDeltaTime());

    if (paused == false) {
      // freecam->update(Time::GetDeltaTime() /*0.016f*/);
      // frame.updateViewProjectionMatrices(freecam->view,
      // freecam->projection);

      if (s_delayRestart > 0.0f) {
        s_delayRestart -= Time::GetDeltaTime();
        if (s_delayRestart <= 0.0f) {
          s_delayRestart = 0.0f;

          window.endFrame();

          StartLevel(bufferSize, models);

          continue;
        }
      }

      // selectables->process(frame, Time::GetDeltaTime());

      if (cameraMode.mode == CameraMode::Mode::Flight) {
        Node* player = SceneManager::GetNode(Hash("ZincAF100"));
        /* Play the animation */
        LandOffAnimation.update(Time::GetDeltaTime());
        /* And update the flying camera */
        if (LandOffAnimation.isFinished() == false) {
          freecam->planePosition = LandOffAnimation.currentTransform.position;
          // TODO: OnStartCallback:
          freecam->lockControls = true;
        } else {
          /* Move the player node
           */
          if (player) {
            player->transformation =
              glm::translate(glm::mat4(1.0f), freecam->planePosition) *
              glm::mat4_cast(freecam->modelOrientation);
          }

          /* Test if player is in the Gameplay zone, if so, pop music and
           * enemies.
           */
          // TODO: Different way to trigger the GP
          if (player && s_gamePlayStarted == false) {
            const Sphere combatZoneTrigger(glm::vec3(0.0f), 100.0f);

            if (combatZoneTrigger.contains(
                  GetPosition(player->transformation))) {
              AudioManager::PlayMusic(Hash("0017.Critical_Point.ogg"),
                                      AudioManager::MusicProperties(0.8f));

              PopEnemies();
              s_gamePlayStarted = true;
            }
          }
        }

        freecam->update(Time::GetDeltaTime() /*0.016f*/);
        frame.updateViewProjectionMatrices(freecam->view, freecam->projection);

        /* temp rotate the propeller
         */
        // TODO:
        TempRotatePropeller();
        // ---
      } else if (cameraMode.mode == CameraMode::Mode::ToTakeOff) {
        // TODO: FIXME: We should not have to process the camera (otherwise
        // the player might do different action).
        // freecam->update(Time::GetDeltaTime());

#if 0
        glm::mat4 view = landedCamera->view;
        view[3]        = glm::vec4(
          glm::mix(landedCamera->position, freecam->position, cameraMode.mix),
          1.0f);
        frame.updateViewProjectionMatrices(view, landedCamera->projection);
#else
        // const glm::vec3 pos =
        //   QuadraticBezier(cameraMode.startPosition,
        //                   cameraMode.startPosition + glm::vec3(0, 2, 0),
        //                   cameraMode.finalPosition + glm::vec3(0, 1, 0),
        //                   cameraMode.finalPosition, cameraMode.mix);

        // const glm::vec3 pos =
        // glm::cubic(cameraMode.startPosition,
        // 	   cameraMode.startPosition + glm::vec3(0, 1, 0),
        // 	   cameraMode.finalPosition + glm::vec3(0, 1, 0),
        // 	   cameraMode.finalPosition, 1.0f - cameraMode.mix);

        // const glm::vec3 pos =
        //   glm::cubic(cameraMode.finalPosition,
        //              cameraMode.finalPosition + glm::vec3(0, 1, 0),
        //              cameraMode.startPosition + glm::vec3(0, 2, 0),
        //              cameraMode.startPosition, cameraMode.mix);

        // const glm::vec3 startPosition(0, 0, 0);
        // const glm::vec3 finalPosition(50, 0, 0);
        // const glm::vec3 pos =
        //   QuadraticBezier(startPosition,
        //                   startPosition + glm::vec3(0, 100, 0),
        //                   finalPosition + glm::vec3(0, 100, 0),
        //                   finalPosition, cameraMode.mix);
        const glm::vec3 pos = glm::mix(
          cameraMode.startPosition, cameraMode.finalPosition, cameraMode.mix);
        const glm::vec3 center =
          glm::mix(cameraMode.startCenter, cameraMode.center, cameraMode.mix);

        SOLEIL__LOGGER_DEBUG(
          "SPLINE MIX=", 1.0f - cameraMode.mix, ". POS=", pos,
          ". (Start Position=", cameraMode.startPosition, ")");

        // glm::gtx::spline::catmullRom();

        // freecam->update(Time::GetDeltaTime());
        // cameraMode.finalPosition = freecam->position +
        // (freecam->cameraAttitude * freecam->offset);

        // const glm::vec3 pos = glm::mix(cameraMode.startPosition,
        // cameraMode.finalPosition, cameraMode.mix);

        // glm::mat4 view = landedCamera->view;
        // view[3]        = glm::vec4(pos, 1.0f);
        // frame.updateViewProjectionMatrices(view, landedCamera->projection);

        glm::mat4 view = glm::lookAt(pos, center, VectorUp());
        frame.updateViewProjectionMatrices(view, landedCamera->projection);

#endif
        cameraMode.mix += Time::GetDeltaTime() * 0.4f;
        // cameraMode.mix = 1.0f;
        if (cameraMode.mix >= 1.0f) {
          cameraMode.mode = CameraMode::Mode::Flight;
          CameraManager::SetCurrentCamera(freecam.get());

          /* Play in loop the Cessna engine sound */
          cessnaEngineAmbiantSound = AudioManager::PlaySound(
            Hash("cessna.ogg"), AudioManager::SoundProperties::Ambient(true));
        }

        ImGui::Begin("Camera Mix");
        {
          ImGui::Text("Start: %s", toString(cameraMode.startPosition).c_str());
          ImGui::Text("Final: %s", toString(cameraMode.finalPosition).c_str());
          ImGui::SliderFloat3("Final", glm::value_ptr(cameraMode.finalPosition),
                              -100.f, 100.f);
          ImGui::Text("Mix: %s", toString(cameraMode.mix).c_str());
        }
        ImGui::End();

      } else {
        assert(cameraMode.mode == CameraMode::Mode::Landed);

        landedCamera->update(Time::GetDeltaTime() /*0.016f*/);
        frame.updateViewProjectionMatrices(landedCamera->view,
                                           landedCamera->projection);

        if (landedCamera->toFlight) {
          cameraMode.mode = CameraMode::Mode::ToTakeOff;

          // TODO: Just get the Animation start pos
          LandOffAnimation.update(Time::GetDeltaTime());
          freecam->planePosition = LandOffAnimation.currentTransform.position;
          freecam->lockControls  = true;
          freecam->update(Time::GetDeltaTime());
          cameraMode.startPosition = landedCamera->position;
          cameraMode.startCenter   = landedCamera->center;
          cameraMode.finalPosition = freecam->position;
          cameraMode.center        = freecam->center;

          const glm::vec3 planePos = GetPosition(
            SceneManager::GetNode(Hash("ZincAF100"))->transformation);
          // cameraMode.finalPosition = planePos + glm::vec3(0, 5, 10);
          // cameraMode.center        = planePos;

          SOLEIL__LOGGER_DEBUG(
            "FREECAM.POSITION=", freecam->position,
            "Computed POSITION=", planePos + glm::vec3(0, 5, 10));
        }
      }

      ImGui::Begin("Cursor Position");
      {
        ImGui::Text("Absolute: %f, %f", Soleil::Inputs::GetCursorPosition().x,
                    Soleil::Inputs::GetCursorPosition().y);
      }
      ImGui::End();

      /* --- Scene Update --- */
      // SOLEIL__TIMER_START(SceneGraphComputation);
      PhysicsWorld::Update(Time::GetDeltaTime());
      ShootTracer::Update(Time::GetDeltaTime());
      SceneManager::ComputeScene();
      // SOLEIL__TIMER_COMPUTE(SceneGraphComputation);
      /* --- Scene Update --- */

      /* --- Components Update --- */
      AlienCraft::Update(Time::GetDeltaTime());
      /* --- Compoment Update --- */
    }

    //////////////////////////////////////////////////////////////////
    // Drawing:

    ////////////////////////////////////////////////////////////////////////////
    // Draw SCENE
    // glDepthFunc(GL_LESS);

    // --- Prepare a frame buffer for the depth rendering
    static GLuint depthFBO = GLuint(-1);
    static GLuint depthTexture;
    static GLuint dirLightDepthFBO = GLuint(-1);
    static GLuint dirLightDepthTexture;
    if (depthFBO == GLuint(-1)) {
      CreateDepthFBO(frame, &depthFBO, &depthTexture);
      CreateDepthFBO(frame, &dirLightDepthFBO, &dirLightDepthTexture);
    }

    ImGui::Begin("Font");
    {
      // static Font font = Assets::LoadFont("zinc/juju.ttf");
      static Font* font           = Assets::LoadFont("zinc/PARPG.ttf");
      const ImVec2 imageFrameSize = ImVec2(512, 512);
      ImGui::Image((void*)font->texture, imageFrameSize);

#if 0
      static BillboardText hello(font);
      hello.setCenter(glm::vec3(0, 20, 0));
      static char text[256] = {0};
      if (ImGui::InputText("Label", text, 255)) { hello.setText(text); }
      BillboardTextSystem::PushBillboard(&hello);
#endif
    }
    ImGui::End();

    // Draw Depth map for the camera
    /// -------------------
    RenderDepth(frame, depthFogConfig, depthFBO, depthTexture);
    // Draw Depth map for the light
    /// -------------------
    Frame lightFrame = frame;
    {

      static float pos[3] = {350, 500, 0};
      static float q      = 100.0f;
      static float near      = 300.0f;
      static float far      = 650.0f;
      ImGui::Begin("Depth light");
      {
        ImGui::DragFloat3("Pos", pos);
        ImGui::DragFloat3("Dir", glm::value_ptr(lights[1].position), 0.1f);
        ImGui::DragFloat("Ortho dimensions", &q);
        ImGui::DragFloat("Ortho near", &near);
        ImGui::DragFloat("Ortho far", &far);

        const glm::vec3 eye    = glm::vec3(pos[0], pos[1], pos[2]);
        const glm::vec3 center = eye - lights[1].position;
        ImGui::Text("Center: [%f, %f, %f]", center.x, center.y, center.z);
      }
      ImGui::End();

      const glm::vec3 eye = glm::vec3(pos[0], pos[1], pos[2]);
      // Node* player = SceneManager::GetNode(Hash("ZincAF100"));
      // assert(player);
      // glm::vec3 eye      = GetPosition(player->transformation);
      // eye.y += 100.0f;
      // glm::vec3       eye              = freecam->position;
      // eye.y += 200;
      const glm::vec3 lightDir         = -lights[1].position;
      const glm::quat lightOrientation = quatLookAt(lightDir, VectorUp());
      // const glm::quat lightOrientation = glm::quat();
      // const glm::vec3 center = eye+lightDir;
      const glm::vec3 center = eye + lightDir;
      glm::mat4       Projection = glm::ortho(-q, q, -q, q, near, far);
      // glm::mat4 Projection = 
      //   glm::frustum(-1.0f, 1.0f, -1.0f, 1.0f, 1.0f, 300.0f);

      glm::mat4 View = glm::lookAt(eye, center, VectorUp());
      // glm::mat4 View = glm::lookAt(eye,  center, VectorRight());
      lightFrame.updateViewProjectionMatrices(View, Projection);
      RenderDepth(lightFrame, depthConfig, dirLightDepthFBO,
                  dirLightDepthTexture);

      const float aspect = 1920.0f / 1080.0f;
      // frame.Projection = glm::frustum(-1.0f, 1.0f, -aspect, aspect, 1.0f, 300.0f);
      SetShadowMap(dirLightDepthTexture, lightFrame.ViewProjection);

      Soleil::DebugManager::DrawSphere(Sphere(eye, 0.3f), glm::vec4(1.0f));
      Soleil::DebugManager::DrawLine(eye, center, glm::vec4(1.0f));
      Soleil::DebugManager::DrawLine(eye, glm::normalize(lightDir) * 300.0f,
                                     glm::vec4(1.0f));
    }

    Draw(drawList, meshConfig, lights, frame);
    SceneManager::RenderDrawList(meshConfig, lights, frame);
    ////////////////////////////////////////////////////////////////////////////

    // ----------------------------------------------------
    throwOnGlError();

    ////////////////////////////////////////////////////////////////////////////
    // Skybox
    glDepthFunc(GL_LEQUAL);
    glUseProgram(p.program);
    glBindVertexArray(emptyVAO);

    const glm::mat4 skyboxMat =
      frame.Projection * glm::mat4(glm::mat3(frame.View));
    glUniformMatrix4fv(viewMatrixUniform, 1, GL_FALSE,
                       glm::value_ptr(skyboxMat));
    throwOnGlError();

    glActiveTexture(GL_TEXTURE0);
    throwOnGlError();
    glUniform1i(textureCubeUniform, 0);
    throwOnGlError();
    glBindTexture(GL_TEXTURE_CUBE_MAP, textureCubeMap);
    throwOnGlError();

    // Bind texture cube
    glDrawArrays(GL_TRIANGLES, 0, 36);
    throwOnGlError();
    glEnable(GL_DEPTH_TEST);
    throwOnGlError();
    glDepthFunc(GL_LESS);

    ////////////////////////////////////////////////////////////////////////////
    // glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    // --------------------------
    // Dynamic Fog
    // TODO: Should be in a class
    // --------------------------
    static float FogConstant = 5.0f;
    // static glm::vec3 FogColor    = glm::vec3(0.973f, 0.875f, 0.608f);
    static glm::vec3 FogColor    = glm::vec3(1.0f, 1.0f, 1.0f);
    static float     FogDistance = -10.0f;
    // static int       FogNumberIteration = 1;
    // static float     FogNoixOffsetX     = 0.1f;
    // static float     FogNoixOffsetY     = 0.1f;
    static float FogNoiseTiling = 100.0f;
    ImGui::Begin("Fog Configuration");
    {
      ImGui::InputFloat("Constant", &FogConstant);
      // ImGui::SliderFloat3("Fog Color:", glm::value_ptr(FogColor),
      // 0.0f, 1.0f);
      ImGui::ColorPicker3("Fog Color", glm::value_ptr(FogColor));
      ImGui::SliderFloat("Fog Distance", &FogDistance, 20.0f, -20.0f);

      //
      // ImGui::SliderInt("Fog iteration", &FogNumberIteration, 1, 10);
      // ImGui::SliderFloat("FogNoixOffsetX", &FogNoixOffsetX, 0.1f, 3.0f);
      // ImGui::SliderFloat("FogNoixOffsetY", &FogNoixOffsetY, 0.1f, 3.0f);
      ImGui::SliderFloat("FogNoiseTiling", &FogNoiseTiling, 10.0f, 1000.0f);
    }
    ImGui::End();

    // auto texBruit = Assets::GetTexture("bruit3.png");

    // glDepthFunc(GL_GREATER);
    glDisable(GL_DEPTH_TEST);
    throwOnGlError();
    glUseProgram(dynfog.program);
    throwOnGlError();
    // glUniform3f(dynfog.getUniform("FogColor"), 1.0f, 1.0f, 1.0f);
    throwOnGlError();
    // glUniform3fv(dynfog.getUniform("aV"), glm::value_ptr(const genType
    // &v));

    glUniform3fv(dynfog.getUniform("CameraPosition"), 1,
                 glm::value_ptr(CameraManager::GetCurrentCamera()->position));

    throwOnGlError();
    // glUniform1f(dynfog.getUniform("FogConstant"), FogConstant);
    throwOnGlError();
    glUniform1i(dynfog.getUniform("DepthMapTexture"), 0);
    throwOnGlError();
    glActiveTexture(GL_TEXTURE0);
    throwOnGlError();
    glBindTexture(GL_TEXTURE_2D, depthTexture);
    // glBindTexture(GL_TEXTURE_2D, texBruit);
    // glBindTexture(GL_TEXTURE_2D, Assets::GetTexture("checkboard.png"));
    throwOnGlError();

    glUniform4fv(dynfog.getUniform("ViewPort"), 1,
                 glm::value_ptr(frame.ViewPort));

    glUniform3fv(dynfog.getUniform("FogColor"), 1, glm::value_ptr(FogColor));
    glUniform1f(dynfog.getUniform("FogDistance"), FogDistance);

    //
    // glUniform1f(dynfog.getUniform("FogNoixOffsetX"), FogNoixOffsetX);
    // glUniform1f(dynfog.getUniform("FogNoixOffsetY"), FogNoixOffsetY);
    // glUniform1i(dynfog.getUniform("FogNumberIteration"),
    // FogNumberIteration);
    glUniform1f(dynfog.getUniform("FogNoiseTiling"), FogNoiseTiling);
    throwOnGlError();

    // const glm::mat4 fogMat = glm::inverse(frame.View);
    // const glm::mat4 fogMat = frame.View;
    // const glm::mat4 fogMat = glm::mat4(1.0f);
    // const glm::mat4 fogMat = frame.ViewProjection;
    // const glm::mat4 fogMat = frame.Projection;
    // auto texBruit = Assets::GetTexture("bruit3.png");
    glActiveTexture(GL_TEXTURE1);
    glUniform1i(dynfog.getUniform("NoiseTexture"), 1);
    throwOnGlError();
    glBindTexture(GL_TEXTURE_2D, texBruit);
    throwOnGlError();
    throwOnGlError();

    //
    // Lights
    if (false) {
      const Light& light = lights[1];
      glUniform1i(dynfog.getUniform("numberOfLight"), 1);
      // const glm::vec3 lightDirection(1, 1, 0);
      glUniform1i(dynfog.getUniform("lights[0].isLocal"), light.isLocal);
      glUniform1i(dynfog.getUniform("lights[0].isSpot"), light.isSpot);

      glUniform3fv(dynfog.getUniform("lights[0].ambiant"), 1,
                   glm::value_ptr(light.ambiant));
      glUniform3fv(dynfog.getUniform("lights[0].color"), 1,
                   glm::value_ptr(light.color));
      glUniform3fv(dynfog.getUniform("lights[0].position"), 1,
                   glm::value_ptr(light.position));
      // Directional Light
      glUniform3fv(dynfog.getUniform("lights[0].halfVector"), 1,
                   glm::value_ptr(normalize(light.position)));
    }
    // End Light
    //

#if 0
    const glm::mat4 fogMat = glm::inverse(frame.ViewProjection);
    glUniformMatrix4fv(dynfog.getUniform("InverseViewMatrix"), 1, GL_FALSE,
                       glm::value_ptr(fogMat));
#elif 1
    const glm::mat4 noTranslationView = glm::mat4(glm::mat3(frame.View));
    const glm::mat4 fogMat = glm::inverse(frame.Projection * noTranslationView);
    glUniformMatrix4fv(dynfog.getUniform("InverseViewMatrix"), 1, GL_FALSE,
                       glm::value_ptr(fogMat));
#else
    const glm::mat4 inverseProjectionMatrix = glm::inverse(frame.Projection);
    // const glm::mat4 inverseProjectionMatrix =
    // glm::inverse(glm::mat4(1.0f));
    glUniformMatrix4fv(dynfog.getUniform("InverseProjectionMatrix"), 1,
                       GL_FALSE, glm::value_ptr(inverseProjectionMatrix));

    const glm::mat4 inverseViewMatrix =
      glm::inverse(glm::mat4(glm::mat3(frame.View)));
    // const glm::mat4 inverseViewMatrix = glm::inverse(frame.View);
    // const glm::mat4 inverseViewMatrix = glm::inverse(glm::mat4(1.0));
    glUniformMatrix4fv(dynfog.getUniform("InverseViewMatrix"), 1, GL_FALSE,
                       glm::value_ptr(inverseViewMatrix));

#endif
    // const glm::mat4 fogMat = frame.ViewProjection;
    // const glm::mat4 fogMat = glm::translate(glm::mat4(1.0f),
    // freecam->position);
    // glUniformMatrix4fv(dynfog.getUniform("uViewRotationMtx"), 1, GL_FALSE,
    // 		       glm::value_ptr(glm::mat4(glm::mat3(glm::inverse(frame.View)))));

    throwOnGlError();

    glUniform1i(glGetUniformLocation(dynfog.program, "PCFIterations"),
		0);
    glm::mat4 scale_bias_matrix = glm::mat4(
      glm::vec4(0.5f, 0.0f, 0.0f, 0.0f),
      glm::vec4(0.0f, 0.5f, 0.0f, 0.0f),
      glm::vec4(0.0f, 0.0f, 0.5f, 0.0f),
      glm::vec4(0.5f, 0.5f, 0.5f, 1.0f));
    glm::mat4 shadow_matrix = scale_bias_matrix * lightFrame.ViewProjection;
      glActiveTexture(GL_TEXTURE2);
      throwOnGlError();
      glUniform1i(glGetUniformLocation(dynfog.program, "shadowMap"), 2);
      glBindTexture(GL_TEXTURE_2D, dirLightDepthTexture);
      throwOnGlError();
      glUniformMatrix4fv(glGetUniformLocation(dynfog.program, "LightSpace"), 1, GL_FALSE,
                         glm::value_ptr(shadow_matrix));
      throwOnGlError();



    glBindVertexArray(emptyVAO);
    throwOnGlError();
    glEnable(GL_BLEND);
    // TODO: Restore fog
    glDrawArrays(GL_TRIANGLES, 0, 6);
    throwOnGlError();
    glBindTexture(GL_TEXTURE_2D, 0);
    glActiveTexture(GL_TEXTURE0);

    ////////////////////////////////////////////////////////////////////////////
    // Particles
    // Required to be drawed after the skybox for transparancy reasons
    // TODO: Separate the Update method from the draw method an put the update
    // in the if-not-paused scope,
    ParticleSystem::DrawParticleSystems(frame, Time::GetDeltaTime());
    InstancedSystem::DrawParticleSystems(frame, Time::GetDeltaTime());
    ShootTracer::Draw(frame);

    ////////////////////////////////////////////////////////////////////////////
    // Billboards
    // TODO: The depth map may have been overwritten by the Fog and sky.
    if (cameraMode.mode == CameraMode::Mode::Landed) {
      // TODO: We may have other billboard in flight mode. We must have a
      // specific billboarding while on a base.
      BillboardTextSystem::Draw(frame);
      selectables->process(frame, Time::GetDeltaTime());
    }

    // Display UI ------------------------------------------
    int ret;
    ret = foxui_render(render);
    assert(ret == 0);

    ImGui::Begin("SDF");
    {
      const ImVec2 imageFrameSize = ImVec2(512, 512);
      ImGui::Image((void*)uiparam.atlas_font, imageFrameSize);
    }
    ImGui::End();
    throwOnGlError();

// Display Debug --------------------------------------------
#if 1 // SOLEIL__USE_IMGUI
      // AlienCraft::DisplayDebug();
      // DebugManager::DisplayMetrics(); // TODO:
    DebugManager::DisplayTools(Time::GetDeltaTime());
    DebugManager::DisplayPrimitives(frame);
#endif

    ////////////////////////////////////////////////////////////////////
    // Events
    window.endFrame();

    Soleil::EventManager::ProcessEvents();
  }

  freecam = nullptr;
  InstancedSystem::Destroy();
  ParticleSystem::Destroy();

  selectables = nullptr;
  BillboardTextSystem::Destroy();

  Soleil::AudioManager::StopMusic();

  Zinc::Destroy();
  return 0;
}

// TODO: This function must be in the render pipeline
void
RenderDepth(Soleil::Frame& frame, const Soleil::MeshShader& depthConfig,
            GLuint depthFBO, GLuint depthTexture)
{
  glEnable(GL_DEPTH_TEST);
  glBindFramebuffer(GL_FRAMEBUFFER, depthFBO);

  glClearDepth(1.0f);
  glClear(GL_DEPTH_BUFFER_BIT);

  static float polygonOffsetFactor = 2.0f;
  static float polygonOffsetUnit = 4.0f;
  ImGui::Begin("PolygonOffset");
  {
    ImGui::SliderFloat("Factor", &polygonOffsetFactor, -2.0f, 10.0f);
    ImGui::SliderFloat("Unit", &polygonOffsetUnit, -2.0f, 10.0f);
  }
  ImGui::End();
  
  glEnable(GL_POLYGON_OFFSET_FILL);
  glPolygonOffset(polygonOffsetFactor, polygonOffsetUnit);

  int viewportDim[4] = { 0 };
  glGetIntegerv(GL_VIEWPORT, viewportDim);
  glViewport(0, 0, SHADOW_MAP_WIDTH, SHADOW_MAP_HEIGHT);
  
  DrawDepth(drawList, depthConfig, frame);
  // SceneManager::RenderDrawList(depthConfig, lights, frame);
  DrawDepth(SceneManager::GetDrawList(), depthConfig, frame);
  glBindFramebuffer(GL_FRAMEBUFFER, 0);
  ImGui::Begin("Depth Computation");
  {
    ImGui::PushID(depthTexture);
    const ImVec2 imageFrameSize =
      ImVec2(frame.ViewPort.x / 4.0f, frame.ViewPort.y / 4.0f);
    ImGui::Image((void*)depthTexture, imageFrameSize, ImVec2(0, 1),
                 ImVec2(1, 0));
    ImGui::PopID();
  }
  ImGui::End();
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  glDisable(GL_POLYGON_OFFSET_FILL);

  glViewport(viewportDim[0], viewportDim[1], viewportDim[2], viewportDim[3]);
}

void
CreateDepthFBO(Soleil::Frame& frame, GLuint* depthFBO, GLuint* depthTexture)
{
  glGenFramebuffers(1, depthFBO);
  // Create the Texture
  glGenTextures(1, depthTexture);
  glBindTexture(GL_TEXTURE_2D, *depthTexture);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, SHADOW_MAP_WIDTH,
               SHADOW_MAP_HEIGHT, 0, GL_DEPTH_COMPONENT, GL_FLOAT, NULL);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  // Set up depth comparison mode
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE,
                  GL_COMPARE_REF_TO_TEXTURE);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_FUNC, GL_LEQUAL);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
  float borderColor[] = { 1.0f, 1.0f, 1.0f, 1.0f };
  glTexParameterfv(GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, borderColor);  


  
  // Attach it to the framebuffer
  glBindFramebuffer(GL_FRAMEBUFFER, *depthFBO);
  glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D,
                         *depthTexture, 0);
  glDrawBuffer(GL_NONE);
//  glReadBuffer(GL_NONE);
  if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE) {
    SOLEIL__RUNTIME_ERROR("Failed to configure the Depth rendering pass");
  }
  SOLEIL__LOGGER_DEBUG("----------------------------------------------------");
  SOLEIL__LOGGER_DEBUG("Constructed the Depth Frame Buffer -----------------");

  glBindFramebuffer(GL_FRAMEBUFFER, 0);
}
