/*
 * Copyright (C) 2019  Florian GOLESTIN
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <glm/glm.hpp>

#include <zinc/ArcBallCamera.h>

#include "GlWindow.h"
#include <Errors.h>
#include <Inputs.h>
#include <Logger.h>
#include <Utils.h>

#include <TypesToOStream.h>

#include <DebugUI.h>

namespace Soleil {

  ArcBallCamera::ArcBallCamera(const float width, const float height)
    : screenWidth(width)
    , screenHeight(height)
    , aspect(width / height)
    , fov(glm::radians(55.0f))
    , yaw(1.14f)
    , pitch(0.34f)
    , distance(0.0f, 0.0f, -100.0f)
  {
    // position = glm::vec3(50.0f, 30.0f, -50.0f);
    updateRotation();
  }

  void ArcBallCamera::update(float deltaTime)
  {	
    if (Inputs::GetKeyPress(SOLEIL__KEY_SPACE)) {
      glm::quat rot =
        glm::angleAxis(-yaw, VectorUp()) * glm::angleAxis(pitch, VectorRight());
      rot = glm::normalize(rot);

      interpolationStartQuat = rot;
      toDock                 = true;
    }

#if 0
    if (Inputs::GetKeyPress(SOLEIL__KEY_S)) {
      glm::quat rot =
        glm::angleAxis(-yaw, VectorUp()) * glm::angleAxis(pitch, VectorRight());
      rot = glm::normalize(rot);

      Save = rot;
    }
#endif

    if (toDock) {

      const float MinPitch = 0.0f;
      const float MaxPitch = glm::pi<float>() * 0.3f;
      pitch                = glm::clamp(pitch, MinPitch, MaxPitch);

      constexpr float toDockSpeed = 1.0f;
      glm::quat       rot;
      rot = glm::slerp(interpolationStartQuat, dockFlightQuat, toDockSlerpTime);
      toDockSlerpTime += toDockSpeed * deltaTime;
      if (toDockSlerpTime >= 1.0f) {
        toDockSlerpTime = 0.0f;
        toDock          = false;
        toFlight        = true;
      }

      const glm::vec3 distance(0.0f, 0.0f, -100.0f);
      position = rot * distance;

      // TODO: Update yaw pitch from new orientation?
      // TODO: we can Set Saved orientation to slerp between them (The plane,
      // the Doc, ...)

      // return ;
    }

#if SOLEIL__USE_IMGUI
    ImGui::Begin("ArcBallCamera");
    {
      const glm::dvec2 scroll = Inputs::GetMouseScroll();
      ImGui::Text("Cursor scroll: %s", toString(scroll).c_str());
      ImGui::Text("Distance: %s", toString(distance).c_str());
    }
    ImGui::End();
#endif // SOLEIL__USE_IMGUI
    const bool alienFocus = ImGui::IsAnyItemActive();

    if (Inputs::GetMouseButton(MouseButtonLeft).justPressed) {
      // previousCusorPosition = Soleil::Inputs::GetCursorPosition();
      previousCusorPosition = Soleil::Inputs::GetCursorPosition() /
                                glm::vec2(screenWidth, screenHeight) * 2.0f -
                              1.0f;
      return;
    }

    const glm::dvec2 scroll = Inputs::GetMouseScroll();
    if (scroll.y != 0.0) {
      distance.z -= scroll.y * 300.0 * deltaTime;
      distance.z = glm::clamp(distance.z, -120.0f, -20.0f);
      updateRotation();
    }

    if (Inputs::GetMouseButton(MouseButtonLeft).pressed && alienFocus == false) {
      // const auto cursorPosition = Soleil::Inputs::GetCursorPosition();
      const auto cursorPosition = Soleil::Inputs::GetCursorPosition() /
                                    glm::vec2(screenWidth, screenHeight) *
                                    2.0f -
                                  1.0f;

      if (cursorPosition != previousCusorPosition) {
        yaw += (cursorPosition.x - previousCusorPosition.x);
        pitch += (cursorPosition.y - previousCusorPosition.y);

        updateRotation();

        previousCusorPosition = cursorPosition;
      }
    }

    // TODO: Do not update each frame
    this->center     = glm::vec3(0.0f);
    this->view       = glm::lookAt(position, glm::vec3(0.0f), VectorUp());
    this->projection = glm::perspective(fov, aspect, 0.1f, 1000.0f);
  }

  void ArcBallCamera::onActivate(Camera* /*previousCamera*/)
  {
    updateRotation();
  }

  void ArcBallCamera::updateRotation()
  {
    const float MinPitch = 0.0f;
    const float MaxPitch = glm::pi<float>() * 0.3f;
    pitch                = glm::clamp(pitch, MinPitch, MaxPitch);

    glm::quat rot =
      glm::angleAxis(-yaw, VectorUp()) * glm::angleAxis(pitch, VectorRight());
    rot = glm::normalize(rot);

    position = rot * distance;
  }

  void ArcBallCamera::lookAt(const glm::vec3& /*point*/)
  {
    SOLEIL__RUNTIME_ERROR("Not implemented");
  }

} // namespace Soleil
