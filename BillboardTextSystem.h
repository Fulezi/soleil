/*
 * Copyright (C) 2019  Florian GOLESTIN
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#pragma once

#include <Font.h>
#include <Frame.h>
#include <Program.hpp>
#include <string>

namespace Soleil {

  struct BillboardTextUniforms
  {
    GLint BillboardCenter;
    GLint LineHalfWidth;
    GLint Scale;
  };

  class BillboardText
  {
  public:
    BillboardText(Font* font);
    ~BillboardText();

    BillboardText(BillboardText const&) = delete;

    void setText(std::string const& text);
    void setCenter(glm::vec3 const& center);
    void setScale(const float scale);

    glm::vec3 const& getCenter() const noexcept { return center; }

    void draw(Frame& frame, BillboardTextUniforms const& uniforms) const;

  private:
    GLuint      vertexArray;
    GLuint      vertexBuffer;
    std::size_t lineTriSize;
    glm::vec3   lineHalfWidth;
    glm::vec3   center = glm::vec3(0.0f);
    float       scale  = 0.1f;

  private:
    Font* font;
  };

  class BillboardTextSystem
  {
  public:
    static void Init();
    static void Destroy();
    static void Draw(Frame& frame);

    /// Push the billboard for the next frame only
    static void PushBillboard(BillboardText* billboard);

    /// Create a billboard mananged by the billboard system
    static BillboardText* CreateBillboard(Font* font, std::string const& text);

    /// Destroy the managed billboard
    static void DestroyBillboard(BillboardText* billboard);

  public:
    BillboardTextSystem();

    void draw(Frame& frame);

    /// Push the billboard for the next frame only
    void pushBillboard(BillboardText* billboard);

    /// Create a billboard mananged by the billboard system
    BillboardText* createBillboard(Font* font, std::string const& text);

    /// Destroy the managed billboard
    void destroyBillboard(BillboardText* billboard);

  private:
    Program                                     program;
    BillboardTextUniforms                       uniforms;
    std::vector<std::unique_ptr<BillboardText>> billboards;
    std::vector<BillboardText*>                 billboardsImmediate;
  };

} // namespace Soleil
