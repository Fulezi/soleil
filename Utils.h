/*
 * Copyright (C) 2018  Florian GOLESTIN
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#pragma once

#include <glm/gtc/quaternion.hpp>
#include <glm/matrix.hpp>
#include <glm/vec3.hpp>

namespace Soleil {

  template <typename T> int Sign(T val) { return (T(0) < val) - (val < T(0)); }

  inline glm::vec3 VectorFront() { return glm::vec3(0, 0, -1); }

  inline glm::vec3 VectorUp() { return glm::vec3(0, 1, 0); }

  inline glm::vec3 VectorRight() { return glm::vec3(1, 0, 0); }

  // inline glm::vec4& GetTransformation(glm::mat4& mat) { return mat[3]; }

  // inline const glm::vec4& GetTransformation(const glm::mat4& mat)
  // {
  //   return mat[3];
  // }

  inline float GetSquaredLength(const glm::vec3& v)
  {
    return v.x * v.x + v.y * v.y + v.z * v.z;
    // return glm::dot(v, v);
  }

  inline glm::vec3 GetPosition(const glm::mat4& mat)
  {
    // return glm::vec3(mat[3]) / mat[3][3];
    return glm::vec3(mat[3]);
  }

  inline glm::quat GetOrientation(const glm::mat4& mat)
  {
    return glm::quat_cast(mat);
  }

  inline glm::vec3 GetScale2(const glm::mat4& mat)
  {
    return glm::vec3(GetSquaredLength(glm::vec3(mat[0])),
                     GetSquaredLength(glm::vec3(mat[1])),
                     GetSquaredLength(glm::vec3(mat[2])));
  }

  /**
   * Return a number close to 1.0f if A face B, less if not facing. A negative
   * number if A face away B
   */
  inline float facing(const glm::vec3& A, const glm::vec3& directionOfA,
                      const glm::vec3& B)
  {
    const glm::vec3 aToB   = glm::normalize(B - A);
    const float     facing = glm::dot(aToB, glm::normalize(directionOfA));
    return facing;
  }

  /**
   * Limit a vector size to maxSize
   */
  inline glm::vec3 limit(const glm::vec3& vec, const float maxSize)
  {
    if (vec.length() <= maxSize) return vec;
    // return vec * (1 - maxSize / vec.length());
    return glm::normalize(vec) * maxSize;
  }

  inline glm::quat quatLookAt(const glm::vec3& dir, const glm::vec3& up)
  {
    if (glm::length(dir) == 0.0f) {
      assert(false && "Zero length direction in quatLookAt(const glm::vec3& "
                      "dir, const glm::vec3& up)");
      return glm::quat();
    }

    if (up != dir) {
      const glm::vec3 v = dir + up * -(up * dir);
      const glm::quat q(VectorFront(), v);
      const glm::quat r(v, dir);
      // return q * r; // * q;
      return r * q;
    }

    const glm::quat q(VectorFront(), dir);
    return q;
  }

  /**
   * While X is in range [A, B], returns its corresponding value in range [C, D]
   */
  inline float map(const float A, const float B, const float C, const float D,
                   const float X)
  {
    return (X - A) / (B - A) * (D - C) + C;
  }

  inline bool AreEqual(const glm::vec3& l, const glm::vec3& r)
  {
    for (int i = 0; i < l.length(); ++i) {
      const float diff = l[i] - r[i];
      if (glm::abs(diff) > glm::epsilon<float>()) return false;
    }
    return true;
  }

  inline glm::vec3 CursorToWorld(const glm::vec2& cursor, const glm::mat4& view,
                                 const glm::mat4& projection,
                                 float            depth = 0.0f)
  {
    const glm::mat4 inverseViewProjection = glm::inverse(projection * view);
    const glm::vec4 windowCursor(cursor, depth, 1.0f);

    const glm::vec4 result = inverseViewProjection * windowCursor;
    return glm::vec3(result / result.w);
    //return glm::vec3(result);
  }

} // namespace Soleil
