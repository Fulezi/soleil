/*
 * Copyright (C) 2017  Florian GOLESTIN
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include "Draw.h"

#include "SceneManager.h"
#include <deque>
#include <glm/gtc/type_ptr.hpp>

#include <DebugManager.h>
#include <DebugUI.h>

namespace Soleil {

  struct Slot
  {
    glm::mat4   trasnformation; // accumulated from parent
    const Node* node;
  };

  namespace {
    unsigned int s_shadowTexture;
    glm::mat4    s_shadowMat;
  } // namespace

  void SetShadowMap(const unsigned int texture, const glm::mat4& mat)
  {
    s_shadowTexture = texture;
    s_shadowMat     = mat;
  }

  // TODO: Reuse SceneManager data
  // TODO: Cull objects outside of the viewport
  void ComputeDrawList(DrawList& list, const Node& root)
  {
    Slot             iterator;
    glm::mat4        currentTransformation;
    std::deque<Slot> toProcess;

    toProcess.push_front({glm::mat4(1.0f), &root});

    int i = 0;
    while (toProcess.empty() == false) {
      i++;
      iterator = toProcess.front();
      toProcess.pop_front();

      currentTransformation =
        iterator.trasnformation * iterator.node->transformation;
      if (iterator.node->type == NodeTypeMesh) {
        list.push_back({currentTransformation,
                        SceneManager::GetMesh(iterator.node->object)});
      }
      for (const auto& n : iterator.node->children) {
        toProcess.push_back({currentTransformation, &n});
      }
    }

    SOLEIL__LOGGER_DEBUG("Processed ", i, " nodes including ", list.size(),
                         " meshes.");
  }

  void Draw(const DrawList& drawList, const MeshShader& config,
            const std::vector<Light>& lights, Frame& frame)
  {
    glUseProgram(config.program);

    // Lights:
    glUniform1i(config.numberOfLight, lights.size());
    unsigned int i = 0;
    for (const auto& l : lights) {
      setupLightUniform(l, config, glm::vec3(),
                        i); // TODO: Remove direction vector
      ++i;
      throwOnGlError();
    }

    // Fog:
    //     glUniform3f(p.getUniform("FogColor"), 0.8f, 0.8f, 0.8f);
    // //glUniform3fv(p.getUniform("aV"), glm::value_ptr(const genType &v));
    // glUniform3fv(p.getUniform("CameraPosition"), 1,
    // glm::value_ptr(freecam->position));

    // static float FogConstant = 10.0f;
    // ImGui::Begin("Fog Configuration");
    // {
    //   ImGui::InputFloat("Constant", &FogConstant);
    // }
    // ImGui::End();

    static int pcfIteration = 0;
    ImGui::Begin("Draw Configuration");
    {
      ImGui::SliderInt("PCF Iteration", &pcfIteration, -1, 5);
    }
    ImGui::End();

    glUniform1i(glGetUniformLocation(config.program, "PCFIterations"),
		pcfIteration);
    

    // glUniform1f(p.getUniform("FogConstant"), FogConstant);

    glm::mat4 scale_bias_matrix = glm::mat4(
      glm::vec4(0.5f, 0.0f, 0.0f, 0.0f),
      glm::vec4(0.0f, 0.5f, 0.0f, 0.0f),
      glm::vec4(0.0f, 0.0f, 0.5f, 0.0f),
      glm::vec4(0.5f, 0.5f, 0.5f, 1.0f));
    glm::mat4 shadow_matrix = scale_bias_matrix * s_shadowMat;

    // Draw:
    for (const auto& draw : drawList) {
      const glm::mat4 ModelViewProjection =
        frame.ViewProjection * draw.transformation;

      glUniformMatrix4fv(config.ModelViewProjection, 1, GL_FALSE,
                         glm::value_ptr(ModelViewProjection));
      throwOnGlError();

      glUniformMatrix4fv(config.ModelView, 1, GL_FALSE,
                         glm::value_ptr(draw.transformation));
      throwOnGlError();

      Mesh* mesh = draw.mesh;
      glUniform3fv(config.materialEmissive, 1,
                   glm::value_ptr(mesh->material.emissive));
      glUniform3fv(config.materialAmbiant, 1,
                   glm::value_ptr(mesh->material.ambiant));
      glUniform3fv(config.materialDiffuse, 1,
                   glm::value_ptr(mesh->material.diffuse));
      glUniform3fv(config.materialSpecular, 1,
                   glm::value_ptr(mesh->material.specular));
      throwOnGlError();

      glUniform1f(config.materialShininess, mesh->material.shininess);
      throwOnGlError();
      glUniform1f(config.materialOpacity, mesh->material.opacity);
      throwOnGlError();

      glActiveTexture(GL_TEXTURE0);
      throwOnGlError();
      glUniform1i(config.materialDiffuseTexture, 0);
      throwOnGlError();
      if (mesh->material.diffuseTexture > 0) {
        glBindTexture(GL_TEXTURE_2D, mesh->material.diffuseTexture);
      } else {
        glBindTexture(GL_TEXTURE_2D, frame.nullTexture);
      }
      throwOnGlError();

      glActiveTexture(GL_TEXTURE1);
      throwOnGlError();
      glUniform1i(config.shadowMap, 1);
      glBindTexture(GL_TEXTURE_2D, s_shadowTexture);
      throwOnGlError();
      glUniformMatrix4fv(config.LightSpace, 1, GL_FALSE,
                         glm::value_ptr(shadow_matrix));
      throwOnGlError();
      glUniformMatrix4fv(config.Model, 1, GL_FALSE,
                         glm::value_ptr(draw.transformation));
      throwOnGlError();

      // glUniform3fv(config.diffuseColor, 1, glm::value_ptr(diffuseColor));

      // assert(textures.size() < 2 && "Currently only one texture supported");

      // // Diffuse texture
      // glActiveTexture(GL_TEXTURE0);
      // glUniform1i(config.diffuseTexture, 0);
      // if (textures.size() == 1) {
      //   glBindTexture(GL_TEXTURE_2D, textures[0].id);
      // } else {
      //   glBindTexture(GL_TEXTURE_2D, frame.nullTexture);
      // }
      throwOnGlError();

      glBindVertexArray(mesh->vertexArray);
      glDrawElements(GL_TRIANGLES, mesh->numIndices, GL_UNSIGNED_INT, 0);
      glBindVertexArray(0);
      throwOnGlError();
    }
  }

  void DrawDepth(const DrawList& drawList, const MeshShader& config,
                 Frame& frame)
  {
    glUseProgram(config.program);
    glCullFace(GL_FRONT);
    for (const auto& draw : drawList) {
#if 0
      const glm::mat4 ModelViewProjection =
        frame.ViewProjection * draw.transformation;
      
      glUniformMatrix4fv(config.ModelViewProjection, 1, GL_FALSE,
                         glm::value_ptr(ModelViewProjection));
#else
      const glm::mat4 ModelView = frame.View * draw.transformation;

      const glm::mat4 Projection = frame.Projection;

      // TODO: Fog depth only
      glUniformMatrix4fv(glGetUniformLocation(config.program, "ModelView"), 1,
                         GL_FALSE, glm::value_ptr(ModelView));
      glUniformMatrix4fv(glGetUniformLocation(config.program, "Projection"), 1,
                         GL_FALSE, glm::value_ptr(Projection));
      glUniform1f(glGetUniformLocation(config.program, "Far"),
                  frame.ViewPort.w);

      // TODO: Depth only
      const glm::mat4 ModelViewProjection =
        frame.ViewProjection * draw.transformation;
      glUniformMatrix4fv(
        glGetUniformLocation(config.program, "ModelViewProjection"), 1,
        GL_FALSE, glm::value_ptr(ModelViewProjection));
#endif

      Mesh* mesh = draw.mesh;
      glBindVertexArray(mesh->vertexArray);
      glDrawElements(GL_TRIANGLES, mesh->numIndices, GL_UNSIGNED_INT, 0);
      glBindVertexArray(0);
      throwOnGlError();
    }
    glCullFace(GL_BACK);
  }

} // namespace Soleil
