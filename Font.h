
#pragma once

#include <OpenGLInclude.h>
#include <glm/vec2.hpp>
#include <string>
#include <vector>

#include <assets/stb_truetype.h> // TODO: How to not include this

namespace Soleil {
  struct TriGlyph
  {
    glm::vec2 advance;
    glm::vec2 tex;
  };

  class Font
  {
  public:
    void release();

    /// Convert a line of text into a polygon (list of triangles + uv).
    /// @param size Return the maximum size (width and height)
    /// @param textScale Scale the vertices. A scale of 1 means the vertex has
    /// the size of the glyph.
    /// @param widthMax Maximum width before breaking the line (in the previous
    /// space at best or on the word as worst). A value of 0 deactivate this
    /// feature.
    std::vector<TriGlyph> writeLine(std::string const& line, glm::vec2& size,
                                    const float textScale = 1.0f,
                                    float       widthMax  = 0.0f);

  private:
    void backFont();

  public:
    // std::vector<stbtt_bakedchar> cdata;
    std::vector<stbtt_packedchar> cdata;
    GLuint                        texture;
    stbtt_fontinfo                info;

    int   width    = 512;
    int   height   = 512;
    float fontSize = STBTT_POINT_SIZE(32.0f);

  public:
    static std::unique_ptr<Font> CreateWithDefault(const std::string& path);
  };
} // namespace Soleil
