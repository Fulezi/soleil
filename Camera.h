/*
 * Copyright (C) 2017  Florian GOLESTIN
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#pragma once

#include <glm/matrix.hpp>

namespace Soleil {

  class Camera
  {
  public:
    virtual void update(float deltaTime) = 0;
    virtual void onActivate(Camera* /*previousCamera*/) {}
    virtual void onDeactivate() {}

  public:
    glm::mat4 view;
    glm::mat4 projection;

  public:
    glm::vec3 position = glm::vec3(0.0f);
    glm::vec3 center   = glm::vec3(0, 0, -1);
    glm::vec3 up       = glm::vec3(0, 1, 0);
  };

  class Freecam : public Camera
  {
  public:
    Freecam(const float width, const float height);
    virtual void update(float deltaTime) override;
    virtual void onActivate(Camera* previousCamera) override;

    void lookAt(const glm::vec3& point);

    float aspect;
    float fov;
    float yaw;
    float pitch;

    glm::vec3 velocity;
    // glm::vec3 direction;
    glm::vec2 previousCusorPosition;
  };

  class EditorCamera : public Freecam
  {
  public:
    EditorCamera(const float width, const float height);
    virtual void update(float deltaTime) override;
    virtual void onActivate(Camera* previousCamera) override;
  };

} // namespace Soleil
