/*
 * Copyright (C) 2017  Florian GOLESTIN
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/**
 * Inputs can be managed differently such as
 */
// enum ButtonState
// {
//   Inactive,
//   JustPressed,
//   Active,
//   Released
// };
//
// char buttons[NumberOfButtons] = {Inactive};
//
// // On Callback:
// button[i] = JustePressed;
//
// // On each frame:
// if (button[i] > Inactive) {
//   button[i]++;
//   if (button[i] > Released) {
//     button[i == Inactive];
//   }
// }
/*
 */

#pragma once

#include <EventManager.h>
#include <glm/vec2.hpp>
#include <map>
#include <stringutils.h>

namespace Soleil {

  // TODO: Enum?
  constexpr int MouseMaximumInputButton  = 3;
  constexpr int MouseButtonLeft          = 0;
  constexpr int KeyboardMaximumInputKeys = 255;

  struct MouseButton
  {
    bool pressed      = false;
    bool justPressed  = false;
    bool justReleased = false;

    MouseButton() = default;
  };

  enum SOLEIL__KEYS
  {
    SOLEIL__KEY_FORWARD = 0,
    SOLEIL__KEY_BACKWARD,
    SOLEIL__KEY_LEFT,
    SOLEIL__KEY_RIGHT,
    SOLEIL__KEY_F1,
    SOLEIL__KEY_F2,
    SOLEIL__KEY_F3,
    SOLEIL__KEY_F4,
    SOLEIL__KEY_F5,
    SOLEIL__KEY_F6,
    SOLEIL__KEY_F7,
    SOLEIL__KEY_F8,
    SOLEIL__KEY_F9,
    SOLEIL__KEY_F10,
    SOLEIL__KEY_F11,
    SOLEIL__KEY_F12,
    SOLEIL__KEY_PAUSE,
    SOLEIL__KEY_SPACE,
  };

  // TODO: JustPressed key state can be represented by storing the frame number
  // the key was pressed and checking if the current frame is at distance 0 or 1
  // or not.
  // TODO: Rename InputsManager
  class Inputs
  {
  public:
    using Key      = unsigned char;
    using KeyState = bool;
    using KeyMap   = std::map<int, Key>;

  public:
    Inputs();
    glm::vec2          getCursorPosition(void) const noexcept;
    glm::vec2          getMouseOffsetMove(void) const noexcept;
    void               setCursorPosition(glm::vec2 position);
    void               setMouseButtonPressed(int button, bool pressed);
    const MouseButton& getMouseButton(int button) const noexcept;
    void               setMouseScroll(glm::dvec2 const& offset);
    glm::dvec2 const&  getMouseScroll(void) const noexcept;

    /// Return a value between 0.0f (the key is completely released) and 1.0f
    /// (the key is completely pressed). Most inputs cannot handle value in
    /// between and either 0 or 1 will be returned. The param `key` is can came
    /// from the `SOLEIL__KEYS` enum of from something else.
    float getKeyPress(Key key);

    /// Translate key from any underlying system (int) to the Soleil Key mapping
    /// (see SOLEIL__KEYS enum).
    bool translateKey(int key, Key& out);

    /// Update the state of a key (pressed or released).
    void updateKeyState(Key key, KeyState state);

  public:
    void update();

  public:
    glm::vec2  offsetCursorPosition;
    glm::vec2  cursorPosition;
    glm::dvec2 scrollOffset;

    MouseButton mouseButtons[MouseMaximumInputButton];
    KeyState    keys[KeyboardMaximumInputKeys];

    KeyMap keymap;

  public:
    static void               Init(KeyMap keymap);
    static void               Destroy();
    static void               Update();
    static glm::vec2          GetCursorPosition(void) noexcept;
    static glm::vec2          GetMouseOffsetMove(void) noexcept;
    static void               SetCursorPosition(glm::vec2 position);
    static void               SetMouseButtonPressed(int button, bool pressed);
    static const MouseButton& GetMouseButton(int button);
    static void               SetMouseScroll(glm::dvec2 const& offset);
    static glm::dvec2 const&  GetMouseScroll(void) noexcept;
    static float              GetKeyPress(Key key);
    static bool               TranslateKey(int key, Key& out);
    static void               UpdateKeyState(Key key, KeyState state);
  };

  class KeyUpdateEvent : public Soleil::Event
  {
  public:
    constexpr static unsigned long Type() { return Hash("KeyUpdateEvent"); }

    KeyUpdateEvent(Inputs::Key key, Inputs::KeyState state, float pressure)
      : Soleil::Event(Type())
      , key(key)
      , state(state)
      , pressure(pressure)
    {}

    Inputs::Key      key;
    Inputs::KeyState state;
    float            pressure;
  };

} // namespace Soleil
