/*
 * Copyright (C) 2018  Florian GOLESTIN
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#pragma once

#include <Node.h>
#include <memory>
#include <physics/Intersections.h>

namespace Soleil {

  class PhysicsWorld
  {
  public:
    struct RayCastResult
    {
      std::size_t           count;
      std::vector<ObjectID> objects;
    };

    enum class ShapeType
    {
      Sphere,
      Polygon
    };

    typedef Polygon* PolygonPtr;

    struct Shape
    {
      ShapeType type;
      union
      {
        Sphere     sphere;
        PolygonPtr polygon;
        Triangle   t;
      };
      // ObjectID id;

      Shape(Sphere s // , const ObjectID id = 0
            )
        : type(ShapeType::Sphere)
        , sphere(s)
      //, id(id)
      {}

      Shape(PolygonPtr p // , const ObjectID id = 0
            )
        : type(ShapeType::Polygon)
        , polygon(p)
      //, id(id)
      {}

      ~Shape() = default;

      ObjectID rigidBodyID = 0;
      // std::vector<Shape> children;
    };

    struct RigidBody
    {
    public:
      enum class Type
      {
        StaticBody,
        RigidBody,
        KinematicBody
      };

    public:
      ObjectID           rigidBodyID;
      std::vector<Shape> shape; // TODO: A vector of shape
      Type               type;

    public:
      RigidBody(ObjectID oid, Type const type)
        : rigidBodyID(oid)
        , type(type)
      {}
    };

    struct MotionState
    {
      // Real one:
      // constexpr static float Gravity = -9.78033f;
      constexpr static float Gravity = -0.0978033f;

      ObjectID physicBodyID; // TODO: Rename rigidBodyID

      // Primary
      glm::vec3 position = glm::vec3(0.0f);
      glm::vec3 force    = glm::vec3(0.0f, Gravity, 0.0f); /// Or momentum
      //
      glm::quat orientation     = glm::quat();
      glm::vec3 angularMomentum = glm::vec3(0.0f, 0.0f, 0.0f);

      // Secondary
      glm::vec3 velocity     = glm::vec3(0.0f);
      glm::vec3 acceleration = glm::vec3(0.0f);
      //
      glm::quat spin            = glm::quat();
      glm::vec3 angularVelocity = glm::vec4(0.0f);

      // Constant
      float mass;
      float inverseMass;
      float friction;
      //
      float intertia;
      float inverseInertia;

      MotionState(ObjectID physicBodyID, const float mass = 1.0f,
                  const float friction = 0.1f, const float inertia = .10f)
        : physicBodyID(physicBodyID)
        , mass(mass)
        , inverseMass(1.0f / mass)
        , friction(friction)
        , intertia(inertia)
        , inverseInertia(1.0f / inertia)
      {}

      /// Must be called when a primary or a constant changes
      void compute() {
	acceleration = force * inverseMass;

	angularVelocity += angularMomentum * inverseInertia;
	orientation = glm::normalize(orientation);

	glm::quat q(0.0f, angularVelocity);
	spin = 0.5f * q * orientation;

	spin = glm::normalize(spin);
      }
    };

    typedef std::unique_ptr<Shape> ShapePtr;

  public:
    // /// Compute this node and hierarchy as one bounding sphere. Use the root
    // ID
    // /// (0 is skipped) as this bounding shape ID. Add the shape to the
    // pysical
    // /// world. The shape is intended to be static (never moved).
    // void computeAndAddStaticShapeAsSphere(Node* n);
    // // The fact there is a bhv or not is a matter of internal implementation

    /// Compute this node as one bounding type. Use the root ID
    /// (0 is skipped) as this bounding shape ID. Add the shape to the pysical
    /// world. The shape is intended to be static (never moved).
    void addSphereFromVertices(const std::vector<glm::vec3>& vertices,
                               ShapeType shapeType, ObjectID id = 0);
    // The fact there is a bhv or not is a matter of internal implementation

    void addSphereShapeFromPolygon(Polygon polygon, ObjectID id);

    void addShape(Polygon polygon, ObjectID id = 0);
    void addShape(Sphere sphere, ObjectID id);
    void transformShapeAndChildren(Shape& shape, glm::mat4 const& tr);

  public:
    void       addRigidBody(ObjectID id, RigidBody::Type bodyType);
    RigidBody* getRigidBody(ObjectID physicBodyID);
    void       removeRigidBody(ObjectID const id);
    void       updateRigidBodyTransform(ObjectID const id, glm::mat4 const& tr);

    MotionState* findMotionState(const ObjectID physicBodyID);

  protected:
    void removeMotionState(const ObjectID physicBodyID);

  public:
    void update(const float deltaTime, int maxStep = 1,
                const float stepTime = 1.0f / 60.0f);

  public:
    /* ------------- Data Holder ----------------*/
    std::vector<Polygon*>    polygons;
    std::vector<ShapePtr>    shapes;
    std::vector<RigidBody>   entities;
    std::vector<MotionState> motionStates;

    std::vector<Shape> world;
    // TODO: Shapes haves to be allocated contiguously

    bool doDebugDraw = false;

  public:
    RayCastResult rayCast(const glm::vec3& startPoint,
                          const glm::vec3& endPoint, ObjectID sender = 0);

  private:
    bool intersect(RigidBody* rigidBody);
    bool intersect(Shape const& s1, Shape const& s2);
    bool intersectSphereShape(Shape const& s1, Shape const& s2);

    /* ------------- Statics ------------------- */
  public:
    static void Init();
    static void AddSphereFromVertices(const std::vector<glm::vec3>& vertices,
                                      ShapeType shapeType, ObjectID id = 0);
    static void AddSphereShapeFromPolygon(Polygon polygon, ObjectID id = 0);

    static void AddShape(Polygon polygon, ObjectID id = 0);
    static void AddShape(Sphere sphere, ObjectID id);

    static void         AddRigidBody(ObjectID id, RigidBody::Type bodyType);
    static void         RemoveRigidBody(ObjectID const id);
    static void         UpdateRigidBodyTransform(ObjectID const   id,
                                                 glm::mat4 const& tr);
    static MotionState* FindMotionState(const ObjectID physicBodyID);

    static RayCastResult RayCast(const glm::vec3& startPoint,
                                 const glm::vec3& endPoint,
                                 ObjectID         sender = 0);

    static void Update(const float deltaTime, int maxStep = 1,
                       const float stepTime = 1.0f / 60.0f);
  };

} // namespace Soleil
