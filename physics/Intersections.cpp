#include <physics/Intersections.h>

#include <Logger.h>
#include <glm/gtc/matrix_access.hpp>

#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtx/string_cast.hpp>

#include <BoundingBox.h>
#include <Random.h>

namespace Soleil {

  static float Determinant(const glm::mat4& m)
  {
    const float d = glm::determinant(m);
    if (glm::abs(d) < glm::epsilon<float>()) {
      SOLEIL__LOGGER_DEBUG("Determinant is: ", d, "\n");
      // assert(false && "Determinant is zero");
    }
    return d;
  }

#if 1

  Sphere ComputeMinimalBoundingShpere(const std::vector<glm::vec3>& points)
  {
    if (points.size() == 0) return Sphere();

    BoundingBox bb;
    glm::vec3   mean(0.0f);
    for (std::size_t i = 0; i < points.size(); ++i) {
      bb.expandBy(points[i]);
      mean += points[i];
    }
    mean /= points.size();

    float radius = 0.0f;
    for (std::size_t i = 0; i < points.size(); ++i) {
      // radius = glm::max(radius, glm::distance(mean, points[i]));
      // radius = glm::min(radius, glm::distance(mean, points[i]));
      // radius += glm::distance(mean, points[i]);
      radius = glm::max(radius, glm::distance(mean, points[i]));
    }
    // radius /= numberPoints;
    return Sphere(mean, radius);
  }

// ---------------------------------------------------------------------------
#else

  Sphere ComputeMinimalBoundingShpere(std::vector<glm::vec3>& points,
                                      std::vector<glm::vec3>& sos)
  {
    // Stop the recursion when we have no more points
    // Or the number of support is more than three
    if (points.size() == 0 || sos.size() == 4) {
      switch (sos.size()) {
        case 0: return Sphere();
        case 1: return Sphere(sos[0], 0.00f);
        case 2: {
          const glm::vec3 halfPan = (sos[1] - sos[0]) * 0.5f;
          return Sphere(sos[0] + halfPan, glm::length(halfPan));
        };
        case 3: {
#if 0
          // If we have three points: Look for the barycenter
          const glm::vec3& A = sos[0];
          const glm::vec3& B = sos[1];
          const glm::vec3& C = sos[2];
          const float      v = 1.0f / 3.0f;
          const float      w = 1.0f / 3.0f;

          const glm::vec3 P      = ((1 - v - w) * A) + v * B + w * C;
          const float     radius = glm::length(P - A);
          return Sphere(P, radius);
#else
          // const glm::vec3 a        = sos[1] - sos[0];
          // const glm::vec3 b        = sos[2] - sos[0];
          // const glm::vec3 c        = sos[0];
          // const float     IIaII    = glm::length(a);
          // const float     IIbII    = glm::length(b);
          // const glm::vec3 axb      = glm::cross(a, b);
          // const float     IIaxbII  = glm::length(axb);
          // const float     IIaxbII2 = IIaxbII * IIaxbII;

          // const glm::vec3 p =
          //   (glm::cross((IIaII * IIaII * b - IIbII * IIbII * a), axb) /
          //    (IIaxbII2 * 2.0f)) +
          //   c;
          // const float r = glm::length((IIaII * IIbII * glm::length(a - b)) /
          //                             (2.0f * IIaxbII));
          // return Sphere(p, r);

          const glm::vec3& c = sos[2];
          const glm::vec3  A = sos[0] - c;
          const glm::vec3  B = sos[1] - c;

          const glm::vec3 cross = glm::cross(A, B);
          const glm::vec3 center =
            c + glm::cross(GetSquaredLength(A) * B - GetSquaredLength(B) * A,
                           cross) /
                  (2.0f * GetSquaredLength(cross));
          return Sphere(center, glm::distance(sos[0], center));
#endif
        };
        case 4: {
#if 0
          // If we have four points: Look for the barycenter
          const glm::vec3& A = sos[0];
          const glm::vec3& B = sos[1];
          const glm::vec3& C = sos[2];
          const glm::vec3& D = sos[3];
          const float      v = 0.25f;
          const float      w = 0.25f;
          const float      x = 0.25f;

          const glm::vec3 P = ((1 - v - w - x) * A) + v * B + w * C + x * D;
          const float     radius = glm::length(P - A);
          return Sphere(P, radius);
#else
          const glm::vec3& p1 = sos[0];
          const glm::vec3& p2 = sos[1];
          const glm::vec3& p3 = sos[2];
          const glm::vec3& p4 = sos[3];

          return Sphere::FromTetrahedron(p1, p2, p3, p4);
#endif
        };
        default:
          assert(false && "We cannot have more than four Set of Support");
      }
    }

#if 0
    const int    indexPoint = numberPoints - 1;
    const Sphere smallestSphere =
      ComputeMinimalBoundingShpere(points, numberPoints - 1, sos, sos.size());
    if (smallestSphere.contains(points[indexPoint])) { return smallestSphere; }

    // assert(std::find(sos, sos + numberSos, points[indexPoint], inf) == sos +
    // numberSos);
    for (std::size_t i = 0; i < numberSos; ++i) {
      if (AreEqual(sos[i], points[indexPoint])) {
        // assert(false && "Already exists");
        return smallestSphere;
      }
    }

    //sos[numberSos] = points[indexPoint];
    sos.push_back(points[indexPoint]);
    // return ComputeMinimalBoundingShpere(points, numberPoints - 1, sos,
    //                                     numberSos + 1);
    return ComputeMinimalBoundingShpere(points, numberPoints - 1, sos,
                                        sos.size());
#else
    std::size_t removeAt = Random(std::size_t(0), points.size() - 1);
    SOLEIL__LOGGER_DEBUG("-------------------- Removed: ", removeAt);
    const glm::vec3 removed = points[removeAt];
    points[removeAt] = points.back();
    points.pop_back();

    Sphere smallestSphere = ComputeMinimalBoundingShpere(points, sos);

    bool alreadyInSet = false;
    for (const glm::vec3 os : sos) {
      if (AreEqual(os, removed)) {
        alreadyInSet = true;
        break;
      }
    }

    if (alreadyInSet == false && smallestSphere.contains(removed) == false) {
      sos.push_back(removed);
      smallestSphere = ComputeMinimalBoundingShpere(points, sos);
      sos.pop_back();
    }
    points.push_back(removed);
    return smallestSphere;
#endif
  }

#endif
  // ---------------------------------------------------------------------------

  Sphere Sphere::FromTetrahedron(const glm::vec3& p1, const glm::vec3& p2,
                                 const glm::vec3& p3, const glm::vec3& p4)
  {
    // assert(AreEqual(p1, p2) == false);
    // assert(AreEqual(p1, p3) == false);
    // assert(AreEqual(p1, p4) == false);
    // assert(AreEqual(p2, p3) == false);
    // assert(AreEqual(p2, p4) == false);
    // assert(AreEqual(p3, p4) == false);

    // const float tripleScalarProduct =
    //   glm::dot(p4 - p1, glm::cross(p3 - p1, p2 - p1));
    // assert(glm::abs(tripleScalarProduct) > glm::epsilon<float>() &&
    //        "Points are coplanar");
    // if (glm::abs(tripleScalarProduct) <= glm::epsilon<float>()) {
    //   return Sphere();
    // }
    //

    // const glm::vec3 p1(21.21, 42.21, 84.21);
    // const glm::vec3 p2(21.42, 42.42, 84.42);
    // const glm::vec3 p3(21.84, 42.84, 84.84);
    // const glm::vec3 p4(21.168, 42.168, 84.168);

    // const glm::mat4 matrix = {
    //   glm::vec4(p1, 1.0f),
    //   glm::vec4(p2, 1.0f),
    //   glm::vec4(p3, 1.0f),
    //   glm::vec4(p4, 1.0f),
    // };
    // const glm::mat4 matrix =
    //   glm::transpose(glm::mat4(glm::vec4(p1, 1.0f),
    //   glm::vec4(p2, 1.0f),
    //                            glm::vec4(p3, 1.0f),
    //                            glm::vec4(p4, 1.0f)));
#if 1
    glm::mat4 matrix(1.0f);
    matrix = glm::row(matrix, 0, glm::vec4(p1.x, p2.x, p3.x, p4.x));
    matrix = glm::row(matrix, 1, glm::vec4(p1.y, p2.y, p3.y, p4.y));
    matrix = glm::row(matrix, 2, glm::vec4(p1.z, p2.z, p3.z, p4.z));
    matrix = glm::row(matrix, 3, glm::vec4(1.0f, 1.0f, 1.0f, 1.0f));

    // matrix = glm::transpose(matrix);
    // SOLEIL__LOGGER_DEBUG("Mat=", glm::to_string(matrix));

    const float a = Determinant(matrix);
    // SOLEIL__LOGGER_DEBUG(
    //   "----------------------------------------------------");
    // const float a = glm::determinant(matrix);
    // if (a == 0.0f) {
    //   SOLEIL__LOGGER_DEBUG(
    //     "DETERMINANT IS 0 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1");
    // } else {
    //   SOLEIL__LOGGER_DEBUG("DETERMINANT IS OKAY: ", a);
    // }

    glm::mat4 D = matrix;
    glm::vec3 center(0.0f);

    const glm::vec4 squares = {
      GetSquaredLength(p1),
      GetSquaredLength(p2),
      GetSquaredLength(p3),
      GetSquaredLength(p4),
    };

    // D[0] = squares;
    D        = glm::row(D, 0, squares);
    center.x = glm::determinant(D);

    // D[1] = matrix[0];
    D        = glm::row(D, 1, glm::row(matrix, 0));
    center.y = -glm::determinant(D);

    // D[2] = matrix[1];
    D        = glm::row(D, 2, glm::row(matrix, 1));
    center.z = glm::determinant(D);

    // if (a <= glm::epsilon<float>()) {
    //   center = glm::vec3(0.0f);
    // } else {
    center /= (2.0f * a);
    // }
#else
    glm::mat4 matrix(glm::vec4(p1, 1.0f), glm::vec4(p2, 1.0f),
                     glm::vec4(p3, 1.0f), glm::vec4(p4, 1.0f));

    const float a = Determinant(matrix);
    // SOLEIL__LOGGER_DEBUG(
    //   "----------------------------------------------------");
    // const float a = glm::determinant(matrix);
    // if (a == 0.0f) {
    //   SOLEIL__LOGGER_DEBUG(
    //     "DETERMINANT IS 0 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1");
    // } else {
    //   SOLEIL__LOGGER_DEBUG("DETERMINANT IS OKAY: ", a);
    // }

    glm::mat4 D = matrix;
    glm::vec3 center(0.0f);

    const glm::vec4 squares = {
      GetSquaredLength(p1),
      GetSquaredLength(p2),
      GetSquaredLength(p3),
      GetSquaredLength(p4),
    };

    // D[0] = squares;
    D        = glm::column(D, 0, squares);
    center.x = glm::determinant(D);

    // D[1] = matrix[0];
    D        = glm::column(D, 1, glm::column(matrix, 0));
    center.y = -glm::determinant(D);

    // D[2] = matrix[1];
    D        = glm::column(D, 2, glm::column(matrix, 1));
    center.z = glm::determinant(D);

    // if (a <= glm::epsilon<float>()) {
    //   center = glm::vec3(0.0f);
    // } else {
    center /= (2.0f * a);
    // }

#endif
    return Sphere(center, glm::distance(p1, center));
  }

} // namespace Soleil
