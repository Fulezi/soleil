/*
 * Copyright (C) 2018  Florian GOLESTIN
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <physics/Intersections.h>
#include <physics/PhysicsWorld.h>

#include <DebugManager.h>
#include <glm/vec4.hpp>

// TODO: Triangle may have sphere/AABB?
// TODO: Triangle in octree
// TODO: We cannot move a static body at all
// TODO: One RigidBody for all the sames models?
// TODO: FIXME Move a plygon, move its sphere
// TODO: FIXME If Polygon.boundingSphere is not initialized, it will raise a
// false negative! Add a constructor!

namespace Soleil {
  static glm::vec3 TransformVec3(const glm::mat4& tr, const glm::vec3& vec)
  {
    return glm::vec3(tr * glm::vec4(vec, 1.0f));
  }

  void PhysicsWorld::addSphereFromVertices(
    const std::vector<glm::vec3>& vertices, ShapeType shapeType, ObjectID id)
  {
    assert(false && "Deprecated");
    switch (shapeType) {
      case ShapeType::Sphere: {
        Sphere s = ComputeMinimalBoundingShpere(vertices);
        // this->shapes.emplace_back(std::make_unique<Shape>(s));
        RigidBody* e = this->getRigidBody(id);
        assert(e && "RigidBody not foud from ID");
        e->shape.push_back(s);
      } break;
      case ShapeType::Polygon: {
        assert(false && "Deprecated?");
        // Sphere s = ComputeMinimalBoundingShpere(vertices);
        // Shape  earlyOut(s);
        // this->shapes.push_back(earlyOut);
      } break;
      default: assert(false && "Unknown shape type"); return;
    }
  }

  void PhysicsWorld::addSphereShapeFromPolygon(Polygon polygon, ObjectID id)
  {
    // TODO: Specialize the ComputeMinimalBoundingShpere to accept polygon
    const Sphere s = [&polygon]() {
      std::vector<glm::vec3> vertices;
      vertices.reserve(polygon.triangles.size() * 3);

      for (const auto& tri : polygon.triangles) {
        vertices.push_back(tri.vert[0]);
        vertices.push_back(tri.vert[1]);
        vertices.push_back(tri.vert[2]);
      }
      return ComputeMinimalBoundingShpere(vertices);
    }();

    // this->shapes.emplace_back(std::make_unique<Shape>(s));
    RigidBody* e = this->getRigidBody(id);
    assert(e && "RigidBody not foud from ID");
    e->shape.push_back(s);
    e->shape.back().rigidBodyID = e->rigidBodyID;
  }

  void PhysicsWorld::addShape(Polygon polygon, ObjectID id)
  {
    // TODO: Specialize the ComputeMinimalBoundingShpere to accept polygon
    const Sphere s = [&polygon]() {
      std::vector<glm::vec3> vertices;
      vertices.reserve(polygon.triangles.size() * 3);

      for (const auto& tri : polygon.triangles) {
        vertices.push_back(tri.vert[0]);
        vertices.push_back(tri.vert[1]);
        vertices.push_back(tri.vert[2]);
      }
      return ComputeMinimalBoundingShpere(vertices);
    }();

    // TODO: Raw Ppointer ... RRROOOAAARR pointer!!
    Polygon* p        = new Polygon(std::move(polygon));
    p->boundingSphere = s;
    polygons.emplace_back(p);

    // TODO: Add Sphere to polygon
    // Shape shape(s);
    // shape.children.push_back(Shape(p));

    RigidBody* e = this->getRigidBody(id);
    assert(e && "RigidBody not foud from ID");
    // e->shape.push_back(shape);
    e->shape.push_back(Shape(p));
    e->shape.back().rigidBodyID = e->rigidBodyID;
  }

  void PhysicsWorld::addShape(Sphere sphere, ObjectID id)
  {
    // this->shapes.emplace_back(std::make_unique<Shape>(sphere));

    RigidBody* rb = this->getRigidBody(id);
    assert(rb && "RigidBody not foud from ID");

    rb->shape.push_back(sphere);
    rb->shape.back().rigidBodyID = rb->rigidBodyID;
  }

  void PhysicsWorld::transformShapeAndChildren(Shape&           shape,
                                               glm::mat4 const& tr)
  {
    switch (shape.type) {
      case ShapeType::Sphere: {
        const glm::vec3 scale2 = GetScale2(tr);
#if 0
        assert(scale2.x == scale2.y == scale2.z &&
               "Scaling is not uniform for Sphere");
#endif
        const auto maxScale2 =
          std::max_element(&scale2[0], &scale2[0] + scale2.length());
        // TODO: If always uniform take the .x component

        shape.sphere.center = TransformVec3(tr, shape.sphere.center);
        // shape.sphere.center = GetPosition(tr);
        shape.sphere.radius = glm::sqrt(*maxScale2) * shape.sphere.radius;

        // DebugManager::DrawSphere(shape.sphere, glm::vec4(1.0f));

      } break;
      case ShapeType::Polygon: {
        for (auto& t : shape.polygon->triangles) {
          t.vert[0] = glm::vec3(tr * glm::vec4(t.vert[0], 1.0f));
          t.vert[1] = glm::vec3(tr * glm::vec4(t.vert[1], 1.0f));
          t.vert[2] = glm::vec3(tr * glm::vec4(t.vert[2], 1.0f));
        }
        // SOLEIL__LOGGER_DEBUG("Updated polygon");
      } break;
      default: assert(false && "Not yet implemented");
    }

    // if (shape.children.size() > 0) {
    //   for (auto& c : shape.children) { transformShapeAndChildren(c, tr); }
    // }
  }

  void PhysicsWorld::addRigidBody(ObjectID        physicObjectID,
                                  RigidBody::Type bodyType)
  {
    entities.emplace_back(physicObjectID, bodyType);

    // TODO: StaticBody Should not have a motion state, instead it can be pushed
    // by an immediate method PushRigidBody();
#if 1
    if ((bodyType == RigidBody::Type::RigidBody) ||
        (bodyType == RigidBody::Type::KinematicBody)) {
#endif
      motionStates.emplace_back(entities.back().rigidBodyID);
#if 1
    }
#endif
  }

  PhysicsWorld::RigidBody* PhysicsWorld::getRigidBody(ObjectID physicBodyID)
  {
    for (RigidBody& e : entities) {
      if (e.rigidBodyID == physicBodyID) return (&e);
    }
    assert(false && "RigidBody not found");
  }

  void PhysicsWorld::removeRigidBody(ObjectID const id)
  {
    assert(id > 0 && "Removing Null ID");

    for (auto itRigidBody = entities.begin(); itRigidBody != entities.end();
         ++itRigidBody) {

      if (itRigidBody->rigidBodyID == id) {
        if ((itRigidBody->type == RigidBody::Type::RigidBody) ||
            (itRigidBody->type == RigidBody::Type::KinematicBody)) {
          removeMotionState(id);
        } else {
          assert(itRigidBody->type == RigidBody::Type::StaticBody);
        }
        this->entities.erase(itRigidBody);
        SOLEIL__LOGGER_DEBUG("Removed Physic rigidBody: ", id);
        return;
      }
    }

    assert(false && "RigidBody not found");
  }

  void PhysicsWorld::updateRigidBodyTransform(ObjectID const   id,
                                              glm::mat4 const& tr)
  {

    RigidBody* rigidBody = this->getRigidBody(id);
    assert(rigidBody);

    if ((rigidBody->type == RigidBody::Type::RigidBody) ||
        (rigidBody->type == RigidBody::Type::KinematicBody)) {
      MotionState* ms = this->findMotionState(rigidBody->rigidBodyID);
      assert(ms);

      // TODO: Rotation (orientation)
      ms->position = GetPosition(tr);
    } else {
      assert(rigidBody->type == RigidBody::Type::StaticBody);
      for (auto& s : rigidBody->shape) transformShapeAndChildren(s, tr);
      assert(false && "This method has to be tested");
    }
  }

  PhysicsWorld::MotionState* PhysicsWorld::findMotionState(
    const ObjectID physicBodyID)
  {
    for (auto& ms : motionStates) {
      if (ms.physicBodyID == physicBodyID) { return &ms; }
    }
    return nullptr;
  }

  static void DebugDrawShape(PhysicsWorld::Shape const& s)
  {
    const glm::vec4 color(0.208f, 0.992f, 0.831f, 1.0f);

    switch (s.type) {
      case PhysicsWorld::ShapeType::Sphere: {
        DebugManager::DrawSphere(s.sphere, color);
      } break;
      case PhysicsWorld::ShapeType::Polygon: {
        for (Triangle const& tri : s.polygon->triangles) {
          DebugManager::DrawTriangle(tri.vert[0], tri.vert[1], tri.vert[2],
                                     color);
        }
      } break;
      default: assert(false && "Unknown shape type"); return;
    }
  }

  void PhysicsWorld::update(const float deltaTime, int /*maxStep*/,
                            const float /*stepTime*/)
  {
    // TODO: Do multiple steps:
    /*
    int step = 0;
    while ((step < maxStep) &&
    */

    world.clear(); // TODO: two world to Switch world
    std::vector<std::pair<ObjectID, ObjectID>> collisionPairs;

    // Add all static shapes
    // TODO: A PushRigidbody method, sir?
    for (RigidBody const& e : entities) {
      if (e.type == RigidBody::Type::StaticBody) {
        // TODO: use motion state to be able to teleport a static body?
        // TODO: Keep a base vector will all static bodies?
        for (Shape const& s : e.shape) {
          world.push_back(s);

          if (this->doDebugDraw) { DebugDrawShape(s); }
        }
      }
    }

    for (MotionState& state : motionStates) {
      RigidBody* e = this->getRigidBody(state.physicBodyID);
      assert(e && "Motion state not paired to Physics id");

      // SOLEIL__LOGGER_DEBUG("Update rigidBody transform: ", e->rigidBodyID,
      //                      " with first shape=", (int)e->shape->type,
      //                      " and motion state: ", state.force);

      // TODO: Do no do like this...
      // TODO: Process only The motion state
      // Use a queue to process multiple time the same nodes
      // RigidBody* e = this->getRigidBody(state.physicBodyID);
      // if (this->intersect(e)) {
      //   // state.velocity = glm::vec3(0.0f);
      //   state.velocity = (-state.velocity) * 0.8f;
      //   // continue;
      // }
      // ------------------------------------------------------

      if (e->type == RigidBody::Type::RigidBody) {
        state.compute(); // TODO: Compute only if a primary has changed

        state.velocity += state.acceleration * deltaTime;

        // state.velocity.x = glm::pow(state.friction * state.mass, deltaTime);
        // state.velocity.z = glm::pow(state.friction * state.mass, deltaTime);

        state.position += state.velocity;
        state.orientation += state.spin * deltaTime;
        state.orientation = glm::normalize(state.orientation);

      } else {
        // Nothing to do. Compute velocity?
      }

      // TODO: Save matric in motion state to avoid recomputing it in the scene
      // manager
      const glm::mat4 tr = glm::translate(glm::mat4(1.0f), state.position) *
                           glm::mat4_cast(state.orientation);

      // for (auto& s : e->shape) transformShapeAndChildren(s, tr);
      for (Shape s : e->shape) // Copied on purpose
      {
        // TODO: Move the shape only if it did not collided the last frame(, or
        // it collision was removed?)
        // TODO: Polygon shape will not be copied
        transformShapeAndChildren(s, tr);
        /////////////////////
        // Test Collision
        std::size_t j = 0;
        while (j < world.size()) {
          if (intersect(s, world[j])) {
            collisionPairs.emplace_back(s.rigidBodyID, world[j].rigidBodyID);
            // TODO: s = e->shape; // Restore previous position
            break;
          }
          ++j;
        }
        /////////////////////
        world.push_back(s);

        if (this->doDebugDraw) { DebugDrawShape(s); }
      }
    }

    /////////////////////////////////////
    // Collision phase
    {
#if 0
      if (world.size() < 2) return;
#endif

      // std::vector<std::pair<ObjectID, ObjectID>> collisionPairs;

#if 0
      std::size_t i = 0;
      while (i < (world.size() - 2)) {
        std::size_t j = i + 1;
        while (j < world.size()) {
          if (intersect(world[i], world[j])) {
            collisionPairs.emplace_back(world[i].rigidBodyID,
                                        world[j].rigidBodyID);
          }
          ++j;
        }
        ++i;
      }
#endif
      // TODO: Uniquefy vector of pairs (one pair of same rigidbodyids)
      for (auto const& p : collisionPairs) {
        // RigidBody* rb1 = this->getRigidBody(p.first);
        MotionState* ms1 = this->findMotionState(p.first);
        if (ms1) { ms1->velocity = -ms1->velocity; }
        MotionState* ms2 = this->findMotionState(p.second);
        if (ms2) { ms2->velocity = -ms2->velocity; }
      }
    }
  }

  void PhysicsWorld::removeMotionState(const ObjectID physicBodyID)
  {
    for (auto it = motionStates.begin(); it != motionStates.end(); ++it) {
      if (it->physicBodyID == physicBodyID) {
        motionStates.erase(it);
        return;
      }
    }
    assert(false && "No motion state found associated to this RigidBody");
  }

  // TODO: Actually segment... Uniformize!
  static bool rayCastTest(const glm::vec3& startPoint,
                          const glm::vec3& endPoint, const glm::vec3& direction,
                          const float                  segmentLength,
                          const PhysicsWorld::Shape&   s,
                          PhysicsWorld::RayCastResult& ret)
  {
    float     t = 0.0f; // TODO: Max
    glm::vec3 q;

    bool intersected = false;
    switch (s.type) {
      case PhysicsWorld::ShapeType::Sphere: {
        if (IntersectRaySphere(startPoint, direction, s.sphere, t, q)) {
          if (t <= segmentLength) {
            // DebugManager::DrawSphere(s.sphere,
            //                          glm::vec4(1.0f, 0.0f, 0.0f, 1.0f));
            intersected = true;
          }
        }
      } break;
      case PhysicsWorld::ShapeType::Polygon: {
        if (IntersectSegmentPolygon(startPoint, endPoint, direction,
                                    *(s.polygon), t, q)) {
          // if (IntersectSegmentPolgon(startPoint, direction, *(s.polygon), t,
          // q)) {
          // TODO: Draw Debug
          intersected = true;
        }
      } break;
      default: assert(false && "No such test implemented");
    }

    if (intersected) {
      // if (s.children.size() > 0) {
      //   bool rigidBodyIntersected = false;

      //   for (const auto c : s.children) {
      //     rigidBodyIntersected |=
      //       rayCastTest(startPoint, endPoint, direction, segmentLength, c,
      //       ret);
      //   }
      //   return rigidBodyIntersected;
      // } else {
      ret.count++;
      return true;
      //      }
    }
    return false;
  }

  PhysicsWorld::PhysicsWorld::RayCastResult PhysicsWorld::rayCast(
    const glm::vec3& startPoint, const glm::vec3& endPoint, ObjectID sender)
  {
    // TODO: Allow exit on fist collision

    // DebugManager::DrawLine(startPoint, endPoint,
    //                        glm::vec4(1.0f, 0.0f, 0.0f, 1.0f));

    RayCastResult ret;
    ret.count                     = 0;
    const glm::vec3 segment       = endPoint - startPoint;
    const float     segmentLength = glm::length(segment);
    const glm::vec3 d             = glm::normalize(segment);
    // for (const auto& e : entities) {
    //   if (sender > 0 && e.rigidBodyID == sender) { continue; }
    //   // assert(s.type == ShapeType::Sphere);
    //   // float     t = 0.0f; // TODO: Max
    //   // glm::vec3 q;
    //   // if (IntersectRaySphere(startPoint, d, s.sphere, t, q)) {
    //   //   if (t <= segmentLength) {
    //   //     DebugManager::DrawSphere(s.sphere, glm::vec4(1.0f));
    //   //     ret.count++;
    //   //     // TODO: early exit
    //   //   }
    //   // }
    //   for (Shape const& shape : e.shape) {
    for (Shape const& shape : world) { // TODO: not thread safe
      assert(shape.rigidBodyID > 0);

      if (shape.rigidBodyID == sender) { continue; }
      if (rayCastTest(startPoint, endPoint, d, segmentLength, shape, ret)) {
        // SOLEIL__ASSERT_BREAK(false);
        ret.objects.push_back(shape.rigidBodyID);
        break; // Stop at first collision with rigid bodoy
      }
    }
    //}
    return ret;
  }

  bool PhysicsWorld::intersectSphereShape(Shape const& s1, Shape const& s2)
  {
    assert(s1.type == PhysicsWorld::ShapeType::Sphere);

    bool intersected = false;
    switch (s2.type) {
      case ShapeType::Sphere:
        intersected = TestSphereSphere(s1.sphere, s2.sphere);
        // DebugManager::DrawSphere(s1.sphere, glm::vec4(1.0f));
        // DebugManager::DrawSphere(s2.sphere, glm::vec4(1.0f));
        // intersected = false;
        break;
      case PhysicsWorld::ShapeType::Polygon:
        intersected = TestSpherePolygon(s1.sphere, *(s2.polygon));
        // intersected = false;
        break;
      default: assert(false && "No such test implemented");
    }

    if (intersected) {
      // if (s2.children.size() > 0) {
      //   bool rigidBodyIntersected = false;

      //   for (const auto c : s2.children) {
      //     rigidBodyIntersected |= intersectSphereShape(s1, c);
      //   }
      //   return rigidBodyIntersected;
      // } else {
      return true;
      // }
    }

    return false;
  }

  bool PhysicsWorld::intersect(Shape const& s1, Shape const& s2)
  {
    bool intersected = false;

    switch (s1.type) {
      case PhysicsWorld::ShapeType::Sphere:
        intersected = intersectSphereShape(s1, s2);
        break;
      case PhysicsWorld::ShapeType::Polygon:
        // intersected = true;
#if 1
        assert(false && "Not implemented yet");
#endif
        // TODO: FIXME assert uncommented because we test all motion state
        // including static ones. Restore the asset when the immediate
        // PushRigidbody method will be here.
        break;
      default: assert(false && "No such test implemented");
    }

    return intersected;
#if 0 // Browse emitter shape (s1) children
    if (intersected) {
      if (s1.children.size() > 0) {
        bool rigidBodyIntersected = true;

        for (const auto c : s1.children) {
          //rigidBodyIntersected |= intersect(c, s2);
        }
        return rigidBodyIntersected;
      } else {
        return true;
      }
    }

    return false;
#endif
  }

  bool PhysicsWorld::intersect(RigidBody* rigidBody)
  {
    if (rigidBody->shape.size() < 1) return false;

    MotionState* meMs = this->findMotionState(rigidBody->rigidBodyID);
    assert(meMs);
    Shape meShape = rigidBody->shape[0];
    this->transformShapeAndChildren(
      meShape, glm::translate(glm::mat4(1.0f), glm::vec3(meMs->position)));

    for (RigidBody const& otherRigidBody : entities) {
      if (&otherRigidBody == rigidBody) { continue; }

      if (otherRigidBody.shape.size() < 1) { continue; }

      MotionState* youMs    = this->findMotionState(otherRigidBody.rigidBodyID);
      Shape        youShape = otherRigidBody.shape[0];
      if (youMs) {
        this->transformShapeAndChildren(
          youShape,
          glm::translate(glm::mat4(1.0f), glm::vec3(youMs->position)));
      }

      // TODO: FIXME: How to check all shapes together:
      if (intersect(meShape, youShape)) {
        // TODO: Process only those with a MotionState
        // TODO: if motion, stop motion, bounce,
        // TODO: Send callback? Set a state?
        return true;
      }
    }
    return false;
  }

/* ------------- Statics ------------------- */
#include <memory>
  std::unique_ptr<PhysicsWorld> s_instance = nullptr;

  void PhysicsWorld::Init() { s_instance = std::make_unique<PhysicsWorld>(); }

  void PhysicsWorld::AddSphereFromVertices(
    const std::vector<glm::vec3>& vertices, ShapeType shapeType, ObjectID id)
  {
    s_instance->addSphereFromVertices(vertices, shapeType, id);
  }

  void PhysicsWorld::AddSphereShapeFromPolygon(Polygon polygon, ObjectID id)
  {
    s_instance->addSphereShapeFromPolygon(polygon, id);
  }

  void PhysicsWorld::AddShape(Polygon polygon, ObjectID id)
  {
    s_instance->addShape(polygon, id);
  }

  void PhysicsWorld::AddShape(Sphere sphere, ObjectID id)
  {
    s_instance->addShape(sphere, id);
  }

  void PhysicsWorld::AddRigidBody(ObjectID id, RigidBody::Type bodyType)
  {
    s_instance->addRigidBody(id, bodyType);
  }

  void PhysicsWorld::RemoveRigidBody(ObjectID const id)
  {
    s_instance->removeRigidBody(id);
  }

  void PhysicsWorld::UpdateRigidBodyTransform(ObjectID const   id,
                                              glm::mat4 const& tr)
  {
    s_instance->updateRigidBodyTransform(id, tr);
  }

  PhysicsWorld::MotionState* PhysicsWorld::FindMotionState(
    const ObjectID physicBodyID)
  {
    return s_instance->findMotionState(physicBodyID);
  }

  PhysicsWorld::RayCastResult PhysicsWorld::RayCast(const glm::vec3& startPoint,
                                                    const glm::vec3& endPoint,
                                                    ObjectID         sender)
  {
    return s_instance->rayCast(startPoint, endPoint, sender);
  }

  void PhysicsWorld::Update(const float deltaTime, int maxStep,
                            const float stepTime)
  {
    s_instance->update(deltaTime, maxStep, stepTime);
  }

} // namespace Soleil
