/*
 * Copyright (C) 2018  Florian GOLESTIN
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#pragma once

#include <Utils.h>
#include <glm/geometric.hpp>
#include <glm/vec3.hpp>
#include <vector>
#include <array>


namespace Soleil {
  void Hahaha(const glm::vec3& p0, const glm::vec3& p2, const glm::vec3& p3,
              const glm::vec4& color); // TODO: temp
}

#include <Logger.h>

// TODO: Find the windows.h and use the #define NOMINMAX
#undef max
#undef min

namespace Soleil {

  inline float MyGetSquaredLength(const glm::vec3& v)
  {
    return v.x * v.x + v.y * v.y + v.z * v.z;
    // return glm::dot(v, v);
  }

  struct Sphere
  {
    glm::vec3 center;
    float     radius;

    Sphere()
      : center(0.0f)
      , radius(0.0f)
    {}

    Sphere(const glm::vec3& center, const float radius)
      : center(center)
      , radius(radius)
    {}

    ~Sphere() = default;

    static Sphere FromTetrahedron(const glm::vec3& p1, const glm::vec3& p2,
                                  const glm::vec3& p3, const glm::vec3& p4);

    bool contains(const glm::vec3& point) const noexcept
    {
      // if (radius < glm::epsilon<float>()) return false;
      const glm::vec3 d = center - point;
      // const float     dist2 = glm::dot(d, d);
      const float dist2   = MyGetSquaredLength(d);
      const float radius2 = (radius * radius);
      if (dist2 <= radius2) { return true; }
      return false;
    }
  };

  struct Triangle
  {
    Triangle()
    {
      vert[0] = glm::vec3(0.0f);
      vert[1] = glm::vec3(0.0f);
      vert[1] = glm::vec3(0.0f);
    }

    std::array<glm::vec3, 3> vert;
    // glm::vec3 a;
    // glm::vec3 b;
    // glm::vec3 c;
    // glm::vec3& operator[](std::size_t idx)
    // {
    //   assert(idx < 3 && "Triangle has only 3 angles");
    //   switch (
    // }

    ~Triangle() = default;
  };

  struct Polygon
  {
    Sphere                boundingSphere;
    std::vector<Triangle> triangles;
  };

  struct Capsule
  {
    glm::vec3 start;
    glm::vec3 end;
    float     radius;
  };

  inline float SquaredDistancePointSegment(const glm::vec3& segmentStart,
                                           const glm::vec3& segmentEnd,
                                           const glm::vec3& point)
  {
    // segmentStart = a;
    // segmentEnd = b;
    // point = c;
    const glm::vec3 ab = segmentEnd - segmentStart;
    const glm::vec3 ac = point - segmentStart;
    const glm::vec3 bc = point - segmentEnd;

    const float e = glm::dot(ac, ab);
    // check c outside ab:
    if (e <= 0.0f) return glm::dot(ac, ac);

    const float f = glm::dot(ab, ab);
    if (e >= f) return glm::dot(bc, bc);
    // Case where c projects onto ab
    return glm::dot(ac, ac) - e * e / f;
  }

  inline float ClosestPointSegmentSegment(const glm::vec3& p1,
                                          const glm::vec3& q1,
                                          const glm::vec3& p2,
                                          const glm::vec3& q2, float& s,
                                          float& t, const glm::vec3& c1,
                                          const glm::vec3& c2)
  {
    (void)p1;
    (void)q1;
    (void)p2;
    (void)q2;
    (void)s;
    (void)t;
    (void)c1;
    (void)c2;
    
    assert(false && "Not done yet");
    return 0.0f;
  }

  inline glm::vec3 ClosestPointTrianglePoint(glm::vec3 const& p,
                                             glm::vec3 const& a,
                                             glm::vec3 const& b,
                                             glm::vec3 const& c)
  {
    const glm::vec3 ab = b - a;
    const glm::vec3 ac = c - a;
    const glm::vec3 bc = c - b;
    const glm::vec3 ap = p - a;

    // Compute parametric position s for projection P’ of P on AB,
    // P’ = A + s*AB, s = snom/(snom+sdenom)
    const float snom   = glm::dot(ap, ab);
    const float sdenom = glm::dot(p - b, a - b);

    // Compute parametric position t for projection P’ of P on AC,
    // P’ = A + t*AC, s = tnom/(tnom+tdenom)
    const float tnom   = glm::dot(ap, ac);
    const float tdenom = glm::dot(p - c, a - c);

    if (snom <= 0.0f && tnom <= 0.0f) return a;

    // Compute parametric position u for projection P’ of P on BC,
    // P’ = B + u*BC, u = unom/(unom+udenom)
    const float unom   = glm::dot(p - b, bc);
    const float udenom = glm::dot(p - c, b - c);

    if (sdenom <= 0.0f && unom <= 0.0f) return b;
    if (tdenom <= 0.0f && udenom <= 0.0f) return c;

    // P is outside (or on) AB if the triple scalar product [N PA PB] <= 0
    const glm::vec3 n  = glm::cross(ab, ac);
    const float     vc = glm::dot(n, glm::cross(a - p, b - p));
    // If P outside AB and within feature region of AB,
    // return projection of P onto AB
    if (vc <= 0.0f && snom >= 0.0f && sdenom >= 0.0f)
      return a + snom / (snom + sdenom) * ab;

    // P is outside (or on) BC if the triple scalar product [N PB PC] <= 0
    const float va = glm::dot(n, glm::cross(b - p, c - p));
    // If P outside BC and within feature region of BC,
    // return projection of P onto BC
    if (va <= 0.0f && unom >= 0.0f && udenom >= 0.0f)
      return b + unom / (unom + udenom) * bc;

    // P is outside (or on) CA if the triple scalar product [N PC PA] <= 0
    const float vb = glm::dot(n, glm::cross(c - p, a - p));
    // If P outside CA and within feature region of CA,
    // return projection of P onto CA
    if (vb <= 0.0f && tnom >= 0.0f && tdenom >= 0.0f)
      return a + tnom / (tnom + tdenom) * ac;

    // P must project inside face region. Compute Q using barycentric
    // coordinates
    const float u = va / (va + vb + vc);
    const float v = vb / (va + vb + vc);
    const float w = 1.0f - u - v; // = vc / (va + vb + vc)
    return u * a + v * b + w * c;
  }

  inline bool TestSphereTriangle(Sphere const& s, glm::vec3 const& a,
                                 glm::vec3 const& b, glm::vec3 const& c,
                                 glm::vec3& p)
  {
    p = ClosestPointTrianglePoint(s.center, a, b, c);

    const glm::vec3 v = p - s.center;
    return glm::dot(v, v) <= (s.radius * s.radius);
  }

  inline bool TestSphereSphere(const Sphere& s1, const Sphere& s2)
  {
    const glm::vec3 d                 = s1.center - s2.center;
    const float     centerDistSquared = glm::dot(d, d);
    const float     radiusSum         = s1.radius + s2.radius;
    return centerDistSquared <= (radiusSum * radiusSum);
  }

  inline bool TestSpherePolygon(Sphere const& s, Polygon const& p)
  {
    glm::vec3 point;

    // Test early exit
    if (TestSphereSphere(s, p.boundingSphere) == false) { return false; }

    for (const Triangle& t : p.triangles) {
      if (TestSphereTriangle(s, t.vert[0], t.vert[1], t.vert[2], point))
        return true;
    }
    return false;
  }

  inline bool TestSphereCapsule(const Sphere& s, const Capsule& capsule,
                                float* dist = nullptr)
  {
    // Compute (squared) distance between sphere center and capsule line segment
    const float dist2 =
      SquaredDistancePointSegment(capsule.start, capsule.end, s.center);
    // If (squared) distance smaller than (squared) sum of radii, they collide
    const float radius       = s.radius + capsule.radius;
    const float closestPoint = dist2 - (radius * radius);
    if (dist) {
      const float sgn = Sign(closestPoint);
      *dist           = sgn * glm::sqrt(glm::abs(closestPoint));
      //*dist = dist2 - (radius * radius);
    }
    return closestPoint <= 0.0f;
  }

  inline bool TestCapsuleCapsule(const Capsule& capsule1,
                                 const Capsule& capsule2)
  {
    float     s;
    float     t;
    glm::vec3 c1;
    glm::vec3 c2;

    const float dist2 = ClosestPointSegmentSegment(
      capsule1.start, capsule1.end, capsule2.start, capsule2.end, s, t, c1, c2);
    // If (squared) distance is smaller than (squared) sum of the radii, they
    // collide
    const float radius = capsule1.radius + capsule2.radius;
    return dist2 <= radius * radius;
  }

  // Intersects ray r = p + td, |d| = 1, with sphere s and, if intersecting,
  // returns t value of intersection and intersection point q
  inline bool IntersectRaySphere(const glm::vec3 p, const glm::vec3 d,
                                 const Sphere& s, float& t, glm::vec3& q)
  {
    glm::vec3 m = p - s.center;
    float     b = glm::dot(m, d);
    float     c = glm::dot(m, m) - s.radius * s.radius;
    // Exit if r’s origin outside s (c > 0) and r pointing away from s (b > 0)
    if (c > 0.0f && b > 0.0f) return false;
    float discr = b * b - c;
    // A negative discriminant corresponds to ray missing sphere
    if (discr < 0.0f) return false;
    // Ray now found to intersect sphere, compute smallest t value of
    // intersection
    t = -b - glm::sqrt(discr);
    // If t is negative, ray started inside sphere so clamp t to zero
    if (t < 0.0f) t = 0.0f;
    q = p + t * d;
    return true;
  }

  inline bool IntersectSegmentTriange(const glm::vec3& p, const glm::vec3& q,
                                      const glm::vec3& a, const glm::vec3& b,
                                      const glm::vec3& c, glm::vec3 uvw,
                                      float& t)
  {
// Compute the triangle normal
// TODO: May be precomputed if we are testing the same triangle multiple
// times
#ifdef SOLEIL__TEST_CCW
    // Test if CCW
    glm::vec3 ab = b - a;
    glm::vec3 ac = c - a;
    glm::vec3 qp = p - q;

    glm::vec3   n = glm::cross(ab, ac);
    const float w = glm::dot(n, (a - p));
    if (glm::abs(w) <= glm::epsilon<float>()) {
      // assert(false && "Coplanar");
    } else if (w > 0) {
      // CW
      auto A = c;
      auto B = b;
      auto C = a;

      ab = B - A;
      ac = C - A;
    }
    // ---
    n = glm::cross(ab, ac);
#else
    const glm::vec3 ab = b - a;
    const glm::vec3 ac = c - a;
    const glm::vec3 qp = p - q;

    const glm::vec3 n = glm::cross(ab, ac);
#endif

    // Compute the denominator d. If d <= 0, segment is parallel to or point
    // away from the triangle.
    const float d = glm::dot(qp, n);
    if (d <= 0.0f) { return false; }

    // Compute the intesection t value of pq with the plane of the triangle. A
    // ray intersects iff 0 <= t. A Segment intersects iff 0 <= t <= 1.
    const glm::vec3 ap = p - a;
    t                  = glm::dot(ap, n);
    if (t < 0.0f) return false;
    if (t > d)
      return false; // For segment only (// TODO: remove this line for ray)

    // Compute the barycentric components and check if in the bounds
    const glm::vec3 e = glm::cross(qp, ap);
    uvw.y             = glm::dot(ac, e);
    if (uvw.y < 0.0f || uvw.y > d) return false;
    uvw.z = -glm::dot(ab, e);
    if ((uvw.z < 0.0f) || ((uvw.y + uvw.z) > d)) return false;

    const float ood = 1.0f / d;
    t *= ood;
    uvw.y *= ood;
    uvw.z *= ood;
    uvw.x = 1.0f - uvw.y - uvw.z;
    return true;
  }

  inline bool IntersectSegmentPolygon(const glm::vec3& s, const glm::vec3& e,
                                      const glm::vec3& d, const Polygon& poly,
                                      float& t, glm::vec3& q)
  {
    // TODO: No need to comute t and q for this, is it optimized to pass them a
    // pointer and skip them if null?
    //
    // Early exit test
    if (IntersectRaySphere(s, d, poly.boundingSphere, t, q) == false) {
      return false;
    }
    // TODO: Early exit if t > segment length
    // return true;// TODO: debug

    float smallestT = std::numeric_limits<float>::max();

    float     T = std::numeric_limits<float>::max();
    glm::vec3 uvw(0.0f);

    for (const Triangle& tri : poly.triangles) {
      // Soleil::Hahaha(tri.vert[0], tri.vert[1], tri.vert[2],
      //                glm::vec4(1.0f, 0.0f, 0.0f, 1.0f));

      if (IntersectSegmentTriange(s, e, tri.vert[0], tri.vert[1], tri.vert[2],
                                  uvw, T)) {
        if (T < smallestT) {
          smallestT = T;
          t         = T;
          q         = uvw;
        }
        Soleil::Hahaha(tri.vert[0], tri.vert[1], tri.vert[2],
                       glm::vec4(1.0f, 0.0f, 1.0f, 1.0f));
      }
    }
    return (smallestT < std::numeric_limits<float>::max());
  }

  Sphere ComputeMinimalBoundingShpere(const std::vector<glm::vec3>& points);

} // namespace Soleil
